import socket
import struct

phone = socket.socket()

phone.connect(('127.0.0.1', 8888))

# 发消息
while 1:
    cmd = input('>>>').strip()
    phone.send(cmd.encode('utf-8'))

    # 1. 接收报头
    head_bytes = phone.recv(4)

    # 2. 将报头反解回int类型
    total_size = struct.unpack('i', head_bytes)[0]  # 1517

    # 3. 循环接收原数据

    total_data = b''

    while len(total_data) < total_size:
        total_data += phone.recv(1024)

    print(total_data.decode('gbk'))

# 关机
phone.close()
