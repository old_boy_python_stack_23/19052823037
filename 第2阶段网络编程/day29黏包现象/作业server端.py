# 3. 基于tcp协议的socket，完成一个文件上传下载功能。（今天完不成，明天可以继续）
#
#    1. 客户端用户需要认证登录。
#    2. 客户端用户登录成功之后，可以选择上传或者下载（文件视频都可以）。
#    3. 上传：
#       - 将客户端本地文件或者视频上传到服务端提前设置好的目录中。
#    4. 下载：
#       - 服务端有一个专门目录供客户端选择下载。
#       - 此目录中放置若干文件（或视频）供客户端选择。
#       - 客户端成功选择之后，将选择的文件下载到客户端指定的目录中。
#
#    5. 要考虑内存因素。
#
#    6. 报头要按照字典的格式设计。
#
#    7. 文件要有MD5验证，验证通过方可下载。

import socket
import os
import struct
import json
import hashlib

FILE_PATH = os.path.dirname(__file__)


# print(FILE_PATH)

# 定义一个对文件进行hashlibMD5 求值的函数 传入文件路径即可
def hsmd(file):
    ret = hashlib.md5()
    with open(file, mode='rb') as f:
        while 1:
            lit = f.read(1024)
            if lit:
                ret.update(lit)
            else:
                break
    return ret.hexdigest()


# 开始连接
phone = socket.socket()
phone.bind(("127.0.0.2", 8849))
phone.listen(5)
print("star")
conn, addr = phone.accept()
# 循环进行在client端输入q停止循环
while 1:
    four_bytes = conn.recv(4)
    # 判断情况 如果是'2'则为下载运行if 为 q 停止循环 其他 视为上传
    if four_bytes.decode('utf-8') == '2':
        # 获取特定目录下所有的文件名
        lis = os.listdir(os.path.join(os.path.dirname(__file__), 'server.data'))
        # 字典形式方便c端使用
        dic = {i + 1: lis[i] for i in range(len(lis))}
        # 发送给客户端
        file_dic_json = json.dumps(dic)
        conn.send(file_dic_json.encode('utf-8'))
        # 接收c端选择的文件名
        file_name = conn.recv(1024).decode('utf-8')
        # 求文件MD5值
        md5 = hsmd(os.path.join(FILE_PATH, 'server.data', file_name))
        # 制作 报头
        head_dict = {
            "md5": md5,
            'file_name': os.path.basename(os.path.join(FILE_PATH, 'server.data', file_name)),
            'file_size': os.path.getsize(os.path.join(FILE_PATH, 'server.data', file_name))
        }

        head_dic_json = json.dumps(head_dict)
        head_dic_json_bytes = head_dic_json.encode('utf-8')
        head_len = len(head_dic_json_bytes)
        four_bytes = struct.pack('i', head_len)

        conn.send(four_bytes)
        conn.send(head_dic_json_bytes)

        with open(os.path.join(FILE_PATH, 'server.data', file_name), mode="rb") as f:
            while 1:
                cmd = f.read(1024)
                if cmd:
                    conn.send(cmd)
                else:
                    break
    elif four_bytes.decode('utf-8').upper() == "Q":
        break
    # 接收上传文件
    else:
        head_len = struct.unpack('i', four_bytes)[0]

        head_dic_json_bytes = conn.recv(head_len)

        head_dic_json = head_dic_json_bytes.decode('utf-8')

        head_dic = json.loads(head_dic_json)
        # 写入本地
        with open(f"{os.path.join(os.path.dirname(__file__), 'server.data', head_dic['file_name'])}", mode='wb') as f:
            while 1:
                total_size = 0
                while total_size < head_dic['file_size']:
                    every_data = conn.recv(1024)
                    f.write(every_data)
                    total_size += len(every_data)
                else:
                    break

conn.close()
phone.close()
