# 一
# import socket
#
# phone = socket.socket()
# phone.connect(('127.0.0.1', 8848))
# while 1:
#     obj = input(">>>")
#     phone.send(obj.encode("utf-8"))
#     from_sever_data = phone.recv(1024)
#     print(from_sever_data.decode("utf-8"))
#
# phone.close()

# 二
# import socket
#
# phone = socket.socket()
# phone.connect(('127.0.0.1', 8848))
# while 1:
#     obj = input(">>>")
#     if obj.upper()== "Q":
#         phone.send('q'.encode('utf-8'))
#         break
#     phone.send(obj.encode("utf-8"))
#     from_sever_data = phone.recv(1024)
#     print(from_sever_data.decode("utf-8"))
#
# phone.close()

# 三
import socket

phone = socket.socket()
phone.connect(('127.0.0.1', 8848))
while 1:
    obj = input(">>>")
    if obj.upper()== "Q":
        phone.send('q'.encode('utf-8'))
        break
    phone.send(obj.encode("utf-8"))
    from_sever_data = phone.recv(1024*8)
    print(from_sever_data.decode("utf-8"))

phone.close()