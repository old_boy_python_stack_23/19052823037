## socket套接字

五层协议: 从传输层包括传输层以下 , 都是操作系统帮助我们封装的各种head(报头), 不用去关心

```
# content = input('>>>')
# print(content)
# 怎么交给操作系统( ⊙o⊙ )?
# 模块,或者内置函数 必须有方法内置的一些代码接收数据,然后在底层交由操作系统.

# socket套接字充当的就是内置模块的角色.

# 你说一下socket的套接字?
'''
socket 套接字,它存在于传输层与应用层之间的抽象层,
    1. 避免你学习各层的接口,以及协议的使用, socket已经封装好了所有的接口.
        直接使用这些接口或者方法即可,使用起来方便,提升开发效率.
    2. socket就是一个模块.通过使用学习模块提供的功能,
        建立客户端与服务端的通信,使用方便.
```

## 基于TCP协议的socket通信:

![1563272394183](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1563272394183.png)



## 单个客户与服务端通信

服务端:

```
content = input('>>>')
pritn(content)
怎么交给操作系统?
通过模块 或者内置函数 必须有方法内置的一些代码接收数据, 然后在底层交给操作系统
socket套接字充当的就是内置模块的角色
什么是套接字?
socket套接字 它存在于传输层与应用层之间的抽象层
	1. 避免我们学习各层的接口,以及协议的使用 socket已经封装好了所有接口
	2. socket即使一个模块, 通过使用学习模块提供的功能,建立客户端与服务器的通信,使用方便
```

客户端

```
import socket

# 1. 创建socket对象(买手机)
phone = socket.socket() # 可以默认不写

# 连接服务器ip地址与端口
phone.connect(('127.0.0.1', 8848))

# 发消息
to_server = input('>>>').strip()
phone.send(to_server.encode('utf-8'))
# 接收消息
from_server_data = phone.recv(1024)  # 夯住,等待服务端的数据传过来
print(f'来自服务端消息:{from_server_data.decode("utf-8")}')

# 关机
phone.close()
```

## 通信循环

服务端

```
import socket

phone = socket.socket()

phone.bind(('127.0.0.1', 8888))

phone.listen(5)

# 4. 接收连接
while 1:
    print('start')
    conn, addr = phone.accept()  # 程序夯住
    print(conn,addr)
    while 1:
        try:
            from_client_data = conn.recv(1024)  #　至多接收1024个字节
            if from_client_data == b'q':
                break
            print(f'来自客户端{addr}消息{from_client_data.decode("utf-8")}')
            to_client = input('>>>')
            conn.send(to_client.encode('utf-8'))
        except ConnectionResetError:
            break
    conn.close()
phone.close()
```

客户端

```
import socket

# 1. 创建socket对象(买手机)
phone = socket.socket() # 可以默认不写

# 连接服务器ip地址与端口
phone.connect(('127.0.0.1', 8888))

# 发消息
while 1:
    to_server = input('>>>').strip()
    if to_server.upper() == 'Q':
        phone.send('q'.encode('utf-8'))
        break
    phone.send(to_server.encode('utf-8'))
    # 接收消息
    from_server_data = phone.recv(1024)  # 夯住,等待服务端的数据传过来
    print(f'来自服务端消息:{from_server_data.decode("utf-8")}')

# 关机
phone.close()
```

## 通信,连接循环

服务端

```
# import socket
#
# # phone = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
# # 1. 创建socket对象(买手机)
# phone = socket.socket() # 可以默认不写
#
# # 2. 绑定ip地址和端口(办卡)
# phone.bind(('127.0.0.1', 8848))  # 本地回环地址
#
# # 3. 监听.(开机状态)
# phone.listen(5)
#
# # 4. 接收连接
# print('start')
# conn, addr = phone.accept()  # 程序夯住
# # print(conn,addr)
# while 1:
#     from_client_data = conn.recv(1024)  #　至多接收1024个字节
#     print(f'来自客户端{addr}消息{from_client_data.decode("utf-8")}')
#     to_client = input('>>>')
#     conn.send(to_client.encode('utf-8'))
#
# conn.close()
# phone.close()

# 无论你的客户端是否正常关闭,服务端都应该正常关闭,而不是报错.


import socket

# phone = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
# 1. 创建socket对象(买手机)
phone = socket.socket() # 可以默认不写

# 2. 绑定ip地址和端口(办卡)
phone.bind(('127.0.0.1', 8888))  # 本地回环地址

# 3. 监听.(开机状态)
phone.listen(5)

# 4. 接收连接
print('start')
conn, addr = phone.accept()  # 程序夯住
# print(conn,addr)
while 1:
    try:
        from_client_data = conn.recv(1024)  #　至多接收1024个字节
        if from_client_data == b'q':
            break
        print(f'来自客户端{addr}消息{from_client_data.decode("utf-8")}')
        to_client = input('>>>')
        conn.send(to_client.encode('utf-8'))
    except ConnectionResetError:
        break
conn.close()
phone.close()
```



客户端

```
import socket

# 1. 创建socket对象(买手机)
phone = socket.socket() # 可以默认不写

# 连接服务器ip地址与端口
phone.connect(('127.0.0.1', 8888))

# 发消息
while 1:
    to_server = input('>>>').strip()
    if to_server.upper() == 'Q':
        phone.send('q'.encode('utf-8'))
        break
    phone.send(to_server.encode('utf-8'))
    # 接收消息
    from_server_data = phone.recv(1024)  # 夯住,等待服务端的数据传过来
    print(f'来自服务端消息:{from_server_data.decode("utf-8")}')

# 关机
phone.close()
```

## 利用socket完成获取远端命令的示例

服务器端

```
import subprocess

import socket

phone = socket.socket()
phone.bind(('127.0.0.1', 8848))
phone.listen(8)
while 1:
    print("star")
    conn, addr = phone.accept()
    print(conn, addr)
    while 1:
        try:
            from_client_data = conn.recv(1024*8)
            if from_client_data == b"q":
                break
            print(f"来自客户端{addr}消息{from_client_data.decode('utf-8')}")

            obj = subprocess.Popen(f'{from_client_data.decode("utf-8")}',
                                   shell=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   )
            conn.send(f"{obj.stdout.read().decode('gbk')},{obj.stderr.read().decode('gbk').encode('utf-8')}".encode("utf-8"),)
        except ConnectionAbortedError:
            break

    conn.close()
phone.close()
```



客户端

```
import socket

phone = socket.socket()
phone.connect(('127.0.0.1', 8848))
while 1:
    obj = input(">>>")
    if obj.upper()== "Q":
        phone.send('q'.encode('utf-8'))
        break
    phone.send(obj.encode("utf-8"))
    from_sever_data = phone.recv(1024*8)
    print(from_sever_data.decode("utf-8"))

phone.close()
```

## 粘包现象

...