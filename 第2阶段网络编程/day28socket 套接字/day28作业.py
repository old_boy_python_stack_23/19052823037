# 1. 使用基于TCP协议的socket套接字，完成qq聊天的功能（连接+通信循环）。
#
#    ​	服务端：
#
#    1. 服务端模拟qq客服，可以与客户实现正常聊天。
#    2. 服务端可以选择勿扰模式，如果选择勿扰模式，则给客户端统一回复：‘您好，我现在有事不在，请稍后联系’。
#    3. 客户在正常（非正常退出）时，服务端不受影响运行正常。
#
#    ​    客户端：
#
#    1. 客户端可以实现与qq客户的正常聊天。
#    2. 客户端可以选择正常退出聊天。
# 服务器端
# import socket
#
# mode = False
# dic = {'1': "正常模式", '2': '勿扰模式'}
# choice = input("请输入模式序号:\n1:正常模式\n2:勿扰模式").strip()
# if choice == "2":
#     mode = True
# print(mode)
# phone = socket.socket()
# phone.bind(('127.0.0.1', 8848))
# phone.listen(5)
# print("star")
# conn, addr = phone.accept()
#
# while 1:
#     try:
#         from_client_date = conn.recv(1024)
#         if from_client_date == b'q':
#             break
#         print(f'来自客户端{addr}的消息{from_client_date.decode("utf-8")}')
#
#         if mode:
#             conn.send("您好，我现在有事不在，请稍后联系".encode("utf-8"))
#         else:
#             to_client = input(">>>")
#             conn.send(to_client.encode("utf-8"))
#
#     except ConnectionResetError:
#         break
# conn.close()
# phone.close()

# 客户端
# import socket
# phone = socket.socket()
# phone.connect(("192.168.0.28",9848 ))
# while 1:
#     to_server = input(">>>")
#     if to_server.upper() == "Q":
#         phone.send('q'.encode('utf-8'))
#         break
#     phone.send(to_server.encode("utf-8"))
#     from_server_date = phone.recv(1024)
#     print(f"来自服务端消息:{from_server_date.decode('utf-8')}")
# phone.close()


# 2. 使用基于TCP协议的socket套接字，完成一个用户登录认证的需求。
#
#    服务端：
#
#    1. 等待客户端来发送数据：用户名，密码（密文）。
#
#    2. 本地文件查看用户名密码是否合法。
#
#    3. 给客户端返回登录成功与否的信息：合法：登录成功。否则：用户名或者密码错误。


# import socket
#
# phone = socket.socket()
# phone.bind(('127.0.0.1', 8848))
# phone.listen(8)
# while 1:
#     print("star")
#     conn, addr = phone.accept()
#     print(conn, addr)
#     while 1:
#         try:
#             from_client_data = conn.recv(1024 * 8).decode("utf-8")
#             if from_client_data == b"q":
#                 break
#             print(f"来自客户端{addr}消息{from_client_data}")
#             username_input = from_client_data.split("|")[0]
#             user_password = from_client_data.split("|")[1]
#             with open(r"D:\Python_study\day28socket 套接字\user", mode='r') as f:
#                 for i in f.readlines():
#                     if username_input == i.strip().split("|")[0] and user_password == i.strip().split("|")[1]:
#                         conn.send('合法：登录成功。'.encode("utf-8"))
#                         break
#                 else:
#                     conn.send("用户名或者密码错误。".encode("utf-8"))
#         except ConnectionAbortedError:
#             break
#
# conn.close()
# phone.close()

# 客户端
# import hashlib
#
# import socket
#
# phone = socket.socket()
# phone.connect(('127.0.0.1', 8848))
# while 1:
#     username_input = input("请输入用户名:").strip()
#     user_password = input("请输入密码:").strip()
#     ret = hashlib.md5()
#     ret.update(user_password.encode('utf-8'))
#     if username_input.upper()== "Q":
#         phone.send('q'.encode('utf-8'))
#         break
#     info = username_input +"|" + ret.hexdigest()
#     phone.send(info.encode("utf-8"))
#     from_sever_data = phone.recv(1024*8)
#     print(from_sever_data.decode("utf-8"))
#
# phone.close()


# 3. 面试题小练习：
#
#    1. 将列表中得元素根据位数合并成字典
#
#       ```
# lst = [1, 2, 4, 6, 8, 16, 32, 64, 128, 256, 512, 1024, 32769, 65536, 4294967296, ]
#
#       {
#       1： [1,2,4,6,8],
#       2:  [16,32,64,],
#       3:  [128,256,512,],
#       4:  [1024,],
#       5:  [32769,65536,],
#       10:  [4294967296],
#       }
#       ```
# dic = {}
# for i in [1, 2, 3, 4, 5, 10]:
#     lis = []
#     for l in lst:
#         if len(str(l)) == i:
#             lis.append(l)
#     dic[i] = lis
# print(dic)

#    2. 请用尽量简洁的方法将二位数组转化成一维数组lst = [[1,2,3],[4,5,6],[7,8,9]]
# lst = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
# print([l for i in lst for l in i])


#    3. 判断一个字典中是否有这些key： ‘AAA’,’BB’,’C’,’DD’,’EEE’(不使用for while)
# dic = {"AAA": 123}
# print(dic.get("AAA"))


# def func(a):
#     return dic.get(a)


#    4. 将aaabbcccd这种形式的字符串压缩成a3b2c3d1这种形式
#
s = "aaabbcccd"


# print("".join(['a', f"{s.count('a')}", 'b', f"{s.count('b')}", 'c', f"{s.count('c')}", 'd', f"{s.count('d')}"]))


def func(a):
    lis = []
    [lis.append(i) for i in a if i not in lis]
    print("".join(list(str(l) for i in zip("".join(lis), [s.count(l) for l in lis]) for l in i)))


func(s)
