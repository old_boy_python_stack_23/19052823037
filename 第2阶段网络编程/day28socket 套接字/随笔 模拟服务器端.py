# 一
import socket

phone = socket.socket()
phone.bind(('127.0.0.1', 9000))
phone.listen(8)
print("star")

conn, addr = phone.accept()
print(conn, addr)
while 1:
    from_client_data = conn.recv(1024)
    print(f"来自客户端{addr}消息{from_client_data.decode('utf-8')}")
    to_client = input(">>>")
    conn.send(to_client.encode("utf-8"))


conn.close()
phone.close()


# 二
# import socket
#
# phone = socket.socket()
# phone.bind(('127.0.0.1', 8848))
# phone.listen(8)
# while 1:
#     print("star")
#     conn, addr = phone.accept()
#     print(conn, addr)
#     while 1:
#         try:
#             from_client_data = conn.recv(1024)
#             if from_client_data == b"q":
#                 break
#             print(f"来自客户端{addr}消息{from_client_data.decode('utf-8')}")
#             to_client = input(">>>")
#             conn.send(to_client.encode("utf-8"))
#         except ConnectionAbortedError:
#             break
#
#     conn.close()
# phone.close()

# 三
# import subprocess
#
# import socket
#
# phone = socket.socket()
# phone.bind(('127.0.0.1', 9000))
# phone.listen(8)
# while 1:
#     print("star")
#     conn, addr = phone.accept()
#     print(conn, addr)
#     while 1:
#         try:
#             cmd = conn.recv(1024)
#             obj = subprocess.Popen(cmd.decode("utf-8"))
#             if cmd == b"q":
#                 break
#             print(f"来自客户端{addr}消息{cmd.decode('utf-8')}")
#
#             ob = subprocess.Popen(f'{cmd.decode("utf-8")}',
#                                   shell=True,
#                                   stdout=subprocess.PIPE,
#                                   stderr=subprocess.PIPE,
#                                   )
#             conn.send(f"{ob.stdout.read().decode('gbk')},{ob.stderr.read().decode('gbk').encode('utf-8')}".encode("utf-8"))
#         except ConnectionAbortedError:
#             break
#
# conn.close()
# phone.close()
