# 进程之间的通信

## 互斥锁

进程之间数据不共享,但是共享同一套文件系统, 所以可以访问同一个文件,或同一个打印终端,但是共享会带来竞争, 竞争带来的结果就是混乱, 如何控制进程有序的进行 就是使用枷锁处理

即 : 为了保证共享数据的完整性, 每个对象都应用于一个称为互斥锁的标记, 用来保证任一时刻,只能有一个线程访问该对象(文件)

个人理解 : 互斥锁是一组方法 来保证 对共享数据文件进行操作是 只能有一个线程访问

Lock类:

使用方法: 先实例化一个对象lock  = Lock()

两个方法 acquire.(锁住) release(开锁)  成对出现

注意: 不能连续两个acquire 会使系统夯住 慎用

```
# 业务: 三台电脑同一时刻共同调用打印机,完成打印业务.
# 3个进程,同一时刻共抢一个资源: 输出平台.
# from multiprocessing import Process
# import time
# import random
# def task1():
#     print('task1: 开始打印')
#     time.sleep(random.randint(1,3))
#     print('task1: 打印完成')
#
# def task2():
#     print('task2: 开始打印')
#     time.sleep(random.randint(1, 3))
#     print('task2: 打印完成')
#
# def task3():
#     print('task3: 开始打印')
#     time.sleep(random.randint(1, 3))
#     print('task3: 打印完成')
#
# if __name__ == '__main__':
#     p1 = Process(target=task1,)
#     p2 = Process(target=task2,)
#     p3 = Process(target=task3,)
#
#     p1.start()
#     p2.start()
#     p3.start()

# 多个进程共抢一个资源,你要是做到结果第一位,效率第二位.
# 你应该牺牲效率,保求结果.串行.

# 版本二:

# from multiprocessing import Process
# import time
# import random
# def task1():
#     print('task1: 开始打印')
#     time.sleep(random.randint(1,3))
#     print('task1: 打印完成')
#
# def task2():
#     print('task2: 开始打印')
#     time.sleep(random.randint(1, 3))
#     print('task2: 打印完成')
#
# def task3():
#     print('task3: 开始打印')
#     time.sleep(random.randint(1, 3))
#     print('task3: 打印完成')
#
# if __name__ == '__main__':
#     p1 = Process(target=task1,)
#     p2 = Process(target=task2,)
#     p3 = Process(target=task3,)
#
#     p2.start()
#     p2.join()
#     p1.start()
#     p1.join()
#     p3.start()
#     p3.join()

# 虽然说上面版本完成了串行结果,保证了顺序,但是没有实现公平.
# 顺序是人为写好的.我们要做到他公平去抢占打印机资源,谁先抢到,先执行谁.


# 三个人共住, 谁先起来谁先上厕所,你们抢的是厕所的那把锁,抢到锁锁上,你就上厕所.完事儿之后,打开锁.

# 版本三:


from multiprocessing import Process
from multiprocessing import Lock
import time
import random


def task1(lock):
    print('task1')  # 验证cpu遇到io切换了
    lock.acquire()
    print('task1: 开始打印')
    time.sleep(random.randint(1, 3))
    print('task1: 打印完成')
    lock.release()

def task2(lock):
    print('task2')  # 验证cpu遇到io切换了
    lock.acquire()
    print('task2: 开始打印')
    time.sleep(random.randint(1, 3))
    print('task2: 打印完成')
    lock.release()


def task3(lock):
    print('task3') # 验证cpu遇到io切换了
    lock.acquire()
    print('task3: 开始打印')
    time.sleep(random.randint(1, 3))
    print('task3: 打印完成')
    lock.release()


if __name__ == '__main__':

    lock = Lock()

    p1 = Process(target=task1, args=(lock,))
    p2 = Process(target=task2, args=(lock,))
    p3 = Process(target=task3, args=(lock,))

    p1.start()
    p2.start()
    p3.start()

# 上锁:
# 一定要是同一把锁: 只能按照这个规律:上锁一次,解锁一次.

# 互斥锁与join区别共同点? (面试题)

# 共同点: 都是完成了进程之间的串行.
# 区别: join认为控制的进程串行,互斥锁是随机的抢占资源.保证了公平性
```

## 模拟抢票系统

```
# 业务需求分析:

# 买票之前先要查票,必须经历的流程: 你在查票的同时,100个人也在查本此列票.
# 买票时,你要先从服务端获取到票数,票数>0 ,买票,然后服务端票数减一. 中间肯定有网络延迟.
from multiprocessing import Process
from multiprocessing import Lock
import time
import json
import os
import random
# 多进程原则上是不能互相通信的,它们在内存级别数据隔离的.不代表磁盘上数据隔离.
# 它们可以共同操作一个文件.

def search():
    time.sleep(random.random())
    with open('db.json',encoding='utf-8') as f1:
        dic = json.load(f1)
    print(f'剩余票数{dic["count"]}')


def get():
    with open('db.json',encoding='utf-8') as f1:
        dic = json.load(f1)
    time.sleep(random.randint(1,3))
    if dic['count'] > 0:
        dic['count'] -= 1
        with open('db.json', encoding='utf-8', mode='w') as f1:
            json.dump(dic,f1)
        print(f'{os.getpid()}用户购买成功')
    else:
        print('没票了.....')

def task(lock):
    search()
    lock.acquire()
    get()
    lock.release()


if __name__ == '__main__':
    lock = Lock()
    for i in range(5):
        p = Process(target=task,args=(lock,))
        p.start()


    # with open('db.json', encoding='utf-8',mode='w') as f1:
    #     json.dump({'count': 3},f1)
```

## 进程之间的通信: 队列

什么是队列: 定义：队列，又称为伫列（queue），是先进先出（FIFO, First-In-First-Out）的线性表。在具体应用中通常用链表或者数组来实现。队列只允许在后端（称为rear）进行插入操作，在前端（称为front）进行删除操作。

Queue 对象

```
from multiprocessing import Queue

# q = Queue(3)  # 可以设置元素个数
#
# def func():
#     print('in func')
#
# q.put('alex')
# q.put({'count': 1})
# q.put(func)
# print(222)
# q.put(666)  # 当队列数据已经达到上限,在插入数据的时候,程序就会夯住.
# print(111)

# print(q.get())
# print(q.get())
# print(q.get())
# ret = q.get()
# ret()

# 当你将数据取完继续在取值时,
# print(q.get())
# print(q.get())
# print(q.get())
# print(q.get())

# 1. maxsize()   q = Queue(3)  数据量不易过大.精简的重要的数据.
# 2. put block 默认为True 当你插入的数据超过最大限度,默认阻塞.
# q = Queue(3)  # 可以设置元素个数
#
# q.put('alex')
# q.put({'count': 1})
# q.put(22)  #
# q.put(333,block=False)  # 改成False 数据超过最大限度,不阻塞了直接报错.
# 3. put timeout() 参数
# q = Queue(3)  # 可以设置元素个数
#
# q.put('alex')
# q.put({'count': 1})
# q.put(22)  #
# q.put(333,timeout=3)  # 延时报错,超过三秒再put不进数据,就会报错.

# q = Queue()
# q.put('alex')
# q.put({'count': 1})
# q.put(22)  #
#
# print(q.get())
# print(q.get())
# print(q.get())
# print(q.get(block=False))  timeout
```

## 进程之间的通信实例

```
# 小米:抢手环4.预期发售10个.
# 有100个人去抢.
import os
from multiprocessing import Queue
from multiprocessing import Process

def task(q):
    try:
        q.put(f'{os.getpid()}',block=False)
    except Exception:
        return


if __name__ == '__main__':
    q = Queue(10)
    for i in range(100):
        p = Process(target=task,args=(q,))
        p.start()
    for i in range(1,11):
        print(f'排名第{i}的用户: {q.get()}',)
```

## 生产者消费者模型



```
from multiprocessing import Process
from multiprocessing import Queue
import time
import random


# def producer(q):
#     time.sleep(3)
#     q.put('包子')
#     # for i in range(5):
#
#
#
#
# def consumer(q):
#     print(q.get())


def producer(name,q):
    for i in range(1,6):
        time.sleep(random.randint(1,3))
        res = f'{i}号包子'
        q.put(res)

        print(f'\033[0;32m 生产者{name}: 生产了{res}\033[0m')



def consumer(name,q):
    while 1:
        try:
            time.sleep(random.randint(1,3))
            ret = q.get(timeout=5)
            print(f'消费者{name}: 吃了{ret}')
        except Exception:
            return




if __name__ == '__main__':

    q = Queue()

    p1 = Process(target=producer, args=('太白',q))
    p2 = Process(target=consumer, args=('MC骚强',q))

    p1.start()
    p2.start()
```