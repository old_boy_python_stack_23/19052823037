from multiprocessing import Process
from multiprocessing import Lock
from multiprocessing import Queue
import time
import random
import json
import os


# dic = {"count": 1}
# with open('db.json', encoding='utf-8', mode='w') as f:
#     json.dump(dic, f)
#
# def task1(lock):
#     print("task1")
#     lock.acquire()
#     print('1开始打印')
#     time.sleep(random.randint(1, 3))
#     print("1 打印完成")
#     lock.release()
#
#
# def task2(lock):
#     print("task2")
#     lock.acquire()
#     print('2 开始打印')
#     time.sleep(random.randint(1, 3))
#     print("2 打印完成")
#     lock.release()
#
#
# def task3(lock):
#     print("task3")
#     lock.acquire()
#     print('3 开始打印')
#     time.sleep(random.randint(1, 3))
#     print("3 打印完成")
#     lock.release()
#
#
# if __name__ == '__main__':
#     lock = Lock()
#     p1 = Process(target=task1, args=(lock,))
#     p2 = Process(target=task2, args=(lock,))
#     p3 = Process(target=task3, args=(lock,))
#
#     p1.start()
#     p2.start()
#     p3.start()

# def search():
#     time.sleep(random.random())
#     with open("db.json", encoding='utf-8') as f1:
#         dic = json.load(f1)
#     print(f'剩余票数{dic["count"]}')
#
#
# def get():
#     with open("db.json", encoding='utf-8') as f1:
#         dic = json.load(f1)
#     time.sleep(random.randint(1, 3))
#     if dic['count'] > 0:
#         dic['count'] -= 1
#         with open('db.json', encoding='utf-8', mode='w') as f1:
#             json.dump(dic, f1)
#         print(f'{os.getpid()}用户购买成功')
#     else:
#         print('票没了')
#
#
# def task(lock):
#     search()
#     lock.acquire()
#     get()
#     lock.release()
#
#
# if __name__ == '__main__':
#     lock = Lock()
#     for i in range(5):
#         p = Process(target=task, args=(lock,))
#         p.start()
# q = Queue(3)


# def func():
# #     print('in func')
# #
# #
# # q.put("alex")
# # q.put({'count': 2})
# # q.put(func)
# # print(q.get())
# # print(q.get())
# # print(q.get())
# # print(q.get())

def task(q):
    try:
        q.put(f'{os.getpid()}', block=False)
    except Exception:
        return


if __name__ == '__main__':
    q = Queue(10)
    for i in range(100):
        p = Process(target=task, args=(q,))
        p.start()
    for i in range(1, 11):
        print(f'排名{i}的用户:{q.get()}')
