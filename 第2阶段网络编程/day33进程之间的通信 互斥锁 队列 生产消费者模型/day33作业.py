# 业务需求分析:

# 买票之前先要查票,必须经历的流程: 你在查票的同时,100个人也在查本此列票.
# 买票时,你要先从服务端获取到票数,票数>0 ,买票,然后服务端票数减一. 中间肯定有网络延迟.
# from multiprocessing import Process
# from multiprocessing import Lock
# import time
# import json
# import os
# import random
# from multiprocessing import Queue
#
#
# # 多进程原则上是不能互相通信的,它们在内存级别数据隔离的.不代表磁盘上数据隔离.
# # 它们可以共同操作一个文件.
#
# def search(q):
#     time.sleep(random.random())
#     dic = q.get()
#     q.put(dic)
#     print(f'剩余票数{dic["count"]}')
#
#
# def get(q):
#     time.sleep(random.randint(1, 3))
#     dic = q.get()
#     if dic['count'] > 0:
#         dic['count'] -= 1
#
#         print(f'{os.getpid()}用户购买成功')
#     else:
#         print('没票了.....')
#     q.put(dic)
#
#
# def task(q):
#     search(q)
#     get(q)
#
#
# if __name__ == '__main__':
#     q = Queue(1)
#     with open('db.json', encoding='utf-8') as f1:
#         dic = json.load(f1)
#         q.put(dic)
#     for i in range(5):
#         p = Process(target=task, args=(q,))
#         p.start()
#     time.sleep(5)
#
#     with open('db.json', encoding='utf-8', mode="w") as f1:
#         json.dump(q.get(), f1)


from multiprocessing import Process
from multiprocessing import Lock
import time
import json
import os
import random
from multiprocessing import Queue


# 多进程原则上是不能互相通信的,它们在内存级别数据隔离的.不代表磁盘上数据隔离.
# 它们可以共同操作一个文件.

def search(q):
    time.sleep(random.random())
    dic = q.get()
    q.put(dic)
    print(f'剩余票数{dic["count"]}')


def get(q):
    time.sleep(random.randint(1, 3))
    dic = q.get()
    if dic['count'] > 0:
        dic['count'] -= 1

        print(f'{os.getpid()}用户购买成功')
    else:
        print('没票了.....')
    q.put(dic)


def task(q):
    search(q)
    get(q)


if __name__ == '__main__':
    q = Queue(1)
    with open('db.json', encoding='utf-8') as f1:
        dic = json.load(f1)
        q.put(dic)
    for i in range(5):
        p = Process(target=task, args=(q,))
        p.start()
    time.sleep(5)
    try:
        with open('db.json', encoding='utf-8', mode="w") as f1:
            json.dump(q.get(block=False), f1)
    except Exception:
        pass
