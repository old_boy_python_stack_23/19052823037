# import socket
#
# client = socket.socket()
# client.connect(('127.0.0.9', 8848))
# while 1:
#     obj = input(">>>").strip()
#     client.send(obj.encode("utf-8"))
#     if obj.upper() == 'Q':
#         break
#     from_sever_data = client.recv(1024)
#     print(from_sever_data.decode("utf-8"))
# client.close()

# 进程池 线程池
# from concurrent.futures import ProcessPoolExecutor  # 进程
# from concurrent.futures import ThreadPoolExecutor  # 线程
# import time
# import os
# import random
#
#
# def task(name):
#     print(name)
#     print(f'{os.getpid()} 准备接客')
#     time.sleep(random.randint(1, 3))
#
#
# if __name__ == '__main__':
#
#     p = ProcessPoolExecutor()  # ,默认cpu数量*5
#     for i in range(23):
#         p.submit(task, 1)  # 给进程池放任务,传参


# class Gpp:
#     _instance = None
#
#     def __new__(cls, *args, **kwargs):
#         if not cls._instance:
#             cls._instance = object.__new__(cls)
#         return cls._instance
#
#
# print(Gpp())
# print(Gpp())
# print(Gpp())
# print(Gpp())

# import gevent
#
#
# def ad(name):
#     print(name)
#
#
# name = "alex"
# t = gevent.spawn(ad, name)
# t.start()


import gevent
import time


def eat(name):
    print('%s eat 1' % name)  # 1
    # gevent.sleep(2)
    time.sleep(300)
    print('%s eat 2' % name)


def play(name):
    print('%s play 1' % name)  # 2
    # gevent.sleep(1)
    time.sleep(3)
    print('%s play 2' % name)


g1 = gevent.spawn(eat, 'alex')
g2 = gevent.spawn(play, name='taibai')
g1.start()

# g1.join()
# g2.join()
