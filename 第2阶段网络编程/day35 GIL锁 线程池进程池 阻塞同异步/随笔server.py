import socket
from threading import Thread

# server = socket.socket()
# server.bind(("127.0.0.9", 8848))
# server.listen()
# while 1:
#     conn, addr = server.accept()
#     print(f"{addr}客户:")
#     while 1:
#         try:
#             from_client_data = conn.recv(1024)
#             print(from_client_data.decode('utf-8'))
#             to_client = input(">>>").strip()
#             conn.send(to_client.encode('utf-8'))
#         except Exception:
#             break
#     conn.close()
# server.close()


def commumication(conn):
    while 1:
        try:
            from_client_data = conn.recv(1024)
            print(from_client_data.decode("utf-8"))

            to_client_data = input(">>>").strip()
            conn.send(to_client_data.encode("utf-8"))
        except Exception:
            break
    conn.close()


def customer_service():
    server = socket.socket()
    server.bind(("127.0.0.9", 8848))
    server.listen()

    while 1:
        conn, addr = server.accept()
        print(f"{addr}客户:")
        t = Thread(target=commumication, args=(conn,))
        t.start()
    server.close()


if __name__ == '__main__':
    customer_service()
