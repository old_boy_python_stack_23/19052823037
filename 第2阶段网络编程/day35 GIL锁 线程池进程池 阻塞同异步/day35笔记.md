# day35

## GIL锁

GIL锁:全局解释器锁,就是一把互斥锁,将并发变成串行,同一时刻只能有一个线程使用共享资源,牺牲效率,保证数据安全.

python并发不行,趁着python蹭热度

![1564046894839](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1564046894839.png)

Ipython:交互式解释器,可以不全代码

Jpython:Java字节码剩下的一样

pypy:动态编译 JAT技术,技术有缺陷,bug

![1564046999812](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1564046999812.png)

![1564047011367](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1564047011367.png)

设置全局锁:GIL

1. 保证解释器里面的数据安全.当时开发Python是, 只有单核
2. 强行加锁:减轻了开发人员的负担

双刃剑:加了这把锁, 带来了什么问题

![1564047310803](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1564047310803.png)




问题1:

单进程的多线程不能利用多核.诟病之一

多进程的多线程可以利用多核

问题二:

感觉上不能并发的执行问题??

讨论:单核处理IO阻塞的多线程,与多核处理IO阻塞的多线程效率差不多

![1564047597413](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1564047597413.png)

有了多核之后,GIL锁为什么不去掉:

源代码太多,改不动了

![1564048323333](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1564048323333.png)

![1564048334970](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1564048334970.png)

总结:

多核的前提下: 如果任务Io密集型: 多线程并发.
如果任务计算密集型: 多进程并发.

## 验证CPython并发执行效率

```
# 计算密集型
# 开启四个进程,开启四个线程
# from multiprocessing import Process
# from threading import Thread
# import time
# import os
# # print(os.cpu_count())
#
# def task1():
#     res = 1
#     for i in range(1, 100000000):
#         res += i
#
#
# def task2():
#     res = 1
#     for i in range(1, 100000000):
#         res += i
#
#
# def task3():
#     res = 1
#     for i in range(1, 100000000):
#         res += i
#
#
# def task4():
#     res = 1
#     for i in range(1, 100000000):
#         res += i
#
# if __name__ == '__main__':
#     # 四个进程 四个cpu 并行 效率
#     start_time = time.time()
#     p1 = Process(target=task1)
#     p2 = Process(target=task2)
#     p3 = Process(target=task3)
#     p4 = Process(target=task4)
#
#     p1.start()
#     p2.start()
#     p3.start()
#     p4.start()
#
#     p1.join()
#     p2.join()
#     p3.join()
#     p4.join()
#     print(f'主: {time.time() - start_time}')  # 7.53943133354187
#
#     # 一个进程 四个线程 1 cpu 并发  25.775474071502686
#     # start_time = time.time()
#     # p1 = Thread(target=task1)
#     # p2 = Thread(target=task2)
#     # p3 = Thread(target=task3)
#     # p4 = Thread(target=task4)
#     #
#     # p1.start()
#     # p2.start()
#     # p3.start()
#     # p4.start()
#     #
#     # p1.join()
#     # p2.join()
#     # p3.join()
#     # p4.join()
#     # print(f'主: {time.time() - start_time}')  # 25.775474071502686

# 计算密集型:  多进程的并行  单进程的多线程的并发执行效率高很多.


# 讨论IO密集型: 通过大量的任务去验证.
#
from multiprocessing import Process
from threading import Thread
import time
import os


# print(os.cpu_count())

def task1():
    res = 1
    time.sleep(3)


# if __name__ == '__main__':
    
    # 开启150个进程(开销大,速度慢),执行IO任务, 耗时 9.293531656265259

    # start_time = time.time()
    # l1 = []
    # for i in range(150):
    #     p = Process(target=task1)
    #     l1.append(p)
    #     p.start()
    # for i in l1:
    #     i.join()
    # print(f'主: {time.time() - start_time}')


    # 开启150个线程(开销小,速度快),执行IO任务, 耗时 3.0261728763580322
    # start_time = time.time()
    # l1 = []
    # for i in range(150):
    #     p = Thread(target=task1)
    #     l1.append(p)
    #     p.start()
    # for i in l1:
    #     i.join()
    # print(f'主: {time.time() - start_time}')  # 3.0261728763580322


# 任务是IO密集型并且任务数量很大,用单进程下的多线程效率高.
```



## 讨论GIL全局解释器锁与自定义互斥锁的关系

```
# 互斥锁

# 1. GIL 自动上锁解锁, 文件中的互斥锁Lock 手动上锁解锁.
# 2. GIL锁 保护解释器的数据安全. 文件的互斥锁Lock 保护的文件数据的安全.

# from threading import Thread
# from threading import Lock
# import time
#
# lock = Lock()
# x = 100
#
# def task():
#     global x
#     lock.acquire()
#     temp = x
#     # time.sleep(1)
#     temp -= 1
#     x = temp
#     lock.release()
#
#
#
# if __name__ == '__main__':
#     t_l = []
#     for i in range(100):
#         t = Thread(target=task)
#         t_l.append(t)
#         t.start()
#
#     for i in t_l:
#         i.join()
#
#     print(f'主线程{x}')
#
# # 线程全部是计算密集型:当程序执行,开启100个线程时,第一个线程先要拿到GIL锁,然后拿到lock锁,释放lock锁,最后释放GIL锁.



from threading import Thread
from threading import Lock
import time

lock = Lock()
x = 100

def task():
    global x
    lock.acquire()
    temp = x
    time.sleep(1)
    temp -= 1
    x = temp
    lock.release()



if __name__ == '__main__':
    t_l = []
    for i in range(100):
        t = Thread(target=task)
        t_l.append(t)
        t.start()

    for i in t_l:
        i.join()

    print(f'主线程{x}')
'''
线程IO密集型:当程序执行,开启100个线程时,第一个线程先要拿到GIL锁,然后拿到lock锁,
运行,遇到....截图上有解释..

总结: 自己加互斥锁,一定要加在处理共享数据的地方,加的范围不要扩大,

'''
# 释放lock锁,最后释放GIL锁.
```

## 进程池线程池

池': 容器, 进程池: 放置进程的一个容器, 线程池: 放置线程的一个容器.
完成了一个简单的socket通信, 服务端必须与一个客户端交流完毕并且这个客户端断开连接之后,服务端才
能接待下一个客户端.....
不合理.

```
import socket
from threading import Thread

def communication(conn):
    while 1:
        try:
            from_client_data = conn.recv(1024)  # 阻塞
            print(from_client_data.decode('utf-8'))

            to_client_data = input('>>>').strip()
            conn.send(to_client_data.encode('utf-8'))
        except Exception:
            break
    conn.close()


def customer_service():

    server = socket.socket()
    server.bind(('127.0.0.1', 8080))
    server.listen()

    while 1:
        conn,addr = server.accept()  # 阻塞
        print(f'{addr}客户:')
        t = Thread(target=communication,args=(conn,))
        t.start()
    server.close()

if __name__ == '__main__':
    customer_service()
```

客户端:

```
import socket

client = socket.socket()
client.connect(('127.0.0.1', 8080))

while 1:
    to_server_data = input('>>>').strip()
    client.send(to_server_data.encode('utf-8'))
    from_server_data = client.recv(1024)
    print(f'客服回信: {from_server_data.decode("utf-8")}')

client.close()
```

线程即使开销小,你的电脑不可以无限的开线程,我们应该对线程(进程)做数量的限制.在计算机的能满足的最大情况下,更多的创建线程(进程).

线程池好,进程池好?

多线程,多进程.: IO 计算



同步是指：当程序1调用程序2时，程序1停下不动，直到程序2完成回到程序1来，程序1才继续执行下去。  
异步是指：当程序1调用程序2时，程序1径自继续自己的下一个动作，不受程序2的的影响。

在进行网络编程时，我们常常见到同步、异步、阻塞和非阻塞四种调用方式。这些方式彼此概念并不好理解。下面是我对这些术语的理解。
同步
所谓同步，就是在发出一个功能调用时，在没有得到结果之前，该调用就不返回。按照这个定义，其实绝大多数函数都是同步调用（例如sin, isdigit等）。但是一般而言，我们在说同步、异步的时候，特指那些需要其他部件协作或者需要一定时间完成的任务。最常见的例子就是 SendMessage。该函数发送一个消息给某个窗口，在对方处理完消息之前，这个函数不返回。当对方处理完毕以后，该函数才把消息处理函数所返回的 LRESULT值返回给调用者。
异步
异步的概念和同步相对。当一个异步过程调用发出后，调用者不能立刻得到结果。实际处理这个调用的部件在完成后，通过状态、通知和回调来通知调用者。以CAsycSocket类为例（注意，CSocket从CAsyncSocket派生，但是起功能已经由异步转化为同步），当一个客户端通过调用 Connect函数发出一个连接请求后，调用者线程立刻可以朝下运行。当连接真正建立起来以后，socket底层会发送一个消息通知该对象。这里提到执行部件和调用者通过三种途径返回结果：状态、通知和回调。可以使用哪一种依赖于执行部件的实现，除非执行部件提供多种选择，否则不受调用者控制。如果执行部件用状态来通知，那么调用者就需要每隔一定时间检查一次，效率就很低（有些初学多线程编程的人，总喜欢用一个循环去检查某个变量的值，这其实是一种很严重的错误）。如果是使用通知的方式，效率则很高，因为执行部件几乎不需要做额外的操作。至于回调函数，其实和通知没太多区别。
阻塞
阻塞调用是指调用结果返回之前，当前线程会被挂起。函数只有在得到结果之后才会返回。有人也许会把阻塞调用和同步调用等同起来，实际上他是不同的。对于同步调用来说，很多时候当前线程还是激活的，只是从逻辑上当前函数没有返回而已。例如，我们在CSocket中调用Receive函数，如果缓冲区中没有数据，这个函数就会一直等待，直到有数据才返回。而此时，当前线程还会继续处理各种各样的消息。如果主窗口和调用函数在同一个线程中，除非你在特殊的界面操作函数中调用，其实主界面还是应该可以刷新。socket接收数据的另外一个函数recv则是一个阻塞调用的例子。当socket工作在阻塞模式的时候，如果没有数据的情况下调用该函数，则当前线程就会被挂起，直到有数据为止。
非阻塞
非阻塞和阻塞的概念相对应，指在不能立刻得到结果之前，该函数不会阻塞当前线程，而会立刻返回。
对象的阻塞模式和阻塞函数调用
对象是否处于阻塞模式和函数是不是阻塞调用有很强的相关性，但是并不是一一对应的。阻塞对象上可以有非阻塞的调用方式，我们可以通过一定的API去轮询状态，在适当的时候调用阻塞函数，就可以避免阻塞。而对于非阻塞对象，调用特殊的函数也可以进入阻塞调用。函数select就是这样的一个例子。