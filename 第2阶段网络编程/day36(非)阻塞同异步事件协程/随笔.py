# 阻塞 非阻塞 异步 同步
#
# from concurrent.futures import ProcessPoolExecutor
# import os
# import time
# import random
#
#
# def task():
#     print(f'{os.getpid()} is running')
#     time.sleep(1)
#     return f'{os.getpid()} is finish'
#
#
# if __name__ == '__main__':
#
#     p = ProcessPoolExecutor(4)
#
#     for i in range(10):
#         obj = p.submit(task, )  # 异步发出.
#         print(obj.result())


# from concurrent.futures import ProcessPoolExecutor
# import os
# import time
# import random
#
#
# def task():
#     print(f'{os.getpid()} is running')
#     time.sleep(random.randint(0, 2))
#     return f'{os.getpid()} is finish'
#
#
# if __name__ == '__main__':
#     p = ProcessPoolExecutor(4)
#     obj_l1 = []
#     # for i in range(10):
#     #     obj = p.submit(task, )  # 异步发出.
#     #     obj_l1.append(obj)
#
#     print(p.map(task, range(15)))
    # time.sleep(3)
    # p.shutdown(wait=True)
    # # 1. 阻止在向进程池投放新任务,
    # # 2. wait = True 十个任务是10,一个任务完成了-1,直至为零.进行下一行.
    # print(666)
    # for i in obj_l1:
    #     print(i.result())

# from concurrent.futures import ProcessPoolExecutor
# import os
# import time
#
#
# def task():
#     print(f"{os.getpid()} 进程begin")
#     time.sleep(2)
#     print(f"{os.getpid()} 进程结束")
#
#
# if __name__ == '__main__':
#     p = ProcessPoolExecutor(4)
#     for i in range(15):
#         p.submit(task)


import time
from multiprocessing import Process, Pool


def f1(n):
    n = 0
    for i in range(5):
        n = n + i
    return n


if __name__ == '__main__':
    # 统计进程池执行100个任务的时间
    pool = Pool(4)  # 里面这个参数是指定进程池中有多少个进程用的,4表示4个进程,如果不传参数,默认开启的进程数一般是cpu的个数
    # pool.map(f1,[1,2])  #参数数据必须是可迭代的
    pool.map(f1, range(100))  # 参数数据必须是可迭代的,异步提交任务,自带join功能
