# 进程



## 获取进程以及父进程的pid

进程在内存中开启多个,操作系统如何区分这些进程?每个进程都有一个唯一标识,

1. 在终端查看进程的pid.

   ![1563779943088](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1563779943088.png)

   ![1563779957492](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1563779957492.png)

2. 在终端查看执行的进程pid

   ![1563780012612](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1563780012612.png)

3. 通过代码查看pid

   ```
   import os
   import time
   print(f'子进程:{os.getpid()}')
   print(f'父进程:{os.getppid()}')
   time.sleep(5000)
   ```

## 验证进程之间的数据间隔

```
# from multiprocessing import Process
#
# x = 1000
#
# def task(name):
#     print(name)
#     global x
#     x = 2
#
#
# if __name__ == '__main__':
#     p1 = Process(target=task,args=('太白',))
#     p1.start()
#
#     print(f'主进程:{x}')


# 上面代码没有做到 主进程一定要在子进程运行之后才执行.
# from multiprocessing import Process
# import time
# x = 1000
#
# def task(name):
#     print(name)
#     global x
#     x = 2
#
# if __name__ == '__main__':
#     p1 = Process(target=task,args=('太白',))
#     p1.start()
#     time.sleep(3)
#     print(f'主进程:{x}')


# 上面已经验证了 子进程与父进程之间的空间隔离.,要验证初始变量是否是一个id.
# 数字-5~256 主进程子进程是沿用同一个.
# from multiprocessing import Process
# import time
# # x = 1000
# x = 255
# # s = 'a'
# # l1 = [1, 2, 3]
#
# def task(name):
#     print(f'子进程:{id(x)}')
#
# if __name__ == '__main__':
#     print(f'主进程:{id(x)}')
#     p1 = Process(target=task,args=('太白',))
#     p1.start()
#     # time.sleep(3)
```



## join方法

```
# join 等待,主进程等待子进程结束之后,在执行.

# from multiprocessing import Process
# import time
#
#
# def task(name):
#
#     print(f'{name} is running')
#
#
#
# if __name__ == '__main__':
#
#     p = Process(target=task,args=('李业',))
#     p.start()
#     time.sleep(1)
#     print('===主进程')


# 上面的版本,虽然达到目的,但是生产环境中,子进程结束的时间不定. 需要用到join

# from multiprocessing import Process
# import time
#
#
# def task(name):
#     time.sleep(1)
#     print(f'{name} is running')
#
#
#
# if __name__ == '__main__':
#
#     p = Process(target=task,args=('李业',))
#     p.start()
#     p.join()  # 告知主进程,p进程结束之后,主进程在运行.
#     # join感觉有一些阻塞的意思.
#     print('===主进程')


# 开启多个子进程去验证:
# from multiprocessing import Process
# import time
#
#
# def task(name):
#     time.sleep(1)
#     print(f'{name} is running')
#
#
#
# if __name__ == '__main__':
#
#     p1 = Process(target=task, args=('李业',))
#     p2 = Process(target=task, args=('怼哥',))
#     p3 = Process(target=task, args=('mc骚Q',))
#
#     p1.start()
#     p2.start()
#     p3.start()
#     # p1, p2, p3 三个子进程运行的先后顺序不定.
#     # start只是通知一下操作系统,三个start几乎同一时刻发给操作系统,
#     # 操作系统调用cpu先运行谁,谁先执行.
#
#     # print('===主进程')


# 下面的版本验证了: 如此写join并不是串行.
# from multiprocessing import Process
# import time
#
#
# def task(name,sec):
#     time.sleep(sec)
#     print(f'{name} is running')
#
#
# if __name__ == '__main__':
#     p1 = Process(target=task, args=('李业', 1))
#     p2 = Process(target=task, args=('怼哥', 2))
#     p3 = Process(target=task, args=('mc骚Q', 3))
#     start_time = time.time()
#     p1.start()
#     p2.start()
#     p3.start()
#     # p1, p2, p3 三个子进程运行的先后顺序不定.
#     # start只是通知一下操作系统,三个start几乎同一时刻发给操作系统,
#     # 操作系统调用cpu先运行谁,谁先执行.
#     p1.join()  # 阻塞不在这. time.sleep(1)
#     p2.join()
#     p3.join()
#     print(f'===主进程:{time.time() - start_time}之后,执行')

#
# from multiprocessing import Process
# import time
#
#
# def task(name,sec):
#     time.sleep(sec)
#     print(f'{name} is running')
#
#
# if __name__ == '__main__':
#     p1 = Process(target=task, args=('李业', 1))
#     p2 = Process(target=task, args=('怼哥', 1))
#     p3 = Process(target=task, args=('mc骚Q', 1))
#     start_time = time.time()
#     p1.start()
#     p2.start()
#     p3.start()
#     # p1, p2, p3 三个子进程运行的先后顺序不定.
#     # start只是通知一下操作系统,三个start几乎同一时刻发给操作系统,
#     # 操作系统调用cpu先运行谁,谁先执行.
#     p1.join()
#     print(f'===主进程:{time.time() - start_time}之后,执行')


# 下面的结果是什么,为什么!



# from multiprocessing import Process
# import time
#
#
# def task(name,sec):
#
#     print(f'{name} is running')
#     time.sleep(sec)
#     print(f'{name} is done')
#
#
#
# if __name__ == '__main__':
#     p1 = Process(target=task, args=('李业', 3))
#     # p2 = Process(target=task, args=('怼哥', 2))
#     # p3 = Process(target=task, args=('mc骚Q', 3))
#     start_time = time.time()
#     p1.start()
#     p1.join()
#     # print(111)
#     p2.start()
#     p2.join()
#     p3.start()
#     p3.join()
#
#     print(f'===主进程:{time.time() - start_time}之后,执行')


#
# from multiprocessing import Process
# import time
#
#
# def task(name,sec):
#
#     print(f'{name} is running')
#     time.sleep(sec)
#     print(f'{name} is done')
#
#
#
# if __name__ == '__main__':
#     p1 = Process(target=task, args=('李业', 3))
#     # p2 = Process(target=task, args=('怼哥', 2))
#     # p3 = Process(target=task, args=('mc骚Q', 3))
#     start_time = time.time()
#     p1.start()
#     p1.join()
#     p2.start()
#     p2.join()
#     p3.start()
#     p3.join()
#
#     print(f'===主进程:{time.time() - start_time}之后,执行')


# from multiprocessing import Process
# import time
#
#
# def task(name,sec):
#     time.sleep(sec)
#     print(f'{name} is running')
#
#
# if __name__ == '__main__':
#     p1 = Process(target=task, args=('李业', 3))
#     p2 = Process(target=task, args=('怼哥', 2))
#     p3 = Process(target=task, args=('mc骚Q', 1))
#     start_time = time.time()
#     p1.start()
#     p2.start()
#     p3.start()
#     # p1, p2, p3 三个子进程运行的先后顺序不定.
#     # start只是通知一下操作系统,三个start几乎同一时刻发给操作系统,
#     # 操作系统调用cpu先运行谁,谁先执行.
#     p1.join()
#     p2.join()
#     p3.join()
#     print(f'===主进程:{time.time() - start_time}之后,执行')




# from multiprocessing import Process
# import time
#
#
# def task(sec):
#     time.sleep(sec)
#     print(f'{sec}: is running')
#
#
# if __name__ == '__main__':
#     # p1 = Process(target=task, args=(1,))
#     # p2 = Process(target=task, args=(2,))
#     # p3 = Process(target=task, args=(3,))
#     # start_time = time.time()
#     # p1.start()
#     # p2.start()
#     # p3.start()
#     start_time = time.time()
#     p_l = []
#     for i in range(1,4):
#         p = Process(target=task, args=(i,))
#         p.start()
#         p_l.append(p)
#
#     for i in p_l:
#         i.join()
#
#
#     print(f'===主进程:{time.time() - start_time}之后,执行')
```



## 进程对象之间的其他属性

```
# from multiprocessing import Process
# import time
#
#
# def task(name):
#
#     print(f'{name} is running')
#     time.sleep(3)
#     print(f'{name} is done')
#
#
# if __name__ == '__main__':
#
#     p = Process(target=task,args=('怼哥',) ,name='任务1')  # name给进程对象设置name属性
#     p.start()
#     # print(p.pid)  # 获取进程pid号
#     # print(p.name)
#     # time.sleep(1)
#     p.terminate() # 终止(结束)子进程
#     # terminate 与 start一样的工作原理: 都是通知操作系统终止或者开启一个子进程,内存中终止或者开启(耗费时间)
#     # time.sleep(1)
#     # print(p.is_alive())  # 判断子进程是否存活
#     # 只是查看内存中p子进程是否运行.
#     print('===主进程')
```



## 僵尸进程和孤儿进程

```
# linux(mac)环境下才强调的两个概念,windows下没有.
# 面试官会问到这两个概念.


from multiprocessing import Process
import time
import os

def task(name):

    print(f'{name} is running')
    print(f'子进程开始了:{os.getpid()}')
    time.sleep(50)


if __name__ == '__main__':
    for i in range(100000):
        p = Process(target=task,args=('怼哥',))
        p.start()
    print(f'主进程开始了:{os.getpid()}')

# 主进程是子进程的发起者,按理说,主进程不会管子进程是否结束,对于结束来说,两个进程是没有任何关系的.
# 但是通过代码我们发现: 主进程并没有结束,实际上你的主进程要等到所有的子进程结束之后,主进程在结束.

# 所以此时的主进程称之为:僵尸进程.: 僵尸是什么? 死而不腐.
# 父进程为什么不关闭?
# 此时主进程形成了僵尸进程:
# 内存中只包含: 主进程的pid,以及子进程的开启时间,结束时间. 至于主进程的代码以及文件,数据库数据等等全部消失.
# 因为主进程要进行收尸环节.
# 利用这个waitepid()方法,对所有的结束的子进程进行收尸.


# 孤儿进程: 此时如果主进程由于各种原因,提前消失了,它下面的所有的子进程都成为孤儿进程了.

#谁给孤儿进程收尸? 孤儿院,民政局  一段时间之后, init就会对孤儿进程进行回收.

# 孤儿进程无害,如果僵尸进程挂了,init会对孤儿进程进行回收.
# 僵尸进程有害?
# 父进程(僵尸进程)无限的开启子进程,递归的开启,子进程越来越多,僵尸进程还没有结束,
# 导致进程会越来越多,占用内存.
```



## 守护进程

主进程创建守护进程
    其一:守护进程会在主进程代码执行结束后就终止
    其二:守护进程无法再开启子进程,否则抛出异常:AssertionError: daemonic processes are not allowed to have children
注意: 进程之间是相互独立的, 主进程代码运行结束, 守护进程随即终止
```
# 守护: 我守护者你,你要是死了,我就跟你一起.
# 子进程对父进程可以进行守护.
# 生产者消费者模型会讲到.
from multiprocessing import Process
import time
import os

def task(name):

    print(f'{name} is running')
    print(f'子进程开始了:{os.getpid()}')
    time.sleep(50)


if __name__ == '__main__':

    p = Process(target=task,args=('怼哥',))
    p.daemon = True  # 将p子进程设置成守护进程,守护主进程,只要主进程结束,子进程无论执行与否,都马上结束.
    p.start()
    time.sleep(2)
    print(f'主进程开始了:{os.getpid()}')
```