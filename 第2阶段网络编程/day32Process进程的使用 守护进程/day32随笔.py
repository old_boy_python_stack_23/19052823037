from multiprocessing import Process
import time

# 调用方式一
# def dask(name):
#     print(f"{name} is running")
#     time.sleep(3)
#     print(f"{name} is done")
#
#
# if __name__ == '__main__':
#     p = Process(target=dask, args= ('太白金星',))
#     p.start()
#     p.join()
#     print("===主程序")

# 调用方式二
# class MyProcess(Process):
#     def __init__(self, name):
#         super().__init__()
#         self.name = name
#
#     def run(self):
#         print(self.name)
#         print(f'{self.name} is running')
#         time.sleep(3)
#         print(f"{self.name} is done")
#
#
# if __name__ == '__main__':
#     p = MyProcess('太白金星')
#     p.start()
#     print("===主程序")

# 验证进程之间的相互隔离
# 同一个进程
# x = 1000
#
#
# def task():
#     global x
#     x = 2
#
#
# task()
# print(x)

# 不同进程中
# from multiprocessing import Process
# import time
#
# x = 1000
#
#
# def task():
#     global x
#     x = 2
#
#
# if __name__ == '__main__':
#     p = Process(target=task)
#     p.start()
#     time.sleep(3)
#     print(3)

# 进程对象的join方法
from multiprocessing import Process
import time

# 父进程等待子进程结束之后再执行
# 方法一sleep 不可取 极大降低了运行效率
# def task():
#     time.sleep(3)
#     print("子进程结束")
#
#
# if __name__ == '__main__':
#     p = Process(target=task, args=('太白金星',))
#     p.start()
#     time.sleep(1)
#     print("子进程结束")

# 方法二join
# from multiprocessing import Process
# import time
#
#
# def task(n):
#     print("子进程开始")
#     time.sleep(3)
#     print("子进程结束")
#
#
# if __name__ == '__main__':
#     p = Process(target=task, args=('太白金星',))
#     p.start()
#     # p.join()
#     print('主进程开始运行')

# 僵尸进程 和 孤儿进程
# 僵尸进程有害 一个进程使用fork创建子进程 如果子进程退出
# 而父进程没有调用wait 或者 waitpid获取子进程的状态信息,那么子进程的进程描述符任然保存在系统中
# 这种进程称为僵死进程

import os
import sys
import time

# pid = os.getpid()  # 进程id
# ppid = os.getppid()  # 父进程id
# print("in father", "pid", pid, "ppid", ppid)
# pid = os.fork()
# print(pid)

from multiprocessing import Process
from threading import Thread
import time


def foo():
    print(123)
    time.sleep(1)
    print("end123")


def bar():
    print(456)
    time.sleep(3)
    print("end456")


if __name__ == '__main__':
    p1 = Process(target=foo)
    p2 = Process(target=bar)

    p1.daemon = True
    p1.start()
    p2.start()
    print("main-------")
