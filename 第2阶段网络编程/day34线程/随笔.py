from threading import Thread

# class MyThread(Thread):
#     def run(self) -> None:
#         print(f'{self.name} is running')
#
#
# if __name__ == '__main__':
#     t = MyThread()
#     t.start()
#     print('主线程')

# x = 1000
#
#
# def task():
#     global x
#     x = 0
#
#
# if __name__ == '__main__':
#     t = Thread()
#     print(f'主先程:{x}')


# from threading import Thread
# import time
#
#
# def foo():
#     print(123)
#     time.sleep(3)
#     print("end123")
#
#
# def bar():
#     print(456)
#     time.sleep(1)
#     print("end456")
#
#
# if __name__ == '__main__':
#     t1 = Thread(target=foo)
#     t2 = Thread(target=bar)
#
#     t1.daemon = True
#     t1.start()
#     t2.start()
#     print("main-------")

# 开启线程的两种方式
# 第一种
# from threading import Thread
#
#
# def task(name):
#     print(f"{name} is running")
#
#
# if __name__ == '__main__':
#     t = Thread(target=task,args=('mc saoQ',))
#     t.start()
#     print('主进程')

# 方式二
# from threading import Thread
#
#
# class MyThread(Thread):
#     def run(self):
#         print(f'{self.name} is running')
#
#
# if __name__ == '__main__':
#     t = MyThread(args=('mc saoQ',))
#     t.start()

# # 开启速度对比
# from threading import Thread
# def task(name):
#     print(f'{name} is running')
#
# if __name__ == '__main__':
#     t= Thread(target=task,args=('mc saoQ',))
#     t.start()
#     print('主进程')
#
#
#
# # 结果:
# # 线程绝对要比进程快

# from threading import Thread
# import os
#
#
# def task():
#     print(f'子线程:{os.getpid()}')
#
#
# if __name__ == '__main__':
#     t = Thread(target=task, )
#     t.start()
#
#     print(f'主线程{os.getpid()}')

# 一个进程内所有的线程的编号都一样
# 子线程:27024
# 主线程27024

