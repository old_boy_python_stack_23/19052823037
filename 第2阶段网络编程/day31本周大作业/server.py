import socketserver
import socket
import os
import struct
import json
import hashlib

FILE_PATH = os.path.dirname(__file__)


def hsmd(file):
    ret = hashlib.md5()
    with open(file, mode='rb') as f:
        while 1:
            lit = f.read(1024)
            if lit:
                ret.update(lit)
            else:
                break
    return ret.hexdigest()


class MyServer(socketserver.BaseRequestHandler):  # 继承的类固定的

    def handle(self):  # 必须是这个handle名字.
        while 1:
            four_bytes = self.request.recv(4)
            # 判断情况 如果是'2'则为下载运行if 为 q 停止循环 其他 视为上传
            if four_bytes.decode('utf-8') == '2':
                # 获取特定目录下所有的文件名
                lis = os.listdir(os.path.join(os.path.dirname(__file__), 'server.data'))
                # 字典形式方便c端使用
                dic = {i + 1: lis[i] for i in range(len(lis))}
                # 发送给客户端
                file_dic_json = json.dumps(dic)
                self.request.send(file_dic_json.encode('utf-8'))
                # 接收c端选择的文件名
                file_name = self.request.recv(1024).decode('utf-8')
                # 求文件MD5值
                md5 = hsmd(os.path.join(FILE_PATH, 'server.data', file_name))
                # 制作 报头
                head_dict = {
                    "md5": md5,
                    'file_name': os.path.basename(os.path.join(FILE_PATH, 'server.data', file_name)),
                    'file_size': os.path.getsize(os.path.join(FILE_PATH, 'server.data', file_name))
                }

                head_dic_json = json.dumps(head_dict)
                head_dic_json_bytes = head_dic_json.encode('utf-8')
                head_len = len(head_dic_json_bytes)
                four_bytes = struct.pack('i', head_len)

                self.request.send(four_bytes)
                self.request.send(head_dic_json_bytes)

                with open(os.path.join(FILE_PATH, 'server.data', file_name), mode="rb") as f:
                    while 1:
                        cmd = f.read(1024)
                        if cmd:
                            self.request.send(cmd)
                        else:
                            break
            elif four_bytes.decode('utf-8').upper() == "Q":
                break
            # 接收上传文件
            else:
                head_len = struct.unpack('i', four_bytes)[0]

                head_dic_json_bytes = self.request.recv(head_len)

                head_dic_json = head_dic_json_bytes.decode('utf-8')

                head_dic = json.loads(head_dic_json)
                # 写入本地
                with open(f"{os.path.join(os.path.dirname(__file__), 'server.data', head_dic['file_name'])}",
                          mode='wb') as f:
                    while 1:
                        total_size = 0
                        while total_size < head_dic['file_size']:
                            every_data = self.request.recv(1024)
                            f.write(every_data)
                            total_size += len(every_data)
                        else:
                            break


if __name__ == '__main__':
    ip_port = ("127.0.0.2", 8849)
    server = socketserver.ThreadingTCPServer(ip_port, MyServer)
    server.serve_forever()
