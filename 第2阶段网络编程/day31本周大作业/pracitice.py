import json
import os
import hashlib

file_path = os.path.dirname(__file__)
user_name = input("请输入用户名").strip()
password = input("请输入密码").strip()

ret = hashlib.md5()
ret.update(password.encode('utf-8'))
dic = {user_name: ret.hexdigest()}

with open(os.path.join(file_path, "UserInfo"), mode="w", encoding='utf-8') as f:
    f.write(json.dumps(dic))
