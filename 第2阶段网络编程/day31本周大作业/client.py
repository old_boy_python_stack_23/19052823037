import hashlib
import os
import socket
import struct
import json

FILE_PATH = os.path.dirname(__file__)
dic_info = {}
with open(os.path.join(FILE_PATH, "UserInfo"), mode="r", encoding="utf-8") as f:
    dic_login = json.loads(f.read())


def hsmd(file):
    ret = hashlib.md5()
    with open(file, mode='rb') as f:
        while 1:
            lit = f.read(1024)

            if lit:
                ret.update(lit)
            else:
                break
    return ret.hexdigest()


def login():  # 登陆功能 三次退出此功能 return False 成功 return True
    count = 0
    while True:
        if count < 3:
            username_input = input("请输入用户名:").strip()
            user_password = input("请输入密码:").strip()
            ret = hashlib.md5()
            ret.update(user_password.encode('utf-8'))
            try:
                if ret.hexdigest() == dic_login[username_input]:
                    print("登陆成功")
                    dic_info['username'] = username_input
                    return True
                else:
                    print(f"用户名或密码错误!剩余登陆次数为:{2 - count}")
                    count += 1
            except KeyError:
                print(f"用户名或密码错误!剩余登陆次数为:{2 - count}")
                count += 1
        else:
            print("超过三次退出")
            exit(0)


def register():
    with open(os.path.join(FILE_PATH, "UserInfo"), mode='r+', encoding='utf-8') as f:
        dic = json.loads(f)
        user_name = input("请输入用户名").strip()
        password = input("请输入密码").strip()
        if not dic[user_name]:
            ret = hashlib.md5()
            ret.update(password.encode('utf-8'))
            dic[user_name] = ret.hexdigest()
            f.write(json.dumps(dic))
            dic_info['username'] = user_name
            return True
        else:
            print('用户名已存在')
            return False


def src():
    phone = socket.socket()
    phone.connect(("127.0.0.2", 8849))
    print('star')
    while 1:
        user_choice = input("请输入选择序号:\n1:上传\n2:下载").strip()
        # 上传
        if user_choice == "1":
            # 获取字典格式的目录下文件{4:""}
            lis = os.listdir(os.path.join(FILE_PATH, dic_info['username']))
            dic = {i + 1: lis[i] for i in range(len(lis))}
            print(dic)
            # 选择文件,并发送
            # 获取文件哈希值
            while 1:
                try:
                    file_choice = input("请输入选择的文件序号:").strip()
                    md5 = hsmd(os.path.join(FILE_PATH, dic_info['username'], dic[int(file_choice)]).encode('utf-8'))
                    break
                except Exception:
                    print('格式错误,请重新输入')

            head_dict = {
                "md5": md5,
                'file_name': os.path.basename(os.path.join(FILE_PATH, dic_info['username'], dic[int(file_choice)])),
                'file_size': os.path.getsize(os.path.join(FILE_PATH, dic_info['username'], dic[int(file_choice)]))
            }
            head_dic_json = json.dumps(head_dict)
            head_dic_json_bytes = head_dic_json.encode('utf-8')
            head_len = len(head_dic_json_bytes)
            four_bytes = struct.pack('i', head_len)

            phone.send(four_bytes)
            phone.send(head_dic_json_bytes)

            with open(f"{os.path.join(FILE_PATH, dic_info['username'], dic[int(file_choice)])}", mode="rb") as f:
                while 1:
                    cmd = f.read(1024)
                    if cmd:
                        phone.send(cmd)
                    else:
                        break
        # 下载
        elif user_choice == "2":
            phone.send(user_choice.encode('utf-8'))
            da = phone.recv(1024).decode('utf-8')
            print(f'{da}da')

            data_dic = json.loads(da)
            print(data_dic)
            while 1:
                try:
                    file_down_choice = input('请输入选择下载的文件序号:').strip()
                    phone.send(data_dic[file_down_choice].encode('utf-8'))
                    break
                except Exception:
                    print('输入格式错误')

            four_bytes = phone.recv(4)

            head_len = struct.unpack('i', four_bytes)[0]

            head_dict_json_bytes = phone.recv(head_len)

            head_dict_json = head_dict_json_bytes.decode('utf-8')

            head_dict = json.loads(head_dict_json)

            with open(os.path.join(os.path.dirname(__file__), dic_info['username'], head_dict['file_name']),
                      mode='wb') as f:
                while 1:
                    total_size = 0
                    while total_size < head_dict['file_size']:
                        every_data = phone.recv(1024)
                        f.write(every_data)
                        total_size += len(every_data)
                    else:
                        break

            md5 = hsmd(os.path.join(FILE_PATH, dic_info['username'], head_dict['file_name']).encode('utf-8'))
            if head_dict['md5'] == md5:
                print('校验成功')
            else:
                os.remove(os.path.join(os.path.dirname(__file__), dic_info['username'], head_dict['file_name']))
                print('校验失败,文件已删除')
        elif user_choice.upper() == 'Q':
            phone.send(user_choice.encode('utf-8'))
            break
        else:
            print('输入格式错误,请重新输入!')
    phone.close()


if __name__ == '__main__':
    _dic = {'1': register, '2': login}
    while 1:
        user_chioce = input("请输入功能序号:\n1 注册\n2 登陆")
        boom = _dic[user_chioce]()
        if boom:
            if not os.path.exists(os.path.join(FILE_PATH, dic_info['username'])):
                os.mkdir(os.path.join(FILE_PATH, dic_info['username']))
            src()
