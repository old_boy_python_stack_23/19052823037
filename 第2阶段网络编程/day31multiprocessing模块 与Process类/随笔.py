from multiprocessing import Process
import time


def task(name):
    print(f'{name}is running')
    time.sleep(3)
    print(f"{name} is done")


if __name__ == '__main__':
    while 1:
        p = Process(target=task, args=('怼哥',))
        p.start()
        print("主进程")
