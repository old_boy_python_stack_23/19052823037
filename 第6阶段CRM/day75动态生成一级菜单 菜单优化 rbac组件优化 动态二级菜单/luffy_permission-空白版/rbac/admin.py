from django.contrib import admin
from rbac import models


# Register your models here.

class PermissionAdmin(admin.ModelAdmin):
    # 展示列表
    list_display = ['id', 'title', 'url']
    # 可编辑列表
    list_editable = ['title', 'url']


admin.site.register(models.Menu)
admin.site.register(models.User)
admin.site.register(models.Role)
admin.site.register(models.Permission, PermissionAdmin)
