from django import template
from django.conf import settings

register = template.Library()


@register.inclusion_tag('rbac/menu.html')
def menu(request):
    # url = request.path_info
    menu_dict = request.session.get(settings.MENU_SESSION_KEY)

    return {'menu_dict': menu_dict.values()}
