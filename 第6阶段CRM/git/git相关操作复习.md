在码云（或者是GitHub）上创建仓库，然后

Git 全局设置:

```
git config --global user.name "杨森"
git config --global user.email "893534362@qq.com"
```

创建 git 仓库:

```
mkdir gityanshicangku
cd gityanshicangku
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/Jacobyangsen/gityanshicangku.git
git push -u origin master
```

已有仓库?

```
cd existing_git_repo
git remote add origin https://gitee.com/Jacobyangsen/gityanshicangku.git
git push -u origin master
```