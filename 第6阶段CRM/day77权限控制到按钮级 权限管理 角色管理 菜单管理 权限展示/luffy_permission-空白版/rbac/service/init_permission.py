from django.conf import settings


def init_permissions(user_obj, request):
    # 查询当前用户的权限信息
    permissions = user_obj.roles.filter(permissions__url__isnull=False).values(
        'permissions__url',
        'permissions__title',
        'permissions__menu__title',
        'permissions__menu__icon',
        'permissions__menu__id',
    ).distinct()

    # 权限列表
    permission_list = []

    # 菜单的字典
    menu_dict = {}

    for i in permissions:
        permission_list.append({'url': i['permissions__url']})
        menu_id = i.get('permission__menu_id')
        if not menu_id:
            continue

        menu_dict.setdefault(menu_id, {
            'title': i['permissions__menu__title'],
            'icon': i['permissions__menu__icon'],
            'children': [],
        })
        menu_dict[menu_id]['children'].append({'title': i['permissions__title'], 'url': i['permissions__url']})
    # # 菜单列表
    # menu_list = []

    """for i in permissions:
        permission_list.append({'url': i['permissions__url']})
        if i.get('permissions__is_menu'):
            menu_list.append({
                'title': i['permissions__title'],
                'icon': i['permissions__icon'],
                'url': i['permissions__url'],
            })"""

    # 保存权限信息到session中
    request.session[settings.PERMISSION_SESSION_KEY] = permission_list

    # 保存菜单的信息
    request.session[settings.MENU_SESSION_KEY] = menu_dict
