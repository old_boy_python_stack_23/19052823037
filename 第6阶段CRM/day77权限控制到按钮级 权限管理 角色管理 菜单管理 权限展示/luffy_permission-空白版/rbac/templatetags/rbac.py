from django import template
from django.conf import settings

register = template.Library()


@register.inclusion_tag('rbac/menu.html')
def menu(request):
    url = request.path_info
    menu_list = request.session.get(settings.MENU_SESSION_KEY)
    for menu in menu_list:
        if url == menu['url']:
            menu['class'] = 'active'
            break
    return {'menu_list': request.session.get(settings.MENU_SESSION_KEY), 'request': request}
