data = [
    {
        'permissions__url': '/customer/list/',
        'permissions__title': '客户列表',
        'permissions__menu__title': '客户管理',
        'permissions__menu__icon': 'fa-clipboard',
        'permissions__menu_id': 1
    }, {
        'permissions__url': '/customer/add/',
        'permissions__title': '添加客户',
        'permissions__menu__title': None,
        'permissions__menu__icon': None,
        'permissions__menu_id': None
    }, {
        'permissions__url': '/customer/edit/(?P<cid>\\d+)/',
        'permissions__title': '编辑客户',
        'permissions__menu__title': None,
        'permissions__menu__icon': None,
        'permissions__menu_id': None
    }, {
        'permissions__url': '/customer/del/(?P<cid>\\d+)/',
        'permissions__title': '删除客户',
        'permissions__menu__title': None,
        'permissions__menu__icon': None,
        'permissions__menu_id': None
    }, {
        'permissions__url': '/payment/list/',
        'permissions__title': '缴费列表',
        'permissions__menu__title': '财务管理',
        'permissions__menu__icon': 'fa-coffee',
        'permissions__menu_id': 2
    }, {
        'permissions__url': '/order/list/',
        'permissions__title': '订单列表',
        'permissions__menu__title': '财务管理',
        'permissions__menu__icon': 'fa-coffee',
        'permissions__menu_id': 2
    }, {
        'permissions__url': '/payment/add/',
        'permissions__title': '添加缴费',
        'permissions__menu__title': None,
        'permissions__menu__icon': None,
        'permissions__menu_id': None
    }, {
        'permissions__url': '/payment/edit/(?P<pid>\\d+)/',
        'permissions__title': '编辑缴费',
        'permissions__menu__title': None,
        'permissions__menu__icon': None,
        'permissions__menu_id': None
    }, {
        'permissions__url': '/payment/del/(?P<pid>\\d+)/',
        'permissions__title': '删除缴费',
        'permissions__menu__title': None,
        'permissions__menu__icon': None,
        'permissions__menu_id': None
    }]

"""

menu_dict = {

    1:{
        'title':'客户管理',
        'icon':'fa-clipboard',
        'children': [
            { 'title' 'url' }
        ]
    },
    2:{
        'title':'财务管理',
        'icon':'fa-coffee',
         'class':'hidden'
        'children': [
            { 'title' 'url' }，
            { 'title' 'url' }
        ]
    }

}

"""
menu_dict = {}

for i in data:
    menu_id = i.get('permissions__menu_id')
    if not menu_id:
        continue
    # if menu_id not in menu_dict:
    #     menu_dict[menu_id] = {
    #         'title': i['permissions__menu__title'],
    #         'icon': i['permissions__menu__icon'],
    #         'children': [
    #             {'title': i['permissions__title'], 'url': i['permissions__url']}
    #         ]
    #     }
    # else:
    #     menu_dict[menu_id]['children'].append({'title': i['permissions__title'], 'url': i['permissions__url']})

    menu_dict.setdefault(menu_id, {
        'title': i['permissions__menu__title'],
        'icon': i['permissions__menu__icon'],
        'children': [

        ]
    })
    menu_dict[menu_id]['children'].append({'title': i['permissions__title'], 'url': i['permissions__url']})

print(menu_dict)
