from collections import OrderedDict

# od = OrderedDict()
# od[3] = [3]
# od[1] = [1]
# od[2] = [3]
#
# print(od)


dic = {
    '1': {
        'title': '客户管理',
        'icon': 'fa-clipboard',
        'weight': 1,
        'children': [{
            'title': '客户列表',
            'url': '/customer/list/'
        }]
    },
    '2': {
        'title': '财务管理',
        'icon': 'fa-coffee',
        'weight': 10,
        'children': [{
            'title': '缴费列表',
            'url': '/payment/list/'
        }]
    }
}

ret = sorted(dic,key=lambda x:dic[x]['weight'],reverse=True)
print(ret)

ret = sorted(dic.values(), key=lambda x: x['weight'], reverse=True)
print(ret)
