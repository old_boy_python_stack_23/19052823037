from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import reverse, HttpResponse, redirect
from django.conf import settings
import re


class RbacMiddleWare(MiddlewareMixin):

    def process_request(self, request):
        # 获取当前访问的地址
        url = request.path_info
        # 白名单
        for i in settings.WHITE_LIST:
            if re.match(i, url):
                return

        # 登陆状态的校验
        is_login = request.session.get('is_login')
        if not is_login:
            return redirect('login')
        # 免认证的校验
        for i in settings.PASS_AUTH_LIST:
            if re.match(i, url):
                return

        # 获取权限信息
        permissions = request.session.get('permission')
        # 权限的校验
        for i in permissions:
            if re.match(r'^{}$'.format(i['permissions__url']), url):
                return
        # 拒绝请求
        return HttpResponse('没有访问权限，请联系管理员')
