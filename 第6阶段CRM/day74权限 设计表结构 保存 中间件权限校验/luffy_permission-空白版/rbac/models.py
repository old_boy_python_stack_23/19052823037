from django.db import models


# 权限
class Permission(models.Model):
    url = models.CharField(verbose_name='权限', max_length=108)
    title = models.CharField(verbose_name='标题', max_length=32)

    def __str__(self):
        return self.title


# 角色
class Role(models.Model):
    name = models.CharField('角色名称', max_length=32)
    permissions = models.ManyToManyField('Permission', verbose_name='角色所拥有的权限', blank=True)

    def __str__(self):
        return self.name


# 用户
class User(models.Model):
    username = models.CharField('用户名', max_length=32)
    password = models.CharField('密码', max_length=32)
    roles = models.ManyToManyField('Role', verbose_name='用户所拥有的角色', blank=True)

    def __str__(self):
        return self.username
