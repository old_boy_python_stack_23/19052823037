from django.shortcuts import render
from app01 import models
from django.views import View
import time


# Create your views here.
class Register(View):
    def get(self, request):
        return render(request, 'register.html', )

    def post(self, request, *args, **kwargs):
        name = request.POST.get('username')
        password = request.POST.get('password')

        error = ""
        if name == 'alex' and password == 'alexdsb':
            time.sleep(1)
            return render(request, 'login.html')
        elif not name and not password:
            error = '请输入用户名密码'
            return render(request, 'register.html', {'error': error})
        else:
            time.sleep(1)
            error = '用户名或密码格式不对'
            return render(request, 'register.html', {'error': error})
