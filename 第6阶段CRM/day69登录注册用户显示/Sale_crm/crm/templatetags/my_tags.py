from django import template
from django.urls import reverse
from django.http.request import QueryDict

register = template.Library()


@register.simple_tag
def url_tag(request, name, *args, **kwargs):
    url = reverse(name, args=args, kwargs=kwargs)
    next = request.get_full_path()
    qd = QueryDict(mutable=True)
    qd['next'] = next
    return "{}?{}".format(url, qd.urlencode())
