from django.conf.urls import url
from crm import views

urlpatterns = [
    url(r'login/', views.login, name='login'),
    url(r'reg/', views.reg, name='reg'),
    # 公户
    url(r'^customer_list/', views.CustomerList.as_view(), name='customer_list'),
    # 私户
    url(r'^my_customer/', views.CustomerList.as_view(), name='my_customer'),
    # 增加客户
    url(r'^customer_add/', views.customer_change, name='customer_add'),
    # 编辑客户
    url(r'^customer_edit/(\d+)/', views.customer_change, name='customer_edit'),



    # 展示当前用户的所有的跟进记录
    url(r'^consult_record_list/', views.ConsultRecordList.as_view(), name='consult_record_list'),
    # 展示当前用户的所有的跟进记录
    url(r'^one_customer_record_list/(?P<customer_id>\d+)', views.ConsultRecordList.as_view(),
        name='one_customer_record_list'),
    # ?P<customer_id>\d+ customer_id
    # 增加跟进记录
    url(r'^consult_record_add/', views.consult_record_change, name='consult_record_add'),
    # 编辑跟进记录
    url(r"^consult_record_edit/(\d+)/", views.consult_record_change, name='consult_record_edit'),

    # # 展示当前用户的所有的报名表
    url(r'enrollment_list/$', views.EnrollmentList.as_view(), name='enrollment_list'),
    #
    # # 展示当前用户的所有的跟进记录
    url(r'^one_enrollment_list/(?P<customer_id>\d+)/', views.EnrollmentList.as_view(),
        name='one_enrollment_list'),
    #
    # # 增加报名表
    url(r'^enrollment_add/(?P<customer_id>\d+)/', views.enrollment_change, name='enrollment_add'),
    # # 编辑报名表
    url(r'^enrollment_edit/(?P<pk>\d+)/', views.enrollment_change, name='enrollment_edit'),
]
