from django.views import View
from django.db.models import Q
from django.shortcuts import render, HttpResponse, redirect, reverse
from crm import models
import hashlib
from crm.forms import RegForm, CustomerForm, ConsultRecordForm, EnrollmentForm
from utils.pagination import Pagination


# 登陆
def login(request):
    if request.method == 'POST':

        user = request.POST.get('username')
        pwd = request.POST.get('password')
        md5 = hashlib.md5()
        md5.update(pwd.encode('utf-8'))
        pwd = md5.hexdigest()

        obj = models.UserProfile.objects.filter(username=user, password=pwd, is_active=True).first()
        if obj:
            # session保存用户的id 登陆状态
            request.session['user_id'] = obj.pk
            request.session['is_login'] = '1'

            return redirect('crm:customer_list')
        return render(request, 'login.html', {'error': '用户名或密码错误'})

    return render(request, 'login.html')


# 注册
def reg(request):
    form_obj = RegForm()
    if request.method == 'POST':
        form_obj = RegForm(request.POST)
        if form_obj.is_valid():
            form_obj.save()  # 新增
            return redirect('crm:login')
    return render(request, 'reg.html', {'form_obj': form_obj})


# 查询所有的客户CBV
class CustomerList(View):
    def get(self, request):
        # 查询字段设置q
        q = self.search(field_list=['name', 'phone'])
        if request.path_info == reverse('crm:customer_list'):
            all_customers = models.Customer.objects.filter(q, consultant__isnull=True)
        else:
            all_customers = models.Customer.objects.filter(q, consultant=request.user_obj)
        page_obj = Pagination(request.GET.get('page', 1), all_customers.count(), request.GET.copy())

        return render(request, 'customer_list.html',
                      {'all_customers': all_customers[page_obj.start:page_obj.end], 'page_html': page_obj.page_html})

    def post(self, request):
        action = request.POST.get('action')
        if not hasattr(self, action):
            return HttpResponse('非法操作')
        getattr(self, action)()
        return self.get(request)

    def multi_apply(self):
        # 公户变私户
        pk_list = self.request.POST.getlist("pk")
        models.Customer.objects.filter(pk__in=pk_list).update(consultant_id=self.request.session.get('user_id'))

    def multi_pub(self):
        # 私户变公户
        pk_list = self.request.POST.getlist('pk')
        # 方式一
        models.Customer.objects.filter(pk__in=pk_list).update(consultant=None)
        # 方拾二
        # self.request.user_obj.customer.remove(*models.Customer.objects.filter(pk__in=pk_list))

    def search(self, field_list):
        query = self.request.GET.get('query', '')
        q = Q()
        q.connector = "OR"
        q.children.append(Q(qq__contains=query))
        # q.children.append(Q(('name__contains', query)))
        for field in field_list:
            q.children.append(Q(('{}__contains'.format(field), query)))
        return q


# 新增和编辑 二合一
def customer_change(request, pk=None):
    obj = models.Customer.objects.filter(pk=pk).first()
    form_obj = CustomerForm(instance=obj)
    if request.method == 'POST':
        form_obj = CustomerForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()  # 新增 和 编辑
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('crm:customer_list')

    title = '编辑客户' if pk else '新增客户'
    return render(request, 'customer_change.html', {'form_obj': form_obj, 'title': title})


class ShowList(View):
    def search(self, field_list):
        query = self.request.GET.get('query', '')
        q = Q()
        q.connector = 'OR'
        for field in field_list:
            q.children.append(Q(('{}__contains'.format(field), query)))
        return q

    def post(self, request):
        # 找操作的名字
        action = request.POST.get('action')
        # 反射获取方法
        if not hasattr(self, action):
            return HttpResponse('非法操作')

        getattr(self, action)()  # 获取到对应的方法并执行

        # 返回值
        return self.get(request)


# 展示当前用户的所有的跟进记录
class ConsultRecordList(ShowList):

    def get(self, request, customer_id=None, *args, **kwargs):
        q = self.search(['note'])

        if customer_id:
            all_consult_record = models.ConsultRecord.objects.filter(q, customer_id=customer_id,
                                                                     customer__consultant=request.user_obj,
                                                                     delete_status=False)

        else:
            all_consult_record = models.ConsultRecord.objects.filter(q, consultant=request.user_obj,
                                                                     delete_status=False)

        page = Pagination(request.GET.get('page', 1), all_consult_record.count(), request.GET.copy(), 2)

        return render(request, 'consult_record_list.html',
                      {'all_consult_record': all_consult_record.order_by('-date')[page.start:page.end],
                       'page_html': page.page_html,
                       'url': reverse('crm:customer_list')})


# 新增跟进记录
def consult_record_add(request):
    form_obj = ConsultRecordForm(request)
    if request.method == 'POST':
        form_obj = ConsultRecordForm(request.POST)
        if form_obj.is_valid():
            form_obj.save()
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('crm:my_customer')

    return render(request, 'form.html', {'form_obj': form_obj, 'title': '新增跟进记录'})


# 编辑跟进记录
def consult_record_edit(request, pk):
    obj = models.ConsultRecord.objects.filter(pk=pk).first()
    form_obj = ConsultRecordForm(instance=obj)
    if request.method == 'POST':
        form_obj = ConsultRecordForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('crm:my_customer')

    return render(request, 'form.html', {'form_obj': form_obj, 'title': '新增跟进记录'})


# 新增和编辑跟进记录
def consult_record_change(request, pk=None):
    obj = models.ConsultRecord.objects.filter(pk=pk).first()
    form_obj = ConsultRecordForm(request, instance=obj)
    if request.method == 'POST':
        form_obj = ConsultRecordForm(request, request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('crm:my_customer')

    return render(request, 'form.html', {'form_obj': form_obj, 'title': '新增跟进记录'})


# 报名表
class EnrollmentList(ShowList):

    def get(self, request, customer_id=None, *args, **kwargs):

        if customer_id:
            all_enrollment = models.Enrollment.objects.filter(customer_id=customer_id)
        else:
            all_enrollment = models.Enrollment.objects.filter(customer__consultant=request.user_obj)

        page = Pagination(request.GET.get('page', 1), all_enrollment.count(), request.GET.copy(), 2)

        return render(request, 'enrollment_list.html',
                      {'all_enrollment': all_enrollment.order_by('-enrolled_date')[page.start:page.end],
                       'page_html': page.page_html,
                       })


# def enrollment_change(request, customer_id=None, pk=None):
#     obj = models.Enrollment.objects.filter(pk=pk).first()
#
#     form_obj = EnrollmentForm(customer_id, instance=obj)
#     if request.method == 'POST':
#         form_obj = EnrollmentForm(customer_id, request.POST, instance=obj)
#
#         if form_obj.is_valid():
#             form_obj.save()
#             next = request.GET.get('next')
#             if next:
#                 return redirect(next)
#             return redirect('crm:enrollment_list')
#
#     title = '编辑报名表' if pk else '新增报名表'
#     return render(request, 'form.html', {'form_obj': form_obj, 'title': title})


def enrollment_change(request, customer_id=None, pk=None):
    obj = models.Enrollment(customer_id=customer_id) if customer_id else models.Enrollment.objects.filter(pk=pk).first()

    form_obj = EnrollmentForm(instance=obj)
    if request.method == 'POST':
        form_obj = EnrollmentForm(request.POST, instance=obj)

        if form_obj.is_valid():
            form_obj.save()
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('crm:enrollment_list')

    title = '编辑报名表' if pk else '新增报名表'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})
