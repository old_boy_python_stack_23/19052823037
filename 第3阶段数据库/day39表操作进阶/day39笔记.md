`## 约束

**show databases; 查看一共有多少库**

**select database(); 查看当前所在的库**

**unique 能不能重复插入多个null -- 可以**

**对于mysql来说 数据与数据之间相等就是重复null 不能用 = 判断**

- unsigned 数字

- not null 非空约束
      

- default  设置默认值
      

- unique
          唯一
          联合唯一
      

  ```
  create table t2(
      id int primary key,
      servername char(12) not null,
      ip char(15),
      port int,
      unique(ip,port)
  )
  ```

  

- auto_increment
          自增 针对int
          自带 not null
          前提 : 需要设置unique
      不受删除影响

  ```
  create table t3(
      id int unique auto_increment,
      name char(12) not null
  )
  ```

  

- primary key
          相当于 非空 + 唯一
   即not null  + unique      一张表只能有一个,并且必须有一个
          联合主键
   

  ```
  方法一:
  create table t5(
      family_name char(4),
      name char(12),
      primary key(family_name,name)
  )
  方法二:
  create table t5(		
      family_name char(4) not null,
      name char(12) not null,
      unique(family_name,name)
  )
  ```

  

- foreign key
        外键约束
        约束的字段至少unique
      级联删除 on delete cascade
        级联更新 on update cascade

  推荐使用主键作为关联字段

  ```
  create table class3(
    id int primary ,
    cname char(12) not null unique,
    start_date date,
    period char(12),
    course char(12),
    teacher char(12)
  );
  create table student3(
    id int primary key auto_increment,
    name char(12) not null,
    gender enum('male','female'),
    cid int,
    foreign key(cid) references class3(id) on delete cascade on update cascade
  );
  ```

数据的插入操作

```
insert into class3 select * from class;插入已有库的内容
insert into class3(id,name) select id,cname from class;
```



## 表与表关系

- 校区表 班级表  一对多
          

  ​		校区表  一个校区可以有多个班级      一对多
  ​        

  ​		班级表  一个班级可不可以对应多个校区
  ​        

  ​		校区表 校区id 校区名称 校区城市 校区地址
  ​        

  ​		班级表 班级id 班级名称 开班日期 班主任  校区id
  ​        

  ​		多(foreign key)关联一这张表
  ​        

  ​		班级表创建foreign key关联校区表的校区id字段
  ​    

- 学生表  班级表 多对多
          

  ​		站在学生的角度上 一个学生属于多个班级      一对多
  ​        

  ​		站在班级的角度上 一个班级可以有多个学生么  多对一
  ​        

  ​		学生表 学生id 学生姓名 ...
  ​        

  ​		班级表 班级id 班级名称 ...
  ​        

  ​		产生第三张表
  ​            

  ​				一个字段是外键关联另一个表的主键
  ​            

  ​				另一个字段外键关联另一张表的主键
  ​    

- 学生表  客户表 一对一
          

  ​		一个客户对应一个学生
  ​        

  ​		学生表gid foreign key 关联客户表id主键
  ​        

  ​		并且gid还要设置为unique

## 单表查询

- select 某一个东西
          

  ​		可以查一个,多个,*所有
  ​        

  ​		调用函数 : now() user() database() concat() concat_ws()
  ​        

  ​		进行四则运算
   select emp_name,salary*12 from 表

  ​		可以去重 distinct
  ​    select distinct 字段 from 表    

  ​		可以进行条件判断 case when语句
  
  ```
  select(
      case
      when emp_name = 'alex' then
          concat(emp_name,'BIGSB')
      when emp_name = 'jingliyang' then
          emp_name
      else
          concat(emp_name,'sb')
      end
      ) as new_name
  ```
  
  

- where 筛选

  ```
  select * from 表 where 条件
      范围查询
          > < >= <= = !=/<>
          between a and b
          in (1,2,3,4)   n选1
      模糊查询
          like
              % 一个百分号代表任意长度的任意字符
                  'a%'
                  '%ing'
                  '%a%'
              _ 一个下划线代表一个任意字符
                  'a__'
          regexp
              '^a'
              '\d+'
      is is not
          is null
          is not null
      逻辑运算
          and
          or
          not
  ```