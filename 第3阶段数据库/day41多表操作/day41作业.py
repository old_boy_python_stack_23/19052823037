



# 1、查询男生、女生的人数；
# select gender,count(gender) from student group by gender;
# +--------+---------------+
# | gender | count(gender) |
# +--------+---------------+
# | 女     |             6 |
# | 男     |            10 |
# +--------+---------------+
# 2 rows in set (0.01 sec)
# 2、查询姓“张”的学生名单；
# select * from student where sname like '张%';
# +-----+--------+----------+--------+
# | sid | gender | class_id | sname  |
# +-----+--------+----------+--------+
# |   3 | 男     |        1 | 张三   |
# |   4 | 男     |        1 | 张一   |
# |   5 | 女     |        1 | 张二   |
# |   6 | 男     |        1 | 张四   |
# +-----+--------+----------+--------+
# 3、课程平均分从高到低显示
# mysql> select * from score group by course_id order by num;
# +-----+------------+-----------+-----+
# | sid | student_id | course_id | num |
# +-----+------------+-----------+-----+
# |   2 |          1 |         2 |   9 |
# |   1 |          1 |         1 |  10 |
# |   5 |          1 |         4 |  66 |
# |   8 |          2 |         3 |  68 |
# +-----+------------+-----------+-----+
# 4、查询有课程成绩小于60分的同学的学号、姓名；
# mysql> select sid,sname from student where sid in (select student_id from score where num < 60);
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |   1 | 理解   |
# |   2 | 钢蛋   |
# |   4 | 张一   |
# |   5 | 张二   |
# |   6 | 张四   |
# |   7 | 铁锤   |
# |   8 | 李三   |
# |   9 | 李一   |
# |  10 | 李二   |
# |  11 | 李四   |
# |  12 | 如花   |
# +-----+--------+
# 11 rows in set (0.00 sec)

# 5、查询至少有一门课与学号为1的同学所学课程相同的同学的学号和姓名；
# select student.sid,sname from student right join (select * from score where student_id = 1)as o on course_id = o.course_id group by sname;
# +------+--------+
# | sid  | sname  |
# +------+--------+
# |   14 | 刘一   |
# |   13 | 刘三   |
# |   15 | 刘二   |
# |   16 | 刘四   |
# |   12 | 如花   |
# |    4 | 张一   |
# |    3 | 张三   |
# |    5 | 张二   |
# |    6 | 张四   |
# |    9 | 李一   |
# |    8 | 李三   |
# |   10 | 李二   |
# |   11 | 李四   |
# |    1 | 理解   |
# |    2 | 钢蛋   |
# |    7 | 铁锤   |
# +------+--------+
# 16 rows in set (0.01 sec)
# 6、查询出只选修了一门课程的全部学生的学号和姓名；
# select student_id,sname from student right join (select student_id from score as s right join (select course_id from score where student_id = 1) as o on s.course_id = o.course_id group by student_id ) as p on sid = p.student_id;
# +------------+--------+
# | student_id | sname  |
# +------------+--------+
# |          1 | 理解   |
# |          2 | 钢蛋   |
# |          3 | 张三   |
# |          4 | 张一   |
# |          5 | 张二   |
# |          6 | 张四   |
# |          7 | 铁锤   |
# |          8 | 李三   |
# |          9 | 李一   |
# |         10 | 李二   |
# |         11 | 李四   |
# |         12 | 如花   |
# +------------+--------+
# 12 rows in set (0.00 sec)
# 7、查询各科成绩最高和最低的分：以如下形式显示：课程ID，最高分，最低分；
# mysql> select course_id,max(num),min(num) from score group by course_id;
# +-----------+----------+----------+
# | course_id | max(num) | min(num) |
# +-----------+----------+----------+
# |         1 |       91 |        8 |
# |         2 |      100 |        9 |
# |         3 |       87 |       43 |
# |         4 |      100 |       22 |
# +-----------+----------+----------+
# 4 rows in set (0.00 sec)

# 8、查询课程编号“2”的成绩比课程编号“1”课程低的所有同学的学号、姓名；
# select sid,sname from student  inner join (select q.student_id from (select * from score where course_id = 1) as q inner join (select * from score where course_id = 2) as p on p.student_id = q.student_id where q.num > p.num) as m on sid = student_id;
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |   1 | 理解   |
# |   3 | 张三   |
# |   4 | 张一   |
# |   5 | 张二   |
# |   9 | 李一   |
# |  10 | 李二   |
# |  11 | 李四   |
# |  12 | 如花   |
# +-----+--------+
# 8 rows in set (0.00 sec)
# 9、查询“生物”课程比“物理”课程成绩高的所有学生的学号；
# select q.student_id from (select * from score inner join course on cid = course_id where cname = '生物') as q inner join (select * from score inner join course on cid = course_id where cname = '物理') as p on q.student_id = p.student_id where q.num > p.num;
# +------------+
# | student_id |
# +------------+
# |          1 |
# |          3 |
# |          4 |
# |          5 |
# |          9 |
# |         10 |
# |         11 |
# |         12 |
# +------------+
# 8 rows in set (0.00 sec)
# 10、查询平均成绩大于60分的同学的学号和平均成绩;
#  select student_id,avg(num) from score group by student_id having avg(num)>60;
# +------------+----------+
# | student_id | avg(num) |
# +------------+----------+
# |          3 |  82.2500 |
# |          4 |  64.2500 |
# |          5 |  64.2500 |
# |          6 |  69.0000 |
# |          7 |  66.0000 |
# |          8 |  66.0000 |
# |          9 |  67.0000 |
# |         10 |  74.2500 |
# |         11 |  74.2500 |
# |         12 |  74.2500 |
# |         13 |  87.0000 |
# +------------+----------+
# 11 rows in set (0.01 sec)

# 11、查询所有同学的学号、姓名、选课数、总成绩；
# select student_id,sname,count(course_id),sum(num) from student inner join score on student.sid = student_id group by sname;
# +------------+--------+------------------+----------+
# | student_id | sname  | count(course_id) | sum(num) |
# +------------+--------+------------------+----------+
# |         13 | 刘三   |                1 |       87 |
# |         12 | 如花   |                4 |      297 |
# |          4 | 张一   |                4 |      257 |
# |          3 | 张三   |                4 |      329 |
# |          5 | 张二   |                4 |      257 |
# |          6 | 张四   |                4 |      276 |
# |          9 | 李一   |                4 |      268 |
# |          8 | 李三   |                4 |      264 |
# |         10 | 李二   |                4 |      297 |
# |         11 | 李四   |                4 |      297 |
# |          1 | 理解   |                3 |       85 |
# |          2 | 钢蛋   |                3 |      175 |
# |          7 | 铁锤   |                4 |      264 |
# +------------+--------+------------------+----------+
# 12、查询姓“李”的老师的个数；
# mysql> select count(tname) from teacher where  tname like "李%";
# +--------------+
# | count(tname) |
# +--------------+
# |            2 |
# +--------------+
# 1 row in set (0.00 sec)
# 13、查询没学过“张磊老师”课的同学的学号、姓名；
# select student_id,sname from (select student_id,sname,course_id from student inner join score on student.sid = student_id) as q inner join (select tid from teacher where  tname != "张磊老师") as p on course_id = tid group by sname;
# +------------+--------+
# | student_id | sname  |
# +------------+--------+
# |         13 | 刘三   |
# |         12 | 如花   |
# |          4 | 张一   |
# |          3 | 张三   |
# |          5 | 张二   |
# |          6 | 张四   |
# |          9 | 李一   |
# |          8 | 李三   |
# |         10 | 李二   |
# |         11 | 李四   |
# |          1 | 理解   |
# |          2 | 钢蛋   |
# |          7 | 铁锤   |
# +------------+--------+
# 13 rows in set (0.00 sec)
# 14、查询学过“1”并且也学过编号“2”课程的同学的学号、姓名；
# select sid,sname from student inner join (select q.student_id  from (select * from score where course_id = 1) as q inner join (select * from score where course_id = 2) as p on q.student_id = p.student_id) as w on sid = student_id;
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |   1 | 理解   |
# |   3 | 张三   |
# |   4 | 张一   |
# |   5 | 张二   |
# |   6 | 张四   |
# |   7 | 铁锤   |
# |   8 | 李三   |
# |   9 | 李一   |
# |  10 | 李二   |
# |  11 | 李四   |
# |  12 | 如花   |
# +-----+--------+
# 11 rows in set (0.00 sec)
# 15、查询学过“李平老师”所教的所有课的同学的学号、姓名；
# select sid,sname from student inner join (select student_id from score inner join (select cid from course inner join (select tid from teacher where tname = '李平老师') as q on teacher_id = tid) as w on course_id = cid group by student_id) as e on sid = student_id;
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |   1 | 理解   |
# |   2 | 钢蛋   |
# |   3 | 张三   |
# |   4 | 张一   |
# |   5 | 张二   |
# |   6 | 张四   |
# |   7 | 铁锤   |
# |   8 | 李三   |
# |   9 | 李一   |
# |  10 | 李二   |
# |  11 | 李四   |
# |  12 | 如花   |
# +-----+--------+
# 12 rows in set (0.00 sec)








# 1、查询没有学全所有课的同学的学号、姓名；
# 2、查询和“002”号的同学学习的课程完全相同的其他同学学号和姓名；
# 3、删除学习“叶平”老师课的SC表记录；
# 4、向SC表中插入一些记录，这些记录要求符合以下条件：①没有上过编号“002”课程的同学学号；②插入“002”号课程的平均成绩；
# 5、按平均成绩从低到高显示所有学生的“语文”、“数学”、“英语”三门的课程成绩，按如下形式显示： 学生ID,语文,数学,英语,有效课程数,有效平均分；
# 6、查询各科成绩最高和最低的分：以如下形式显示：课程ID，最高分，最低分；
# 7、按各科平均成绩从低到高和及格率的百分数从高到低顺序；
# 8、查询各科成绩前三名的记录:(不考虑成绩并列情况)
# 9、查询每门课程被选修的学生数；
# 10、查询同名同姓学生名单，并统计同名人数；
# 11、查询每门课程的平均成绩，结果按平均成绩升序排列，平均成绩相同时，按课程号降序排列；
# 12、查询平均成绩大于85的所有学生的学号、姓名和平均成绩；
# 13、查询课程名称为“数学”，且分数低于60的学生姓名和分数；
# 14、查询课程编号为003且课程成绩在80分以上的学生的学号和姓名；
# 15、求选了课程的学生人数
# 16、查询选修“杨艳”老师所授课程的学生中，成绩最高的学生姓名及其成绩；
# 17、查询各个课程及相应的选修人数；
# 18、查询不同课程但成绩相同的学生的学号、课程号、学生成绩；
# 19、查询每门课程成绩最好的前两名；
# 20、检索至少选修两门课程的学生学号；
# 21、查询全部学生都选修的课程的课程号和课程名；
# 22、查询没学过“叶平”老师讲授的任一门课程的学生姓名；
# 23、查询两门以上不及格课程的同学的学号及其平均成绩；
# 24、检索“004”课程分数小于60，按分数降序排列的同学学号；
# 25、删除“002”同学的“001”课程的成绩；
