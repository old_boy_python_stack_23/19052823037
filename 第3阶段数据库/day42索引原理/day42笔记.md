# 索引原理

## 1.b+树

数据只存储在叶子节点
在子节点之间加入了双向地址连接,更方便的在子节点之间进行数据的读取



## 2.存储引擎的索引原理

### 2.1innodb

聚集索引:只有存在于主键中(无需回表)
辅助索引:除了主键之外所有的索引都是辅助索引(需要回表)
回表: 只查询一个索引并不能解决查询中的问题,还需要到具体的表中去获取正行数据

聚集索引与辅助索引的区别:

叶子结点存放的是否是一整行的信息,聚集索引的叶子节点存放的是一整行信息,但是辅助索引不是

### 2.2myisam

与innodb相同

聚集索引:只有存在于主键中(无需回表)
辅助索引:除了主键之外所有的索引都是辅助索引(需要回表)

## 3.索引的种类

①priamry key 的创建自带索引效果---------非空 + 唯一 + 聚集索引
②unique 唯一约束的创建也自带索引效果-------唯一 + 辅助索引
③index  普通的索引--------------辅助索引

## 4.创建与删除索引

```mysql
# 创建索引
    # create index ind_name on 表(name);
    create index ind_name on t1(name);
# 删除索引
    # drop index ind_name on 表;
    drop index ind_name on t1;
```

## 5.索引的优缺点

优点 : 查找速度快
缺点 : 浪费空间,拖慢写的速度
注:不要在程序中创建无用的索引

## 6.索引无法命中的情况(重要***********)

1.所查询的列不是创建了索引的列

2.在条件中不能带运算或者函数,必须是"字段 = 值"

3.如果创建索引的列的内容重复率高也不能有效利用索引
    重复率不超过10%的列比较适合做索引

4.数据对应的范围如果太大的话,也不能有效利用索引
    between and, > , < , >=, <= , != , not in

5.like如果把%放在最前面也不能命中索引

6.多条件的情况
    ①and:只要有一个条件列是索引列就可以命中索引
    ②or:只有所有的条件列都是索引才能命中索引

7.联合索引
    在多个条件相连的情况下,使用联合索引的效率要高于使用单字段的索引
    where a=xx and b=xxx;
    对a和b都创建索引 - 联合索引

    ```mysql
create index ind_mix on s1(id,email)
    ```



    1.创建索引的顺序id,email 条件中从哪一个字段开始出现了范围,索引就失效了
    select * from s1 where id=1000000 and email like 'eva10000%'  命中索引
    select count(*) from s1 where id > 2000000 and email = 'eva2000000'  不能命中索引
    
    2.联合索引在使用的时候遵循最左前缀原则
    select count(*) from s1 where email = 'eva2000000@oldboy';
    
    3.联合索引中只有使用and能生效,使用or失效

**注:建表时,字段能够尽量的固定长度,就固定长度,而且变长的字段要尽量排在固定长度字段后面**



## 7.mysql 神器 explain

查看sql语句的执行计划,以及是否命中了索引,命中的索引的类型

例:

```mysql
explain select * from s1 where id < 1000000;
```



## 8.覆盖索引与索引合并

### 8.1覆盖索引

覆盖索引,即索引中包含所有需要查询的字段的值

using index 表示覆盖索引



### 8.2索引合并

索引合并,创建的时候是分开创建的,用的时候临时和在一起了
using union 表示索引合并

## 9.慢日志

慢日志能记录查询速度慢的sql语句

慢日志是通过配置文件开启

## 10.多表联查速度慢怎么办?

1.表结构
尽量用固定长度的数据类型代替可变长数据类型,把固定长度的字段放在前面

2.数据的角度上来说
如果表中的数据越多 查询效率越慢
列多 : 垂直分表
行多 : 水平分表

3.从sql的角度来说
①尽量把条件写的细致点儿,where条件多做筛选
②多表尽量连表代替子查询
③创建有效的索引,而规避无效的索引

4.配置角度上来说
开启慢日志查询 确认具体的有问题的sql

5.数据库
读写分离,解决数据库读的瓶颈

## 11.数据表\库的导入导出

```mysql
# 打开cmd输入一下命令
# 备份表 :homwork库中的所有表和数据
mysqldump -uroot -p123 homework > D:\s23\day42\a.sql
# 备份单表
mysqldump -uroot -p123 homework course > D:\s23\day42\a.sql
# 备份库 :
mysqldump -uroot -p123 --databases homework > D:\s23\day42\db.sql

# 恢复数据:
# 进入mysql 切换到要恢复数据的库下面
sourse D:\s23\day42\a.sql
```

## 12.开启事务,给数据加锁

```mysql
begin;
select id from t1 where name = 'alex' for update;
update t1 set id = 2 where name = 'alex';
commit;
```

## 13.sql注入

SQL注入攻击指的是通过构建特殊的输入作为参数传入Web应用程序，
而这些输入大都是SQL语法里的一些组合，
通过执行SQL语句进而执行攻击者所要的操作，
其主要原因是程序没有细致地过滤用户输入的数据，致使非法数据侵入系统。

```python
import pymysql

conn = pymysql.connect(host='127.0.0.1', user='eva', password="123",
                 database='db2')
cur = conn.cursor()
user = "'akhksh' or 1=1 ;-- "# 使用特殊语法使sql语句必查询成功
password = '*******'
sql = "select * from user where user_name = %s and password =%s;"% (user,password)
print(sql)
cur.execute(sql)
ret = cur.fetchone()
print(ret)
cur.close()
conn.close()
```