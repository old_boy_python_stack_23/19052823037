



# 表的后续操作



## 表的操作总结

```
表操作
        创建 crate table
        删除 drop table
        查看表结构 desc 表/show create table 表
        修改表
            id age name varchar(255)
            alter table 表名 rename 新表明
            alter table 表名 add 新字段 类型(宽度) 约束;
                            add 新字段 类型(宽度) 约束 after id
                            add 新字段 类型(宽度) 约束 first
            alter table 表名 drop 字段名;
            alter table 表名 change 旧字段 新字段 类型(宽度) 约束;
                            change name username char(12) not null
                            change name name char(12) not null
                            change name name varchar(255) after id;
            alter table 表名 modify 存在的字段 新类型(新宽度) 新约束;
                            modify name char(12) unique;
                            modify name char(12) unique after id;
    数据操作
        id name gender
        增加
            insert into 表名 value (1,'alex','female')
            insert into 表名 values (1,'alex','female'),(1,'alex','female');
            insert into 表名(name,gender) values ('alex','female');             *****
            insert into 表名(name,gender) select (username,sex) from day38.表2;
        删除
            delete from 表名 where 条件
        修改
            update 表名 set 字段=值1 where 条件
            update 表名 set 字段1=值1,字段2=值2 where 条件
        查
            单表查
```



## 单表查询

select  想要的列 from 表

where 先从这张表中查询的行

group by 分组

having 对组过滤

order by 排序

limit 取一个区间

顺序: 6



![1564632988352](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1564632988352.png)



```
select * from 表 group by 字段名
    分组 group by
    根据某个重复率比较高的字段进行的
    这个字段有多少种可能就分成多少个组
        根据性别分组 : 男的一组 女的一组
        根据部门分组 : 销售一组 教学一组 ...
    去重
    一旦分组了就不能对具体某一条数据进行操作了
        永远都是考虑这组xxx
    group_concat : 只用来做最终的显示,不能作为中间结果操作其他数据
聚合函数
    99.99%的情况都是和分组一起用的
    如果没有和分组一起用,默认一整张表是一组
    count(id)  / count(*) 计数 :每个组对应几条数据
    max 求最大值: 这个组中某字段的最大值
    min 求最大值: 这个组中某字段的最小值
    avg 求平均值
    sum 求和值

    select min(hire_date) from employee
        求employee中所有人里最早入职的时间
    select min(hire_date) from employee group by post
        求每个部门中入职最早的时间

练习
    求各部门薪资大于1w的人的个数
    select * from employee where salary >10000;
    select post,count(id) from employee where salary >10000 group by post;

having 过滤条件
    就是一个对组进行筛选的条件
    要部门人数大于3个人的部门 count(id)>3
    select post from employee group by post having count(id)>3;


    having的问题 不建议你用
        select id,emp_name,age from employee having age>20;
        select id,emp_name from employee group by age having age>20;

order by
    order by 字段
    order by 字段 asc
    order by 字段 desc

    order by 字段1,字段2
    order by 字段 asc,字段2 desc
    order by 字段 desc,字段2 asc
    order by 字段 desc,字段2 desc

limit
    1.显示分页
        limit m,n
            表示从m+1开始,取n条
            limit 0,6 表示从1开始取6条
            limit 6,6 表示从7开始取6条
            limit 12,6 表示从13开始取6条
            limit 18,6 表示从19开始取6条
    2.取前n名
        limit n   m默认为0
        跟order by一起用
    limit n offset m :从m+1开始,取n条


select 想要的列 from 表
where 先从这张表中查询的行
group by 分组
having 对组过滤
order by 排序
limit 取一个区间
```





## Python操作数据库

```
sql注入  # 还欠着没讲


查询
import pymysql
conn = pymysql.Connection(host='127.0.0.1', user='root', password="123",
                 database='day40')
cur = conn.cursor()  # 游标 数据库操作符
sql = 'select emp_name,salary from employee where age = %s'
cur.execute(sql,(80,))
# 获取结果
ret1 = cur.fetchone()
ret2 = cur.fetchmany(2)
ret3 = cur.fetchall()
print(ret1)
print(ret2)
print(ret3)
cur.close()
conn.close()

insert语句 写入
import pymysql
conn = pymysql.Connection(host='127.0.0.1', user='root', password="123",
                 database='day40')
cur = conn.cursor()  # 游标 数据库操作符
username = '太亮'
sql = 'insert into employee(id,emp_name,sex,age,hire_date) values(%s,%s,"female",88,20170707)'
cur.execute(sql,(21,username,))
conn.commit()
cur.close()
conn.close()


文件操作数据库
conn = pymysql.Connection(host='127.0.0.1', user='root', password="123",
               database='day40')
cur = conn.cursor()  # 游标 数据库操作符
cur.excute(sql,(值1,))
    查询
        fetchone fethmany(10) fetchaLL
    插入\更新\删除
        conn.commit()
cur.close()
conn.close()
```

