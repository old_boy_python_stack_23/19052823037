# 数据库初识

## 讨论

假设现在你已经是某大型互联网公司的高级程序员，让你写一个火车票购票系统，来hold住十一期间全国的购票需求，你怎么写？

　　由于在同一时段抢票的人数太多，所以你的程序不可能写在一台机器上，应该是多台机器一起分担用户的购票请求。

　　那么问题就来了，票务信息的数据存在哪里？存在文件里么？

　　如在哪一台机器上呢？是每台机器上都存储一份么？

　　首先，如果其中一台机器上卖出的票另外两台机器是感知不到的，

　　其次，是如果我们将数据和程序放在同一个机器上，如果程序和数据有一个出了问题都会导致整个服务不可用

　　最后，是操作文件，修改文件对python代码来说是一件很麻烦的事

　　基于上面这些问题，单纯的将数据存储在和程序同一台机器上的文件中是非常不明智的。

## 为什么要用数据库

第一，将文件和程序存在一台机器上是很不合理的。

第二，操作文件是一件很麻烦的事

## 数据库

概念:

你可以理解为 数据库 是一个可以在一台机器上独立工作的，并且可以给我们提供高效、便捷的方式对数据进行增删改查的一种工具。

　　如此就帮助我们解决了上面出现的问题，如果将所有的数据都存储在一个独立的机器上，而对用户提供服务的机器只是存放你写的代码。

![img](https://img2018.cnblogs.com/blog/827651/201809/827651-20180919190259670-821942916.png)

数据库的优势:

1. 程序稳定性 ：这样任意一台服务所在的机器崩溃了都不会影响数据和另外的服务。

2. 数据一致性 ：所有的数据都存储在一起，所有的程序操作的数据都是统一的，就不会出现数据不一致的现象

3. 并发 ：数据库可以良好的支持并发，所有的程序操作数据库都是通过网络，而数据库本身支持并发的网络操作，不需要我们自己写socket

4. 效率 ：使用数据库对数据进行增删改查的效率要高出我们自己处理文件很多

### 什么是数据（Data）

*描述事物的符号记录称为数据，描述事物的符号既可以是数字，也可以是文字、图片，图像、声音、语言等，数据由多种表现形式，它们都可以经过数字化后存入计算机*

*在计算机中描述一个事物，就需要抽取这一事物的典型特征，组成一条记录，就相当于文件里的一行内容，如：*

```
1 alex,不详,83,1935,山东,oldboy
```

*单纯的一条记录并没有任何意义，如果我们按逗号作为分隔，依次定义各个字段的意思，相当于定义表的标题*

```
id,name,sex,age,birth,born_addr,company # 字段/列名
1,alex,不详,83,1935,山东,oldboy # 数据
```

### **什么是数据库（DataBase，简称DB）**

*数据库即存放数据的仓库，只不过这个仓库是在计算机存储设备上，而且数据是按一定的格式存放的*

*过去人们将数据存放在文件柜里，现在数据量庞大，已经不再适用*

*数据库是长期存放在计算机内、有组织、可共享的数据集合。*

*数据库中的数据按一定的数据模型组织、描述和储存，具有较小的冗余度、较高的数据独立性和易扩展性，并可为各种 用户共享*

### **什么是数据库管理系统（DataBase Management System 简称DBMS）**

*在了解了Data与DB的概念后，如何科学地组织和存储数据，如何高效获取和维护数据成了关键*

*这就用到了一个系统软件---数据库管理系统*

*如MySQL、Oracle、SQLite、Access、MS SQL Server*

*mysql主要用于大型门户，例如搜狗、新浪等，它主要的优势就是开放源代码，因为开放源代码这个数据库是免费的，他现在是甲骨文公司的产品。*
*oracle主要用于银行、铁路、飞机场等。该数据库功能强大，软件费用高。也是甲骨文公司的产品。*
*sql server是微软公司的产品，主要应用于大中型企业，如联想、方正等。*

*数据库管理员 DBA（Database Administrator）*

### **数据库服务器、数据管理系统、数据库、表与记录的关系（重点）**

**记录：1 朱葛 13234567890 22（多个字段的信息组成一条记录，即文件中的一行内容）**

**表：userinfo,studentinfo,courseinfo（即文件）**

**数据库：db（即文件夹）**

**数据库管理系统：如mysql（是一个软件）**

**数据库服务器：一台计算机（对内存要求比较高）**

**总结：**

​    **数据库服务器-：运行数据库管理软件**

​    **数据库管理软件：管理-数据库**

​    **数据库：即文件夹，用来组织文件/表**

​    **表：即文件，用来存放多行内容/多条记录**



## 初识MySQL

### **数据库管理软件分类**

　　管理数据的工具有很多种，不止MySQL一个。关于分类其实可以从各个纬度来进行划分，但是我们最常使用的分类还是根据他们存取数据的特点来划分的，主要分为关系型和非关系型。

　　可以简单的理解为，关系型数据库需要有表结构*，*非关系型数据库是key-value存储的，没有表结构

```python
# 关系型：如sqllite，db2，oracle，access，sql server，MySQL，注意：sql语句通用
# 非关系型：mongodb，redis，memcache
```



### mysql

　　MySQL是一个[关系型数据库管理系统](https://baike.baidu.com/item/关系型数据库管理系统/696511)，由瑞典MySQL AB 公司开发，目前属于 [Oracle](https://baike.baidu.com/item/Oracle) 旗下产品。MySQL 是最流行的[关系型数据库管理系统](https://baike.baidu.com/item/关系型数据库管理系统/696511)之一，在 WEB 应用方面，MySQL是最好的 [RDBMS](https://baike.baidu.com/item/RDBMS/1048260) (Relational Database Management System，关系数据库管理系统) 应用软件。

　　MySQL是一种关系数据库管理系统，关系数据库将数据保存在不同的表中，而不是将所有数据放在一个大仓库内，这样就增加了速度并提高了灵活性。

　　MySQL所使用的 SQL 语言是用于访问[数据库](https://baike.baidu.com/item/数据库/103728)的最常用标准化语言。MySQL 软件采用了双授权政策，分为社区版和商业版，由于其体积小、速度快、总体拥有成本低，尤其是[开放源码](https://baike.baidu.com/item/开放源码/7176422)这一特点，一般中小型网站的开发都选择 MySQL 作为网站数据库。



**下载和安装**

mysql为我们提供开源的安装在各个操作系统上的安装包，包括ios，linux，windows。

　　[mysql的安装、启动和基础配置 —— linux版本](https://www.cnblogs.com/Eva-J/articles/9676655.html) （https://www.cnblogs.com/Eva-J/articles/9664401.html）

　　[mysql的安装、启动和基础配置 —— mac版本](https://www.cnblogs.com/Eva-J/articles/9664401.html) （https://www.cnblogs.com/Eva-J/articles/9664401.html）

　　[mysql的安装、启动和基础配置 —— windows版本](https://www.cnblogs.com/Eva-J/articles/9669675.html) （https://www.cnblogs.com/Eva-J/articles/9669675.**html）**

**mysql的账号操作**

```
#进入mysql客户端
$mysql
mysql> select user();  #查看当前用户
+----------------+
| user()         |
+----------------+
| ODBC@localhost |
+----------------+
临时用户
mysql> exit     # 也可以用\q quit退出

# 默认用户登陆之后并没有实际操作的权限
# 需要使用管理员root用户登陆
$ mysql -uroot -p   # mysql5.6默认是没有密码的
#遇到password直接按回车键
mysql> set password = password('root'); # 给当前数据库设置密码

# 创建账号
mysql> create user 'eva'@'192.168.10.%'   IDENTIFIED BY '123';# 指示网段
mysql> create user 'eva'@'192.168.10.5'   # 指示某机器可以连接
mysql> create user 'eva'@'%'                    #指示所有机器都可以连接  
mysql> show grants for 'eva'@'192.168.10.5';查看某个用户的权限 
# 远程登陆
$ mysql -uroot -p123 -h 192.168.10.3

# 给账号授权
mysql> grant all on *.* to 'eva'@'%';
mysql> flush privileges;    # 刷新使授权立即生效

# 创建账号并授权
mysql> grant all on *.* to 'eva'@'%' identified by '123' 
```





设想一下，当我们想要从文件中存取数据的时候，是一个非常繁琐的过程，主要是因为文件中所有的内容对我们来说是连续的，没有规则的。如果我们将数据按照规则存在一个文件中，在设计一种规则可以拼凑组合成我们需要的操作，并通过这些指示在文件中存取数据，那么操作数据是不是能够变得更加简单快速呢？这串规则就被我们成为SQL。

　　SQL : 结构化查询语言(Structured Query Language)简称SQL，是一种特殊目的的编程语言，是一种数据库查询和[程序设计语言](https://baike.baidu.com/item/程序设计语言/2317999)，用于存取数据以及查询、更新和管理[关系数据库系统](https://baike.baidu.com/item/关系数据库系统)

　　SQL语言主要用于存取数据、查询数据、更新数据和管理关系数据库系统,SQL语言由IBM开发。SQL语言分为3种类型：

　　1、DDL语句 数据库定义语言： 数据库、表、视图、索引、存储过程，例如CREATE DROP ALTER

　　2、DML语句 数据库操纵语言： 插入数据INSERT、删除数据DELETE、更新数据UPDATE、查询数据SELECT

　　3、DCL语句 数据库控制语言： 例如控制用户的访问权限GRANT、REVOKE

```
1. 操作文件夹（库）
   增：create database db1 charset utf8;
   查：show databases;
   改：alter database db1 charset latin1;
   删除: drop database db1;


2. 操作文件（表）
   先切换到文件夹下：use db1
   增：create table t1(id int,name char);
   查：show tables;
   改：alter table t1 modify name char(3);
      alter table t1 change name name1 char(2);
   删：drop table t1;
    

3. 操作文件中的内容（记录）
   增：insert into t1 values(1,'egon1'),(2,'egon2'),(3,'egon3');
   查：select * from t1;
   改：update t1 set name='sb' where id=2;
   删：delete from t1 where id=1;

   清空表：
       delete from t1; #如果有自增id，新增的数据，仍然是以删除前的最后一样作为起始。
       truncate table t1;数据量大，删除速度比上一条快，且直接从零开始，

*auto_increment 表示：自增
*primary key 表示：约束（不能重复且不能为空）；加速查找
```