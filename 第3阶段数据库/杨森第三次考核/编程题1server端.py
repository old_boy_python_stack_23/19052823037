import socket
import gevent
import time

def commumication(conn):
    while 1:
        try:
            from_client_data = conn.recv(1024)

            conn.send(from_client_data.decode("utf-8").upper().encode("utf-8"))
        except Exception:
            break
    conn.close()


def customer_service():
    server = socket.socket()
    server.bind(("127.0.0.9", 8848))
    server.listen()
    while 1:
        conn, addr = server.accept()
        print(f"{addr}客户:")
        t = gevent.spawn(commumication, conn)
        t.start()
        t.join()
    server.close()



if __name__ == '__main__':
    customer_service()
