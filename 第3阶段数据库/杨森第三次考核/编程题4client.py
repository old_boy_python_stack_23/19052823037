import socket

udp_client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
while 1:
    to_server_data = input(">>>").strip()
    udp_client.sendto(to_server_data.encode("utf-8"), ("127.0.0.11", 9001))
    from_server_data = udp_client.recvfrom(1024)
    print(from_server_data[0].decode('utf-8'))
