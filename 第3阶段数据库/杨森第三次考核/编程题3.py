from concurrent.futures import ThreadPoolExecutor
from threading import Thread

"""
logging配置
"""

import logging.config
import os


# 定义三种日志输出格式 开始


def md_logger(filename):
    standard_format = '[%(asctime)s][%(threadName)s:%(thread)d][task_id:%(name)s][%(filename)s:%(lineno)d]' \
                      '[%(levelname)s][%(message)s]'  # 其中name为getlogger指定的名字

    simple_format = '[%(levelname)s][%(asctime)s][%(filename)s:%(lineno)d]%(message)s'

    # 定义日志输出格式 结束
    logfile_path_staff = os.path.dirname(__file__)
    zuihou_path = os.path.join(logfile_path_staff, 'log.log')

    print(logfile_path_staff)
    # log配置字典
    # LOGGING_DIC第一层的所有的键不能改变

    LOGGING_DIC = {
        'version': 1,  # 版本号
        'disable_existing_loggers': False,  # 固定写法
        'formatters': {
            'standard': {
                'format': standard_format
            },
            'simple': {
                'format': simple_format
            },
        },
        'filters': {},
        'handlers': {
            # 打印到终端的日志
            'sh': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',  # 打印到屏幕
                'formatter': 'simple'
            },
            # 打印到文件的日志,收集info及以上的日志
            'fh': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',  # 保存到文件
                'formatter': 'standard',
                'filename': zuihou_path,  # 日志文件
                'maxBytes': 1024 * 1024 * 5,  # 日志大小 5M
                'backupCount': 5,
                'encoding': 'utf-8',  # 日志文件的编码，再也不用担心中文log乱码了
            },
        },
        'loggers': {
            # logging.getLogger(__name__)拿到的logger配置
            '': {
                'handlers': ['sh', 'fh'],  # 这里把上面定义的两个handler都加上，即log数据既写入文件又打印到屏幕
                'level': 'DEBUG',
                'propagate': True,  # 向上（更高level的logger）传递
            },
        },
    }
    logging.config.dictConfig(LOGGING_DIC)  # 导入上面定义的logging配置
    logger = logging.getLogger()  # 生成一个log实例
    return logger
    # logger.debug('It works!')  # 记录该文件的运行状态


poor = ThreadPoolExecutor(20)


def func():
    print(1)
    return 1


def begin():
    for i in range(200):
        t = Thread(target=func)
        # t.start()
        poor.submit(t)


if __name__ == '__main__':
    begin()
    md_logger("log").info("开始")
    l = poor.shutdown()
    md_logger("log").debug("结束")
