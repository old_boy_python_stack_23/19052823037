import socket
import time

udp_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_server.bind(("127.0.0.11", 9001))
while 1:
    from_client_data = udp_server.recvfrom(1024)
    to_client_data = time.strftime(f"{from_client_data[0].decode('utf-8')}")
    udp_server.sendto(to_client_data.encode("utf-8"), from_client_data[1])


