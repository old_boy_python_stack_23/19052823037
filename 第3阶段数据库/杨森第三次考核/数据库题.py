# 1)
# create table user(
#     user_id int(18) primary key,
#     user_name char(10) not null,
#     password int(16) not null
# );
#
# create table orders(
#     order_id int(18) primary key,
#     order_time datetime not null,
#     payment float(10,2) not null ,
#     uid int(18) not null unique
# )
#
# create table goods(
#     goods_id int(18) primary key,
#     gname char(5) not null unique,
#     price float(10,2) not null,
#     g_num int(10) not null
# )
#
# create table goods_orders(
#     id int(18) primary key,
#     g_id int(18) not null unique,
#     o_id int(18) not null unique,
#     buy_num int(10) not null
# )


# 2)

# 1
import pymysql


# 定义一个可以由读取数据直接写入mysql表格的函数table是要写入数据的表明，数据是从文件中读取的内容
# 利用了SQL注入（字符串拼接和SQL语句产生的bug）的原理 不过和SQL注释 -- 无关
# 前提条件主键id必须在最前面且为数字，否则可能会报错
def writ(table, shuju):
    conn = pymysql.Connection(host='127.0.0.1',
                              user='root',
                              password='root',
                              database='data')
    cur = conn.cursor()
    m = ""
    for k in shuju.split(","):  # k只要不是纯数字都加上双单引号
        if '\u4e00' <= k <= '\u9fff' or ":" in k or not k.isdigit() or k.isalnum():
            m += "".join("\'" + k + "\'" + ",")
        else:
            m += k + ","
    print(m[0:-1])
    sql = "insert into %s values(%s);" % (str(table), m[0:-1])

    cur.execute(sql)
    conn.commit()
    cur.close()
    conn.close()


import os

file_path = os.path.dirname(__file__)
path = os.path.join(file_path, "data")
file_name = os.listdir(path)
for i in file_name:
    file_name_path = os.path.join(path, i)
    with open(file_name_path, encoding="utf-8") as f:
        da = f.readlines()
        for l in da:
            l = l.strip()
            # print(l)
            writ(i, l)

# # 2
# select goods_id,max(prince) from goods;
#
# # 3
# select order_id,max(payment) from orders;
#
# # 4
# select gname,sum(buy_num) from goods inner join (select g_id,sum(buy_num) from goods_orders group by g_id) as q on g_id = goods_id:
#
# # 5
# select user_name from user inner join (select uid from orders where payment > 1000) as q on uid = user_id;
# # 6
# select gname,max(max_num) from goods inner join (select g_id,sum(buy_num) as max_num from goods_orders group by g_id) as q on g_id = goods_id:
# # 7
#
# # 8
