# 表的存储引擎与基本操作

## 存储引擎:

​	存储引擎是数据的存储方式 ,使用不同的存储引擎,数据是以不同的方式存储的

- mysql5.6支持的存储引擎包括  InnoDB、MyISAM、MEMORY、CSV、BLACKHOLE、FEDERATED、MRG_MYISAM、ARCHIVE、PERFORMANCE_SCHEMA。其中NDB和InnoDB提供事务安全表，其他存储引擎都是非事务安全表。(MySQL不区分大小写)

### InnoDB   2个文件

- ​	MySQL 5.6以上  默认的存储方式

- ​	transaction 事务 为保证数据安全, 数据的完整性而设置的概念

- ​	InnoDB是一个事务安全的存储引擎，它具备提交、回滚以及崩溃恢复的功能以保护用户数据。

- ​	row-level  locking  行级锁 

- ​	table-level locking  表级锁

- ​	InnoDB 将用户数据存储在聚集索引中以减少基于主键的普通查询所带来的 I/O 开销

- ​	foreign keys  外键约束

- ​	树tree  - 加速查询(树形结构+数据+表结构)

### memory  1个文件

​	在内存中存储所有数据，应用于对非关键数据由快速查找的场景。Memory类型的表访问数据非常快，因为它的数据是存放在内存中的，并且默认使用HASH索引，但是一旦服务关闭，表中的数据就会丢失

### MyISAM 

MyISAM既不支持事务、也不支持外键、其优势是访问速度快，但是表级别的锁定限制了它在读写负载方面的性能，因此它经常应用于只读或者以读为主的数据场景。

### BLACKHOLE

黑洞存储引擎，类似于 Unix 的 /dev/null，Archive 只接收但却并不保存数据。对这种引擎的表的查询常常返回一个空集。这种表可以应用于 DML 语句需要发送到从服务器，但主服务器并不会保留这种数据的备份的主从配置中。



## 存储引擎在MySQL中的使用

### 存储引擎相关sql语句

- 查看存储引擎的方法 

```
show engines;
```

​	![1564474743110](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1564474743110.png)

- 查看当前默认存储引擎

```
show variables like "default_storage_engine";
```

![1564476870053](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1564476870053.png)

### 指定存储引擎建表

- 在建表示指定

  ```
  mysql> create table ai(id bigint(12),name varchar(200)) ENGINE=MyISAM; 
  
  mysql> create table country(id int(4),cname varchar(50)) ENGINE=InnoDB;
  
  也可以使用alter table语句，修改一个已经存在的表的存储引擎。
  
  mysql> alter table ai engine = innodb;
  ```

  

- 在配置文件中指定

  ```
  #my.ini文件
  [mysqld]
  default-storage-engine=INNODB
  ```

  

## MySQL的工作流程

![img](https://img2018.cnblogs.com/blog/827651/201809/827651-20180928181850055-806373159.png)

MySQL架构总共四层，在上图中以虚线作为划分。 
　　首先，最上层的服务并不是MySQL独有的，大多数给予网络的客户端/服务器的工具或者服务都有类似的架构。比如：连接处理、授权认证、安全等。 
　　第二层的架构包括大多数的MySQL的核心服务。包括：查询解析、分析、优化、缓存以及所有的内置函数（例如：日期、时间、数学和加密函数）。同时，所有的跨存储引擎的功能都在这一层实现：存储过程、触发器、视图等。

　　第三层包含了存储引擎。存储引擎负责MySQL中数据的存储和提取。服务器通过API和存储引擎进行通信。这些接口屏蔽了不同存储引擎之间的差异，使得这些差异对上层的查询过程透明化。存储引擎API包含十几个底层函数，用于执行“开始一个事务”等操作。但存储引擎一般不会去解析SQL（InnoDB会解析外键定义，因为其本身没有实现该功能），不同存储引擎之间也不会相互通信，而只是简单的响应上层的服务器请求。

　　第四层包含了文件系统，所有的表结构和数据以及用户操作的日志最终还是以文件的形式存储在硬盘上。

## 表的基本操作

### 什么是表

表就相当于文件，表中的一条记录就相当于文件的一行内容，不同的是，表中的一条记录有对应的标题，称为表的字段

还记得我们之前写过的‘员工信息表作业’么？存储这员工信息的文件是这样的：

```
id,name,age,sex,phone,job
1,Alex,83,female,13651054608,IT
2,Egon,26,male,13304320533,Tearcher
3,nezha,25,male,13332353222,IT
4,boss_jin,40,male,13332353333,IT
```

如果把上面这个文件改成一张表,应该是下面这个样子

| id   | name     | age  | sex    | phone       | job     |
| ---- | -------- | ---- | ------ | ----------- | ------- |
| 1    | Alex     | 83   | female | 13651054608 | IT      |
| 2    | Egon     | 26   | male   | 13304320533 | Teacher |
| 3    | nezha    | 25   | male   | 13332353222 | IT      |
| 4    | boss_jin | 40   | male   | 13332353333 | IT      |

id,name,age,sex,phone,job称为字段，其余的，一行内容称为一条记录 

### 创建表

- 语法

```
#语法：
create table 表名(
字段名1 类型[(宽度) 约束条件],
字段名2 类型[(宽度) 约束条件],
...
);

#注意：
1. 在同一张表中，字段名是不能相同
2. 宽度和约束条件可选
3. 字段名和类型是必须的
```



- 创建表

```
mysql> create database staff;
Query OK, 1 row affected (0.00 sec)

mysql> use staff;
Database changed
mysql> create table staff_info (id int,name varchar(50),age int(3),sex enum('male','female'),phone bigint(11),job varchar(11));
Query OK, 0 rows affected (0.02 sec)


mysql> show tables;
+-----------------+
| Tables_in_staff |
+-----------------+
| staff_info      |
+-----------------+
1 row in set (0.00 sec)

mysql> desc staff_info;
+-------+-----------------------+------+-----+---------+-------+
| Field | Type                  | Null | Key | Default | Extra |
+-------+-----------------------+------+-----+---------+-------+
| id    | int(11)               | YES  |     | NULL    |       |
| name  | varchar(50)           | YES  |     | NULL    |       |
| age   | int(3)                | YES  |     | NULL    |       |
| sex   | enum('male','female') | YES  |     | NULL    |       |
| phone | bigint(11)            | YES  |     | NULL    |       |
| job   | varchar(11)           | YES  |     | NULL    |       |
+-------+-----------------------+------+-----+---------+-------+
6 rows in set (0.00 sec)

mysql> select id,name,sex from staff_info;
Empty set (0.00 sec)

mysql> select * from staff_info;
Empty set (0.00 sec)
```



- 插入数据

```
mysql> insert into staff_info (id,name,age,sex,phone,job) values (1,'Alex',83,'female',13651054608,'IT');
Query OK, 1 row affected (0.00 sec)

mysql> insert into staff_info values (2,'Egon',26,'male',13304320533,'Teacher');
Query OK, 1 row affected (0.00 sec)

mysql> insert into staff_info values (3,'nezha',25,'male',13332353222,'IT'),(4,'boss_jin',40,'male',13332353333,'IT');
Query OK, 2 rows affected (0.00 sec)
Records: 2  Duplicates: 0  Warnings: 0

mysql> select * from staff_info;
+------+----------+------+--------+-------------+---------+
| id   | name     | age  | sex    | phone       | job     |
+------+----------+------+--------+-------------+---------+
|    1 | Alex     |   83 | female | 13651054608 | IT      |
|    2 | Egon     |   26 | male   | 13304320533 | Teacher |
|    3 | nezha    |   25 | male   | 13332353222 | IT      |
|    4 | boss_jin |   40 | male   | 13332353333 | IT      |
+------+----------+------+--------+-------------+---------+
4 rows in set (0.00 sec)
```

### 查看表结构与创建表时候使用的SQL语句

```
mysql> describe staff_info;
mysql> desc staff_info;   # 缩写
+-------+-----------------------+------+-----+---------+-------+
| Field | Type                  | Null | Key | Default | Extra |
+-------+-----------------------+------+-----+---------+-------+
| id    | int(11)               | YES  |     | NULL    |       |
| name  | varchar(50)           | YES  |     | NULL    |       |
| age   | int(3)                | YES  |     | NULL    |       |
| sex   | enum('male','female') | YES  |     | NULL    |       |
| phone | bigint(11)            | YES  |     | NULL    |       |
| job   | varchar(11)           | YES  |     | NULL    |       |
+-------+-----------------------+------+-----+---------+-------+
6 rows in set (0.00 sec)

mysql> show create table staff_info\G; #查看创建表时候用的SQL语句
*************************** 1. row ***************************
       Table: staff_info
Create Table: CREATE TABLE `staff_info` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `sex` enum('male','female') DEFAULT NULL,
  `phone` bigint(11) DEFAULT NULL,
  `job` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8
1 row in set (0.01 sec)

ERROR: 
No query specified
```



## MySQL中的数据类型

### 数值类型

MySQL支持所有标准SQL数值数据类型。

对于小数的表示，MYSQL分为两种方式：浮点数和定点数。浮点数包括float(单精度)和double(双精度),而定点数只有decimal一种，在mysql中以字符串的形式存放，比浮点数更精确，适合用来表示货币等精度高的数据。

BIT数据类型保存位字段值，并且支持MyISAM、MEMORY、InnoDB和BDB表。

 

| 类型         | 大小                                                  | 范围（有符号）                                               | 范围（无符号）unsigned约束                                   | 用途            |
| ------------ | ----------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | --------------- |
| TINYINT      | 1 字节                                                | (-128，127)                                                  | (0，255)                                                     | 小整数值        |
| SMALLINT     | 2 字节                                                | (-32 768，32 767)                                            | (0，65 535)                                                  | 大整数值        |
| MEDIUMINT    | 3 字节                                                | (-8 388 608，8 388 607)                                      | (0，16 777 215)                                              | 大整数值        |
| INT或INTEGER | 4 字节                                                | (-2 147 483 648，2 147 483 647)                              | (0，4 294 967 295)                                           | 大整数值        |
| BIGINT       | 8 字节                                                | (-9 233 372 036 854 775 808，9 223 372 036 854 775 807)      | (0，18 446 744 073 709 551 615)                              | 极大整数值      |
| FLOAT        | 4 字节float(255,30)                                   | (-3.402 823 466 E+38，-1.175 494 351 E-38)，0，(1.175 494 351 E-38，3.402 823 466 351 E+38) | 0，(1.175 494 351 E-38，3.402 823 466 E+38)                  | 单精度 浮点数值 |
| DOUBLE       | 8 字节double(255,30)                                  | (-1.797 693 134 862 315 7 E+308，-2.225 073 858 507 201 4 E-308)，0，(2.225 073 858 507 201 4 E-308，1.797 693 134 862 315 7 E+308) | 0，(2.225 073 858 507 201 4 E-308，1.797 693 134 862 315 7 E+308) | 双精度 浮点数值 |
| DECIMAL      | 对DECIMAL(M,D) ，如果M>D，为M+2否则为D+2double(65,30) | 依赖于M和D的值                                               | 依赖于M和D的值                                               | 小数值          |

- int整数示例

```
# 创建表一个是默认宽度的int，一个是指定宽度的int(5)
mysql> create table t1 (id1 int,id2 int(5));
Query OK, 0 rows affected (0.02 sec)

# 像t1中插入数据1，1
mysql> insert into t1 values (1,1);
Query OK, 1 row affected (0.01 sec)

# 可以看出结果上并没有异常
mysql> select * from t1;
+------+------+
| id1  | id2  |
+------+------+
|    1 |    1 |
+------+------+
1 row in set (0.00 sec)

# 那么当我们插入了比宽度更大的值，会不会发生报错呢？
mysql> insert into t1 values (111111,111111);
Query OK, 1 row affected (0.00 sec)

# 答案是否定的，id2仍然显示了正确的数值，没有受到宽度限制的影响
mysql> select * from t1;
+------------+--------+
| id1        | id2    |
+------------+--------+
| 0000000001 |  00001 |
| 0000111111 | 111111 |
+------------+--------+
2 rows in set (0.00 sec)

# 修改id1字段 给字段添加一个unsigned表示无符号
mysql> alter table t1 modify id1 int unsigned;
Query OK, 0 rows affected (0.01 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> desc t1;
+-------+------------------+------+-----+---------+-------+
| Field | Type             | Null | Key | Default | Extra |
+-------+------------------+------+-----+---------+-------+
| id1   | int(10) unsigned | YES  |     | NULL    |       |
| id2   | int(5)           | YES  |     | NULL    |       |
+-------+------------------+------+-----+---------+-------+
2 rows in set (0.01 sec)

# 当给id1添加的数据大于214748364时，可以顺利插入
mysql> insert into t1 values (2147483648,2147483647);
Query OK, 1 row affected (0.00 sec)

# 当给id2添加的数据大于214748364时，会报错
mysql> insert into t1 values (2147483647,2147483648);
ERROR 1264 (22003): Out of range value for column 'id2' at row 1
```

- ### 小数示例

```
# 创建表的三个字段分别为float，double和decimal参数表示一共显示5位，小数部分占2位
mysql> create table t2 (id1 float(5,2),id2 double(5,2),id3 decimal(5,2));
Query OK, 0 rows affected (0.02 sec)

# 向表中插入1.23，结果正常
mysql> insert into t2 values (1.23,1.23,1.23);
Query OK, 1 row affected (0.00 sec)

mysql> select * from t2;
+------+------+------+
| id1  | id2  | id3  |
+------+------+------+
| 1.23 | 1.23 | 1.23 |
+------+------+------+
1 row in set (0.00 sec)

# 向表中插入1.234，会发现4都被截断了
mysql> insert into t2 values (1.234,1.234,1.234);
Query OK, 1 row affected, 1 warning (0.00 sec)

mysql> select * from t2;
+------+------+------+
| id1  | id2  | id3  |
+------+------+------+
| 1.23 | 1.23 | 1.23 |
| 1.23 | 1.23 | 1.23 |
+------+------+------+
2 rows in set (0.00 sec)

# 向表中插入1.235发现数据虽然被截断，但是遵循了四舍五入的规则
mysql> insert into t2 values (1.235,1.235,1.235);
Query OK, 1 row affected, 1 warning (0.00 sec)

mysql> select * from t2;
+------+------+------+
| id1  | id2  | id3  |
+------+------+------+
| 1.23 | 1.23 | 1.23 |
| 1.23 | 1.23 | 1.23 |
| 1.24 | 1.24 | 1.24 |
+------+------+------+
3 rows in set (0.00 sec)

# 建新表去掉参数约束
mysql> create table t3 (id1 float,id2 double,id3 decimal);
Query OK, 0 rows affected (0.02 sec)

# 分别插入1.234
mysql> insert into t3 values (1.234,1.234,1.234);
Query OK, 1 row affected, 1 warning (0.00 sec)

# 发现decimal默认值是(10,0)的整数
mysql> select * from t3;
+-------+-------+------+
| id1   | id2   | id3  |
+-------+-------+------+
| 1.234 | 1.234 |    1 |
+-------+-------+------+
1 row in set (0.00 sec)

# 当对小数位没有约束的时候，输入超长的小数，会发现float和double的区别
mysql> insert into t3 values (1.2355555555555555555,1.2355555555555555555,1.2355555555555555555555);
Query OK, 1 row affected, 1 warning (0.00 sec)

mysql> select * from t3;
+---------+--------------------+------+
| id1     | id2                | id3  |
+---------+--------------------+------+
|   1.234 |              1.234 |    1 |
| 1.23556 | 1.2355555555555555 |    1 |
+---------+--------------------+------+
2 rows in set (0.00 sec)
```

### 日期和时间类型

表示时间值的日期和时间类型为DATETIME、DATE、TIMESTAMP、TIME和YEAR。

每个时间类型有一个有效值范围和一个"零"值，当指定不合法的MySQL不能表示的值时使用"零"值。

TIMESTAMP类型有专有的自动更新特性，将在后面描述。

| 类型      | 大小 (字节) | 范围                                                         | 格式                | 用途                     |
| --------- | ----------- | ------------------------------------------------------------ | ------------------- | ------------------------ |
| DATE      | 3           | 1000-01-01/9999-12-31                                        | YYYY-MM-DD          | 年月日                   |
| TIME      | 3           | '-838:59:59'/'838:59:59'                                     | HH:MM:SS            | 时分秒                   |
| YEAR      | 1           | 1901/2155                                                    | YYYY                | 年份值                   |
| DATETIME  | 8           | 1000-01-01 00:00:00/9999-12-31 23:59:59                      | YYYY-MM-DD HH:MM:SS | 年月日时分秒             |
| TIMESTAMP | 4           | 1970-01-01 00:00:00/2038结束时间是第 **2147483647** 秒，北京时间 **2038-1-19 11:14:07**，格林尼治时间 2038年1月19日 凌晨 03:14:07 | YYYYMMDD HHMMSS     | 混合日期和时间值，时间戳 |

- date/time/datatime示例

```
mysql> create table t4 (d date,t time,dt datetime);
Query OK, 0 rows affected (0.02 sec)

mysql> desc t4;
+-------+----------+------+-----+---------+-------+
| Field | Type     | Null | Key | Default | Extra |
+-------+----------+------+-----+---------+-------+
| d     | date     | YES  |     | NULL    |       |
| t     | time     | YES  |     | NULL    |       |
| dt    | datetime | YES  |     | NULL    |       |
+-------+----------+------+-----+---------+-------+
3 rows in set (0.01 sec)

mysql> insert into t4 values (now(),now(),now());
Query OK, 1 row affected, 1 warning (0.01 sec)

mysql> select * from t4;
+------------+----------+---------------------+
| d          | t        | dt                  |
+------------+----------+---------------------+
| 2018-09-21 | 14:51:51 | 2018-09-21 14:51:51 |
+------------+----------+---------------------+
1 row in set (0.00 sec)

mysql> insert into t4 values (null,null,null);
Query OK, 1 row affected (0.01 sec)

mysql> select * from t4;
+------------+----------+---------------------+
| d          | t        | dt                  |
+------------+----------+---------------------+
| 2018-09-21 | 14:51:51 | 2018-09-21 14:51:51 |
| NULL       | NULL     | NULL                |
+------------+----------+---------------------+
2 rows in set (0.00 sec)
```

- timestramp示例

```
mysql> create table t5 (id1 timestamp);
Query OK, 0 rows affected (0.02 sec)

mysql> desc t5;
+-------+-----------+------+-----+-------------------+-----------------------------+
| Field | Type      | Null | Key | Default           | Extra                       |
+-------+-----------+------+-----+-------------------+-----------------------------+
| id1   | timestamp | NO   |     | CURRENT_TIMESTAMP | on update CURRENT_TIMESTAMP |
+-------+-----------+------+-----+-------------------+-----------------------------+
1 row in set (0.00 sec)

# 插入数据null，会自动插入当前时间的时间
mysql> insert into t5 values (null);
Query OK, 1 row affected (0.00 sec)

mysql> select * from t5;
+---------------------+
| id1                 |
+---------------------+
| 2018-09-21 14:56:50 |
+---------------------+
1 row in set (0.00 sec)

#添加一列 默认值是'0000-00-00 00:00:00'
mysql> alter table t5 add id2 timestamp;
Query OK, 0 rows affected (0.02 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> show create table t5 \G;
*************************** 1. row ***************************
       Table: t5
Create Table: CREATE TABLE `t5` (
  `id1` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8
1 row in set (0.00 sec)

ERROR: 
No query specified

# 手动修改新的列默认值为当前时间
mysql> alter table t5 modify id2 timestamp default current_timestamp;
Query OK, 0 rows affected (0.02 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> show create table t5 \G;
*************************** 1. row ***************************
       Table: t5
Create Table: CREATE TABLE `t5` (
  `id1` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id2` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8
1 row in set (0.00 sec)

ERROR: 
No query specified

mysql> insert into t5 values (null,null);
Query OK, 1 row affected (0.01 sec)

mysql> select * from t5;
+---------------------+---------------------+
| id1                 | id2                 |
+---------------------+---------------------+
| 2018-09-21 14:56:50 | 0000-00-00 00:00:00 |
| 2018-09-21 14:59:31 | 2018-09-21 14:59:31 |
+---------------------+---------------------+
2 rows in set (0.00 sec)
```

- timestamp示例2

```
mysql> create table t6 (t1 timestamp);
Query OK, 0 rows affected (0.02 sec)

mysql> desc t6;
+-------+-----------+------+-----+-------------------+-----------------------------+
| Field | Type      | Null | Key | Default           | Extra                       |
+-------+-----------+------+-----+-------------------+-----------------------------+
| t1    | timestamp | NO   |     | CURRENT_TIMESTAMP | on update CURRENT_TIMESTAMP |
+-------+-----------+------+-----+-------------------+-----------------------------+
1 row in set (0.01 sec)

mysql> insert into t6 values (19700101080001);
Query OK, 1 row affected (0.00 sec)

mysql> select * from t6;
+---------------------+
| t1                  |
+---------------------+
| 1970-01-01 08:00:01 |
+---------------------+
1 row in set (0.00 sec)
# timestamp时间的下限是19700101080001
mysql> insert into t6 values (19700101080000);
ERROR 1292 (22007): Incorrect datetime value: '19700101080000' for column 't1' at row 1

mysql> insert into t6 values ('2038-01-19 11:14:07');
Query OK, 1 row affected (0.00 sec)
# timestamp时间的上限是2038-01-19 11:14:07
mysql> insert into t6 values ('2038-01-19 11:14:08');
ERROR 1292 (22007): Incorrect datetime value: '2038-01-19 11:14:08' for column 't1' at row 1
mysql> 
```

- year示例

```
mysql> create table t7 (y year);
Query OK, 0 rows affected (0.02 sec)

mysql> insert into t7 values (2018);
Query OK, 1 row affected (0.00 sec)

mysql> select * from t7;
+------+
| y    |
+------+
| 2018 |
+------+
1 row in set (0.00 sec)
```

- datetime示例

```
mysql> create table t8 (dt datetime);
Query OK, 0 rows affected (0.01 sec)

mysql> insert into t8 values ('2018-9-26 12:20:10');
Query OK, 1 row affected (0.01 sec)

mysql> insert into t8 values ('2018/9/26 12+20+10');
Query OK, 1 row affected (0.00 sec)

mysql> insert into t8 values ('20180926122010');
Query OK, 1 row affected (0.00 sec)

mysql> insert into t8 values (20180926122010);
Query OK, 1 row affected (0.00 sec)

mysql> select * from t8;
+---------------------+
| dt                  |
+---------------------+
| 2018-09-26 12:20:10 |
| 2018-09-26 12:20:10 |
| 2018-09-26 12:20:10 |
| 2018-09-26 12:20:10 |
+---------------------+
4 rows in set (0.00 sec)
```

### 字符串类型

字符串类型指CHAR、VARCHAR、BINARY、VARBINARY、BLOB、TEXT、ENUM和SET。该节描述了这些类型如何工作以及如何在查询中使用这些类型。

| 类型       | 大小                | 用途                            |
| ---------- | ------------------- | ------------------------------- |
| CHAR       | 0-255字节           | 定长字符串                      |
| VARCHAR    | 0-65535 字节        | 变长字符串                      |
| TINYBLOB   | 0-255字节           | 不超过 255 个字符的二进制字符串 |
| TINYTEXT   | 0-255字节           | 短文本字符串                    |
| BLOB       | 0-65 535字节        | 二进制形式的长文本数据          |
| TEXT       | 0-65 535字节        | 长文本数据                      |
| MEDIUMBLOB | 0-16 777 215字节    | 二进制形式的中等长度文本数据    |
| MEDIUMTEXT | 0-16 777 215字节    | 中等长度文本数据                |
| LONGBLOB   | 0-4 294 967 295字节 | 二进制形式的极大文本数据        |
| LONGTEXT   | 0-4 294 967 295字节 | 极大文本数据                    |

CHAR 和 VARCHAR 类型类似，但它们保存和检索的方式不同。它们的最大长度和是否尾部空格被保留等方面也不同。在存储或检索过程中不进行大小写转换。

CHAR列的长度固定为创建表是声明的长度,范围(0-255);而VARCHAR的值是可变长字符串范围(0-65535)。

```
mysql> create table t9 (v varchar(4),c char(4));
Query OK, 0 rows affected (0.01 sec)

mysql> insert into t9 values ('ab  ','ab  ');
Query OK, 1 row affected (0.00 sec)

# 在检索的时候char数据类型会去掉空格
mysql> select * from t9;
+------+------+
| v    | c    |
+------+------+
| ab   | ab   |
+------+------+
1 row in set (0.00 sec)

# 来看看对查询结果计算的长度
mysql> select length(v),length(c) from t9;
+-----------+-----------+
| length(v) | length(c) |
+-----------+-----------+
|         4 |         2 |
+-----------+-----------+
1 row in set (0.00 sec)

# 给结果拼上一个加号会更清楚
mysql> select concat(v,'+'),concat(c,'+') from t9;
+---------------+---------------+
| concat(v,'+') | concat(c,'+') |
+---------------+---------------+
| ab  +         | ab+           |
+---------------+---------------+
1 row in set (0.00 sec)

# 当存储的长度超出定义的长度，会截断
mysql> insert into t9 values ('abcd  ','abcd  ');
Query OK, 1 row affected, 1 warning (0.01 sec)

mysql> select * from t9;
+------+------+
| v    | c    |
+------+------+
| ab   | ab   |
| abcd | abcd |
+------+------+
2 rows in set (0.00 sec)
```

BINARY 和 VARBINARY 类似于 CHAR 和 VARCHAR，不同的是它们包含二进制字符串而不要非二进制字符串。也就是说，它们包含字节字符串而不是字符字符串。这说明它们没有字符集，并且排序和比较基于列值字节的数值值。

BLOB 是一个二进制大对象，可以容纳可变数量的数据。有 4 种 BLOB 类型：TINYBLOB、BLOB、MEDIUMBLOB 和 LONGBLOB。它们区别在于可容纳存储范围不同。

有 4 种 TEXT 类型：TINYTEXT、TEXT、MEDIUMTEXT 和 LONGTEXT。对应的这 4 种 BLOB 类型，可存储的最大长度不同，可根据实际情况选择。

### enum和set类型

ENUM中文名称叫枚举类型，它的值范围需要在创建表时通过枚举方式显示。ENUM**只允许从值集合中选取单个值，而不能一次取多个值**。

SET和ENUM非常相似，也是一个字符串对象，里面可以包含0-64个成员。根据成员的不同，存储上也有所不同。set类型可以**允许值集合中任意选择1或多个元素进行组合**。对超出范围的内容将不允许注入，而对重复的值将进行自动去重。

| 类型 | 大小                                                         | 用途           |
| ---- | ------------------------------------------------------------ | -------------- |
| ENUM | 对1-255个成员的枚举需要1个字节存储;对于255-65535个成员，需要2个字节存储;最多允许65535个成员。 | 单选：选择性别 |
| SET  | 1-8个成员的集合，占1个字节9-16个成员的集合，占2个字节17-24个成员的集合，占3个字节25-32个成员的集合，占4个字节33-64个成员的集合，占8个字节 | 多选：兴趣爱好 |

```
mysql> create table t10 (name char(20),gender enum('female','male'));
Query OK, 0 rows affected (0.01 sec)

# 选择enum('female','male')中的一项作为gender的值，可以正常插入
mysql> insert into t10 values ('nezha','male');
Query OK, 1 row affected (0.00 sec)

# 不能同时插入'male,female'两个值，也不能插入不属于'male,female'的值
mysql> insert into t10 values ('nezha','male,female');
ERROR 1265 (01000): Data truncated for column 'gender' at row 1

mysql> create table t11 (name char(20),hobby set('抽烟','喝酒','烫头','翻车'));
Query OK, 0 rows affected (0.01 sec)

# 可以任意选择set('抽烟','喝酒','烫头','翻车')中的项，并自带去重功能
mysql> insert into t11 values ('yuan','烫头,喝酒,烫头');
Query OK, 1 row affected (0.01 sec)

mysql> select * from t11;
+------+---------------+
| name | hobby        |
+------+---------------+
| yuan | 喝酒,烫头     |
+------+---------------+
1 row in set (0.00 sec)

# 不能选择不属于set('抽烟','喝酒','烫头','翻车')中的项，
mysql> insert into t11 values ('alex','烫头,翻车,看妹子');
ERROR 1265 (01000): Data truncated for column 'hobby' at row 1
```



## 表的完整性约束

为了防止不符合规范的数据进入数据库，在用户对数据进行插入、修改、删除等操作时，DBMS自动按照一定的约束条件对数据进行监测，使不符合规范的数据不能进入数据库，以确保数据库中存储的数据正确、有效、相容。 

　　约束条件与数据类型的宽度一样，都是可选参数，主要分为以下几种：

```
# NOT NULL ：非空约束，指定某列不能为空； 
# UNIQUE : 唯一约束，指定某列或者几列组合不能重复
# PRIMARY KEY ：主键，指定该列的值可以唯一地标识该列记录
# FOREIGN KEY ：外键，指定该行记录从属于主表中的一条记录，主要用于参照完整性
```

- NOT NULL

  是否可空，null表示空，非字符串

  not null - 不可空

  null - 可空 

  示例

  ```
  mysql> create table t12 (id int not null);
  Query OK, 0 rows affected (0.02 sec)
  
  mysql> select * from t12;
  Empty set (0.00 sec)
  
  mysql> desc t12;
  +-------+---------+------+-----+---------+-------+
  | Field | Type    | Null | Key | Default | Extra |
  +-------+---------+------+-----+---------+-------+
  | id    | int(11) | NO   |     | NULL    |       |
  +-------+---------+------+-----+---------+-------+
  1 row in set (0.00 sec)
  
  #不能向id列插入空元素。 
  mysql> insert into t12 values (null);
  ERROR 1048 (23000): Column 'id' cannot be null
  
  mysql> insert into t12 values (1);
  Query OK, 1 row affected (0.01 sec)
  ```

  

- DEFAULT

  我们约束某一列不为空，如果这一列中经常有重复的内容，就需要我们频繁的插入，这样会给我们的操作带来新的负担，于是就出现了默认值的概念。

  默认值，创建列时可以指定默认值，当插入数据时如果未主动设置，则自动添加默认值

  ```
  方法一：
  create table department1(
  id int,
  name varchar(20) unique,
  comment varchar(100)
  );
  
  
  方法二：
  create table department2(
  id int,
  name varchar(20),
  comment varchar(100),
  unique(name)
  );
  
  
  mysql> insert into department1 values(1,'IT','技术');
  Query OK, 1 row affected (0.00 sec)
  mysql> insert into department1 values(1,'IT','技术');
  ERROR 1062 (23000): Duplicate entry 'IT' for key 'name'
  ```

  

  

  ```
  mysql> create table t1(id int not null unique);
  Query OK, 0 rows affected (0.02 sec)
  
  mysql> desc t1;
  +-------+---------+------+-----+---------+-------+
  | Field | Type    | Null | Key | Default | Extra |
  +-------+---------+------+-----+---------+-------+
  | id    | int(11) | NO   | PRI | NULL    |       |
  +-------+---------+------+-----+---------+-------+
  1 row in set (0.00 sec)
  ```

  ```
  create table service(
  id int primary key auto_increment,
  name varchar(20),
  host varchar(15) not null,
  port int not null,
  unique(host,port) #联合唯一
  );
  
  mysql> insert into service values
      -> (1,'nginx','192.168.0.10',80),
      -> (2,'haproxy','192.168.0.20',80),
      -> (3,'mysql','192.168.0.30',3306)
      -> ;
  Query OK, 3 rows affected (0.01 sec)
  Records: 3  Duplicates: 0  Warnings: 0
  
  mysql> insert into service(name,host,port) values('nginx','192.168.0.10',80);
  ERROR 1062 (23000): Duplicate entry '192.168.0.10-80' for key 'host'
  ```

- PRIMARY  KEY

  主键为了保证表中的每一条数据的该字段都是表格中的唯一值。换言之，它是用来独一无二地确认一个表格中的每一行数据。 
  主键可以包含一个字段或多个字段。当主键包含多个栏位时，称为组合键 (Composite [Key](https://www.baidu.com/s?wd=Key&tn=SE_PcZhidaonwhc_ngpagmjz&rsv_dl=gh_pc_zhidao)),也可以叫联合主键。
  主键可以在建置新表格时设定 (运用 CREATE TABLE 语句)，或是以改变现有的表格架构方式设定 (运用 ALTER TABLE)。
  主键必须唯一，主键值非空；可以是单一字段，也可以是多字段组合。

  1.单字段主键

  ```
  ============单列做主键===============
  #方法一：not null+unique
  create table department1(
  id int not null unique, #主键
  name varchar(20) not null unique,
  comment varchar(100)
  );
  
  mysql> desc department1;
  +---------+--------------+------+-----+---------+-------+
  | Field   | Type         | Null | Key | Default | Extra |
  +---------+--------------+------+-----+---------+-------+
  | id      | int(11)      | NO   | PRI | NULL    |       |
  | name    | varchar(20)  | NO   | UNI | NULL    |       |
  | comment | varchar(100) | YES  |     | NULL    |       |
  +---------+--------------+------+-----+---------+-------+
  rows in set (0.01 sec)
  
  #方法二：在某一个字段后用primary key
  create table department2(
  id int primary key, #主键
  name varchar(20),
  comment varchar(100)
  );
  
  mysql> desc department2;
  +---------+--------------+------+-----+---------+-------+
  | Field   | Type         | Null | Key | Default | Extra |
  +---------+--------------+------+-----+---------+-------+
  | id      | int(11)      | NO   | PRI | NULL    |       |
  | name    | varchar(20)  | YES  |     | NULL    |       |
  | comment | varchar(100) | YES  |     | NULL    |       |
  +---------+--------------+------+-----+---------+-------+
  rows in set (0.00 sec)
  
  #方法三：在所有字段后单独定义primary key
  create table department3(
  id int,
  name varchar(20),
  comment varchar(100),
  primary key(id); #创建主键并为其命名pk_name
  
  mysql> desc department3;
  +---------+--------------+------+-----+---------+-------+
  | Field   | Type         | Null | Key | Default | Extra |
  +---------+--------------+------+-----+---------+-------+
  | id      | int(11)      | NO   | PRI | NULL    |       |
  | name    | varchar(20)  | YES  |     | NULL    |       |
  | comment | varchar(100) | YES  |     | NULL    |       |
  +---------+--------------+------+-----+---------+-------+
  rows in set (0.01 sec)
  
  # 方法四：给已经建成的表添加主键约束
  mysql> create table department4(
      -> id int,
      -> name varchar(20),
      -> comment varchar(100));
  Query OK, 0 rows affected (0.01 sec)
  
  mysql> desc department4;
  +---------+--------------+------+-----+---------+-------+
  | Field   | Type         | Null | Key | Default | Extra |
  +---------+--------------+------+-----+---------+-------+
  | id      | int(11)      | YES  |     | NULL    |       |
  | name    | varchar(20)  | YES  |     | NULL    |       |
  | comment | varchar(100) | YES  |     | NULL    |       |
  +---------+--------------+------+-----+---------+-------+
  3 rows in set (0.01 sec)
  
  mysql> alter table department4 modify id int primary key;
  Query OK, 0 rows affected (0.02 sec)
  Records: 0  Duplicates: 0  Warnings: 0
  
  mysql> desc department4;
  +---------+--------------+------+-----+---------+-------+
  | Field   | Type         | Null | Key | Default | Extra |
  +---------+--------------+------+-----+---------+-------+
  | id      | int(11)      | NO   | PRI | NULL    |       |
  | name    | varchar(20)  | YES  |     | NULL    |       |
  | comment | varchar(100) | YES  |     | NULL    |       |
  +---------+--------------+------+-----+---------+-------+
  3 rows in set (0.01 sec)
  ```

  2.多字段主键

  ```
  ==================多列做主键================
  create table service(
  ip varchar(15),
  port char(5),
  service_name varchar(10) not null,
  primary key(ip,port)
  );
  
  
  mysql> desc service;
  +--------------+-------------+------+-----+---------+-------+
  | Field        | Type        | Null | Key | Default | Extra |
  +--------------+-------------+------+-----+---------+-------+
  | ip           | varchar(15) | NO   | PRI | NULL    |       |
  | port         | char(5)     | NO   | PRI | NULL    |       |
  | service_name | varchar(10) | NO   |     | NULL    |       |
  +--------------+-------------+------+-----+---------+-------+
  3 rows in set (0.00 sec)
  
  mysql> insert into service values
      -> ('172.16.45.10','3306','mysqld'),
      -> ('172.16.45.11','3306','mariadb')
      -> ;
  Query OK, 2 rows affected (0.00 sec)
  Records: 2  Duplicates: 0  Warnings: 0
  
  mysql> insert into service values ('172.16.45.10','3306','nginx');
  ERROR 1062 (23000): Duplicate entry '172.16.45.10-3306' for key 'PRIMARY'
  ```

- AUTO_INCREMENT

  约束字段为自动增长，被约束的字段必须同时被key约束

  ```
  #不指定id，则自动增长
  create table student(
  id int primary key auto_increment,
  name varchar(20),
  sex enum('male','female') default 'male'
  );
  
  mysql> desc student;
  +-------+-----------------------+------+-----+---------+----------------+
  | Field | Type                  | Null | Key | Default | Extra          |
  +-------+-----------------------+------+-----+---------+----------------+
  | id    | int(11)               | NO   | PRI | NULL    | auto_increment |
  | name  | varchar(20)           | YES  |     | NULL    |                |
  | sex   | enum('male','female') | YES  |     | male    |                |
  +-------+-----------------------+------+-----+---------+----------------+
  mysql> insert into student(name) values
      -> ('egon'),
      -> ('alex')
      -> ;
  
  mysql> select * from student;
  +----+------+------+
  | id | name | sex  |
  +----+------+------+
  |  1 | egon | male |
  |  2 | alex | male |
  +----+------+------+
  
  
  #也可以指定id
  mysql> insert into student values(4,'asb','female');
  Query OK, 1 row affected (0.00 sec)
  
  mysql> insert into student values(7,'wsb','female');
  Query OK, 1 row affected (0.00 sec)
  
  mysql> select * from student;
  +----+------+--------+
  | id | name | sex    |
  +----+------+--------+
  |  1 | egon | male   |
  |  2 | alex | male   |
  |  4 | asb  | female |
  |  7 | wsb  | female |
  +----+------+--------+
  
  
  #对于自增的字段，在用delete删除后，再插入值，该字段仍按照删除前的位置继续增长
  mysql> delete from student;
  Query OK, 4 rows affected (0.00 sec)
  
  mysql> select * from student;
  Empty set (0.00 sec)
  
  mysql> insert into student(name) values('ysb');
  mysql> select * from student;
  +----+------+------+
  | id | name | sex  |
  +----+------+------+
  |  8 | ysb  | male |
  +----+------+------+
  
  #应该用truncate清空表，比起delete一条一条地删除记录，truncate是直接清空表，在删除大表时用它
  mysql> truncate student;
  Query OK, 0 rows affected (0.01 sec)
  
  mysql> insert into student(name) values('egon');
  Query OK, 1 row affected (0.01 sec)
  
  mysql> select * from student;
  +----+------+------+
  | id | name | sex  |
  +----+------+------+
  |  1 | egon | male |
  +----+------+------+
  row in set (0.00 sec)
  ```

  了解知识

  ```
  #在创建完表后，修改自增字段的起始值
  mysql> create table student(
      -> id int primary key auto_increment,
      -> name varchar(20),
      -> sex enum('male','female') default 'male'
      -> );
  
  mysql> alter table student auto_increment=3;
  
  mysql> show create table student;
  .......
  ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8
  
  mysql> insert into student(name) values('egon');
  Query OK, 1 row affected (0.01 sec)
  
  mysql> select * from student;
  +----+------+------+
  | id | name | sex  |
  +----+------+------+
  |  3 | egon | male |
  +----+------+------+
  row in set (0.00 sec)
  
  mysql> show create table student;
  .......
  ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8
  
  
  #也可以创建表时指定auto_increment的初始值，注意初始值的设置为表选项，应该放到括号外
  create table student(
  id int primary key auto_increment,
  name varchar(20),
  sex enum('male','female') default 'male'
  )auto_increment=3;
  
  
  
  
  #设置步长
  sqlserver：自增步长
      基于表级别
      create table t1（
          id int。。。
      ）engine=innodb,auto_increment=2 步长=2 default charset=utf8
  
  mysql自增的步长：
      show session variables like 'auto_inc%';
      
      #基于会话级别
      set session auth_increment_increment=2 #修改会话级别的步长
  
      #基于全局级别的
      set global auth_increment_increment=2 #修改全局级别的步长（所有会话都生效）
  
  
  #！！！注意了注意了注意了！！！
  If the value of auto_increment_offset is greater than that of auto_increment_increment, the value of auto_increment_offset is ignored. 
  翻译：如果auto_increment_offset的值大于auto_increment_increment的值，则auto_increment_offset的值会被忽略 ，这相当于第一步步子就迈大了，扯着了蛋
  比如：设置auto_increment_offset=3，auto_increment_increment=2
  
  
  
  
  mysql> set global auto_increment_increment=5;
  Query OK, 0 rows affected (0.00 sec)
  
  mysql> set global auto_increment_offset=3;
  Query OK, 0 rows affected (0.00 sec)
  
  mysql> show variables like 'auto_incre%'; #需要退出重新登录
  +--------------------------+-------+
  | Variable_name            | Value |
  +--------------------------+-------+
  | auto_increment_increment | 1     |
  | auto_increment_offset    | 1     |
  +--------------------------+-------+
  
  
  
  create table student(
  id int primary key auto_increment,
  name varchar(20),
  sex enum('male','female') default 'male'
  );
  
  mysql> insert into student(name) values('egon1'),('egon2'),('egon3');
  mysql> select * from student;
  +----+-------+------+
  | id | name  | sex  |
  +----+-------+------+
  |  3 | egon1 | male |
  |  8 | egon2 | male |
  | 13 | egon3 | male |
  +----+-------+------+
  
  步长:auto_increment_increment,起始偏移量:auto_increment_offset
  ```

  

- FOREIGN KEY

  多表 ：

  假设我们要描述所有公司的员工，需要描述的属性有这些 ： 工号 姓名 部门

  公司有3个部门，但是有1个亿的员工，那意味着部门这个字段需要重复存储，部门名字越长，越浪费

  解决方法： 我们完全可以定义一个部门表 然后让员工信息表关联该表，如何关联，即foreign key

  创建外键的条件

  ```
  mysql> create table departments (dep_id int(4),dep_name varchar(11));
  Query OK, 0 rows affected (0.02 sec)
  
  mysql> desc departments;
  +----------+-------------+------+-----+---------+-------+
  | Field    | Type        | Null | Key | Default | Extra |
  +----------+-------------+------+-----+---------+-------+
  | dep_id   | int(4)      | YES  |     | NULL    |       |
  | dep_name | varchar(11) | YES  |     | NULL    |       |
  +----------+-------------+------+-----+---------+-------+
  2 rows in set (0.00 sec)
  
  # 创建外键不成功
  mysql> create table staff_info (s_id int,name varchar(20),dep_id int,foreign key(dep_id) references departments(dep_id));
  ERROR 1215 (HY000): Cannot add foreign key 
  
  # 设置dep_id非空，仍然不能成功创建外键
  mysql> alter table departments modify dep_id int(4) not null;
  Query OK, 0 rows affected (0.02 sec)
  Records: 0  Duplicates: 0  Warnings: 0
  
  mysql> desc departments;
  +----------+-------------+------+-----+---------+-------+
  | Field    | Type        | Null | Key | Default | Extra |
  +----------+-------------+------+-----+---------+-------+
  | dep_id   | int(4)      | NO   |     | NULL    |       |
  | dep_name | varchar(11) | YES  |     | NULL    |       |
  +----------+-------------+------+-----+---------+-------+
  2 rows in set (0.00 sec)
  
  mysql> create table staff_info (s_id int,name varchar(20),dep_id int,foreign key(dep_id) references departments(dep_id));
  ERROR 1215 (HY000): Cannot add foreign key constraint
  
  # 当设置字段为unique唯一字段时，设置该字段为外键成功
  mysql> alter table departments modify dep_id int(4) unique;
  Query OK, 0 rows affected (0.01 sec)
  Records: 0  Duplicates: 0  Warnings: 0
  
  mysql> desc departments;                                                                                                       +----------+-------------+------+-----+---------+-------+
  | Field    | Type        | Null | Key | Default | Extra |
  +----------+-------------+------+-----+---------+-------+
  | dep_id   | int(4)      | YES  | UNI | NULL    |       |
  | dep_name | varchar(11) | YES  |     | NULL    |       |
  +----------+-------------+------+-----+---------+-------+
  2 rows in set (0.01 sec)
  
  mysql> create table staff_info (s_id int,name varchar(20),dep_id int,foreign key(dep_id) references departments(dep_id));
  Query OK, 0 rows affected (0.02 sec)
  ```

  

  外键操作示例

  ```
  #表类型必须是innodb存储引擎，且被关联的字段，即references指定的另外一个表的字段，必须保证唯一
  create table department(
  id int primary key,
  name varchar(20) not null
  )engine=innodb;
  
  #dpt_id外键，关联父表（department主键id），同步更新，同步删除
  create table employee(
  id int primary key,
  name varchar(20) not null,
  dpt_id int,
  foreign key(dpt_id)
  references department(id)
  on delete cascade  # 级连删除
  on update cascade # 级连更新
  )engine=innodb;
  
  
  #先往父表department中插入记录
  insert into department values
  (1,'教质部'),
  (2,'技术部'),
  (3,'人力资源部');
  
  
  #再往子表employee中插入记录
  insert into employee values
  (1,'yuan',1),
  (2,'nezha',2),
  (3,'egon',2),
  (4,'alex',2),
  (5,'wusir',3),
  (6,'李沁洋',3),
  (7,'皮卡丘',3),
  (8,'程咬金',3),
  (9,'程咬银',3)
  ;
  
  
  #删父表department，子表employee中对应的记录跟着删
  mysql> delete from department where id=2;
  Query OK, 1 row affected (0.00 sec)
  
  mysql> select * from employee;
  +----+-----------+--------+
  | id | name      | dpt_id |
  +----+-----------+--------+
  |  1 | yuan      |      1 |
  |  5 | wusir     |      3 |
  |  6 | 李沁洋    |      3 |
  |  7 | 皮卡丘    |      3 |
  |  8 | 程咬金    |      3 |
  |  9 | 程咬银    |      3 |
  +----+-----------+--------+
  6 rows in set (0.00 sec)
  
  
  #更新父表department，子表employee中对应的记录跟着改
  mysql> update department set id=2 where id=3;
  Query OK, 1 row affected (0.01 sec)
  Rows matched: 1  Changed: 1  Warnings: 0
  
  mysql> select * from employee;
  +----+-----------+--------+
  | id | name      | dpt_id |
  +----+-----------+--------+
  |  1 | yuan      |      1 |
  |  5 | wusir     |      2 |
  |  6 | 李沁洋    |      2 |
  |  7 | 皮卡丘    |      2 |
  |  8 | 程咬金    |      2 |
  |  9 | 程咬银    |      2 |
  +----+-----------+--------+
  6 rows in set (0.00 sec)
  ```

  

  on delete (了解)

  ```
     . cascade方式
  在父表上update/delete记录时，同步update/delete掉子表的匹配记录 
  
     . set null方式
  在父表上update/delete记录时，将子表上匹配记录的列设为null
  要注意子表的外键列不能为not null  
  
     . No action方式
  如果子表中有匹配的记录,则不允许对父表对应候选键进行update/delete操作  
  
     . Restrict方式
  同no action, 都是立即检查外键约束
  
     . Set default方式
  父表有变更时,子表将外键列设置成一个默认的值 但Innodb不能识别
  ```

