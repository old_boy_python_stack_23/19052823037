# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-09-11 09:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0005_author'),
    ]

    operations = [
        migrations.CreateModel(
            name='Classes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('c_name', models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('s_name', models.CharField(max_length=32)),
                ('score', models.IntegerField()),
                ('my_class', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Student', to='app01.Classes')),
            ],
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('t_name', models.CharField(max_length=32)),
                ('sex', models.IntegerField(choices=[(1, '男'), (2, '女')])),
                ('age', models.IntegerField()),
            ],
        ),
        migrations.RemoveField(
            model_name='author',
            name='books',
        ),
        migrations.RemoveField(
            model_name='book',
            name='pub',
        ),
        migrations.DeleteModel(
            name='Author',
        ),
        migrations.DeleteModel(
            name='Book',
        ),
        migrations.DeleteModel(
            name='Publisher',
        ),
        migrations.AddField(
            model_name='classes',
            name='teachers',
            field=models.ManyToManyField(related_name='Classes', to='app01.Teacher'),
        ),
    ]
