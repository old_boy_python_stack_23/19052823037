# 导入与html沟通的对象
from django.shortcuts import render, HttpResponse, redirect
# 导入操作数据库的models
from app01 import models
# 导入url的CBV
from django.views import View
# 导入json字符串转化
from django.http import JsonResponse


class ListStudent(View):
    def get(self, request, *args, **kwargs):
        # 从数据库中查询出所有的学生
        all_students = models.Student.objects.all().order_by('pk')
        all_classes = models.Classes.objects.all().order_by('pk')
        # 返回一个页面  页面包含数据
        return render(request, 'student_list.html', {'all_students': all_students, 'all_classes': all_classes})

    # 新增
    def post(self, request):
        # url上携带的参数
        student_name = request.POST.get("student_name")
        score = request.POST.get("score")
        classes = request.POST.get("classes")
        print(models.Classes.objects.filter(c_name=classes).values('id').first().get("id"))
        models.Student.objects.create(s_name=student_name, score=score,
                                      my_class_id=models.Classes.objects.filter(c_name=classes).values(
                                          'id').first().get("id"))
        return redirect('/student_list/')


# 新增
class AddStudent(View):
    def get(self, request):
        all_teacher = models.Teacher.objects.all()
        all_classes = models.Classes.objects.all()
        return render(request, 'student_add.html', {"all_teacher": all_teacher, 'all_classes': all_classes})


# 给页面绑定的ajax方法的路径函数
def ajas(request):
    student_name = request.POST.get('student_name')
    score = request.POST.get('score')
    classes = request.POST.get('classes')
    print(student_name, score, classes)
    if not student_name:
        # 输入为空
        error = '输入不能为空'
    elif models.Student.objects.filter(s_name=student_name):
        # 数据库已存在该数据
        error = '数据已存在'
    else:
        # 插入数据
        models.Student.objects.create(s_name=student_name, score=score,
                                      my_class=models.Classes.objects.filter(c_name=classes).values('id'))

        # 跳转至展示页面（重定向）
        return redirect('/student_list/')
    return render(request, 'student_list.html', {'error': error})


# 删除
class DelStudent(View):
    def get(self, request):
        pk = request.GET.get('pk')
        print(pk)
        query = models.Student.objects.filter(pk=pk)  # 对象列表
        # 删除数据
        query.delete()  # 通过queryset 删除
        # query[0].delete()  # 通过单独的对象 删除
        # 跳转至展示页面
        return JsonResponse({'status': '1'})


# 编辑
class EditStudent(View):
    def get(self, request):
        all_classes = models.Classes.objects.all()
        pk = request.GET.get('pk')  # url上携带的参数
        obj = models.Student.objects.filter(pk=pk).first()
        print(obj.my_class.c_name)
        if not obj:
            return HttpResponse('要编辑的对象不存在')
        return render(request, 'student_edit.html',
                      {'all_classes': all_classes, 'student_name': obj.s_name, 'score': obj.score,
                       'classes': obj.my_class.c_name})

    def post(self, request):
        pk = request.GET.get('pk')  # url上携带的参数
        obj = models.Student.objects.filter(pk=pk).first()
        student_name = request.POST.get('student_name')
        score = request.POST.get('score')
        classes = request.POST.get('classes')
        if not student_name:
            # 输入为空
            error = '输入不能为空'
        elif models.Student.objects.filter(s_name=student_name):
            # 数据库已存在该数据
            error = '数据已存在'
        else:
            # 编辑原始的对象
            obj.s_name = student_name
            obj.score = score
            print(models.Classes.objects.filter(c_name=classes).first().c_name)
            obj.my_class = models.Classes.objects.filter(c_name=classes).first()
            obj.save()
            # 跳转至展示页面
            return redirect('/student_list/')
        return render(request, 'student_edit.html', {'obj': obj, 'error': error})
