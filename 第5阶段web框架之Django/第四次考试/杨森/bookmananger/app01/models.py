from django.db import models


class Student(models.Model):
    s_name = models.CharField(max_length=32)
    score = models.IntegerField()
    my_class = models.ForeignKey('Classes', related_name='Student')


class Teacher(models.Model):
    t_name = models.CharField(max_length=32)
    sex = models.IntegerField(choices=((1, '男'), (2, '女')))
    age = models.IntegerField()

    def __repr__(self):
        return self.t_name

    __str__ = __repr__


class Classes(models.Model):
    c_name = models.CharField(max_length=32)
    teachers = models.ManyToManyField('Teacher', related_name='Classes')
