import os
import django
from app01 import models

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "day62.settings")
django.setup()

# 查找所有书名里包含金老板的书
ret = models.Book.objects.filter(title__contains="金老板")
# select title from app01_book where title like '%金老板%' ;
# 查找出版日期是2018年的书
ret = models.Book.objects.filter(publish_date__year=2018)
# select title from app01_book where year(publish_date) = '2018';
# 查找出版日期是2017年的书名
ret = models.Book.objects.filter(publish_date__year=2017).first().title
# select title from app01_book where year(publish_date) = '2017';
# 查找价格大于10元的书
ret = models.Book.objects.filter(price__gt=10)
# select title from app01_book where price > 10;
# 查找价格大于10元的书名和价格
ret = models.Book.objects.filter(price__gt=10).values('title', 'price')
# select title, price from app01_book where price > 10;
# 查找memo字段是空的书
ret = models.Book.objects.filter(memo__isnull=True)
# select * from app01_book where memo = 'null';
# 查找在北京的出版社
ret = models.Publisher.objects.filter(city='北京')
# select * from app01_publisher where city='北京';
# 查找名字以沙河开头的出版社
ret = models.Publisher.objects.filter(name__startswith='沙河')
# select * from app01_publisher where name like '沙河%';
# 查找“沙河出版社”出版的所有书籍
ret = models.Book.objects.filter(publisher__name='沙河出版社')
# select * from app01_book inner join app01_publisher on app01_book.publisher_id = app01_publisher.id where publisher.name='沙河出版社';
# 查找每个出版社出版价格最高的书籍价格
obj = models.Publisher.objects.all()
for i in obj:
    print(i, i.book_set.values('price').order_by('-price').first())
# select price from app01_publisher inner join app01_book on app01_publisher.id = app01_book.publisher_id having order by price group by app01_publisher.id;
# 查找每个出版社的名字以及出的书籍数量
obj = models.Publisher.objects.all()
for i in obj:
    print(i.name, i.book_set.all().count())
# select name,count(app01_publisher.id) from app01_publisher inner join app01_book on app01_publisher.id = app01_book.publisher_id group by app01_publisher.id;
# 查找作者名字里面带“小”字的作者
ret = models.Author.objects.filter(name__contains='小')
# select * from app01_author where name like '%小%';
# 查找年龄大于30岁的作者
ret = models.Author.objects.filter(age__gt=30)
# select * from app01_author where age>30;
# 查找手机号是155开头的作者
ret = models.Author.objects.filter(phone__startswith='155')
# select * from app01_author where phone like '155%';
# 查找手机号是155开头的作者的姓名和年龄
ret = models.Author.objects.filter(phone__startswith='155').values('name', 'age')
# select name,age from app01_author where phone like '155%';
# 查找每个作者写的价格最高的书籍价格
obj = models.Author.objects.all()
for i in obj:
    print(i, i.book_set.values('price').order_by('-price').first())
# select price from app01_book as a inner join app01_book_aothor as b_a on b.id = b_a.book_id where order by price order by b_a.author_id;
# 查找每个作者的姓名以及出的书籍数量
obj = models.Author.objects.all()
for i in obj:
    print(i.name, i.book_set.all().count())
# select name,count(book_id) from app01_book_author as b_a inner join app01_author on b_a.author_id = a.id where group by name;
# 查找书名是“跟金老板学开车”的书的出版社
ret = models.Book.objects.filter(title='跟金老板学开车')
for i in ret:
    print(i.publisher)
# select * from app01_publisher as a inner join app01_book as b on a.id = b.publisher_id where title='跟金老板学开车';
# 查找书名是“跟金老板学开车”的书的出版社所在的城市
ret = models.Book.objects.filter(title='跟金老板学开车')
for i in ret:
    print(i.publisher.city)
# select city from app01_publisher as a inner join app01_book as b on a.id = b.publisher_id where title='跟金老板学开车';
# 查找书名是“跟金老板学开车”的书的出版社的名称
ret = models.Book.objects.filter(title='跟金老板学开车')
for i in ret:
    print(i.publisher.name)
# select name from app01_publisher as a inner join app01_book as b on a.id = b.publisher_id where title='跟金老板学开车';
# 查找书名是“跟金老板学开车”的书的出版社出版的其他书籍的名字和价格
ret = models.Book.objects.filter(title='跟金老板学开车')
for i in ret:
    print(i.publisher.book_set.values('title', 'price').exclude(title='跟金老板学开车'))
# select title,price from app01_publisher as a inner join app01_book as b on a.id = b.publisher_id where title!='跟金老板学开车';
# 查找书名是“跟金老板学开车”的书的所有作者
ret = models.Book.objects.filter(title='跟金老板学开车').first().author.all()
print(ret)
# select * from (select author_id from app01_book as a inner join app01_book_author as b on a.id = b.book_id where a.name='跟金老板学开车') as a left join app01_author;
# 查找书名是“跟金老板学开车”的书的作者的年龄
ret = models.Book.objects.filter(title='跟金老板学开车').first().author.all()
for i in ret:
    print(i.name)
# select age from (select author_id from app01_book as a inner join app01_book_author as b on a.id = b.book_id where a.name='跟金老板学开车') as a left join app01_author;
# 查找书名是“跟金老板学开车”的书的作者的手机号码
ret = models.Book.objects.filter(title='跟金老板学开车').first().author.all()
for i in ret:
    print(i.phone)
# select phone from (select author_id from app01_book as a inner join app01_book_author as b on a.id = b.book_id where a.name='跟金老板学开车') as a left join app01_author;
# 查找书名是“跟金老板学开车”的书的作者们的姓名以及出版的所有书籍名称和价钱
ret = models.Book.objects.filter(title='跟金老板学开车').first().author.all()
for i in ret:
    print(i.name, i.book_set.values('title', 'price'))
# select group_concat(name),title,price from (select title,book_id,author_id,price from (select id, title,price from app01_book) as t right join app01_book_author on t.id =
# app01_book_author.book_id) as f left join app01_author on f.author_id = app01_author.id where title='跟金老板学开车'group by price;
