## 内容回顾

### 路由系统

#### urlconf

```python
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^publisher_list/',views.publisher_list ,name='publisher'),
]
```

####  正则表达式

/斜杠不用加  

从上到下匹配 匹配到一个就执行函数 $加上

r 表示原生字符串

^ $   [jqjsbnaqw]{4}  [0-9a-zA-z]   \d    \w  ?    +  *  .

#### 分组和命名分组

```python
 url(r'^(publisher)_list/',views.publisher_list ,name='publisher'),
```

将捕获的参数按照 位置参数  传递给视图

```python
 url(r'^(?P<xxx>publisher)_list/',views.publisher_list ,name='publisher'),
```

将捕获的参数按照 关键字参数 传递给视图

#### 路由的分发  include 

```python
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls', namespace='app01')),
    url(r'^app02/', include('app02.urls', namespace='app02')),
]
```

#### url的命名和反向解析

静态路由

```python
 url(r'^publisher_list/',views.publisher_list ,name='publisher'),
```

模板：

​	{%  url  'publisher' %}     ——》   ‘/app01/publisher_list/’

py文件

​	from django.urls  import reverse

​	reverse('publisher')     ——》   ‘/app01/publisher_list/’

分组

```python
 url(r'^(publisher)_list/',views.publisher_list ,name='publisher'),
```

模板：

​	{%  url  'publisher' 'publisher' %}     ——》   ‘/app01/publisher_list/’

py文件

​	from django.urls  import reverse

​	reverse('publisher',args=('publisher',))     ——》   ‘/app01/publisher_list/’

命名分组

```python
 url(r'^(?P<table>publisher)_list/',views.publisher_list ,name='publisher'),
```

模板：

​	{%  url  'publisher' 'publisher' %}     ——》   ‘/app01/publisher_list/’

​	{%  url  'publisher' table='publisher' %}     ——》   ‘/app01/publisher_list/’

py文件

​	from django.urls  import reverse

​	reverse('publisher',args=('publisher',))     ——》   ‘/app01/publisher_list/’

​	reverse('publisher',kwargs={'table':'publisher'})     ——》   ‘/app01/publisher_list/’

redirect(revere('name'))   redirect(name)

#### namespace

```python
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls', namespace='app01')),
    url(r'^app02/', include('app02.urls', namespace='app02')),
]
{%  url  'app01:publisher' 'publisher' %} 
reverse('app01:publisher',args=('publisher',))    
```

## ORM 

### ORM常用字段  和 字段的参数

```
AutoField  # 主键
CharField  # 字符串
TextField  # 大字符串
IntegerField  # 整形
DateTimeField   DateField   # 日期 日期时间
BooleanField  # 布尔
DecimalField  max_digits=5  decimal_places=2   999.99
```

### 字段的参数

null=True   数据库该字段可以为空    

blank=True   校验时可以为空

default   默认值

unique  唯一索引

verbose_name   显示的名称

choices=((True, '男'), (False, '女'))    可选择的参数

### 表的参数

```python
class Person(models.Model):
    pid = models.AutoField(primary_key=True)  # 主键
    name = models.CharField(max_length=32, db_column='username', unique=True, verbose_name='姓名',
                            help_text='填写有效姓名')  # varchar(32)
    age = models.IntegerField(null=True, blank=True)
    birth = models.DateTimeField(auto_now=True)
    phone = MyCharField(max_length=11, null=True, blank=True)
    gender = models.BooleanField(default=True, choices=((True, '男'), (False, '女')))

    class Meta:
        # 数据库中生成的表名称 默认 app名称 + 下划线 + 类名
        db_table = "person"

        # admin中显示的表名称
        verbose_name = '个人信息'

        # verbose_name加s
        verbose_name_plural = '所有用户信息'

        # 联合索引
        index_together = [
            ("name", "age"),  # 应为两个存在的字段
        ]

        # 联合唯一索引
        unique_together = (("name", "age"),)  # 应为两个存在的字段
```

### admin

1. 创建一个超级用户

   python36 manage.py createsuperuser

   输入用户名和密码

2. 注册

   在app下的admin.py中写

   ```python
   from django.contrib import admin
   from app01 import models
   
   admin.site.register(models.Person)
   ```

3. 登陆网页<http://127.0.0.1:8000/admin/>
4. 找到对应的表做增删改查

### 必知必会13条

```python
<1> all():                 查询所有结果

<2> get(**kwargs):         返回与所给筛选条件相匹配的对象，返回结果有且只有一个，如果符合筛选条件的对象超过一个或者没有都会抛出错误。
 
<3> filter(**kwargs):      它包含了与所给筛选条件相匹配的对象
 
<4> exclude(**kwargs):     它包含了与所给筛选条件不匹配的对象
 
<5> values(*field):        返回一个ValueQuerySet——一个特殊的QuerySet，运行后得到的并不是一系列model的实例化对象，而是一个可迭代的字典序列
 
<6> values_list(*field):   它与values()非常相似，它返回的是一个元组序列，values返回的是一个字典序列
 
<7> order_by(*field):      对查询结果排序
 
<8> reverse():             对查询结果反向排序，请注意reverse()通常只能在具有已定义顺序的QuerySet上调用(在model类的Meta中指定ordering或调用order_by()方法)。
 
<9> distinct():            从返回结果中剔除重复纪录(如果你查询跨越多个表，可能在计算QuerySet时得到重复的结果。此时可以使用distinct()，注意只有在PostgreSQL中支持按字段去重。)
 
<10> count():              返回数据库中匹配查询(QuerySet)的对象数量。
 
<11> first():              返回第一条记录
 
<12> last():               返回最后一条记录
 
<13> exists():             如果QuerySet包含数据，就返回True，否则返回False
```



### 单表的双下划线

```python
ret = models.Person.objects.filter(pk__gt=1)  # greater than
ret = models.Person.objects.filter(pk__lt=5)  # less than
ret = models.Person.objects.filter(pk__gte=1)  # greater than equal
ret = models.Person.objects.filter(pk__lte=5)  # less than  equal

ret = models.Person.objects.filter(pk__range=[1,5])  # 范围

ret = models.Person.objects.filter(pk__in=[1,5,7,9])  # 范围

ret = models.Person.objects.filter(name__contains='alex')  # like
ret = models.Person.objects.filter(name__icontains='aLex')  # like ignore 忽略大小写

ret = models.Person.objects.filter(name__startswith='a')  # 以什么开头
ret = models.Person.objects.filter(name__istartswith='a')  # 以什么开头 gnore 忽略大小写

ret = models.Person.objects.filter(name__endswith='x')  # 以什么结尾
ret = models.Person.objects. filter(name__iendswith='a')  # 以什么结尾 gnore 忽略大小写


ret = models.Person.objects.filter(birth__year='2020')
ret = models.Person.objects.filter(birth__contains='2020-10')


ret = models.Person.objects.filter(age__isnull=False)
```

