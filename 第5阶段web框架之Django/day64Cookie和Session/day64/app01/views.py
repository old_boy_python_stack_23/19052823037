from django.shortcuts import render, HttpResponse, redirect


# Create your views here.
def login_required(func):
    def inner(request, *args, **kwargs):
        # 读取cookie
        is_login = request.COOKIES.get('is_login')
        # 读取session
        # is_login = request.session.get('is_login')
        print(is_login)
        print(request.path_info)


        if is_login != '1':
            return redirect('/login/?returnurl={}'.format(request.path_info))
        ret = func(request, *args, **kwargs)
        return ret

    return inner


def login(request):
    # print(request.method)
    if request.method == 'POST':
        user = request.POST.get('user')
        pwd = request.POST.get('pwd')
        if user == 'alex' and pwd == '123':
            returnurl = request.GET.get('returnurl')
            # print(returnurl)
            if returnurl:
                ret = redirect(returnurl)
            else:
                ret = redirect('/home/')
            # 设置cookie
            ret.set_cookie('is_login', '1')
            # 设置session
            request.session['is_login'] = '1'
            return ret
            # return render(request, "{}.html".format(ret))
        return render(request, 'login.html', {'error': '用户名或密码错误'})

    return render(request, "login.html")


@login_required
def home(request):
    return HttpResponse('home ok')


@login_required
def index(request):
    return HttpResponse('index ok')


def logout(request):
    ret = ""
    return ret
