import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "day63.settings")

import django

django.setup()

from app01 import models

# book_obj = models.Book.objects.get(pk=5)
# print(book_obj.publisher)
# print(book_obj.publisher.name)
# print(book_obj.publisher.book_set.all())
author_obj = models.Author.objects.get(pk=3)

print(author_obj.books, type(author_obj.books))
print(author_obj.books.filter(pk=4).first().title)
