## 内容回顾

### cookie和session

#### cookie

1.定义

​	cookie是保存在浏览器上的一组组键值对

2.为什么要有cookie？

​	http协议是无状态。每次的请求相对独立， 之间没有关系。没有办法保存状态。

3.特性

1. 服务器让浏览器进行设置的，浏览器也有权不设置
2. 保存在浏览器本地
3. 下次访问时自动携带相应的cookie

4.django中的操作

1. 获取cookie    请求头 cookie

   request.COOKIES  {} 

   request.COOKIES[key]   request.COOKIES.get(key)

   request.get_signed_cookie(key,salt='xxxx',default='')

2. 设置cookie   set-cookie 响应头

   response.set_cookie(key,value,max_age=10,)

   response.set_signed_cookie(key,value,max_age=10,salt='xxxx')

3. 删除

   response.delete_cookie(key,)

#### session

1.定义：

​	session是保存在服务器上的一组组键值对，必须依赖cookie

2.为什么要有session？

1. cookie保存在浏览器本地，不安全
2. 浏览器对cookie的大小有一定的限制

3.django中操作：

1. 设置

   request.session[key] = value

2. 获取

   request.session[key]  request.session.get(key)

3. 删除

   del request.session[key]   

   request.session.pop(key)

   request.session.delete()  # 删除所有的session数据，不删除对应cookie

   request.session.flush()     # 删除所有的session数据，删除对应cookie

4. 其他的方法

   request.session.set_expiry(value)   # 设置超时时间

   request.session.clear_expired()   #  清除已经失效的session数据

4.配置

​	from django.conf import global_settings

​	SESSION_COOKIE_NAME = 'sessionid'    #  cookie的名字

​	SESSION_COOKIE_AGE = 60 * 60 * 24 * 7 * 2    # 超时时间   默认两周

​	SESSION_SAVE_EVERY_REQUEST = True    # 每次访问都保存session数据

​	SESSION_EXPIRE_AT_BROWSER_CLOSE = True     # 关闭浏览器cookie失效

​	SESSION_ENGINE = 'django.contrib.sessions.backends.db'   # 引擎   默认保存在数据库  缓存 缓存+数据库  文件 加密cookie





## 中间件

### django的中间件

处理django的请求和响应的框架级别的钩子。

django的中间件就是一个类，五个方法。

5个方法   4个特点

### process_request(self,request):

执行时间：

​	在执行视图函数之前,也在路由匹配之前

参数：

​	request：  请求对象   和视图是同一个	

执行的顺序：

​	按照中间件的注册顺序 顺序执行 

返回值：

​	None ：   正常流程  

​	HttpResponse：当前中间件之后的中间件的process_request、路由匹配、视图函数都不执行，直接执行当前中间的process_response的方法，倒序执行之前的process_response的方法，最终返回给浏览器

### process_response(self, request, response)

执行时间：

​	在执行视图函数之后

参数：

​	request：  请求对象   和视图是同一个

​	response:  返回的response对象

执行的顺序：

​	按照中间件的注册顺序  倒序执行 

返回值：

​	HttpResponse：必须返回response对象

### process_view(self, request, view_func, view_args, view_kwargs)

执行时间：

​	在执行视图函数之前，在路由匹配之后

参数：

​	request：  请求对象   和视图是同一个

​	view_func： 视图函数

​	view_args： 传递给视图函数的位置参数    分组的参数

​	view_kwargs： 传递给视图函数的关键字参数    命名分组的参数

执行的顺序：

​	按照中间件的注册顺序  顺序执行 

返回值：

​	None : 正常流程 

​	HttpResponse：当前中间件之后的中间件的process_view、视图函数都不执行，直接执行最后一个中间的process_response的方法，倒序执行之前的process_response的方法，最终返回给浏览器

### process_exception(self, request, exception)

执行时间（触发条件）：

​	视图层面有异常才执行

参数：

​	request：  请求对象   和视图是同一个

​	exception：  错误对象

执行的顺序：

​	按照中间件的注册顺序  倒序执行 

返回值：

​	None : 交给下一个中间件处理异常，所有的中间件都没有处理，交给django处理

​	HttpResponse：当前中间件之前的中间件的process_exception不执行，直接执行最后一个中间的process_response的方法，倒序执行之前的process_response的方法，最终返回给浏览器

### process_template_response(self,request,response):

执行时间（触发条件）：

​	视图返回的response 是一个template_response对象

参数：

​	request：  请求对象   和视图是同一个

​	response：  响应对象

执行的顺序：

​	按照中间件的注册顺序  倒序执行 

返回值：

​	HttpResponse：必须返回