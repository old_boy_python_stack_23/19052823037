from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import HttpResponse


class MD1(MiddlewareMixin):
    def process_request(self, request):
        print('MD1 process_request')
        print('MD1', id(request))
        # return HttpResponse('MD1')

    def process_response(self, request, response):
        print("MD1 process_response")
        print("MD1 ppp", request, response)
        return response


class MD2(MiddlewareMixin):
    def process_request(self, request):
        print('MD2 process_request')

    def process_response(self, request, response):
        print('MD2 process_response')
