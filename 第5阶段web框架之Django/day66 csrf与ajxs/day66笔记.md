## 内容回顾

### 中间件

django的中间件是一个处理django的请求和响应的框架级别的钩子。

五种方法   4个特征

#### process_request(self,request)

执行时间： 路由匹配之前

执行顺序： 按照中间件的注册顺序 顺序执行

返回值：

​	None  正常流程

​	HttpResponse:当前中间之后的process_request、路由匹配、process_view、视图都不执行，直接执行当前中间件的process_response方法，倒叙执行之前中间件的process_response方法。



#### process_view(self,request,func,args,kwargs)

执行时间： 路由匹配之后，视图函数之前

执行顺序： 按照中间件的注册顺序 顺序执行

返回值：

​	None  正常流程

​	HttpResponse:当前中间之后的process_view、视图都不执行，直接执行最后一个中间件的process_response方法，倒叙执行之前中间件的process_response方法。



#### process_response(self,request,response)

执行时间： 视图函数之后

执行顺序： 按照中间件的注册顺序 倒叙执行

返回值：

​	HttpResponse:必须返回



#### process_exception(self,request,exception)

执行时间： 视图函数出现错误

执行顺序： 按照中间件的注册顺序 倒叙执行

返回值：

​	None 当前中间不处理这个错误，交给下一个中间件处理，所有都没有处理，交由django处理

​	HttpResponse:当前的中间件处理了错误，执行最后一个中间件中的process_response。



#### process_template_response(self,request,response)

执行时间：视图函数返回一个template_response对象活者response对象有一个render 

执行顺序： 按照中间件的注册顺序 倒叙执行

返回值：

​	HttpResponse:必须返回

django的请求的生命周期

作业：

限制访问频率

5秒只能访问3次

访问记录   5秒内访问的次数

## 今日内容

### csrf  ：

**CSRF（跨站请求伪造）**

- CSRF 英文全称为 Cross SIte Request Forgery
- CSRF 通常指恶意攻击者盗用用户的名义，发送恶意请求，严重泄露个人信息危害财产的安全

CSRF攻击示意图''

![1568013688377](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1568013688377.png)

解决CSRF攻击

使用csrf_token校验

1.客户端和浏览器向后端发送请求时，后端往往会在响应中的 cookie 设置 csrf_token 的值，可以使用请求钩子实现，在cookie中设置csrf_token

```python
from flask_wtf.csrf import generate_csrf
@app.affter_request
def after_request(resp)
```



2.flask_wtf 中为我们提供了CSRF保护，可以直接调用开启对app的保护



一旦开启CSRF保护，就要设置秘钥：SECRET_KEY



csrf验证

1.表单提交方式

.服务器通过请求钩子在cookie中设置了csrf_token,实际上是在session中存储了未加密的csrf_token，并且将生成的sessionID编号存储在cookie中
在表单中我们添加了csrf的隐藏字段，在浏览器再次访问服务器时：
1.获取到表单中的csrf_token(加密的)，使用secret_key进行解密，得到解密后的csrf_token

2.通过cookie中的sessionID，取到服务器内部存储的session中的csrf_token(未加密的)

3.将两者的值进行比较

 

2.ajax提交请求方式

在js里面，获取到cookie中的csrf_token,将其添加到ajax的请求头中


验证过程：

1.获取请求头中的csrf_token（加密的），然后使用secret_key解密，得到解密后csrf_token。

2.通过cookie中的sessionID，取到服务器内部存储的session中的csrf_token(未加密的)。

3.将两者的值进行比较



### 1.csrf相关的装饰器

```
from django.views.decorators.csrf import csrf_exempt, csrf_protect
# csrf_exempt  豁免csrf校验
# csrf_protect 强制进行csrf校验
csrf_exempt要加在CBV上，只能加dispatch上
```

### 2.csrf的中间件

1.想要能通过csrf校验的前提条件   必须要有csrftoken 的cookie

 1. {% csrf_token %}
 2. from django.views.decorators.csrf import ensure_csrf_cookie

2.从cookie获取csrftoken的值 与 POST提交的数据中的csrfmiddlewaretoken的值做对比 

如果从request.POST中获取不到csrfmiddlewaretoken的值，会尝试从请求头中获取x-csrftoken的值，并且拿这个值与csrftoken的值做对比，对比成功也能通过校验。

### 3.ajax

1. 浏览器地址栏输入地址 get
2. a标签    get
3. form表单  get/post

ajax  

js技术，给服务发送请求。

特点： 异步   传输的数据量小   局部刷新

发送请求：

```html
// 发ajax请求
$.ajax({
    url: '/calc/',    #  发送请求的地址
    type: 'post',    # 请求方式
    data: {			# 发送的数据
        'x1': $('[name="i1"]').val(),
        'x2': $('[name="i2"]').val(),
    },
    success: function (res) {   # 成功响应后回调函数   res  相应体中的数据
        $('[name="i3"]').val(res)
    }

})


$.ajax({
    url: '/test/',
    type: 'post',
    data: {
         'name': 'alex',
         'age': 84,
         'hobby': JSON.stringify(['抽烟', '喝酒', '大保健'])
    },
    success: function (res) {
         console.log(res)
         {#res = JSON.parse(res)#}
         console.log(res.status)
    },
   error:function (res) {
         console.log(res)
   }
})
```

### 4.使用ajax上传文件

```html
$('#b3').click(function () {

    var form = new FormData();
    form.append('filename','xxxxx')
    form.append('f1',$('#f1')[0].files[0])


    // 发ajax请求
    $.ajax({
        url: '/upload/',
        type: 'post',
        processData:false,   // 不需要处理编码方式
        contentType:false,   // 不需要处理contentType
        data:form ,
        success: function (res) {
            console.log(res)
            {# res = JSON.parse(res) #}
            console.log(res.status)
        },

    })
})
```

### 5.ajax通过django的csrf校验的方式

1. 给data中加csrfmiddlewaretoken

   ```html
   data: {
       'csrfmiddlewaretoken':$('[name="csrfmiddlewaretoken"]').val(),
       'x1': $('[name="i1"]').val(),
       'x2': $('[name="i2"]').val(),
   },
   ```

2. 加请求头  x-csrftoken

   ```
   headers:{
       'x-csrftoken':$('[name="csrfmiddlewaretoken"]').val(),
   },  
   ```

3. 导入文件

   设置全局ajax 从cookies中获取x-csrftoken值，在headers中加入值
   
   ```python
   $.ajaxSetup({
       
   })
   ```
   
   