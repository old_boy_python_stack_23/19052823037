from django.shortcuts import render, HttpResponse

# Create your views here.
from django.views import View

from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.decorators import method_decorator


# @method_decorator(csrf_exempt, name='dispatch')
# @method_decorator(csrf_protect, name='dispatch')
class Form(View):
    def get(self, request):
        return render(request, 'index.html')

    def post(self, request):
        error = ''
        return render(request, 'index.html', {'error': error})


error = '初始化'


def ajxs(request):
    return render(request, 'ajxs.html', {'error': error})


def ajas(request):
    lis = ["alex", "jacob"]
    username = request.POST.get('username')
    print(username)
    if request.POST.get('username') in lis:
        error = "用户名已存在请重新输入"
        print(error)
    else:
        error = '用户名可以使用'
    return HttpResponse(error)
