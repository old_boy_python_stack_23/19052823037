以pycharm为例

new project  

![1566809002519](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1566809002519.png)

配置settings

```python
# 前端页面文件配置
STATICFILES_DIRS = [
    os.path.join(BASE_DIR,'static')
]

# 数据库配置
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'day56',
        'HOST': '127.0.0.1',
        'PORT': 3306,
        'USER': 'root',
        'PASSWORD': 'root'
    }
}

# app注册 如果是命令行创建的项目需要注册：最后一行： 'app01.apps.App01Config',
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app01.apps.App01Config',
]

# 在设置ip为0.0.0.0时 这里设置允许链接的ip " * " 是全部允许
ALLOWED_HOSTS = []

POST请求认证忽略
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',    注销此部分，暂时的方法
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


```



url 页面配置

```python
from django.conf.urls import url
from django.contrib import admin
from app01 import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^publisher_list/', views.publisher_list),
]
```

views 流程逻辑

```python
def publisher_list(request):
    # 从数据库中查找所有的出版社
    all_publishers = models.Publisher.objects.all().order_by('pid')
    return render(request, 'publisher_list.html', {'all_publishers': all_publishers})
```

templates HTML页面

```python
新建HTML页面 文件名publisher_list.html
与url中配置的名称相同
```

models数据表的创建

```python
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return "{}{}".format(self.pid, self.name)
创建一张表 也是类，对象
```

