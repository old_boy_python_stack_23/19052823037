## 内容回顾

1. django的命令

   1. 下载

      pip install django==1.11.23 - i 

   2. 创建项目

      django-admin  startproject  项目名

   3. 启动项目

      cd  到项目的根目录

      python manage.py  runserver  # 127.0.0.1：8000 

      python manage.py  runserver  80  # 127.0.0.1：80 

      python manage.py  runserver  0.0.0.0:80  #  0.0.0.0:80

   4. 创建app

      python manage.py startapp  app名称

      注册

   5. 数据库迁移

      python manage.py  makemigrations  # 创建迁移文件 检测已经注册的APP下的models

      python manage.py migrate   # 迁移  将models的变更记录同步到数据库

2. django的配置 settings

   INSTALLED_APPS = [

   ​	'app01',

   ​	'app01.apps.App01Config'

   ]

   

   DATABASES  

   ​	ENGINE : 引擎

   ​	NAME ： 数据库的名称

   ​	HOST :  IP

   ​	PORT : 3306

   ​	USER: 用户名

   ​	PASSWORD: 密码

   静态文件

   ​	STATIC_URL  =  '/static/'

   ​	STATICFILES_DIRS=[

   ​	os.path.join(BASE_DIR，‘static’)

   ]

   中间件

   ​	注释掉csrf   提交POST

   模板  TEMPLATES

    	DIRS =[   	os.path.join(BASE_DIR，‘templates’)]  

3. django使用mysql数据库的流程

   1. 创建一个mysql数据库

   2. settings中配置数据库

      ​	ENGINE : 引擎

      ​	NAME ： 数据库的名称

      ​	HOST :  IP

      ​	PORT : 3306

      ​	USER: 用户名

      ​	PASSWORD: 密码

   3. 告诉django使用pymysql模块连接mysql数据库；

      在与settings同级目录下的`__init__.py`中写

      import pymysql

      pymysql.install_as_MySQLdb()

   4. 在app下的models.py中写类

      ```python
      class Publisher(models.Model):
          name = models.Charfield(max_length=32)   # varcher(32）
      ```

   5. 执行迁移命令：

      python manage.py  makemigrations  # 创建迁移文件 检测已经注册的APP下的models

      python manage.py migrate   # 迁移  将models的变更记录同步到数据库

4. get 和 post区别

   get

   发get请求的方式：

   1. form表单     不指定method
   2. 在地址栏中直接输入地址 回车
   3. a标题 

   ？k1=v1&k2=v2      request.GET     request.GET.get('k1')

   post

   form表单     method='post'	

   request.POST     request.POST.get('k1')

5. orm 

   面向对象和关系型数据库的一种映射

   对应关系：

   ​	类     ——》    表

   ​	对象  ——》    记录 （数据行）

   ​	属性  ——》    字段

   from app import  models

   查询：

   ​	models.Publisher.objects.get(name='xxxxx')       # 查询一个对象  有且唯一   差不多或者多个就报错

   ​	models.Publisher.objects.filter(name='xxxxx')      # 查询满足条件所有对象     对象列表  queryset  

   ​	models.Publisher.objects.all()   # 查询所有的数据  对象列表  queryset  

   新增：

   ​	models.Publisher.objects.create(name='xxxxx')   # 返回值 就是新建的对象

   ​	obj = models.Publisher(name='xxxxx')     obj.save()

   删除：

   ​	models.Publisher.objects.filter(pk=1) .delete() 

   ​	obj = models.Publisher.objects.filter(pk=1) .first()   obj.delete()

   修改：

   ​	obj = models.Publisher.objects.filter(pk=1) .first()

   ​	obj.name =  'xxxxx'

   ​	obj.save()

## 今日内容

6. 模板的语法

   render(request,‘模板的文件名’，{'k1':v1})

   变量

   {{  k1 }}

   for 循环 

   {%  for i in  k1 %}

   ​	{{  forloop.counter }}

   ​	{{ i }}

   {% endfor %}

   

   外键

   一对多 

   图书管理系统

   出版社

   书

   外键的设计

   ```python
   class Book(models.Model):
       title = models.CharField(max_length=32)
       pid = models.ForeignKey('Publisher', on_delete=models.CASCADE) # 外键 
       #  on_delete  2.0 必填 
      
   ```

查询：

```
all_books = models.Book.objects.all()
print(all_books)
for book in all_books:
    print(book)
    print(book.pk)
    print(book.title)
    print(book.pub,type(book.pub))  # 所关联的对象 
    print(book.pub_id,type(book.pub_id))  # 所关联的对象的pk
    print('*' * 32)
```

新增

```
models.Book.objects.create(title=title,pub=models.Publisher.objects.get(pk=pub_id))
models.Book.objects.create(title=title, pub_id=pub_id)

```

删除：

```
pk = request.GET.get('pk')
models.Book.objects.filter(pk=pk).delete()
```

编辑：

```
book_obj.title = title
book_obj.pub_id = pub_id
# book_obj.pub = models.Publisher.objects.get(pk=pub_id)
book_obj.save()
```
