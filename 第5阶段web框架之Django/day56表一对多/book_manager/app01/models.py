from django.db import models


# Create your models here.
# 出版社
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32, unique=True)

    # 如果不执行__str__ 那么 获得的结果是对象
    def __str__(self):
        return "{}{}".format(self.pid, self.name)

    #
    __repr__ = __str__


class Book(models.Model):
    title = models.CharField(max_length=32)
    pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)
