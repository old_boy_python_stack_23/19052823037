##  内容回顾

### 1.json

数据交换的格式

python

支持的数据类型：

​	数字、字符串、列表、字典、布尔、None

方法：

序列化：json.dumps(python的数据类型)

反序列化：json.loads（json的字符串）

js

支持的数据类型：

​	数字、字符串、数组、对象、布尔、null

方法：

序列化：JSON.stringify(js的数据类型)

反序列化：JSON.parse(json的字符串)

返回json的字符串：

JsonResponse({})     JsonResponse([],safe=False)  

### 2.发请求的方式

 1. 地址栏输入地址  get 

 2. a标签  get 

 3. form表单
     	1. action=''    method='post'
     	2. input标签要有name  value 
     	3. 有一个button按钮  或者 input的类型为submit

 4. ajax

    使用js技术给服务器发请求。

    特点：  异步、局部刷新 、传输的数据量小

### 3.使用ajax

jq的用法：

```html
$.ajax({
	url:'请求的地址'，
	type：'请求方法'，   post
	data:{
		k1:v1,
	},	
	success:function（data）{
	}
})
```

### 4.使用ajax上传文件

```
var form= new FormData()   # multipart/form-data
form.append('k1','v1')
form.append('f1',$('#f1')[0].files[0])


$.ajax({
	url:'请求的地址'，
	type：'请求方法'，   post
 	processData: false,   # 不用处理编码格式
	contentType:false,    # 不要处理contentType的请求头
	data:form,	
	success:function（data）{
	}
})
```

### 5.使用ajax通过django的csrf的校验

1. data中加上csrfmiddlewaretoken键值对  
2. 给请求头中加x-csrftoken键值对
3. 引入文件

```
function f() {

}
() => {}  //  不会改变this的指向 
```

## django  form组件

### 使用

视图：

```python
from django import forms
class RegForm(forms.Form):
    user = forms.CharField(label='用户名')
    pwd = forms.CharField(label='密码',widget=forms.PasswordInput)
    gender = forms.ChoiceField(choices=((1,'男'),(2,'女'),(3,'alex')))
    hobby = forms.MultipleChoiceField(choices=((1, "篮球"), (2, "足球"), (3, "双色球"),))
    birth =forms.DateTimeField()

def reg2(request):
    form_obj = RegForm()

    if request.method =='POST':
        form_obj = RegForm(request.POST)
        if form_obj.is_valid():  # 做校验
            # 插入数据库
            return HttpResponse('ok')


    return render(request, 'reg2.html', {'form_obj': form_obj})
```



模板：

```
{{ form_obj.as_p }}    # 展示所有的字段

{{ form_obj.user }}   # input框
{{ form_obj.user.label }}   # label标签的中文提示
{{ form_obj.user.id_for_label }}  # input框的id
{{ form_obj.user.errors  }}    # 一个字段的错误信息
{{ form_obj.user.errors.0  }}   # 一个字段的第一个错误信息
{{ form_obj.errors  }}   # 所有字段的错误
```

### 常用的字段

```
CharField    # input
ChoiceField  #  select
MultipleChoiceField   # 多选

ModelMultipleChoiceField  # 可选数据放入数据库

```

### 字段参数

```python
initial  初始值
label    中文提示
error_messages   错误提示
min_length    最小长度
choices  可选择的值
widget   插件 

```

### 校验

```
required
min_length 
```

### 自定义的校验

1.写函数

```
from django.core.exceptions import ValidationError

def check_user(value):
	# 通过校验   不做任何操作
	# 不通过校验  抛出ValidationError异常 
    if 'alex' in value:
        raise ValidationError('alex 不配！！ ')
```

2.使用内置的校验器

```
from django.core.validators import validate_email, RegexValidator

phone = forms.CharField(validators=[RegexValidator(r'^1[3-9]\d{9}$', '手机号格式不正确')])
```



1. is_valid()中要执行full_clean()：

   1. self._errors ={}    定义一个存放错误信息的字典   

   2. self.cleaned_data = {}   #  定义一个存放有效的数据

2. 执行self._clean_fields()

   1. 先执行内置的校验和校验器的校验
   2. 有局部钩子，执行局部钩子

3. 执行 self.clean() 全局钩子

### 局部钩子和全局钩子

```python
def clean_gender(self):
    v = self.cleaned_data.get('gender')
    # 局部钩子
    # 不通过校验 抛出异常
    # 通过校验   必须返回当前字段的值

def clean(self):
    # 全局钩子
    # 不通过校验 抛出异常
    # 通过校验   必须返回所有字段的值 self.cleaned_data
    pwd = self.cleaned_data.get('pwd')
    re_pwd = self.cleaned_data.get('re_pwd')
    if pwd != re_pwd:
        self.add_error('re_pwd','两次密码不一致!')
        raise ValidationError('两次密码不一致')
    return self.cleaned_data
```

