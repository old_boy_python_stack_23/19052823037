from django.shortcuts import render, HttpResponse

# Create your views here.
# def index(request):
#     user, user_error = '', ''
#     if request.method == "POST":
#         user = request.POST.get('username')
#         pwd = request.POST.get('pwd')
#         # 对数据进行校验
#         if len(user) <= 6:
#             user_error = '用户名格式错误'
#
#         # 校验成功 保存到数据库
#         # 不成功 返回填写数据 错误提示
#     print(request)
#     print(request.POST)
#     print(request.POST.get("pwd"))
#
#     return render(request, 'index.html', {"user_error", user_error})


from django import forms
from app01 import models
from django.core.exceptions import ValidationError
from django.core.validators import validate_email, RegexValidator


def check_error(value):
    if "alex" in value:
        raise ValidationError('alex 不配！')


class RegForm(forms.Form):
    user = forms.CharField(label='用户名', initial='张三', validators=[check_error])
    pwd = forms.CharField(
        label='密码',
        widget=forms.PasswordInput,
        min_length=6,
        validators=[
            RegexValidator(
                r'(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[$@!%*#?&])[A-Za-z\d$@!%*#?&]{6,}$',
                '格式错误,最少6位，包括至少一位大写字母，一位小写字母，一个数字，一个特殊字符'
            )
        ]
    )
    # gender = forms.ChoiceField(
    #     choices=((1, '男'), (2, '女'), (3, 'alex')),
    #     widget=forms.CheckboxSelectMultiple,
    # )
    # hobby = forms.MultipleChoiceField(initial=[1, 3], choices=((1, "basketball"), (2, "football"), (3, "双色球")))


def reg(request):
    form_obj = RegForm()
    if request.method == 'POST':
        form_obj = RegForm(data=request.POST)
        if form_obj.is_valid():
            # 插入数据库
            print('插入数据库成功')
            return HttpResponse('OK')
    return render(request, 'reg.html', {'form_obj': form_obj})
