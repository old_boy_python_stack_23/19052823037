from django.shortcuts import render, HttpResponse, redirect
from app01 import models


def publisher_list(request):
    # 从数据库中查询出所有的出版社
    all_publishers = models.Publisher.objects.all().order_by('pid')

    # 返回一个页面  页面包含数据
    return render(request, 'publisher_list.html', {'all_publishers': all_publishers})


# 新增出版社
def publisher_add(request):
    if request.method == 'GET':
        return render(request, 'publisher_add.html')
    elif request.method == 'POST':
        # 获取提交的出版社的名称
        pub_name = request.POST.get('pub_name')
        if not pub_name:
            # 输入为空
            return render(request, 'publisher_add.html', {'pub_name': pub_name, 'error': '输入不能为空'})
        if models.Publisher.objects.filter(name=pub_name):
            # 数据库已存在该数据
            return render(request, 'publisher_add.html', {'pub_name': pub_name, 'error': '数据已存在'})
        # 把数据插入到数据库中
        # 方式一
        ret = models.Publisher.objects.create(name=pub_name)
        # 方式二
        # obj = models.Publisher(name=pub_name)
        # obj.save()

        # 跳转至展示页面
        return redirect('/publisher_list/')


def publisher_add(request):
    pub_name, error = '', ''

    if request.method == 'POST':
        # 获取提交的出版社的名称
        pub_name = request.POST.get('pub_name')
        if not pub_name:
            # 输入为空
            error = '输入不能为空'
        elif models.Publisher.objects.filter(name=pub_name):
            # 数据库已存在该数据
            error = '数据已存在'
        else:
            # 插入数据
            models.Publisher.objects.create(name=pub_name)
            # 跳转至展示页面
            return redirect('/publisher_list/')
    return render(request, 'publisher_add.html', {'pub_name': pub_name, 'error': error})

# 删除出版社
def publisher_del(request):
    # 删除数据
    pk = request.GET.get('pk')

    query = models.Publisher.objects.filter(pk=pk)  # 对象列表
    if not query:
        # 数据部存在
        return HttpResponse('要删除的数据不存在')
    # 删除数据
    query.delete()  # 通过queryset 删除
    # query[0].delete()  # 通过单独的对象 删除
    # 跳转至展示页面
    return redirect('/publisher_list/')


# 编辑出版社
def publisher_edit(request):
    error = ''
    # 查询要编辑的对象
    pk = request.GET.get('pk')  # url上携带的参数
    obj = models.Publisher.objects.filter(pk=pk).first()
    if not obj:
        return HttpResponse('要编辑的对象不存在')

    if request.method =='POST':
        # 获取新提交的数据
        pub_name = request.POST.get('pub_name')
        if not pub_name:
            # 输入为空
            error = '输入不能为空'
        elif models.Publisher.objects.filter(name=pub_name):
            # 数据库已存在该数据
            error = '数据已存在'
        else:
            # 编辑原始的对象
            obj.name = pub_name
            obj.save()
            # 跳转至展示页面
            return redirect('/publisher_list/')

    return render(request,'publisher_edit.html',{'obj':obj,'error':error})