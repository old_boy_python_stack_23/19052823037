## 内容回顾

1. 下载

   命令行：

   	pip  install django==1.11.23
		
   	pip  install django==1.11.23 -i  源的地址

   pycharm

   	file——》 settings  ——》解释器  ——》 点+号 ——》  输入django ——》 选择版本  ——》下载

2. 创建项目

   命令行：

   	切换到存项目的目录下 
		
   	django-admin  startproject  项目名

   pycharm：

   	file ——》 new project ——》 django  ——》 输入项目的路径  ——》 选择解释器 ——》 写一个APP的名字 ——》 create

3. 启动项目

   命令行：

   	切换进去到项目的根目录  manage.py
		
   	python manage.py  runserver   # 127.0.0.1:8000
		
   	python manage.py  runserver  80   # 127.0.0.1:80
		
   	python manage.py  runserver  0.0.0.0:80   # 0.0.0.0:80

   pycharm:

   	点绿三角 启动 确定是django项目
		
   	可配置ip和端口

4. 配置settings

   静态文件： 

   	在project根目录下创建static文件夹里面放置各种用来配置HTML页面的文件，包含css js img等
   	STATIC_URL = '/static/'
   	
   	STATICFILES_DIRS = [
   	
   		os.path.join(BASE_DIR，’static‘),
   	
   		os.path.join(BASE_DIR，’static1‘),
   	
   	]

   中间件：

   	csrf 中间件注释掉      可以提交POST请求

   INSTALLED_APPS    app相关

   数据库

   模板 DIRS 

5. APP 

   创建app

   命令行： 

   	python manage.py  startapp  app名称

   pycharm：

   	tools  ——》 run manage.py  task   ——》  startapp  app名称

   注册app

   ```python
   INSTALLED_APPS = [
    
       # 'app01',
       'app01.apps.App01Config',
   ]
   ```

6. urls.py 

   ```python
   urlpatterns = [
       url(r'^index/', views.index),
       url(r'^home/', views.home),
       url(r'^login/', views.login),
   ]
   ```

7. views.py

   ```python
   from django.shortcuts import HttpResponse, render, redirect
   
   def index(request,):
       return HttpResponse()
   
   ```

   HttpResponse('字符串')    返回字符串

   render(request,'html文件名')   返回一个html页面  

   redirect(’重定向的地址‘)  重定向

8. form表单

   1. form标签的属性  action =''   提交的地址   method=’post‘  请求方式 novalidate 不校验
   2. input标签要有name  有些需要value
   3. 有一个类型为submit的input或者 button  

9. get 和post的分别

   get  获取一个页面 

   没有请求体

   ?k1=v1&k2=v2

   django 中获取   request.GET   request.GET['k1']  request.GET.get('k1','xxxx')

   

   post 提交数据   数据隐藏

   django 中获取   request.POST request.POST['k1']  request.POST .get('k1','xxxx')

10 .django使用mysql数据库的流程：

 1. 创建一个mysql数据库；

 2. 在settings中配置：

    ENGINE :  mysql;

    NAME: 数据库的名称

    HOST：  ip ;

    PORT: 3306;

    USER : 用户名;

    PASSWORD: 密码；

	3. 告诉django使用pymysql模块连接数据库；

    在与settings同级目录下的`__init__.py`中写代码：

    	import pymysql
		
    	pymysql.install_as_MySQLdb()

4. 在app下的models.py中写类（继承models.Model）:

   ```python
   class User(models.Model):
       username = models.CharField(max_length=32)  # username  varchar(32)
       password = models.CharField(max_length=32)  # password  varchar(32)
   ```

5. 执行数据库迁移的命令

   python manage.py  makemigrations  # 记录models.py的变更记录

   python manage.py  migrate   # 迁移   将变更记录同步到数据库中

11.  ORM 

    对象关系映射 

    对应关系

    类     ——     表  Classes

    对象  ——    记录 数据行 object

    属性  ——     字段 name id等

    ORM能做的操作:

    1. 操作数据表
    2. 操作数据行

    具体操作：

    	查询:
		
    	form  app01 import models  
		
    	models.User.objects.get(username='xxx',password='xxxx')  # 获取一个对象（有且唯一） 没有或者是多个就报错了
		
    	models.User.objects.filter(username='xxx',password='xxxx')  #　获取满足条件的所有对象  queryset 对象列表   []
    

## 今日内容

图书管理系统

出版社  图书  作者 

出版社的管理

### 展示

```python
# 查询所有的数据
all_publishers = models.Publisher.objects.all()   #  对象列表

render(request,'publisher_list.html',{'all_publishers':all_publishers})

```

HTML模板的语法：

```html
{{  all_publishers }}    变量

{% for  i in  all_publishers  %}
	{{ forloop.counter }}    {{  i }}
{% endfor %}
```

### 新增

```python
# 方式一
ret = models.Publisher.objects.create(name=pub_name)
# 方式二
obj = models.Publisher(name=pub_name)
obj.save()
```

### 删除

```python
pk = request.GET.get('pk')
query = models.Publisher.objects.filter(pk=pk)  # 对象列表
query.delete()  # 通过queryset 删除
query[0].delete()  # 通过单独的对象 删除
```

### 编辑

```python
obj = models.Publisher.objects.filter(pk=pk).first()  # 对象列表中第一个对象
obj.name = pub_name  # 内存中修改
obj.save()			# 提交
```

## 项目流程处理

1. 创建项目 注意创建app 
2. 配置setting
   1. settings INSTALLED_APPS 注册app
   2. 注释中间件
   3. templates

3. 创建数据库配置数据库
   1. 地址  'HOST'：‘127.0.0.1’
   2. 端口  'POST'：3306
   3. 用户  ‘USER’：‘root’
   4. 密码  ‘PASSWORD’：‘123’
   5. __init  pymysql 模块 pymysql.install_as_MySQLdb()
   6. models  class xxxx(models.Model)

4. 配置urls.py 设置网络页面与地址
   1. urls.py 配置urlpatterns
   2. views.py 写逻辑函数 设置页面显示内容 ： 绑定 HTML

   3. 配置templates 添加HTML文件 配置文件要显示内容