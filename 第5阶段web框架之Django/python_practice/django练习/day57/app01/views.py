from django.shortcuts import render, redirect, reverse
from app01.models import Book
from django.db.models.manager import Manager
from django.db.models.query import QuerySet

# Create your views here.

def index(request):
    book = Book.objects.all()
    # print(book.filter(id=1), type(book), type(book.all()))
    return render(request, 'index.html', {'book':book})

def add_book(request):
    if request.method == "GET":
        return render(request, 'add_book.html')
    else:
        title = request.POST.get('title')
        price = request.POST.get('price')
        date = request.POST.get('date')
        Book.objects.create(title=title, price=price, date=date)
        return redirect(reverse('index'))

def delete_book(request, n):
    Book.objects.filter(id=n).delete()
    return redirect(reverse('index'))

def edit_book(request, n):
    book_obj = Book.objects.filter(id=n)
    if request.method == "GET":
        return render(request, 'edit.html', {'data':book_obj[0]})
    else:
        title = request.POST.get('title')
        price = request.POST.get('price')
        date = request.POST.get('date')
        book_obj.update(title=title, price=price, date=date)
        return redirect(reverse('index'))