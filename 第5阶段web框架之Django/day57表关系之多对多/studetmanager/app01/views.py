from django.shortcuts import render, HttpResponse, redirect
from app01 import models


def classes_list(request):
    all_classes = models.Classes.objects.all().order_by('pid')
    # print(all_classes)
    return render(request, 'classes_list.html', {'all_classes': all_classes})


def classes_add(request):
    classes_name, error = '', ''

    if request.method == 'POST':
        # 获取提交的出版社的名称
        classes_name = request.POST.get('classes_name')
        if not classes_name:
            # 输入为空
            error = '输入不能为空'
        elif models.Classes.objects.filter(classes_name=classes_name):
            # 数据库已存在该数据
            error = '数据已存在'
        else:
            # 插入数据
            models.Classes.objects.create(classes_name=classes_name)
            # 跳转至展示页面
            return redirect('/classes_list/')
    return render(request, 'classes_add.html', {'classes_name': classes_name, 'error': error})


def classes_del(request):
    pk = request.GET.get('pk')
    query = models.Classes.objects.filter(pk=pk)
    if not query:
        # 数据部存在
        return HttpResponse('要删除的数据不存在')
    # 删除数据
    query.delete()  # 通过queryset 删除
    # query[0].delete()  # 通过单独的对象 删除
    # 跳转至展示页面
    return redirect('/classes_list/')


def classes_edit(request):
    error = ''
    # 查询要编辑的对象
    pk = request.GET.get('pk')  # url上携带的参数
    obj = models.Classes.objects.filter(pk=pk).first()
    if not obj:
        return HttpResponse('要编辑的对象不存在')

    if request.method == 'POST':
        # 获取新提交的数据
        classes_name = request.POST.get('classes_name')
        if not classes_name:
            # 输入为空
            error = '输入不能为空'
        elif models.Classes.objects.filter(classes_name=classes_name):
            # 数据库已存在该数据
            error = '数据已存在'
        else:
            # 编辑原始的对象
            obj.name = classes_name
            obj.save()
            # 跳转至展示页面
            return redirect('/classes_list/')

    return render(request, 'classes_edit.html', {'obj': obj, 'error': error})

# # 展示书籍
# def book_list(request):
#     all_books = models.Book.objects.all()
#     # print(all_books)
#     # for book in all_books:
#     #     print(book)
#     #     print(book.pk)
#     #     print(book.title)
#     #     print(book.pub,type(book.pub))  # 所关联的对象
#     #     print(book.pub_id,type(book.pub_id))  # 所关联的对象的pk
#     #     print('*' * 32)
#
#     return render(request, 'book_list.html', {'all_books': all_books})
#
#
# # 添加书籍
# def book_add(request):
#     if request.method == 'POST':
#         # 获取提交的数据
#         title = request.POST.get('title')
#         pub_id = request.POST.get('pub_id')
#         # 插入数据库
#         # obj = models.Book.objects.create(title=title,pub=models.Publisher.objects.get(pk=pub_id))
#         models.Book.objects.create(title=title, pub_id=pub_id)
#
#         # 跳转到展示页面
#         return redirect('/book_list/')
#
#     # 查询所有的出版社
#     all_publishers = models.Publisher.objects.all()
#
#     return render(request, 'book_add.html', {'all_publishers': all_publishers})
#
#
# def book_del(request):
#     pk = request.GET.get('pk')
#     models.Book.objects.filter(pk=pk).delete()
#     return redirect('/book_list/')
#
#
# def book_edit(request):
#     pk = request.GET.get('pk')
#     book_obj = models.Book.objects.filter(pk=pk).first()
#     if request.method == 'POST':
#         title = request.POST.get('title')
#         pub_id = request.POST.get('pub_id')
#
#         book_obj.title = title
#         book_obj.pub_id = pub_id
#         # book_obj.pub = models.Publisher.objects.get(pk=pub_id)
#         book_obj.save()
#
#         return redirect('/book_list/')
#
#     all_publishers = models.Publisher.objects.all()
#
#     return render(request, 'book_edit.html', {'book_obj': book_obj, 'all_publishers': all_publishers})
#
#
# def author_list(request):
#     # 查询所有的作者
#     all_authors = models.Author.objects.all().order_by('id')
#
#     # for author in all_authors:
#     #     print(author)
#     #     print(author.name)
#     #     print(author.pk)
#     #     print(author.books)  # 关系管理对象
#     #     print(author.books.all())  # 所关联的所有对象
#     #     print('*' * 32)
#
#     return render(request, 'author_list.html', {'all_authors': all_authors})
#
#
# def author_add(request):
#     if request.method == 'POST':
#         name = request.POST.get('name')
#         books = request.POST.getlist('books')
#
#         # 插入数据库
#         # 新建作者
#         author_obj = models.Author.objects.create(name=name)
#         # 给作者和书籍绑定关系
#         author_obj.books.set(books)
#
#         # 跳转到展示页面
#         return redirect('/author_list/')
#     # 查询所有的书籍
#     all_books = models.Book.objects.all()
#
#     return render(request, 'author_add.html', {'all_books': all_books})
#
#
# def author_del(request):
#     pk = request.GET.get('pk')
#     models.Author.objects.filter(pk=pk).delete()
#     return redirect('/author_list/')
#
#
# def author_edit(request):
#     pk = request.GET.get('pk')
#     obj = models.Author.objects.filter(pk=pk).first()
#
#     if request.method == 'POST':
#         name = request.POST.get('name')
#         books = request.POST.getlist('books')
#         # 修改
#         obj.name = name
#         obj.save()
#         obj.books.set(books)  # 修改作者和书的多对多关系
#
#         # 跳转到展示页面
#         return redirect('/author_list/')
#
#
#
#     all_books = models.Book.objects.all()
#
#     return render(request, 'author_edit.html', {'obj': obj,'all_books':all_books})
