from django.db import models


class Classes(models.Model):
    pid = models.AutoField(primary_key=True)
    classes_name = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return "{} {}".format(self.pid, self.classes_name)

    __repr__ = __str__


# class Student(models.Model):
#     title = models.CharField(max_length=32)
#     pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)
#
#     # authors = models.ManyToManyField('Author')  # 描述多对多的关系   不生成字段  生成关系表
#
#     def __repr__(self):
#         return self.title
#
#     __str__ = __repr__
#
#
# class Teacher(models.Model):
#     name = models.CharField(max_length=32)
#     books = models.ManyToManyField('Book')  # 描述多对多的关系   不生成字段  生成关系表
