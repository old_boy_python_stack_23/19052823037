"""bookmananger URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from app01 import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^publisher_list/', views.publisher_list),
    url(r'^publisher_add/', views.publisher_add),
    url(r'^publisher_del/', views.publisher_del),
    url(r'^publisher_edit/', views.publisher_edit),
    # 展示书籍
    url(r'^book_list/', views.book_list),

    # 添加书籍
    url(r'^book_add/', views.book_add),
    # 删除书籍
    url(r'^book_del/', views.book_del),
    # 编辑书籍
    url(r'^book_edit/', views.book_edit),

    # 展示作者
    url(r'^author_list/', views.author_list),

    # 添加作者
    url(r'^author_add/', views.author_add),

    # 删除作者
    url(r'^author_del/', views.author_del),
    # 编辑作者
    url(r'^author_edit/', views.author_edit),
]
