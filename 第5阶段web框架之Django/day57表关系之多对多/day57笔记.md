## 内容回顾

django

### 1.django处理请求的一个流程

1. 在浏览器上输入地址 回车，发送一个get请求
2. wsgi模块接收请求，把请求相关的内容封装成request对象
3. 根据url地址，找到对应函数。
4. 执行函数，得到返回值。wsgi模块将httpresponse对象按照http响应的格式发送给浏览器。

### 2.发请求的途径

1. 在浏览器上输入地址 回车 get
2. form表    get/post
3. a标签    get

### 3.函数

​    request

​	request.GET       url上携带的参数   ？k1=v1&k2=v2

​	request.POST    POST请求提交的数据

​	request.method   请求方式  GET POST  

​    返回值

​	HttpResponse('xxxxxx')       返回字符串

​	render(request,'模板的文件名',{  })     返回一个完整的页面

​	redirect(要跳转到的路径)       重定向   本质  Location：/publisher_list/

### 4.外键

描述一对多的关系

publisher      book   

```python
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return "{} {}".format(self.pid, self.name)

    __repr__ = __str__


class Book(models.Model):
    title = models.CharField(max_length=32)
    pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)
```

查询：

```python
books = Book.objects.all()  # 查所有
for book in books :
    print(book)   #  一个book对象
    print(book.pk)   print(book.id)   #  主键
    print(book.title)    # 标题
    print(book.pub_id)   # 关联数据的id  
    print(book.pub)      # 关联对象
```

新增

```python
Book.objects.create(title='xxxx',pub=pub_obj)
Book.objects.create(title='xxxx',pub_id=pub_obj.pk)

obj = Book(title='xxxx',pub=pub_obj)
obj.save()

```

删除

Book.objects.filter(pk=1).delete()

Book.objects.get(pk=1).delete()

编辑

obj = Book.objects.filter(pk=1).first()

obj.title='xxxx'  

obj.pub=  pub_obj  

obj.pub_id=  id

obj.save()

### 5.模板

{{  k1 }}  

for 

{%  for  i in list  %}

​	{{   forloop.counter  }}

​	{{  i }}	

{% endfor %}

if 

{% if  条件  %}

​	xxxx

{% else %}

​	qqqq

{% endif  %}

## 今日内容

多对多

### 表结构的设计

```python
class Book(models.Model):
    title = models.CharField(max_length=32)
    pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)


class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book')  # 描述多对多的关系  不生成字段  生成关系表
```



### 查询

```python
all_authors = models.Author.objects.all().order_by('id')

for author in all_authors:
    print(author)
    print(author.name)
    print(author.pk)
    print(author.books)  # 关系管理对象
    print(author.books.all())  # 所关联的所有对象
    print('*' * 32)
```

### 新增

```python
books = request.POST.getlist('books')   # 获取多个元素

# 新建作者
author_obj = models.Author.objects.create(name=name)
# 给作者和书籍绑定关系
author_obj.books.set(books) # 【id,id】

```

### 创建多对多的表的方法

1. django通过ManyToManyField自动创建第三张表

```python
class Book(models.Model):
    title = models.CharField(max_length=32)
    pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)
    # authors = models.ManyToManyField('Author')  # 描述多对多的关系   不生成字段  生成关系表

    def __repr__(self):
        return self.title

    __str__ = __repr__

class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book')  # 描述多对多的关系   不生成字段  生成关系表
```

2. 自己手动创建

```python
class Book(models.Model):
    title = models.CharField(max_length=32)


class Author(models.Model):
    name = models.CharField(max_length=32)


class Book_Author(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    date = models.DateField()
```

3. 自己创建 + ManyToManyField

```python
class Book(models.Model):
    title = models.CharField(max_length=32)


class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField(Book, through='Book_Author')


class Book_Author(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    date = models.DateField()
```



命令行创建项目记得配置这个 搞了半小时  。。。。 

![1566915804618](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1566915804618.png)