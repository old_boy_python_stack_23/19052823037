## 内容回顾

视图

### 1.FBV   CBV

定义CBV

```python
from django.views import View
class AddPublisher(View):
    def get(self,request，*args,**kwargs):
        return  response
    def post(self,request，*args,**kwargs):
        return  response
```

使用：

url(r'add_publisher',AddPublisher.as_view())

### 2.as_view（）的流程

1.程序运行时执行类中的as_view（）  ——》 view

​	url(r'add_publisher', view )

2.请求到来时执行view函数：

 1. 实例化AddPublisher（）   —— 》  self

 2. self.request = request 

 3. 执行dispatch()方法：

     1. 判断请求方法：	http_method_names=[]			

        1. 允许

           通过反射获取到对应请求方式的方法  ——》  handler 

        2. 不允许

           http_method_not_allowed ——》handler 

        	2. 执行handler  获取到返回值 

### 3.加装饰器

FBV

```python
from functools import wraps
def timer(func):
    @wraps(func)
    def inner(request, *args, **kwargs):
        """
        asdasd
        :return:
        """
        start = time.time()
        print(func)
        print(args)

        ret = func(request, *args, **kwargs)

        print('{}'.format(time.time() - start))
        return ret

    return inner
```

CBV

```
from django.utils.decorators import method_decorator
```

1.直接加在方法上：

```python \
@method_decorator(timer)
def get(self,request,)
```

2.加在dispatch方法上：

```python
@method_decorator(timer)
def dispatch(self, request, *args, **kwargs): 
     ret = super().dispatch(request, *args, **kwargs)
     return ret

@method_decorator(timer,name='dispatch')
class AddPublisher(View)
```

3.加在类上

```python
@method_decorator(timer,name='get')
class AddPublisher(View)
```

### 4.request

```python
request.method   ——》 请求方法  GET POST
request.GET    ——》 url上携带的参数  {}  get()  
request.POST   ——》  POST请求提交的数据
request.path_info   ——》 路径信息  不包含IP和端口  不包含参数
request.body    ——》  请求体 
request.META   ——》  请求头  {}  全大写  HTTP_   - _>  _
request.FILES  ——》 上传的文件
	1. form表单 enctype ='multipart/form-data'
    2. input  type='file' 
    3. {% csrf_token %}
request.COOKIES   cookie
request.session   session
request.get_full_path()   ——》 完整的路径  不包含IP和端口  包含参数
request.is_ajax()   判读是否是ajax
```

### 5.response

HttpResponse('xxxx')    ——》  返回字符串     content-type:'text/html'

render(request,'模板的文件名',{ })     ——》  返回一个完整的页面

redirect('url')   ——》 重定向      响应头 ：   Location ： url 

​	ret = HttpResponse('xxxx')     ret['Location '] = url

```python
from django.http.response import JsonResponse
JsonResponse({})     响应头  content-type:'application/json'
JsonResponse([],safe=False) 
```

模板：

{{}}   {%%}

date:Y-m-d H:i:s  

## 今日内容

路由

### 正则表达式

```python
r # 表示原生字符
^ # 以什么开头
$ # 以什么结尾
[0-9] 0-9 # 选一个
[a-zA-Z] 范围 {4} 选几个   
\d # 数字
\w # 数字和字母
? # 0个或1个
+ # 1个或多个
* # 任意多个
. # 除了换行符以外的字符
```

从上到下的进行匹配  匹配到一个不再往下进行匹配

开头不加 /  

### 分组

```
url(r'^blog/([0-9]{4})/([0-9]{2})/$', views.blogs),
```

从URL上捕获到的参数的类型都是字符串

捕获的参数按照位置参数传递给视图函数

### 命名分组

```python
url(r'^blog/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.blogs),
```

捕获的参数按照关键字参数传递给视图函数

### 命名URL和URL反向解析

静态路由

```
url(r'^publisher_list/',views.publisher_list ,name='publisher'),
```

反向解析

模板

​	{% url ‘publisher’ %}   —— 》 /publisher_list/	

py文件

​	from  django.urls  import reverse

​	reverse('publisher')   ——》  /publisher_list/	

分组

```
url(r'^blogs/([0-9]{4})/([0-9]{2})/$', views.blogs,name='blogs'),
```

反向解析

模板

​	{% url ‘blogs’ '2019' '09' %}   —— 》 /app01/blogs/2019/09/

py文件

​	from  django.urls  import reverse

​	reverse('blogs',args=('2019','09'))   ——》 /app01/blogs/2019/09/

命名分组

```
url(r'^blogs/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.blogs,name='blogs'),
```

反向解析

模板

​	{% url ‘blogs’ '2019' '09' %}   —— 》 /app01/blogs/2019/09/

​	{% url 'blogs'  month='10' year='2019' %}   ——》   /app01/blogs/2019/10/

py文件

​	from  django.urls  import reverse

​	reverse('blogs',args=('2019','09'))   ——》 /app01/blogs/2019/09/

​	reverse('blogs', kwargs={'year': '2019', 'month': '12'})     ——》 /app01/blogs/2019/12/

### namesapce

```
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls', namespace='app01')),
    url(r'^app02/', include('app02.urls', namespace='app02')),

]
```

反向解析时  name  ——》   namespace:name

### 作业

删除 出版社、书籍、作者   3个url 3个视图函数

1个url 1个视图  