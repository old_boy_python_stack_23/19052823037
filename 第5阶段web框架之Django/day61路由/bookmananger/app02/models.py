from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=32)


class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField(Book, through='Book_Author', through_fields=['author', 'book', ])


class Book_Author(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name='x')
    tuijian_author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name='x1')
    date = models.DateField()
