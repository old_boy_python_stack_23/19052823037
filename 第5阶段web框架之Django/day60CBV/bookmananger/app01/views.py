from django.shortcuts import render, HttpResponse, redirect
from app01 import models
from django.views import View
from django.utils.decorators import method_decorator
import time


def timer(func):
    def inner(*args, **kwargs):
        start = time.time()
        ret = func(*args, **kwargs)
        print("{}".format(time.time() - start))
        return ret

    return inner


class ListPublisher(View):
    @method_decorator(timer)
    def get(self, request):
        # 从数据库中查询出所有的出版社
        all_publishers = models.Publisher.objects.all().order_by('pid')
        # 返回一个页面  页面包含数据
        return render(request, 'publisher_list.html', {'all_publishers': all_publishers})


# 新增出版社
class AddPublisher(View):
    http_method_names = ['get']

    @method_decorator(timer)
    def dispatch(self, request, *args, **kwargs):
        ret = super().dispatch(request, *args, **kwargs)
        return ret

    def get(self, request):
        return render(request, 'publisher_add.html')

    def post(self, request):
        pub_name = request.POST.get('pub_name')
        if not pub_name:
            # 输入为空
            error = '输入不能为空'
        elif models.Publisher.objects.filter(name=pub_name):
            # 数据库已存在该数据
            error = '数据已存在'
        else:
            # 插入数据
            models.Publisher.objects.create(name=pub_name)
            # 跳转至展示页面
            return redirect('/publisher_list/')

        return render(request, 'publisher_add.html', {'pub_name': pub_name, 'error': error})


# 删除出版社

class DelPublisher(View):
    def get(self, request):
        pk = request.GET.get('pk')

        query = models.Publisher.objects.filter(pk=pk)  # 对象列表
        if not query:
            # 数据部存在
            return HttpResponse('要删除的数据不存在')
        # 删除数据
        query.delete()  # 通过queryset 删除
        # query[0].delete()  # 通过单独的对象 删除
        # 跳转至展示页面
        return redirect('/publisher_list/')


# 编辑出版社
@method_decorator(timer,name="get")
@method_decorator(timer,name='post')
@method_decorator(timer,name='dispatch')
class EditPublisher(View):
    def get(self, request):
        pk = request.GET.get('pk')  # url上携带的参数
        obj = models.Publisher.objects.filter(pk=pk).first()
        if not obj:
            return HttpResponse('要编辑的对象不存在')
        return render(request, 'publisher_edit.html')

    def post(self, request):
        pk = request.GET.get('pk')  # url上携带的参数
        obj = models.Publisher.objects.filter(pk=pk).first()
        pub_name = request.POST.get('pub_name')
        if not pub_name:
            # 输入为空
            error = '输入不能为空'
        elif models.Publisher.objects.filter(name=pub_name):
            # 数据库已存在该数据
            error = '数据已存在'
        else:
            # 编辑原始的对象
            obj.name = pub_name
            obj.save()
            # 跳转至展示页面
            return redirect('/publisher_list/')
        return render(request, 'publisher_edit.html', {'obj': obj, 'error': error})


# 展示书籍
class ListBook(View):
    def get(self, request):
        all_books = models.Book.objects.all()
        return render(request, 'book_list.html', {'all_books': all_books})


# 添加书籍
class AddBook(View):
    def get(self, request):
        # 查询所有的出版社
        all_publishers = models.Publisher.objects.all()
        return render(request, 'book_add.html', {'all_publishers': all_publishers})

    def post(self, request):
        # 获取提交的数据
        title = request.POST.get('title')
        pub_id = request.POST.get('pub_id')
        # 插入数据库
        # obj = models.Book.objects.create(title=title,pub=models.Publisher.objects.get(pk=pub_id))
        models.Book.objects.create(title=title, pub_id=pub_id)

        # 跳转到展示页面
        return redirect('/book_list/')


# 删除书籍
class DelBook(View):
    def get(self, request):
        pk = request.GET.get('pk')
        models.Book.objects.filter(pk=pk).delete()
        return redirect('/book_list/')


# 编辑书籍
class EditBook(View):
    def get(self, request):
        pk = request.GET.get('pk')
        book_obj = models.Book.objects.filter(pk=pk).first()
        all_publishers = models.Publisher.objects.all()
        return render(request, 'book_edit.html', {'book_obj': book_obj, 'all_publishers': all_publishers})

    def post(self, request):
        pk = request.GET.get('pk')
        book_obj = models.Book.objects.filter(pk=pk).first()
        if request.method == 'POST':
            title = request.POST.get('title')
            pub_id = request.POST.get('pub_id')
            book_obj.title = title
            book_obj.pub_id = pub_id
            book_obj.save()
            return redirect('/book_list/')


# 展示作者
class ListAuthor(View):
    def get(self, request):
        # 查询所有的作者
        all_authors = models.Author.objects.all().order_by('id')
        return render(request, 'author_list.html', {'all_authors': all_authors})


# 添加作者
class AddAuthor(View):
    def get(self, request):
        all_books = models.Book.objects.all()
        return render(request, 'author_add.html', {'all_books': all_books})

    def post(self, request):
        name = request.POST.get('name')
        books = request.POST.getlist('books')
        # 插入数据库
        # 新建作者
        author_obj = models.Author.objects.create(name=name)
        # 给作者和书籍绑定关系
        author_obj.books.set(books)
        # 跳转到展示页面
        return redirect('/author_list/')


# 删除作者
class DelAuthor(View):
    def get(self, request):
        pk = request.GET.get('pk')
        models.Author.objects.filter(pk=pk).delete()
        return redirect('/author_list/')


# 编辑作者
class EditAuthor(View):
    def get(self, request):
        pk = request.GET.get('pk')
        obj = models.Author.objects.filter(pk=pk).first()
        all_books = models.Book.objects.all()
        return render(request, 'author_edit.html', {'obj': obj, 'all_books': all_books})

    def post(self, request):
        name = request.POST.get('name')
        books = request.POST.getlist('books')
        pk = request.GET.get('pk')
        obj = models.Author.objects.filter(pk=pk).first()
        # 修改
        obj.name = name
        obj.save()
        obj.books.set(books)  # 修改作者和书的多对多关系

        # 跳转到展示页面
        return redirect('/author_list/')
