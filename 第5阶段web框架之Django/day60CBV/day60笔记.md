## 内容回顾

tag

### 1.for

{%  for i in list %}

​	{{ i }}   {{ forloop  }}

{% endfor %}

forloop.counter   当前循环的序号  从1开始  

forloop.counter0   当前循环的序号  从开0始  

forloop.revcounter   当前循环的序号  到1结束

forloop.revcounter0   当前循环的序号  到0结束

forloop.first  当前循环是否是第一次循环  布尔值 

forloop.last 当前循环是否是最后一次循环  布尔值 

forloop.parentloop   当前循环的父级循环的变量  {}

for   ..  empty  

{%  for i in list %}

​	{{ i }}   {{ forloop  }}

{% empty %}  

​	空空

{% endfor %}

### 2.if判断

{%  if  条件 %}

​	xx

{% elif  条件2 %}

​	xx

{%  else %}

​	xx

{%  endif %}

if 不支持连续判断 10 > 5  > 1

if 不支持算数运算

### 3.with

{%  with   p1.0.name  as  alex  %}

​	{{  alex }}

{% endwith %}

{%  with  alex = p1.0.name   %}

​	{{  alex }}

{% endwith %}

### 4.csrf_token

把{% csrf_token %} 放在form标签内，form标签中有一个隐藏的input标签，name = 'csrfmiddlewaretoken'

### 5.母板和继承

母板：

​	就是一个html页面，提取到多个页面的公共部分，定义上block块

继承：

1. 写上{% extends  'base.html' %}
2. 重写block块 

注意点：

1. {% extends  'base.html' %} 写在最上面，它的上面不要写代码
2. {% extends  'base.html' %}  'base.html'  加上引号   不加引号会当做变量
3. 展示的内容写在block块中
4. 母板定义多个block块  包含  css   js 

### 6.组件

一小段HTML代码段   nav.html

{%  include   'nav.html' %}

### 7.静态文件相关

{% load static %}

"{% static '相对路径' %}"

{% get_static_prefix %}     STATIC_URL  

### 8.模板中自定义方法

filter  simple_tag   inclusion_tag

1. 在app下创建名为templatetags的python包；

2. 在包内创建一个py文件；    my_tags.py

3. 在py文件中写代码：

   ```python
   from django import template
   register = template.Library()   # register不能变
   ```

4. 写函数   +  加装饰器

   ```python
   # filter
   @register.filter
   def add_str(value,arg):
       return "{}_{}".format(value,arg)
   
   # simple_tag
   @register.simple_tag
   def join_str(*args,**kwargs):
       return  "xxxxx"
   
   # inclusion_tag
   @register.inclusion_tag('li.html')
   def show_li(num):
      	return {'num':range(1,num+1)}
   
   # 模板  li.html
   <ul>
   	{% for i in  num %}
       	<li> {{ i }} </li>
       {% endfor %}
   
   </ul>
   ```

5. 使用

   ```html
   {% load my_tags %}
   {{ value|add_str:'xxx' }}  {%  if value|add_str %}
   
   {% join_str v1 v2 k3=v3 k4=v4  %}
   
   {% show_li 3 %}
   <ul>
   	
       <li> 1 </li>
       <li> 2 </li>
   	<li> 3 </li>
   
   </ul>
   ```

## 今日内容

### 视图

FBV    function based  view  基于函数（功能）的视图

CBV   class  based  view    基于类的视图

### CBV

定义CBV

```PYTHON
from django.views import View


class AddPublisher(View):

    def get(self, request):
        # 处理get请求
        return xx

    def post(self, request):
        # 处理get请求
        return xx
```

写对应关系

```python
url(r'^publisher_add/',views.AddPublisher.as_view() ),
```

### CBV的流程

1. 项目启动时，运行urls.py。

   url(r'^publisher_add/',views.AddPublisher.as_view() ),
   AddPublisher.as_view() 执行  ——》   view函数

2. 请求到来时，实行view函数：

   1. 实例化AddPublisher  ——》  self 

   2. self.request = request  

   3. 执行View中self.dispatch(request, *args, **kwargs)

      1. 判断请求方式是否被允许

         1. 允许

            通过反射获取请求方式对应的方法   ——》 handler

         2. 不允许

            self.http_method_not_allowed  ——》 handler

      2. 执行handler 获取到响应

      

### 视图加装饰器

1. FBV  直接加

2. CBV

   ```
   from django.utils.decorators import method_decorator
   ```

   1. 加在方法上

      ```
      @method_decorator(timer)
      def get(self, request):
      ```

      2. 加在dispatch方法上

   ```python
   @method_decorator(timer)
   def dispatch(self, request, *args, **kwargs):
       # start = time4()
       ret = super().dispatch(request, *args, **kwargs)
       # print('{}'.format(time.time() - start))
       return ret
   
   @method_decorator(timer, name='dispatch')
   class AddPublisher(View):
   ```

   3. 加在类上

      ```
      @method_decorator(timer, name='post')
      @method_decorator(timer, name='get')
      class AddPublisher(View):
      ```

### request

```python
request.method   请求方式  GET POST 
request.GET    url上携带的参数  {}   get()
request.POST   post请求提交的参数  {}   get()
request.path_info # 路径信息    不包含IP和端口 也不包含查询参数
request.body   #  请求体的数据
request.COOKIES   # cookie
request.session   # session 
request.FILES    # 上传的文件 
request.META   # 头的信息

request.get_full_path()   # 完整的路径信息 不包含IP和端口 包含查询参数
request.is_ajax()   # 判断是否是ajax请求
```



上传文件注意事项：

1. form表单的属性  enctype="multipart/form-data"
2. input   type = ‘file’
3. request.FILES   get()      



### response

HttpResponse('字符串')   # 返回字符串

render(request,'模板的文件名'，{})    # 返回一个完整的页面

redirect（'要跳转的地址'）  # 重定向   响应头  Location：  url 

```
from django.http.response import JsonResponse

return JsonResponse(dict)   # Content-type = 'application/json'
return JsonResponse(list,safe=False) 
```