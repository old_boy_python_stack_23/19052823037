## 内容回顾

### 1.MVC 和MTV

MVC

​	M: model   和数据库交互

​	V:  views   视图    展示页面 HTML

​	C： controller  控制器  调度  业务逻辑

MTV:

​	M : model  模型  ORM 

​	T:   template   模板  HTML

​	V:  view   视图   业务逻辑 

### 2.模板语法

#### 变量

render(request,'模板文件名'，{'k1':v1})

{{  k1 }} 

{{  name_list.0  }}

{{  dic.key  }}

{{  dic.keys  }}

{{  dic.values  }}

{{  dic.items}}

{{  p1.name  }}

{{  p1.talk  }}

优先级：   字典的key    >   对象的属性或方法  >  列表的索引

#### 过滤器

改变变量的显示结果 

{{  value|filter_name }}   {{  value|filter_name:参数 }}

##### 内置过滤器

defalut  {{  value|defalut:'默认值'   }}    value找不到或者为空的时候才使用默认值

filesizeformat  文件大小格式化     byte   PB

slice   {{  value|slice:‘1:2:2’ }}     切片

upper  lower  title  ljust  join 

length 返回长度

truncatechars   按照字符进行截断

truncatewors   按照单词进行截断  

add   +  数字的加法 字符串的拼接  列表的拼接

date  日期时间格式化  {{  now|date：‘Y-m-d H:i:s’ }}

​	配置settings   

​	DATETIME_FORMAT = 'Y-m-d H:i:s'  DATE_FORMAT = 'Y-m-d'

​	USE_L10N = False

safe 告诉django不转义   

##### 自定义过滤器

1. 在app下创建templatetags的python包

2. 在包内创建py文件   my_tags.py

3. 在py文件中写代码：

   ```python
   from django import  template
   register = template.Library()
   
   ```

4. 定义函数  +  加装饰器

   ```python
   @register.filter
   def add_arg(value,arg):
      return  "{}_{}".format(value,arg)
    
   @register.filter(name='xxxx')
   def add_arg(value,arg):
      return  "{}_{}".format(value,arg)    
   ```

5. 模板中使用

   ```html
   {% load  my_tags  %}
   
   {{ 'alex'|add_arg:'dsb' }}  'alex_dsb' 
   
   {{ 'alex'|xxxx:'dsb' }}  'alex_dsb' 
   
   
   ```

   

## 今日内容

### tag   {%%}

#### for

{%  for i in list %}

​	{{ forloop.counter }}

​	{{  i }}

{% endfor %}

forloop

{{ forloop.counter }}        当前循环的从1开始的计数

{{ forloop.counter0 }}      当前循环的从0开始的计数

{{ forloop.revcounter }}    当前循环的倒叙计数（到1结束）

{{ forloop.revcounter0 }}    当前循环的倒叙计数（到0结束）

{{ forloop.first}}    当前循环是否是第一次循环  布尔值

{{ forloop.last}}    当前循环是否是最后一次循环  布尔值

{{ forloop.parentloop }}    当前循环父级循环的forloop

```html
{% for name in name_list %}
    {{ name }}

{% empty %}
    空的数据
{% endfor %}
```

#### if判断

```html
{% if p1.age < 18 %}
    他还是个宝宝
{% elif p1.age == 18 %}

    刚成年 可以出家

{% else %}
    是个骚老头子 坏得很
{% endif %}
```

不支持算数运算 

不支持连续判断

```html
{% with alex=person_list.1.name age=person_list.1.age   %}

    {{ alex }} {{ age }}
    {{ alex }}
    {{ alex }}
    {{ alex }}

{% endwith %}
```

{% csrf_token %}

标签放在form标签中，form表单中有一个隐藏的input标签 name  ='csrfmiddlewaretoken'   

### 母板和继承 

母板：

1. html页面  提取多个页面的公共部分
2. 定义多个block块，需要让子页面覆盖填写

继承：

1. {% extends  ‘母板文件名’ %}
2. 重写block块 ，写在block内部

注意：

1. {% extends 'base.html' %}    带上引号   不带的话会当做变量
2. {% extends 'base.html' %} 上面不要写其他内容
3. 要显示的内容写在block块中
4. 母板定义多个block   定义  css js

组件 

html 页面    包含  一小段代码   _>  nav.html

使用：

​	{%  include  ‘nav.hmtl ’ %}

### 静态文件相关

引入静态文件：

```html
{% load static %}

<link rel="stylesheet" href="{% static 'css/dashboard.css' %}">

{% get_static_prefix %}   '/s1/'
```

### 定义  filter  simple_tag   inclusion_tag 

1. 在app下创建templatetags的python包

2. 在包内创建py文件   my_tags.py

3. 在py文件中写代码：

   ```python
   from django import  template
   register = template.Library()
   
   ```

4. 定义函数  +  加装饰器

   ```python
   @register.filter
   def add_arg(value,arg):
      return  "{}_{}".format(value,arg) 
   
   
   @register.simple_tag
   def join_str(*args, **kwargs):
       return '_'.join(args) + "*".join(kwargs.values())
   
   @register.inclusion_tag('page.html')
   def page(num):
   
       return {'num':range(1,num+1)}
   
   page.html 
   
   
   ```



