from django import template

register = template.Library()

@register.simple_tag
def ride(num):
    return num*num
