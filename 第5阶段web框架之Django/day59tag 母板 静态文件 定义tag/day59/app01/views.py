from django.shortcuts import render


# Create your views here.
def tags(request):
    sqr_list = [1,2,3,]
    return render(request, 'tags.html', {"sqr_list": sqr_list})
