from django.shortcuts import render

# Create your views here.
from django.shortcuts import redirect
from app01 import models
import time


def login(request):
    if request.method == "GET":
        return render(request, 'login.html')
    elif request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        ret = models.User.objects.filter(username=username, password=password)

        if ret:
            time.sleep(1)
            return redirect("/home/")
        else:
            return render(request, 'login.html')


def home(request):
    return render(request, 'home.html')
