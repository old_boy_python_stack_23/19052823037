## 内容回顾

### HTTP协议

规定请求和响应的的标准

请求的方法  8种

GET POST HEAD PUT DELETE TRACE OPTIONS CONNECT 

### 状态码

1xx  服务器收到请求  进一步处理

2xx  正常 200 ok 

3xx  重定向 

4xx 请求的错误  404 403 

5xx  服务器的错误  

### url 

https://www.sogou.com/web?query=%E5%85%A8%E8%81%8C%E6%B3%95%E5%B8%88&_asf=www.sogou.com

https 协议

www.sogou.com 域名   ip 

:端口号  https  443  http 80

/web  路径

查询参数  ?query=asxxxxx&k1=v1 #

### 请求和响应

请求（浏览器发给服务器的数据   request）

 	格式：

		"请求方法 路径 协议版本\r\n
	
		k1 : v1\r\n
	
		k2 : v2\r\n
	
		\r\n
		请求数据"          get  请求没有请求体

响应（服务器返回给浏览器的数据  response）

	格式：
	
		"协议版本 状态码 状态描述\r\n
	
		k1 : v1\r\n
	
		k2 :v2\r'n
	
		\r\n
	
		响应数据（响应体）"  html文本

### 浏览器发送请求接收响应的流程

1. 在浏览器的地址栏中输入地址，回车。发送一个GET请求。
2. 按照HTTP协议的格式发送数据
3. 服务器接收到数据，拿到url路径，根据url的路径执行对应的函数，得到返回的内容
4. 服务器把响应的内容按照http的响应格式发送给浏览器
5. 浏览器接收到数据，断开连接。解析数据。

### web框架

	本质： socket服务端
	
	功能：

  		1.  socket收发消息 wsgi   wsgiref    uwsgi 
                		2.  根据不同的路径返回不同的内容
            		3.  返回动态页面（字符串的替换  模板的渲染 jinja2）

### django的使用

1. 下载

	pip install django==1.11.23 -i  

2. 创建项目

   1. 命令行

      django-admin  startproject  项目名称

   2. pycharm

3. 启动项目

   1. 命令行

      切换到项目的根目录  manage.py 

      python  manage.py  runserver   #  127.0.0.1：8000

      python  manage.py  runserver 80   #  127.0.0.1：80

      python  manage.py  runserver 0.0.0.0:80   #  0.0.0.0:80

   2. pycharm

4. 使用

   设计 url   有对应函数   完成函数

    

##  今日内容

### 1.静态文件的配置

```python
STATIC_URL = '/static/'  # 别名

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]


STATICFILES_DIRS = [   #按照列表的顺序进行查找
    os.path.join(BASE_DIR, 'x1'),
    os.path.join(BASE_DIR, 'static'),
    os.path.join(BASE_DIR, 'x2')
]  
```

### 2.登陆的实例

form表单

1. action 提交的地址  method  post  
2. input 需要有name  
3. submit  提交的按钮或者input

目前提交post请求，注释一个csrf 中间件  

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
```

前端模板 [http://www.jq22.com](http://www.jq22.com/)

### 3.app 

##### 新建APP

python manage.py   startapp  app名称

##### 注册APP

在settings中

```python
INSTALLED_APPS = [
 
    'app01',  
    'app01.apps.App01Config',  # 推荐写法
]
```

##### 目录

	admin.py   django admin 
	
	apps.py  app的信息
	
	models.py    模型  model  跟数据库有关
	
	views.py   写函数


​	

### 4.orm

#### 使用mysql数据库的流程/

1. 创建一个mysql数据库；

2. 在settings中配置数据库：

   ```python
   DATABASES = {
       'default': {
           'ENGINE': 'django.db.backends.mysql',   # 引擎
           'NAME': day53web框架之HTTP协议,			# 数据库名称
           'HOST': '127.0.0.1',		# IP
           'PORT': 3306,				# mysql端口号
           'USER': 'root',			# 用户名
           'PASSWORD': '123'			#
       }
   }
   ```

3. 使用pymysql模块连接mysql数据库。

   写在与settings同级目录下的init.py中

   ```python
   import pymysql
   pymysql.install_as_MySQLdb()
   ```

4. 写对应关系，在app下的models.py中写类。

   ```python
   class User(models.Model):
       username = models.CharField(max_length=32)  # username  varchar(32)
       password = models.CharField(max_length=32)  # password  varchar(32)
   ```

5. 执行数据库迁移的命令

   python manage.py  makemigrations    # 记录下models.py的变更记录  

   python manage.py migrate   # 变更记录同步到数据库



#### orm 操作

```
ret = models.User.objects.get(username=user, ) # 找不到就报错  找到多个也报错
ret = models.User.objects.filter(username=user, )  # 对象列表
```