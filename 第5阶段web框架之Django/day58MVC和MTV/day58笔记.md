## 内容回顾

### 1.django所有的命令

下载安装：

pip install django==1.11.23  -i  源

创建项目

django-admin startproject 项目名

启动项目

切换到项目的根目录

python manage.py  runserver   #   127.0.0.1:8000

python manage.py  runserver   80  #   127.0.0.1:80

python manage.py  runserver   0.0.0.0:80  #    0.0.0.0:80

创建APP

python manage.py startapp  app名称

数据库迁移的命令

python manage.py makemigrations   # 记录下所有App下的models的变更记录 

python manage.py migrate   # 同步迁移的记录

### 2.配置

TEMPLATES 模板

	DIRS []

静态文件

	STATIC_URL = '/static/'   # 静态文件的别名
	
	STATICFILES_DIRS = [
	
		os.path.join(BASE_DIR,'static'),
	
	]

数据库 DATABASES

	ENGINE :  mysql
	
	NAME:  数据库名
	
	USER: 用户名
	
	PASSWORD: 密码
	
	HOST : 127.0.0.1
	
	PORT：端口 

中间件
	注释掉 csrf   提交post请求

APP  

	INSTALLED_APPS = [
	
		‘app01’  或者  ‘app01.apps.App01Config’
	
	]  

### 3.django使用mysql数据库的流程

1. 创建一个mysql数据库

2. 在settings中配置数据库的配置

   	ENGINE :  mysql

      	NAME:  数据库名

      	USER: 用户名

      	PASSWORD: 密码

      	HOST : 127.0.0.1

      	PORT：端口 

3. ### 告诉django使用pymysql连接数据库

   写在与settings同级目录下的`__init__.py`中：

   import  pymysql

   pymysql.install_as_MySQLdb()

4. 在app下的models中写类：

   ```python
   class Publisher(models.Model):
       pid = models.AutoField(primary_key=True)
       name = models.CharField(max_length=32)  # varchar(32)
   
   
   class Book(models.Model):
       name = models.CharField(max_length=32)
       pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)
   
   
   class Author(models.Model):
       name = models.CharField(max_length=32)
       books = models.ManyToManyField('Book')
   ```

5. 执行数据库迁移的命令

   python manage.py makemigrations   # 记录下所有App下的models的变更记录 

   python manage.py migrate   # 同步迁移的记录

### 4.request

	request.GET   url上携带的参数   {}         ?k1=v1&k2=v2
	
	request.POST  form表单提交POST请求的数据   {}     get()     getlist()
	
	request.method  请求的方法   GET  POST PUT

### 5.response

	HttpResponse('字符串')    返回字符串
	
	render(request，‘模板的文件名’，{})   返回一个完整的页面
	
	redirect(‘URL的路径’)  重定向     响应头  Location：url  

### 6.orm

对应关系

类   ——》   数据表

对象   ——》   数据行（记录）

属性  ——》    字段

查询：

```python
from app01 import models 
models.Publisher.objects.get(name='xxx')   # 获取一个满足条件的对象  找不到或者是多个就报错
models.Publisher.objects.filter(name='xxx')  # 获取满足条件的所有的对象  queryset 对象列表
models.Publisher.objects.all()   # 获取所有的数据


pub_obj.name # 出版社的名字
pub_obj.pk  pub_obj.pid # 出版社的主键

book_obj.pk
book_obj.name 
book_obj.pub      #  所关联的出版社对象
book_obj.pub_id    #  所关联的出版社对象id

author_obj.books  # 关系管理对象
author_obj.books.all()  # 所关联的所有的书籍对象
```

新增：

```python
pub_obj =models.Publisher.objects.create(name='xxxx')
pub_obj = models.Publisher(name='xxxx')
pub_obj.save()

models.Book.objects.create(name='xxxx',pub=pub_obj)
models.Book.objects.create(name='xxxx',pub_id=pub_obj.pk)


author_obj = models.Author.objects.create(name='xxxx')
author_obj.books.set([1,2])   # 设置多对多的关系
```

删除：

```python
models.Publisher.objects.filter(pk=pk).delete()
pub_obj.delete()
```

编辑：

```python
pub_obj.name='qqqq'
pub_obj.save()

book_obj.name='xxxx'
book_obj.pub=pub_obj
book_obj.pub_id=pub_obj.pk
book_obj.save()

author_obj.name ='xxx'
author_obj.save()
author_obj.books.set([id,id])    # 设置

```

### 7.模板

render(request,'模板的文件名'，{k1：v1})

{{ k1  }}   v1

if 

{% if  条件 %}

	x1

{% else%}

	x2

{% endif %}

for 

{%  for i in list %}

	{{  forloop.counter }}
	
	{{ i }}

{% endfor %}

## 今日内容

### MVC 和 MTV

MVC:

	M: model   模型  操作数据库
	
	V： view  视图  展示页面  HTML
	
	C: controller  控制器   调度  业务逻辑

MTV:

	M : model   模型  操作数据库  orm
	
	T :  template  模板  HTML  
	
	V: view  视图 业务逻辑  

### 变量

{{  变量名 }}

{{  变量名.  }}

.索引   .key   .属性   .方法

### 过滤器

#### 内置的过滤器

修改变量的显示结果

语法：

 {{ value|filter_name }}   {{ value|filter_name:参数 }}

default： 提供默认值

 {{ value|default:'xxxx'}}

传过来的变量不存在或者为空 使用默认值

{{ name_list|slice:'-1::-1' }}  切片

add   + 

数字的加法   字符串的拼接  列表的合并

日期格式化

```python
{{ now|date:'Y-m-d H:i:s' }}
```

settings的配置：

```
USE_L10N = False
DATETIME_FORMAT = 'Y-m-d H:i:s'
```

safe   告诉django不需要转义

```
{{ js|safe }}
```

py文件中

```
from django.utils.safestring import mark_safe

'a':mark_safe('<a href="http://www.baidu.com">跳转</a>')

{{ a }}
```

#### 自定义过滤器

##### 定义

1. 在app下创建一个名为templatetags的python包

2. 在包内创建py文件    —— 》   自定义   my_yags.py

3. 在py文件中写入：

   ```python
   from django import template
   register = template.Library()  # register不能变
   ```

4. 定义函数  + 加装饰

   ```python 
   @register.filter
   def new_upper(value, arg=None):  # arg 最多有一个
       print(arg)
       return value.upper()
   ```

##### 在模板中使用：

```
{% load my_tags %}

{{ 'asd'|new_upper:dic }}


```