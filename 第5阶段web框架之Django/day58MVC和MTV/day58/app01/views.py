from django.shortcuts import render
import datetime
from django.utils.safestring import mark_safe


# Create your views here.
def index(request):
    class Person:
        def __init__(self, name, age):
            self.name = name
            self.age = age

        def talk(self):
            return '我太男了'

        def __str__(self):
            return "<Person obj : {}{}>".format(self.name, self.age)

    num = '1'
    string = '随便'
    boo = True
    name_list = ['怼哥', 'print业', 'MC骚Q', '大象琪琪']
    dic = {
        'name': '太白',
        'age': '83',
        'hobby': ['烫头', '植发', '保健', '洗脚'],
        'keys': 'xxxxx'
    }

    p1 = Person('李多余', 18)

    context = {
        "num": num,
        "string": string,
        "boo": boo,
        "name_list": name_list,
        "dic": dic,
        'p1': p1,
        'qq': None,
        'filesize': 1 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024,
        'long_str': 'Django 的模 板中 会对HTM L标签 和JS等 语法标签 进行 自动 转义，原因显而易见，这样是为了安全。但是有的时候我们可能不希望这些HTML元素被转义，比如我们做一个内容管理系统，后台添加的文章中是经过修饰的，这些修饰可能是通过一个类似于FCKeditor编辑加注了HTML修饰符的文本，如果自',
        'now': datetime.datetime.now(),
        'js': """
        <script>

                for (var i =0; i < 5; i++) {
                    alert('11111')
                }

        </script>
    """,
        'a': mark_safe('<a href="http://www.baidu.com">跳转</a>')
    }
    return render(request, 'index.html', context)
