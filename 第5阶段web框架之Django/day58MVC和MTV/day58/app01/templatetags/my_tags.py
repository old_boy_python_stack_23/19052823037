from django import template

register = template.Library()


@register.filter
def new_upper(value, arg=None):
    print(arg)
    return value.upper()
