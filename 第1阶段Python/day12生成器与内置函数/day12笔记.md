# 生成器

## 生成器初始

+ 生成器本质就是迭代器。python社区生成器与迭代器是一种。生成器与迭代器的唯一区别：生成器是我们自己用python代码构建的。

## 生成器产生方式

+ 生成器函数。
+ 生成器表达式。
+ python给你提供的一些内置函数，返回一个生成器。

## 生成器函数。

- 之前接触的函数：

  ```python
  # def func():
  #     print(111)
  #     return 2
  # ret = func()
  # print(ret)
  # 执行此函数，遇到return结束函数。
  # 将数字2返回给ret。
  
  ```

- 生成器函数： 只要函数中出现了yield那么他就不是函数，它是生成器函数

  ```python
  def func():
      # print(111)
      # print(111)
      # print(111)
      # print(111)
      # print(111)
      # print(111)
      yield 2,4,5
      yield 3
      yield 4
      yield 5
  ret = func()  # 生成器对象
  # print(ret)  # <generator object func at 0x0000000001E10F68>
  '''
  # 类比
  l1 = [2,]  [2,3,4,5]
  obj = iter(l1)
  
  '''
  # 只要函数中出现了yield那么他就不是函数，它是生成器函数。
  # 一个next对应一个yield.
  # print(next(ret))
  # print(next(ret))
  # print(next(ret))
  # print(next(ret))
  # print(next(ret))
  # print(next(ret))
  
  ```

## yiled与return的区别

```python
# return 结束函数，给函数的执行者返回值(多个值通过元组的形式返回)。
# yield  不结束函数，对应着给next返回值(多个值通过元组的形式返回)。
```

## send(了解) 

pass

## 生成器的举例

```python
# def eat_baozi():
#     list1 = []
#     for i in range(1,2001):
#         list1.append(f'{i}号包子')
#     return list1
#
# print(eat_baozi())

def eat_baozi_gen():
    for i in range(1,2001):
        # print(11)
        yield f'{i}号包子'

'''
# ret1 = eat_baozi_gen()
# ret2 = eat_baozi_gen()
# # print(ret1)
# # print(ret2)
# print(next(ret1))
# print(next(ret1))
# print(next(ret1))
# 
# print(next(ret2))
# print(next(ret2))
# print(next(ret2))
# print(next(ret))
# print(next(ret))
'''

# ret = eat_baozi_gen()
#
# for i in range(200):
#     print(next(ret))
#
# for i in range(200):
#     print(next(ret))

```

## yiled与yiled from。

```python
# yield from


# def func():
#     l1 = [1, 2, 3]
#     yield l1
# ret = func()
# print(next(ret))
# print(next(ret))
# print(next(ret))

# def func():
#     l1 = [1, 2, 3]
#     yield from l1
#
#     '''
#     yield 1
#     yield 2
#     yield 3
#     '''
# ret = func()
# print(next(ret))
# print(next(ret))
# print(next(ret))

# yield : 对应next给next返回值
# yield from 将一个可迭代对象的每一个元素返回给next
# yield from 节省代码，提升效率（代替了for循环）
```



## 列表推导式，生成器表达式（字典推导式，集合推导式）。

+ 列表推导式：一行代码构建一个有规律比较复杂的列表。

+ 列表推导式与之前写法对比

```python
# l1 = [1,2,3......100]
# l1 = []
# for i in range(1,101):
#     l1.append(i)
# print(l1)

# 列表推导式
l1 = [i for i in range(1, 101)]
# print(l1)

```

+ 两种构建方式：
  1.循环模式： [变量(加工后的变量) for 变量 in iterable]
  2.筛选模式： [变量(加工后的变量) for 变量 in iterable if 条件]

  

+ 循环模式：

  ```python
  # 循环模式：
  # 将10以内所有整数的平方写入列表。
  # print([i**2 for i in range(1, 11)])
  # 100以内所有的偶数写入列表.
  # print([i for i in range(2, 101, 2)])
  # 从python1期到python100期写入列表list
  # print([f'python{i}期' for i in range(1, 101)])
  
  ```

+ 筛选模式：

  ```python
  # print([i for i in range(1, 101) if i > 49])
  # 三十以内可以被三整除的数。
  # print([i for i in range(1, 31) if i % 3 == 0])
  # 过滤掉长度小于3的字符串列表，并将剩下的转换成大写字母
  # l1 = ['barry', 'fdsaf', 'alex', 'sb', 'ab']
  # print([i.upper() for i in l1 if len(i) > 3])
  # 找到嵌套列表中名字含有两个‘e’的所有名字（有难度）
  names = [['Tom', 'Billy', 'Jefferson', 'Andrew', 'Wesley', 'Steven', 'Joe'],
           ['Alice', 'Jill', 'Ana', 'Wendy', 'Jennifer', 'Sherry', 'Eva']]
  # l1 = []
  # for i in names:
  #     for j in i:
  #         if j.count('e') > 1:
  #             l1.append(j)
  # print(l1)
  
  print([j for i in names for j in i if j.count('e') > 1])
  ```

+ 列表推导式的优缺点：

  ```python
  # 列表推导式的优缺点：
  # 优点：
      # 1， 简单，快捷，装b。
  # 缺点：
      # 2. 可读性不高，不好排错。
  # 慎用，不要入迷。
  ```

+ 生成器表达式：

  与列表推导式几乎一模一样。

  循环模式，筛选模式。

```python
# obj = (i for i in range(1, 11))

# # print(obj)

# # print(next(obj))

# # print(next(obj))

# # print(next(obj))

# # print(next(obj))

# # print(next(obj))

# # print(next(obj))

# # print(next(obj))

# # print(next(obj))

# # print(next(obj))

# # print(next(obj))

# # print(next(obj))
```

+ 如何触发生成器（迭代器）取值？

```python
1. next(obj)

2. for 循环

for i in obj:

    print(i)

3. 数据转化

print(list(obj))

生成器表达式：生成器 节省内存。
```

+ 字典推导式，集合推导式

```python
字典推导式，集合推导式：  两种模式： 循环模式，筛选模式
l1 = ['小潘', '怼怼哥','西门大官人', '小泽ml亚']
{0： '小潘', 1： '怼怼哥', 2: '西门大官人'}
dic = {}
for index in range(len(l1)):
    dic[index] = l1[index]
print(dic)
print({i:l1[i] for i in range(len(l1))})


1~100
print({i for i in range(1, 101)})
```



## 内置函数I

内置函数：内置的函数，python中的内置函数68种。13种是在面向对象之后讲解，还有一些不讲的。

```python
eval exce
慎用
s1 = "{1: 'alex'}"
s2 = '1 + 3'
eval 剥去字符串的外衣，返回里面的本质
ret = eval(s1)
print(ret,type(ret))
print(eval(s2))

exec 代码流，过程。
s3 = '''
for i in range(3):
    print(i)
'''
exec(s3)

s3 = input('>>>')
print(eval(s3))

hash()
print(hash(123214134))
print(hash('fkljdsaklgjdfs'))
print(hash('gjdfs'))

help()
print(help(str.count))


callable  **用于检查一个对象是否是可调用
def a():
    pass
b = 3433

a()
b()
print(callable(a))
print(callable(b))

int  **
print(int(3.14))
print(int('123'))

print(float(100))

print(complex(1,2))

print(bin(100))  # 将十进制转化成二进制。 **
print(oct(10)) # 将十进制转化成八进制字符串并返回。
print(hex(17)) # 将十进制转化成十六进制字符串并返回。 **
print(hex(15))

计算除数与被除数的结果，返回一个包含商和余数的元组(a // b, a % b)分页用到
print(divmod(10, 3))  # **

保留小数的有效位置
print(round(3.1485926,2))

print(pow(3, 3))  # 3*3*3
print(pow(3, 3, 2)) # 第三位是取余

输入字符寻找其在unicode的位置。
print(ord('a'))
print(ord('中'))

输入位置数字找出其对应的字符
print(chr(98))  # **
print(chr(20104))  # 予

repr 原形毕露  **
print('太白')
print(repr('太白'))

msg = '我叫%r' %('太白')
print(msg)


all  any  **
0,'',[],{},set(),(),None

l1 = [1, 'fgdsa', [], {1: 2}]
l2 = [0, '', [], {}]
print(all(l1)) # 判断可迭代对象元素全部都为True，返回True
print(any(l2))  # 判断可迭代对象元素只要有一个True返回True

```



## 匿名函数

```python
# 匿名函数：没有名字的函数
# 匿名函数只能构建简单的函数，一句话函数。


def func(x,y):
    return x + y
# print(func(1, 2))

# 匿名函数构建
# func2 = lambda x,y: x + y
# print(func2(1, 2))

# 匿名函数最常用的就是与内置函数结合使用。

# 写匿名函数：接收一个可切片的数据，返回索引为 0与2的对应的元素（元组形式）。

# func = lambda x: (x[0],x[2])
# print(func('太白金星'))
# 写匿名函数：接收两个int参数，将较大的数据返回。

# func1 = lambda x, y: x if x > y else y
# print(func1(100,2))

# func2 = lambda : 3
# print(func2())
# str() dir() range() len() print() max() min() open()
# input() type() int() enumerate() list() id() set() dict()
# iter() next() tuple() bool() globals() locals() frozenset()
# 

```



## 内置函数II

```python

print（）
# sep 设定分割符。
# end 默认是换行可以打印到一行
# print(1, 2, 3, sep='|')
# print(1, 2, end=' ')
# print(3, 4)
# f = open('log','w',encoding='utf-8')
# # f.write('写入文件')
# print('写入文件', file=f)

list
# l1 = list('fdsafd')
# print(l1)

dict
# 创建字典的几种方式
# dic = {1: 2}
# # 字典推导式
# # print({i: 1 for i in range(3)})
# # dict()
# dic = dict(one=1, two=2, three=3)
# print(dic)
# # fromkeys()

abs()  获取绝对值
# print(abs(-10))

sum() 数字相加求和
# sum(iterable,)
# print(sum([1,2,3,4]))
# print(sum([1, 2, 3, 4], 100))

# 非常非常非常重要的

min 可以加功能
# print(min([22, 11, 45, 2]))
l1 = [('alex', 73, 170), ('太白', 18, 185), ('武大', 35, 159),]
# # l1 = [(73, 'alex'), (35, '武大'), (18, '太白')]
# # 找年龄最小的元组
# # print(min(l1))
# # 通过设置key去使用min
# # key = 函数名
# # def func(x):  # x = ('alex', 73)
# #     return x[1]
#
# # 返回值是什么就按照什么比较最小的。
# # min 会自动的将可迭代对象的每一个元素作为实参传给x，
'''
第一次，x = ('alex', 73)   73
第二次，x = ('武大', 35)   35
第三次，x = ('太白', 18)   18
'''
# 将遍历的那个元素即是 ('太白', 18) 返回
# print(min(l1,key=func))
# print(min(l1,key=lambda x: x[1]))

# def func(x):
#     return x[1]

# print(min(l1,key=func))
# print(min(l1,key=lambda x: x[1]))
# print(min(l1,key=lambda x: x[2])[0])





a = 1
# 练习：
dic = {'a':3,'b':2,'c':1}
# 将dic值最小的键返回。
# print(min(dic,key= lambda x:dic[x]))
# # 将dic值最小的值返回。
# print(dic[min(dic,key= lambda x:dic[x])])

# dic = {'A':['李业', 67],'b': ['怼哥', 95],'c': ['冯垚', 85]}
#
# # 将成绩最低的从属于的那个列表返回。
# print(dic[min(dic,key=lambda x:dic[x][1])])
# # 将成绩最低的分数返回。
# print(dic[min(dic,key=lambda x:dic[x][1])][1])

max() 与min 相同。

reversed()  对一个可迭代对象进行翻转，返回一个迭代器

# s1 = 'alex'
# # print(reversed(s1))
# for i in reversed(s1):
#     print(i)

内置函数：bytes()
s1 = '太白'
# 方法一：
# print(s1.encode('utf-8'))
# 方法二：
# print(bytes(s1,encoding='utf-8'))

# 解码：
# b1 = b'\xe5\xa4\xaa\xe7\x99\xbd'
# # 方法一：
# # print(b1.decode('utf-8'))
# # 方法二：
# print(str(b1, encoding='utf-8'))

# zip 拉链方法 返回一个迭代器
# l1 = [1, 2, 3, 4]
# tu = ('a', 'b', 'c')
# s = 'python23'
# # print(zip(l1,tu,s))
# print(list(zip(l1,tu,s)))

# sorted 排序
l1 = [2, 6, 4, 1, 3]
# 列表原有的方法
# l1.sort()
# print(l1)
# 形成新列表
# print(sorted(l1))
# print(l1)

list1 = [
    {'name': 'alex', 'age': 73},
    {'name': 'wusir', 'age': 35},
    {'name': '太白', 'age': 25},
]
# 加key
# print(sorted(list1))
# print(sorted(list1, key=lambda x:x['age']))
# print(sorted(list1, key=lambda x:x['age'],reverse=True))
#
# l1 = [('张一东', 80), ('张耳洞', 75), ('怼怼哥', 7), ('李业', 59)]
# print(sorted(l1,key=lambda x:x[1]))
# print(sorted(l1,key=lambda x:x[1],reverse=True))

# filter  返回一个生成器
# 生成器表达式的筛选模式
l1 = [56, 67, 12, 34, 78, 90,]
# print([i for i in l1 if i > 60])
# 返回值为True的留下来
# print(filter(lambda x: x>60,l1))
# print(list(filter(lambda x: x>60,l1)))
#
# lst = [{'id':1,'name':'alex','age':50},
#         {'id':1,'name':'wusir','age':17},
#         {'id':1,'name':'taibai','age':16},]
#
# print(list(filter(lambda x:x['age']<30,lst)))


# map 返回一个迭代器，相当于生成器表达式：循环模式
# l1 = [56, 67, 12, 34, 78, 90,]
# # print([i**2 for i in l1])
# # print(map(lambda x:x**2,l1))
# print(list(map(lambda x:x**2,l1)))

# reduce python3x 从内置函数剔除了。
from functools import reduce
'''
第一次：x,y 1,2 求和 3 记录到内存
第二次：x,y 3,3 求和 6 记录到内存
第三次：x,y 6，4 .........

'''
# print(reduce(lambda x,y: x+y,[1,2,3,4,5]))
# print(reduce(lambda x,y: 2*x+y, [1,2,3]))
# print(reduce(lambda x,y: x+y, ['alex','s','b']))  # 可以用字符串拼接
```

