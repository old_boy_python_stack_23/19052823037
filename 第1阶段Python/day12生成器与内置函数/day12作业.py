# 1. 整理今天笔记，课上代码最少敲3遍。
# 2. 用列表推导式做下列小题

# 3. 过滤掉长度小于3的字符串列表，并将剩下的转换成大写字母
# l1 = ['iuhhds','dgfgfgsf','adfuigabjab','shgfdkfjsa','gd','hgd','d']
# boj = (i.upper() for i in l1 if len(i) >= 3 )
# print(list(boj))

# 4. 求(x,y)其中x是0-5之间的偶数，y是0-5之间的奇数组成的元祖列表
# x = (i for i in range(5) if i%2 ==0)
# y = (i for i in range(5) if i%2 !=0)
# print((list(x),list(y)))
# print(list((i,j) for i in range(0,5,2) for j in range(1,6,2)))

# # 5. 求M中3,6,9组成的列表
# M = [[1,2,3],[4,5,6],[7,8,9]]
# obj = list((i[2] for i in M ))
# print(obj)
#
# # 6. 求出50以内能被3整除的数的平方，并放入到一个列表中。
# obj = list((i**2 for i in range(50) if i % 3 == 0))
# print(obj)

# # 7. 构建一个列表：['python1期', 'python2期', 'python3期', 'python4期', 'python6期', 'python7期', 'python8期', 'python9期', 'python10期']
# obj = list(f"python{i}期" for i in range(1,11) if i != 5)
# print(obj)
# # 8. 构建一个列表：[(0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 6)]
# obj = list(((i, i+1) for i in range(6)))
# print(obj)
# # 9. 构建一个列表：[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]
# obj = list((i for i in range(20) if  i % 2 == 0))
# print(obj)

# # 10. 有一个列表
# l1 = ['alex', 'WuSir', '老男孩', '太白']
# # 将其构造成这种列表['alex0', 'WuSir1', '老男孩2', '太白3']
# scq = list((l1[i]+str(i) for i in range(4)))
# print(scq)

# 11. 有以下数据类型：
#
# ```python
x = {'name':'alex',
     'Values':[{'timestamp':1517991992.94,'values':100,},
               {'timestamp': 1517992000.94,'values': 200,},
            {'timestamp': 1517992014.94,'values': 300,},
            {'timestamp': 1517992744.94,'values': 350},
            {'timestamp': 1517992800.94,'values': 280}],}
# # ```
# # 将上面的数据通过列表推导式转换成下面的类型：[[1517991992.94, 100], [1517992000.94, 200], [1517992014.94, 300], [1517992744.94, 350], [1517992800.94, 280]]
# ob = list([i['timestamp'],i['values']] for i in x["Values"])
# print(ob)
# 12. 用列表完成笛卡尔积
#
#     什么是笛卡尔积？ 笛卡尔积就是一个列表，列表里面的元素是由输入的可迭代类型的元素对构成的元组，因此笛卡尔积列表的长度等于输入变量的长度的乘积。
#
# ​	a. 构建一个列表，列表里面是三种不同尺寸的T恤衫，每个尺寸都有两个颜色（列表里面的元素为元组类型)。
#

# colors = ['black', 'white']
# sizes = ['S', 'M', 'L']
# ob = list(((i,color) for i in sizes for color in colors ))
# print(ob)


# ​	b. 构建一个列表,列表里面的元素是扑克牌除去大小王以后，所有的牌类（列表里面的元素为元组类型）。
# l1 = [('A','spades'),('A','diamonds'), ('A','clubs'), ('A','hearts')......('K','spades'),('K','diamonds'), ('K','clubs'), ('K','hearts') ]
# num = ['A', 'K', 'Q', 'J', '10', '9', '8', '7', '6', '5', '4', '3', '2']
# color = ['spades', 'diamonds', 'hearts', 'spades']
# ob = list((n, c) for n in num for c in color)
# print(ob)

#
# 13. 简述一下yield 与yield from的区别。
# yield 只执行返还一次 yield from 对可迭代对象进行迭代返还 yield from 相当于是for循环加上 yield

# # 14. 看下面代码，能否对其简化？说说你简化后的优点？
#
# def chain(*iterables):
# 	for it in iterables:
# 		for i in it:
# 			yield i
# g = chain('abc',(0,1,2))
# print(list(g))  # 将迭代器转化成列表
#
# num = ["abc",(0,1,2)]
# ret = (i for it in num for i in it)
# print(list(ret))
# # 15. 看代码求结果（**面试题**）：
#
# v = [i % 2 for i in range(10)]
# print(v)
# # [0,1,0,1,0,1,0,1,0,1]

# v = (i % 2 for i in range(10))
# print(v)
# # 内存地址
# for i in range(5):
# 	print(i)  # 01234
# print(i) # 4

# 16. 看代码求结果：（**面试题**）
#
# def demo():
#     for i in range(4):
#         yield i
#
# g=demo()
#
# g1=(i for i in g)
# g2=(i for i in g1)
# # print(g2)
# print(list(g1))# [0,1,2,3]
# print(list(g2))# yield 一个,只能执行一次 执行到最后,后面就无法在执行


# 17. 看代码求结果：（**面试题**）
#
# def add(n, i):
#     return n + i
#
#
# def test():
#     for i in range(4):
#         yield i
#
#
# g = test()
# # for i in g:
# #     print(i)
#
# for n in [1,2, 10]:
#     g = (add(n, i) for i in g)      # 不print list for i in g(生成器)并不执行 g的返回值是
# print(list(g))



import time
def test_time(x):
    def inner():
        start_time = time.time()
        x()
        end_time = time.time()
        print(f"此函数的执行效率{end_time - start_time}")
        return True
    return inner
@test_time
def index():
    for i in range(100000000):
        pass
print(index())

