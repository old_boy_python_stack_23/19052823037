# _*_coding:utf-8_*_

# 1. 暴力摩托程序（完成下列需求）：
#
#    1. 创建三个游戏人物，分别是：
#
#       ​	苍井井，女，18，攻击力ad为20，血量200
#
#       ​	东尼木木，男，20，攻击力ad为30，血量150
#
#       ​	波多多，女，19，攻击力ad为50，血量80
class Human:
    def __init__(self, name, sex, age, ad, hp):
        self.name = name
        self.sex = sex
        self.age = age
        self.ad = ad
        self.hp = hp

    def ride(self, moto):
        print(f"{self.name}骑着{moto.name}开着{moto.speed}迈的车行驶在赛道上。")

    def atack(self, human, weapon=None, moto=None):
        if weapon == None and moto == None:
            print(f'{self.name}赤手空拳打了{human.name}{self.ad}滴血，{human.name}还剩{human.hp - self.ad}血。')
        elif weapon:
            print(f'{self.name}用{weapon.weapon_name}打了{human.name}{self.ad + weapon.weapon_ad}滴血，{human.name}还剩{human.hp - (self.ad + weapon.weapon_ad)}血。')
        # elif

cang = Human('苍井井', '女', 18, 20, 200)
dong = Human('东尼木木', '男', 20, 30, 150)
bobo = Human('波多多', '女', 19, 50, 80)


#    2. 创建三个游戏武器，分别是：
#
#       ​  平底锅，ad为20
#
#       ​	斧子，ad为50
#
#       ​	双节棍，ad为65
class Weapon:
    def __init__(self, weapon_name, weapon_ad):
        self.weapon_name = weapon_name
        self.weapon_ad = weapon_ad


w1 = Weapon('平底锅', 20)
w2 = Weapon('斧子', 20)
w3 = Weapon('双截棍', 65)


#    3. 创建三个游戏摩托车，分别是：
#
# ​        小踏板，速度60迈
#
# ​        雅马哈，速度80迈
#
# ​        宝马，速度120迈。
class Moto:
    def __init__(self, name, speed):
        self.name = name
        self.speed = speed


m1 = Moto('小踏板', 60)
m2 = Moto('雅马哈', 80)
m3 = Moto('宝马', 120)

# ​    完成下列需求（利用武器打人掉的血量为武器的ad + 人的ad）：
#
# ​	（1）苍井井骑着小踏板开着60迈的车行驶在赛道上。
Human('苍井井', '女', 18, 20, 200).ride(Moto('小踏板', 60))
# ​	（2）东尼木木骑着宝马开着120迈的车行驶在赛道上。
Human('东尼木木', '男', 20, 30, 150).ride(Moto('宝马', 120))
# ​	（3）波多多骑着雅马哈开着80迈的车行驶在赛道上。
Human('波多多', '女', 19, 50, 80).ride(Moto('雅马哈', 80))
# ​	（4）苍井井赤手空拳打了波多多20滴血，波多多还剩xx血。
Human('苍井井', '女', 18, 20, 200).atack(Human('波多多', '女', 19, 50, 80))
# ​	（5）东尼木木赤手空拳打了波多多30滴血，波多多还剩xx血。
dong.atack(bobo)
# ​	（6）波多多利用平底锅打了苍井井一平底锅，苍井井还剩xx血。
bobo.atack(cang, w1)
# ​	（7）波多多利用斧子打了东尼木木一斧子，东尼木木还剩xx血。
bobo.atack(dong, w2)
# ​	（8）苍井井骑着宝马打了骑着小踏板的东尼木木一双节棍，东尼木木哭了，还剩xx血。（选做）
cang.ride(m3)
# ​	（9）波多多骑着小踏板打了骑着雅马哈的东尼木木一斧子，东尼木木哭了，还剩xx血。（选做）
#
# 2. 定义一个类，计算圆的周长和面积。
import math


class circle:
    def __init__(self, radius):
        self.radius = radius

    def perimeter(self):  # 周长
        return self.radius * math.pi * 2

    def area(self):  # 面积
        return self.radius ** 2 * math.pi


#
# 3. 定义一个圆环类，计算圆环的周长和面积（升级题）。
class annulus:
    def __init__(self, radius1, radius2):
        self.radius1 = radius1
        self.radius2 = radius2

    def perimeter(self):
        return (self.radius1 + self.radius2) * math.pi * 2

    def area(self):
        return (self.radius1 ** 2 - self.radius2 ** 2) * math.pi
