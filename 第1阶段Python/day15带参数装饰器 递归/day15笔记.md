# 带参数装饰器和递归
## 带参数装饰器
```python

def wrapper(f):
    def inner(*args,**kwargs):
        if f.__name__ == 'qq':

            ret = f(*args,**kwargs)
            return ret
        else:

    return inner


def wrapper(f):
    def inner(*args, **kwargs):
        ret = f(*args, **kwargs)
        return ret
    return inner

def wrapper_out(n,*args,sex='男',):
    def wrapper(f):  # f
        def inner(*args,**kwargs):
            ret = f(*args,**kwargs)  # func1()
            return ret
        return inner
    return wrapper

def func1():
    print('in func1')
func = wrapper_out(1)  # wrapper函数名
ly = func(func1)  # wrapper(func1) = inner
ly()  # inner()


def wrapper_out(n):
    def wrapper(f):
        def inner(*args,**kwargs):
            # if n == 'qq':
            #     username = input('请输入用户名：').strip()
            #     password = input('请输入密码：').strip()
            #     with open('qq',encoding='utf-8') as f1:
            #         for line in f1:
            #             user,pwd = line.strip().split('|')
            #             if username == user and password == pwd:
            #                 print('登陆成功')
            #                 ret = f(*args,**kwargs)
            #                 return ret
            #         return False
            # elif n == 'tiktok':
            #     username = input('请输入用户名：').strip()
            #     password = input('请输入密码：').strip()
            #     with open('tiktok', encoding='utf-8') as f1:
            #         for line in f1:
            #             user, pwd = line.strip().split('|')
            #             if username == user and password == pwd:
            #                 print('登陆成功')
            #                 ret = f(*args, **kwargs)
            #                 return ret
            #         return False
            username = input('请输入用户名：').strip()
            password = input('请输入密码：').strip()
            with open(n,encoding='utf-8') as f1:
                for line in f1:
                    user,pwd = line.strip().split('|')
                    if username == user and password == pwd:
                        print('登陆成功')
                        ret = f(*args,**kwargs)
                        return ret
                return False
        return inner
    return wrapper
"""
# @wrapper_out('qq')
# def qq():
#     print('成功访问qq')
# qq()
# 看到带参数的装饰器分两步执行：
'''
@wrapper_out('腾讯')
    1. 执行wrapper_out('腾讯') 这个函数，把相应的参数'腾讯' 传给 n,并且得到返回值 wrapper函数名。
    2. 将@与wrapper结合，得到我们之前熟悉的标准版的装饰器按照装饰器的执行流程执行。
'''
"""


@wrapper_out('qq')
def qq():
    print('成功访问qq')


@wrapper_out('tiktok')
def tiktok():
    print('成功访问抖音')

qq()
tiktok()
开发思路：增强耦合性
```
## 多个装饰器装饰一个函数
```python
def wrapper1(func1):  #　func1 = f原函数
    def inner1():
        print('wrapper1 ,before func')  # 2
        func1()
        print('wrapper1 ,after func')  # 4
    return inner1

def wrapper2(func2):  # func2 == inner1
    def inner2():
        print('wrapper2 ,before func')  # 1
        func2()  # inner1
        print('wrapper2 ,after func')  # 5
    return inner2


@wrapper2  # f = wrapper2(f) 里面的f == inner1  外面的f == inner2
@wrapper1  # f = wrapper1(f) 里面的f == func1  外面的 f == inner1
def f():
    print('in f')  # 3

f()  # inner2()

```
## 递归函数
```python
初识递归。
def func():
    print('in func')

def func1():
    func()
func1()

比如：

def func():
    print(666)
    func()
func()

def func(n):
    print(n)
    n += 1
    func(n)
func(1)

官网规定：默认递归的最大深度1000次。
如果你递归超过100次还没有解决这个问题，那么执意使用递归，效率很低。
import sys
print(sys.setrecursionlimit(1000000))

def func(n):
    print(n)
    n += 1
    func(n)
func(1)

#


'''
1  太白   18
2  景女神  18 + 2
3  宝元    18 + 2 + 2
4  alex    18 + 2+ 2+2  age(4) = age(3) + 2
'''

def age(n):
    if n == 1:
        return 18
    else:
        return age(n-1) + 2

# print(age(4))

def age(4):
    if n == 1:
        return 18
    else:
        return age(3) + 2

age(4) = age(3)  + 2

def age(3):
    if n == 1:
        return 18
    else:
        return age(2) + 2
age(4) = age(2) + 2  + 2


def age(2):
    if n == 1:
        return 18
    else:
        return age(1) + 2
age(4) = age(1) + 2 + 2  + 2

def age(1):
    if n == 1:
        return 18
    else:
        return age(1) + 2

age(4) = 18 + 2 + 2  + 2


l1 = [1, 3, 5, ['太白','元宝', 34, [33, 55, [11,33]]], [77, 88],66]
'''
1
3
5
'太白'
'元宝'
34
66
'''
l1 = [1,3,5,['太白','元宝',34],55]
l2 = [1, 3, 5, ['太白','元宝', 34, [33, 55, [11,33]]], [77, 88],66]
for i in l1:
    if type(i) == list:
        for j in i:
            print(j)
    else:
        print(i)

def func(alist):
    for i in alist:
        if type(i) == list:
            func(i)  # func(['太白','元宝',34])
        else:
            print(i)
func(l1)
func(l2)

```