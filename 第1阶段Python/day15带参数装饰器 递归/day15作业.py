# _*_coding:utf-8_*_
# 1. 请实现一个装饰器，限制该函数被调用的频率，如10秒一次（借助于time模块，time.time()）（面试题,有点难度，可先做其他）
import time


## 要不要用sleep???

def mra(f):
    flag = 0

    def inner():
        nonlocal flag

        second_time = time.time()
        if second_time - flag > 10:
            ret = f()
            flag = second_time
            return ret
        else:
            print("请等待")

    return inner


@mra
def prin():
    print("请休眠十秒 ")

prin()
time.sleep(10)
prin()
prin()
# 2. 请写出下列代码片段的输出结果：

# def say_hi(func):
#    def wrapper(*args,**kwargs):
#        print("HI")
#        ret=func(*args,**kwargs)
#        print("BYE")
#        return ret
#    return wrapper
#
# def say_yo(func):
#    def wrapper(*args,**kwargs):
#        print("Yo")
#        return func(*args,**kwargs)
#    return wrapper
# @say_hi
# @say_yo
# def func():
#    print("ROCK&ROLL")
# func()

# HI
# Yo
# ROCK&ROLL
# BYE
#
# 3. 编写装饰器完成下列需求:
#
#    1. 用户有两套账号密码,一套为京东账号密码，一套为淘宝账号密码分别保存在两个文件中。
# with open("京东",mode="w",encoding="utf-8") as f1:
#     f1.write('''用户名|密码
# alex|taibai250
# 太白|alex520''')
# with open("淘宝", mode="w", encoding="utf-8") as f1:
#     f1.write('''用户名|密码
# alex|taibai250
# 太白|alex520''')
#    2. 设置四个函数，分别代表 京东首页，京东超市，淘宝首页，淘宝超市。
#
#    3. 启动程序后,呈现用户的选项为:
#
# ​		1,京东首页
#
# ​		2,京东超市
#
# ​		3,淘宝首页
#
# ​		4,淘宝超市
#
# ​		5,退出程序
#
#    四个函数都加上认证功能，用户可任意选择,用户选择京东超市或者京东首页,
#    只要输入一次京东账号和密码并成功,则这两个函数都可以任意访问;
#    用户选择淘宝超市或者淘宝首页,只要输入一次淘宝账号和密码并成功,
#    则这两个函数都可以任意访问.
#
#       相关提示：用带参数的装饰器。装饰器内部加入判断，验证不同的账户密码。
#
# def login(n):
#     count = 0
#     while True:
#         if count < 3:
#             username_input = input("请输入用户名:").strip()
#             user_password = input("请输入密码:").strip()
#             with open(n, mode="r", encoding="utf-8") as f:
#                 dic = {line.strip().split('|')[0]: line.strip().split('|')[1] for line in f}
#             if username_input in dic and user_password == dic[username_input]:
#                 print("登陆成功")
#                 return True
#             else:
#                 print(f"用户名或密码错误!剩余登陆次数为:{3-count}")
#         else:
#             print("超过三次退出")
#             return False
#         count += 1
#
# flag = False
# flag1 = False
# def wrapper_out(n):
#     def wrapper(f):
#         def inner(*args, **kwargs):
#             if n ==  '京东':
#                 global flag
#                 if flag:
#                     ret = f(*args, **kwargs)
#
#                     return ret
#                 else:
#                     if login(n):
#                         ret = f(*args, **kwargs)
#                         flag = True
#                         return ret
#             else:
#                 global flag1
#                 if flag1:
#                     ret = f(*args, **kwargs)
#                     return ret
#                 else:
#                     if login(n):
#                         ret = f(*args, **kwargs)
#                         flag1 = True
#                         return ret
#         return inner
#     return wrapper
#
#
#
#
# @wrapper_out("京东")
# def 京东首页():
#     print("成功登录京东首页")
#
# @wrapper_out("京东")
# def 京东超市():
#     print("成功登录京东超市")
#
# @wrapper_out("淘宝")
# def 淘宝首页():
#     print("成功登录淘宝首页")
#
# @wrapper_out("淘宝")
# def 淘宝超市():
#     print("成功登录淘宝超市")
#
#
# print('''1 京东首页
# 2 京东超市
# 3 淘宝首页
# 4 淘宝超市''')
# while True:
#     choice = input("请输入要选择的序号:").strip()
#
#     if int(choice) == 1:
#         京东首页()
#         print(flag)
#     elif int(choice) == 2:
#         京东超市()
#         print(flag)
#
#     elif int(choice) == 3:
#         淘宝首页()
#     elif int(choice) == 4:
#         淘宝超市()


# 4. 用递归函数完成斐波那契数列（面试题）：
#
#    斐波那契数列：1，1，2，3，5，8，13，21..........(第三个数为前两个数的和，但是最开始的1，1是特殊情况，可以单独讨论)
#
#    用户输入序号获取对应的斐波那契数字：比如输入6，返回的结果为8.

# a = 0
# b = 0
# n = 1
# def fibonacci(n):
#     if n <= 2:
#         return 1
#     else:
#
#         return fibonacci(n-1) + fibonacci(n-2)
# print(fibonacci(6))


# #
# # 5. 给l1 = [1,1,2,2,3,3,6,6,5,5,2,2] 去重，不能使用set集合（面试题）。
# l1 = [1,1,2,2,3,3,6,6,5,5,2,2]
# l2 = []
# # for i in l1:
# #     if i not in l2:
# #         l2.append(i)
# # l1 = l2
# # print(l1)
# m = len(l1) - 1
# def de(n):
#     global m
#     if m >= 0:
#         if l1[m] not in l2:
#             l2.append(l1[m])
#         m -= 1
#         de(n)
# de(l1)
# print(l2)
# import practice1
# practice1.qq()
# practice1.did()
