# _*_coding:utf-8_*
dic_status = {"username": None, "status": False}
import os
from lib import common
from conf import settings
import json
import hashlib






def register():
    while True:
        username_register = input("请输入注册用户名:").strip()
        password_register = input("请输入注册密码:").strip()
        ret = hashlib.md5()
        ret.update(password_register.encode('utf-8'))
        dicregister = {username_register:ret.hexdigest(),"money":0}
        if username_register.upper() == "Q" or password_register.upper() == "Q":
            return
        elif username_register.isalnum() and len(password_register) in range(6, 14):
            if os.path.exists(f'..\\db\\{username_register}.json'):
                print("用户名已存在,请重新输入")
            else:
                with open(f'..\\db\\{username_register}.json', mode="w", encoding="utf-8") as f1:
                    f1.write(json.dumps(dicregister))
                    print("注册成功")
                    return
        else:
            print("用户名或密码格式错误,请重新输入")

lis111 = []
def login():  # 登陆功能 三次退出此功能 return False 成功 return True
    count = 0
    while True:
        if count < 3:
            username_input = input("请输入用户名:").strip()
            user_password = input("请输入密码:").strip()
            ret = hashlib.md5()
            ret.update(user_password.encode('utf-8'))
            if os.path.exists(f'..\\db\\{username_input}.json'):
                with open(f'..\\db\\{username_input}.json',mode='r',encoding="utf-8") as fex:
                    retx = fex.read()
                    if retx:
                        dicex = json.loads(retx)
                        lis111.append(dicex)
                if ret.hexdigest()== dicex[username_input]:
                    dic_status["username"] = username_input
                    dic_status["status"] = True
                    print("登陆成功")
                    return True
                else:
                    print(f"用户名或密码错误!剩余登陆次数为:{2 - count}")
            else:
                print("用户名不存在")
        else:
            print("超过三次退出")
            exit(0)
        count += 1

import time
@common.wrapper
def seekmoney():
    print(f"您的余额是{lis111[0]['money']}元")
    common.md_logger(dic_status['username']).info(f"您查询了余额")



@common.wrapper
def savemoney():
    money_input = input("请输入存入的余额:").strip()
    lis111[0]['money'] = lis111[0]['money']+int(money_input)
    with open(f"..\\db\\{dic_status['username']}.json副本", mode='w', encoding='utf-8') as fsave:
        fsave.write(json.dumps(lis111[0]))
    os.remove(f"..\\db\\{dic_status['username']}.json")
    os.rename(f"..\\db\\{dic_status['username']}.json副本",f"..\\db\\{dic_status['username']}.json")
    common.md_logger(dic_status['username']).info(f"您存了{money_input}元")



@common.wrapper
def changemoney():
    changeuse = input("请输入对方的用户名:").strip()
    if os.path.exists(f'..\\db\\{changeuse}.json'):
        changemoney1 = input("请输入您要转账的金额:").strip()
        if lis111[0]['money'] >int(changemoney1):
            lis111[0]['money'] = lis111[0]['money'] - int(changemoney1)
            with open(f"..\\db\\{dic_status['username']}.json", mode='w', encoding='utf-8') as fsave:
                fsave.write(json.dumps(lis111[0]))
            with open(f"..\\db\\{changeuse}.json", mode='r+', encoding='utf-8') as fsave1:
                a = json.loads(fsave1.read())
                a['money'] = a['money'] + int(changemoney1)
            with open(f"..\\db\\{changeuse}.json", mode='w', encoding='utf-8') as fsave2:
                fsave2.write(json.dumps(a))
            common.md_logger(dic_status['username']).info(f"您转给{changeuse}{changemoney1}元")
        else:
            print('您的余额不足!')
    else:
        print('您输入的用户不存在!')

@common.wrapper
def seekmoneywater():
    with open(f"..\\log\\{dic_status['username']}.log",mode='r',encoding='utf-8') as fwat:
        print(fwat.read())



def cancel():
    dic_status['username'] = None
    dic_status['status'] = False
    exit(0)


dic = {1: login, 2: register, 3: seekmoney, 4: savemoney, 5: changemoney, 6: seekmoneywater, 7: cancel}


def run():
    while True:
        print("""
1. 登录（可支持多个账户（非同时）登录）。
2. 注册。
3. 查看余额。
4. 存钱。
5. 转账（给其他用户转钱）。
6. 查看账户流水。
7. 退出""")
        choice = input("请输入序号").strip()
        if choice.isdigit():
            if 0 < int(choice) < 8:
                dic[int(choice)]()
        else:
            print("输入格式错误，请重新输入")
