# _*_coding:utf-8_*_
# 有列表 a = ["7net","www.7net","www.septnet","7net","www"]现需要从中将包含字符7net的元素给删掉，请以最少代码量实现。（3分）
a = ["7net", "www.7net", "www.septnet", "7net", "www"]
for i in a[::-1]:
    if '7net' in i:
        a.remove(i)
print(a)