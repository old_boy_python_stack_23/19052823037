# _*_coding:utf-8_*_
# 现在有两元祖(('a'),('b'))，(('c'),('d'))，请使用python中的匿名函数和内置函数生成列表[{'a':'c'},{'b':'d'}] （3分）
t1 = (('a'), ('b'))
t2 = (('c'), ('d'))

dic = dict(zip((('a'), ('b')), (('c'), ('d'))))
print([{i: dic[i]} for i in dic])
func = lambda t1, t2: [{i: dict(zip((('a'), ('b')), (('c'), ('d'))))[i]} for i in
                       dict(zip((('a'), ('b')), (('c'), ('d'))))]
print(func(t1, t2))
