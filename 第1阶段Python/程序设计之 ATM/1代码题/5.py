# _*_coding:utf-8_*_
# 用filter函数过滤出单价大于100的股票。（3分）
#
portfolio = [
    {'name': 'IBM', 'shares': 100, 'price': 91.1},
{'name': 'AAPL', 'shares': 50, 'price': 543.22},
{'name': 'FB', 'shares': 200, 'price': 21.09},
{'name': 'HPQ', 'shares': 35, 'price': 31.75},
{'name': 'YHOO', 'shares': 45, 'price': 16.35},
{'name': 'ACME', 'shares': 75, 'price': 115.65}]
print(list(filter(lambda x:x['price']>100,portfolio)))
