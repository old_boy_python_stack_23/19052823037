#_*_coding:utf-8_*_
# 写一个生成器，里面的元素是20以内所有奇数的平方减一。(2分)
func = (i**2-1 for i in range(20) if i %2==1)
print(list(func))