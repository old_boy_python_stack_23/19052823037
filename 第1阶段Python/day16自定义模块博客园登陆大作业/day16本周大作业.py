# _*_coding:utf-8_*_
# 作业：用代码模拟博客园系统
# 项目分析：
# 一．首先程序启动，页面显示下面内容供用户选择：


#
# 1.请登录
# 2.请注册
# 3.进入文章页面
# 4.进入评论页面
# 5.进入日记页面
# 6.进入收藏页面
# 7.注销账号
# 8.退出整个程序
#
# 二．必须实现的功能：
#
# 1.注册功能要求：
# a.用户名、密码要记录在文件中。
# b.用户名要求：只能含有字母或者数字不能含有特殊字符并且确保用户名唯一。
# c.密码要求：长度要在6~14个字符之间。
# 定义登陆注册功能


# 2.登录功能要求：
# a.用户输入用户名、密码进行登录验证。
# b.登录成功之后，才可以访问3~7选项，如果没有登录或者登录不成功时访问3~7选项，不允许访问，让其先登录。（装饰器）
# c.超过三次登录还未成功，则退出整个程序。


# 3.进入文章页面要求：
# a.提示欢迎xx进入文章页面。
# b.此时用户可以选择：直接写入内容，还是导入md文件。
# ①如果选择直接写内容：让学生直接写文件名|文件内容......最后创建一个文章。
#
# ②如果选择导入md文件：让用户输入已经准备好的md文件的文件路径（相对路径即可：比如函数的进阶.md），
# 然后将此md文件的全部内容写入文章（函数的进阶.md）中。


# 4.进入评论页面要求：
# 提示欢迎xx进入评论页面。

# 5.进入日记页面要求：
# 提示欢迎xx进入日记页面。

# 6.进入收藏页面要求：
# 提示欢迎xx进入收藏页面。

#
# 7.注销账号要求：
# 不是退出整个程序，而是将已经登录的状态变成未登录状态（访问3~7选项时需要重新登录）。

# 8.退出整个程序要求：

# 就是结束整个程序。
#
# 三．选做功能：
# 1.评论页面要求：
# # a.提示欢迎xx进入评论页面。
# # b.让用户选择要评论的文章。
# # 这个需要借助于os模块实现此功能。将所有的文章文件单独放置在一个目录中，利用os模块listdir功能,
# 可以将一个目录下所有的文件名以字符串的形式存在一个列表中并返回。
# # 例如：
# # 代码：
# # import os
# # print(os.listdir(r'D:\teaching_show\article'))
# # # ['01 函数的初识.text 选课系统 作业设计', '02 函数的进阶.md']
# # c.选择要评论的文章之后，先要将原文章内容全部读一遍，然后输入的你的评论，评论要过滤掉这些敏感字符：
# "苍老师", "东京热", "武藤兰", "波多野结衣"，替换成等长度的"*"之后，写在文章的评论区最下面。
# # 文章的结构：
#
# # 文章具体内容
# # .......
# #
# # 评论区：
# # -----------------------------------------
# #           (用户名)xx:
# #           评论内容
# #           (用户名)oo:
# #           评论内容
# # 原文章最下面如果没有以下两行：
# # """
# # 评论区：
# # -----------------------------------------
# # """
# # 就加上这两行在写入评论，如果有这两行则直接在下面顺延写上：
# # 	(用户名)xx:
# #           	评论内容


import os

dic_status = {"username": None, "status": False}


def register():
    while True:
        username_register = input("请输入注册用户名:").strip()
        password_register = input("请输入注册密码:").strip()
        if username_register.upper() == "Q" or password_register.upper() == "Q":
            return
        elif username_register.isalnum() and len(password_register) in range(6, 15):
            with open("Coed", mode="r", encoding="utf-8") as f:
                dic11 = {line.strip().split("|")[0]: line.strip().split("|")[1] for line in f}
            if username_register not in dic11:
                with open("Coed", mode="a", encoding="utf-8") as f1:
                    f1.write(f"\n{username_register}|{password_register}")
                    print("注册成功")
                    return
            else:
                print("用户名已存在,请重新输入")
        else:
            print("用户名或密码格式错误,请重新输入")


def login():  # 登陆功能 三次退出此功能 return False 成功 return True
    count = 0
    while True:
        if count < 3:
            username_input = input("请输入用户名:").strip()
            user_password = input("请输入密码:").strip()
            with open("Coed", mode='r', encoding="utf-8") as f2:
                dic1 = {line.strip().split("|")[0]: line.strip().split("|")[1] for line in f2}
            if username_input in dic1 and user_password == dic1[username_input]:
                dic_status["username"] = username_input
                dic_status["status"] = True
                print("登陆成功")
                return True
            else:
                print(f"用户名或密码错误!剩余登陆次数为:{2 - count}")
        else:
            print("超过三次退出")
            return False
        count += 1


def wrapper(f):  # 装饰器 登录验证功能
    def inner(*args, **kwargs):
        if dic_status["status"]:
            ret = f(*args, **kwargs)
            return ret
        else:
            if login():
                ret = f(*args, **kwargs)
                return ret

    return inner


def writeing():  # 手动输入功能
    file_input = input("请输入文件名:")
    content_input = input("请输入内容:")
    with open(f"comment/{file_input}", mode='a', encoding='utf-8') as f3:
        f3.write(f"{content_input}")
    return


def lead():  # 导入md文件功能 输入文件名 or 路径
    file1_input = input("请输入文件名:").strip()  # 文件找不到怎么办 加个判断 怎么实现 貌似现在不会啊
    with open(f"{file1_input}", mode='r', encoding='utf-8') as f4:
        content = f4.readline()
    with open(f"comment/{file1_input.split('.')[0]}.text", mode='a', encoding='utf-8') as f5:
        f5.write(content)
    return


dic2 = {1: writeing, 2: lead}


@wrapper
def enter_page():
    print(f"欢迎{dic_status['username']}进入文章页面")
    while True:
        choice2 = input('''请选择
    1, 直接写入内容
    2, 导入md文件''').strip()
        if choice2.isdigit():
            if 0 < int(choice2) < 3:
                dic2[int(choice2)]()
        else:
            print('输入格式错误请重新输入！')


@wrapper
def comment():
    print(f"欢迎{dic_status['username']}进入评论页面")
    oslistdir = os.listdir('comment')
    comment_article = """
评论区：
-----------------------------------------"""
    while True:
        print(os.listdir('comment'))  # ['函数的初识.text 选课系统 作业设计', '函数的进阶.md']
        comment_choice = input("请选择要评论的文章序号:").strip()
        if comment_choice.isdigit():
            if int(comment_choice) in range(len(oslistdir) + 1):
                with open(f"comment/{oslistdir[int(comment_choice) - 1]}", mode="r+", encoding="utf-8") as fb:
                    # print(comment_article in fb.read())
                    comment_comment = input("请输入评论内容:")
                    lis = ["苍老师", "东京热", "武藤兰", "波多野结衣"]
                    for i in lis:
                        if i in comment_comment:
                            comment_comment = comment_comment.replace(i, len(i) * '*')
                    if comment_article in fb.read():
                        fb.write(f'''
          (用户名){dic_status["username"]}
          {comment_comment}''')
                    else:
                        fb.write(f'''
    {comment_article}
          (用户名){dic_status["username"]}
          {comment_comment}''')
        else:
            print("输入错误,请重新输入")


@wrapper
def diary():
    print(f"欢迎{dic_status['username']}进入日记页面")


@wrapper
def collection():
    print(f"欢迎{dic_status['username']}进入收藏页面")


@wrapper
def cancel():
    dic_status['username'] = None
    dic_status['status'] = False


@wrapper
def secede():
    print("成功退出")
    exit(0)


dic = {1: login, 2: register, 3: enter_page, 4: comment, 5: diary, 6: collection, 7: cancel, 8: secede}
def run():
    while True:
        print("""
1.请登录
2.请注册
3.进入文章页面
4.进入评论页面
5.进入日记页面
6.进入收藏页面
7.注销账号
8.退出整个程序""")
        choice = input("请输入序号").strip()
        if choice.isdigit():
            if 0 < int(choice) < 9:
                dic[int(choice)]()
        else:
            print("输入格式错误，请重新输入")

run()