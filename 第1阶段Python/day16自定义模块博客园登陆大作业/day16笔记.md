# 模块



## 1. 模块的定义与分类

什么是模块？一个模块就是一个py文件。

模拟博客园系统作业，100000行代码. 不可能全部存在一个文件.

1. 不易维护.
2. 效率低.

分文件: 10个文件.每个文件有50个函数,有一写相同功能或者相似功能的函数.代码冗余,重复性.我们应该将这10个函数提取出来,放在一个文件中,随用随拿.

1. 节省代码.
2. 容易维护,组织结构更清晰.

一个模块就是一个py文件,这个模块存储很多相似的功能,相似的函数的集合体.

模块的分类:

+ 内置模块,标准库.python解释器自带的,time,os,sys,等等.200多种.
+ 第三方库(模块),各种大神写的一些模块,通过pip install....安装.6000种.
+ 自己写的模块.自定义模块.

2. import

   ```
   # import tbjx
   # # 当我引用tbjx模块的时候,实际上将tbjx.py执行一遍,加载到内存.
   # import tbjx
   # import tbjx
   # import tbjx
   # import tbjx
   # import tbjx
   
   # 只是第一次引用时,将此模块加载到内存.
   ```

   1. 执行文件:02 模块import
   2. 被引用文件(模块): tbjx.py

3. 第一次导入模块发生的三件事

   ```
   import tbjx
   n = 1
   # 引用模块发生三件事.
   '''
       1. 将tbjx.py文件加载到内存.
       2. 在内存中创建一个以tbjx命名的名称空间.
       3. 通过tbjx名称空间的名字.等方式引用此模块的名字(变量,函数名,类名等等).
   '''
   
   # print(tbjx.name)
   # tbjx.read1()
   # tbjx.read2()
   ```

4. 被导入模块有独立的名称空间

   ```python
   # 坑:通过tbjx.的方式引用此模块的名字时,一定一定是从此模块中寻找.
   # 通过import 引用模块 他有自己的独立名称空间,与当前执行文件没有关系.
   name = '李业'
   print(tbjx.name)
   
   def read1():
       print('in 02 模块import')
   
   tbjx.read1()
   
   ```

   

4. 为模块起别名

   将一个比较长的模块名简化成简单的

   import contextlib as cb

   作用 

   ```python
   1 书写方便.
   import tbjx as tb
   print(tb.name)
   tb.read1()
   2 简化代码.
   
   content = input('>>>').strip()
   
   if content == 'mysql':
       import mysql_
       mysql_.sqlprase()
   elif content == 'oracle':
       import oracle_
       oracle_.sqlprase()
   
   content = input('>>>').strip()
   if content == 'mysql':
       import mysql_ as db
   elif content == 'oracle':
       import oracle_ as db
   db.sqlprase()  # 统一化接口
   ```

5. 导入多个模块

   ```
   引入多个模块
   import time,os,sys  # 不推荐.
   
   import time
   import os
   import sys
   
   # 易于阅读 易于编辑 易于搜索 易于维护。
   ```

6. from... import...

   ```
   # from tbjx import name
   # from tbjx import read1
   # print(globals())
   # 相当于从tbjx模块的全局空间中将name,read1变量与值的对应关系
   # 复制到当前执行文件的全局名称空间中.
   # print(name)
   # read1()
   
   # 优点:使用起来方便了.
   # 缺点:容易与当前执行文件产生覆盖效果.
   
   # 示例1:
   # from tbjx import name
   # from tbjx import read1
   # name = '李业'
   # print(name)
   
   # 示例2:
   # name = '怼怼哥'
   # from tbjx import name
   # from tbjx import read1
   # def read1():
   #     print('在执行文件中')
   # # print(name)
   # read1()
   
   # 特殊情况:极值情况,工作中不会出现.(了解)
   # 因为如果你要是引用一些模块的变量,那么执行文件中就不应该出现同名变量.
   # 示例3:
   # from tbjx import name
   # from tbjx import change
   #
   # change()
   # print(name)
   
   # 示例4:
   # from tbjx import change
   #
   # change()
   # from tbjx import name
   # print(name)
   
   # 也可以起别名
   # from tbjx import name as n
   #
   # print(n)
   
   # 导入多个名字
   # from tbjx import name
   # from tbjx import read1
   
   # from ... import *  尽量别单独用
   # from tbjx import *
   # print(name)
   # read1()
   # read2()
   # 1,全部将tbjx的所有名字复制过来,无用功.
   # 2,容易覆盖.
   
   # from ... import * 与__all__配合使用(写在模块文件中)
   
   # from tbjx import *
   #
   # # read1()
   # # read2()
   # change()
   ```

7. py文件的两种功能

   1. py文件的第一个功能:执行文件(承载代码) 脚本.

      直接打印`__name__`返回`__main__`

   2. py文件的第二个功能: 模块(被执行文件).

      直接打印`__name__`返回`tbjx` 模块名

   作用:用来控制.py文件在不同的应用场景下执行不同的逻辑（或者是在模块文件中测试代码）

8. 模块的搜索路径

   ```
   # 寻找模块的路径: 内存 ----> 内置模块  ---> sys.path中找
   # 只要这三个地方:内存 内置模块 sys.path可以找到这个引用的模块的路径,这个模块就可以直接引用到.
   # import sys
   # # print(sys.path)
   # # import tbjx
   # print(sys.modules)
   
   # 如何引用到tbjx1.py
   import sys
   # print(sys.path)
   sys.path.append(r'D:\s23\day15')
   # import tbjx
   import tbjx1
   tbjx1.read1()
   ```




