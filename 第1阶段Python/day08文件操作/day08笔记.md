## 文件操作是什么
操作文件：
f = open("文件",mode="模式",encoding="编码")
open("文件的路径") # 调用操作系统打开文件
mode # 对文件的操作方式
encoding # 文件编码  -- 存储编码要统一
f  文件句柄 --- 操作文件的相当于锅把


## 操作文件流程：
1 打开文件
2 操作文件
3 关闭文件


## 文件操作怎么用？
读 r rb r+ r+b

r
f = open("a",mode="r",encoding="gbk")
content = f.read()  # 全部读取
c1 = f.read(3)  # 字符读取

f.readline()读取一行内内容中多个字符 ()中是读取多少字符
f.readlines() 一行一行读取，存储到列表中  \n是换行

文件读取规则，每次读取完 光标就会停留在读取的最后位置，
再次读取，读取的是光标后的内容,如果文件读取完,再次读取,没有内容

## 坑 :前方高能 请注意

f = open("d:\\a\国际章.txt",mode="r",encoding="utf-8")
print(f.read())
\a 有特殊意义在这里 使用\\a
或者(r"a:\a\国际章.txt")
r  就是 repr()
对比下加 r 和不加r 的区别
print(repr("D:\a\国际章.txt"))
print("D:\a\国际章.txt")


## 路径：
D:\a\国际章.txt  绝对路径 从磁盘的根处查找
相对路径     相对于当前文件进查找 返回符 ："..."

## 查看当前工作路径
import os
print(os.getcwd()))# 查看当前工作路径

## 文件读取规则：
一行一行的读取，否则文件过大会导致内存溢出
for i in f:
    print(i)


## 写  w 清空写   a 追加写
1.先判断有没有文件，文件存在清空文件，文件不存在创建文件
2.写入内容

f = open("day08文件示例",mode="w",encoding="utf-8")
f.write("123")
f.close()

f = open("day08文件示例",mode="a",encoding="utf-8")
f.write("aaa,haokuna")
在文件末尾添加

## 读写 非文本文件
f = open("day08文件示例",mode="rb")
rb,wb,ab 不能指定编码
print(f.read())   # 全部读取
print(f.read(3))  # 读取字节

import requests
ret = requests.get("http://www.521609.com/uploads/allimg/151124/1-1511241G251317.png")
f = open("爬虫照片.jpg",mode="wb")
f.write(ret.content)
f.close()

## 文件的+ 模式
r 读  r+ 读写
w 写  w+ 写读
a 写  a+ 写读

错误示范
f = open("day8",mode="r+",encoding="utf-8")
f.write("你好啊")
print(f.read())

正确示范   -- 后期开发中使用频率比较低
f = open("day8",mode="r+",encoding="utf-8")
print(f.read())
f.write("脑瓜疼啊脑瓜疼")

w+  写读
f = open("day8",mode="w+",encoding="utf-8")
f.write("你您你你")
print(f.read())

a+  追加写读
f = open("a",mode="a+",encoding="gbk")
print(f.tell())  # 查看的是字节
f.seek(0)
print(f.read(1)) # 字符

f.write("真的废物")

其他操作:
查看光标: tell()  返回值 返回的就是当前光标的位置
移动光标:
  seek(0,0) 文件开始位置
  seek(0,1) 光标的当前位置
  seek(0,2) 文件末尾位置
  seek(3)   按照字节调节  使用utf-8是3 gbk是2

## 另一种打开文件的方式
with open("day8",mode="r",encoding="utf-8") as f:  # 面向对象中上下文管理
    print(f.read())

with open 帮助自动关闭文件
with open 同时操作多个文件

with open("day8",mode="r",encoding="utf-8") as f,\
    open("a",mode="w",encoding="gbk") as f1:
    print(f.read())
    f1.write("真饿!")


## 文件的修改
with open("day8",mode="r+",encoding="utf-8")as f:
    content = f.read()
    content = content.replace("您","你")
    f.seek(0,0)
    f.write(content)

with open("day8",mode="r",encoding="utf-8")as f,\
open("new_day8",mode="a",encoding="utf-8")as f1:
    for i in f:
        content = i.replace("你","我")
        f1.write(content)

import os
os.remove("day8")  # 原数据可以使用rename来做备份
os.rename("new_day8","day8")