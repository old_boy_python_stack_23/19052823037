## 1.列表

### 1. 列表初识

#### 什么是列表?

list  []  多个元素 每个元素中间用,逗号隔开

s = "1234Trueabc"  #类型单一

##### 定义方式一:  常用

li = [1,2,3,'123',True,[1,2,5]]   # 容器
列表 == 书包
    水杯
    衣服
    袜子
    钱包
        1,2,5

容器存放的类型是多种,任意类型都可以放到容器中

##### 定义方式二:

li = list("abc")  #底层使用了for循环  "abc" 可迭代的字符串类型
print(li)

#### 列表能干啥?

存储大量数据,字符串存储少量数据的
有序的?  按照咱们写的顺序输出
可变的 -- 可以在本身上进行修改

### 1.2 列表的操作

#### 1.如何使用列表?
s = "123"
print(s)
li = [1,2,3,"123",True]
       0 1 2    3       4
print(li)

##### 有序支持索引
print(li[3],type(li[3]))
print(li[-1],type(li[-1]))

li = [1,2,3,"123",True]
   0 1 2   3      4
   -5 -4 -3  -2    -1
##### 有序支持切片
print(li[1:4])

##### 支持步长
s = "alex"
print(type(s[1:3:1]))   # 切片后还是原来的数据类型
print(type(li[-1:-4:-1]))

li = [1,2,3,"123",True,[1,2,3,False],]
print(li[-1:-4:-2])  # 起始 + 步长 -1 + -2  = -3  + -2 = -5

##### 列表是有序的:

​    支持索引 ,切片,步长
##### 列表是可变的:

​     增加 - 删除 - 修改

#### 2.列表怎么用?

##### 增
​    li = [1,2,3,'alex',3,[1,2,3]]
​          0,1,2,  3,    4, 5
​    li.append('太亮')             # 追加  添加在末尾  添加的操作
​    print(li)

```python
li.insert(0,"大象")           # 插入 第一个参数(索引),第二参数要插入的内容
效率特别低
print(li)

li.extend("abc")    `            # 扩展 -- 迭代添加  -- for
print(li)

li = [1,2,3]
li.extend("abc")

迭代添加开始

for i in "abc":
    li.append(i)

迭代添加结束
print(li)

l3 = [1,2,3]
l2 = ["a","b","c"]
l4 = l3 + l2  # 列表合并
print(l4)
[1,2,3,["a","b","c"]]
```

##### 删

li = [1,2,3,"abc",True]
print(li.pop(3))  弹 

pop有返回值  返回的是被删除的内容

默认删除的是最后一个

可以通过索引指定删除

print(li)

li.remove(1)   # 删除   按照元素名进行删除
print(li)

li = [1,2,3,"abc",True]
del 关键字
del li   # del li 将整个容器都删掉了
del li[2]   #del 支持索引删除
del li[0:3]   #del 支持切片删除
del li[::2]    # del 支持步长删除
print(li)

li = [1,2,3,43]
li.clear()  # 清空
print(li)

##### 改

li = ["水杯",2,3,"abc",]
l2 = [1,2,3,4,66,7]

li[-1] = "奶瓶"

li[1] = [1,2,3]

li[1:2] = []  #[66,7]  # 迭代进去的 可以多,可以少

print(li)

面试题:
li = [1,2,3,4,"abc",5]
li[0:2] = "ABC"
print(li)

结果:
["A","B","C",3,4,"abc",5]

索引
切片
步长 ---  你取几个,就放几个  不能多不能少
li = ["水杯",2,3,"abc",]
li[0:3:2] = [1,2,3],[1234]
print(li)

a,b = [1,23],[3,45]
print(a,b)

a = 1
b = 2
a = 2  b = 1
a,b = b,a
print(a,b)

##### 查
li = [1,2,3,4]
print(li)

print(li[1])

for 循环

for em in li:
    print(em)

### 1.3 列表的嵌套

列表的嵌套:
li = ["水杯","衣服","鞋子","书","电脑","屁",["手机","钥匙",["身份证","银行卡"]],]

l2 = li[6]
l3 = l2[2]
print(l3[0])

li[6]  == ["手机","钥匙",["身份证","银行卡"]]
li[6][2] == ["身份证","银行卡"]
print(li[6][2][0])

li = ["高圆圆","吴奇隆","汪峰",
      ["国际章","张曼兮","龙泽罗拉"],
      "陈冠希",
      ["张柏芝","阿娇",["熊哥",["一本道"],"亮哥",["北京热"],"
      怼哥",["动物园"],"奇奇",["大象"]],"大S",["小S"],"范冰冰"],
      "周星驰",["逃学威龙","功夫","大话西游"]]

print(li[5][2][1])
print(li[5][2][-1][0])

li = [1,2,3,4,1,2]

li.remove()
print(li)

## 2.元组(了解)

### 元组的初识

#### 他是什么?
元组  tuple
tu = ("1")  # ()  每个元素都以 逗号隔开
tu1 = (1)
小括号中一个元素的时候没有逗号就括号中元素的本身
小括号中一个元素的时候有个逗号就是元组
空的小括号就是元组
print(type(tu1))

#### 元组有序:
​    索引,切片,步长
元组不可变数据类型

tu = (1,2,3)
print(tu[1])

元组只能查看不能修改   -- 元组和列表和相似,元组是不可修改的

#### 他能干啥?
存储一些你不想让别人修改的数据  - 元组容器 存储一些任意类型

#### 在哪干?
在配置文件中,程序中  为了防止误操作修改一些数据

### 元组的嵌套


元组的嵌套:

tu = (1,2,3,(4,5,6,[6,7,8,(9,11,10),"abc"],"ABC"))
print(tu[3][3][3][1])  # [6,7,8,(9,11,10),"abc"]
print(tu[-1][-2][-1])
