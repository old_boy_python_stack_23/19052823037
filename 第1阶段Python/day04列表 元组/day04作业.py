# # 1.写代码，有如下列表，按照要求实现每一个功能
# li = ["alex.txt.txt", "WuSir", "ritian", "barry", "wenzhou"]
#
# # 1)计算列表的长度并输出
# print(len(li))
#
# # 2)列表中追加元素"seven", 并输出添加后的列表
# li.append("seven")
# print(li)
#
# # 3)请在列表的第1个位置插入元素"Tony", 并输出添加后的列表
# li.insert(0, "Tony")
# print(li)
#
# # 4)请修改列表第2个位置的元素为"Kelly", 并输出修改后的列表
# li[1] = "Kelly"
# print(li)
#
# # 5)请将列表l2 = [1, "a", 3, 4, "heart"]的每一个元素添加到列表li中，一行代码实现，不允许循环添加。
# l2 = [1, "a", 3, 4, "heart"]
# print(li + l2)
# li.extend(l2)
#
# # 6)请将字符串s = "qwert"的每一个元素添加到列表li中，一行代码实现，不允许循环添加。
# s = "qwert"
# li.extend(s)
# print(li)
#
# # 7)请删除列表中的元素"ritian", 并输出输出后的列表
# li.remove("ritian")
# print(li)
#
# # 8)请删除列表中的第2个元素，并输出删除的元素和删除元素后的列表
# print(li.pop(1))
# print(li)
#
# # 9)请删除列表中的第2至4个元素，并输出删除元素后的列表
# del li[1:5]
# print(li)
#
# # 10)请将列表所有得元素反转，并输出反转后的列表
# print(li[::-1])
#
# # 11)请计算出"alex.txt.txt"元素在列表li中出现的次数，并输出该次数。
# print(li.count("alex.txt.txt"))
#
# # 2.写代码，有如下列表，利用切片实现每一个功能
#
# li = [1, 3, 2, "a", 4, "b", 5, "c"]
# # 1)通过对li列表的切片形成新的列表l1, l1 = [1, 3, 2]
# print(li[0:3])
#
# # 2)通过对li列表的切片形成新的列表l2, l2 = ["a", 4, "b"]
# print(li[3:6])
#
# # 3)通过对li列表的切片形成新的列表l3, l3 = ["1,2,4,5]
# print(li[::2])
#
# # 4)通过对li列表的切片形成新的列表l4, l4 = [3, "a", "b"]
# print(li[1:6:2])
#
# # 5)通过对li列表的切片形成新的列表l5, l5 = ["c"]
# print(list(li[-1]))
#
# # 6)通过对li列表的切片形成新的列表l6, l6 = ["b", "a", 3]
# print(li[-3::-2])
#
# # 3.写代码，有如下列表，按照要求实现每一个功能。
#
# lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
# # 1)将列表lis中的"tt"变成大写（用两种方式）。
# lis[3][2][1][0] = "TT"
# print(lis)
# lis[3][2][1][0] = lis[3][2][1][0].upper()
# print(lis)
#
# # 2)将列表中的数字3变成字符串"100"（用两种方式）。
# lis[1] = "100"
# print(lis)
# del lis[1]
# lis.insert(1, "100")
# print(lis)
#
# # 3)将列表中的字符串"1"变成数字101（用两种方式）。
# lis[-3][-2][-1][-1] = 101
# print(lis)
# del lis[-3][-2][-1][-1]
# lis[-3][-2][-1].insert(2, 101)
# print(lis)
#
# # 4.请用代码实现
# li = ["alex.txt.txt", "wusir", "taibai"]
# # 利用下划线将列表的每一个元素拼接成字符串"alex_wusir_taibai"
# print(li[0], li[1], li[2], sep="_")
# new_li = "_".join(li)
# print(new_li)
#
# # 5.利用for循环和range打印出下面列表的索引
# li = ["alex.txt.txt", "WuSir", "ritian", "barry", "wenzho"]
# for i in range(len(li)):
#     print(i)
#
# # 6.利用while循环打印出下面列表的索引
# li = ["alex.txt.txt", "WuSir", "ritian", "barry", "wenzho"]
# suoyin = 0
# while suoyin < len(li):
#     print(suoyin)
#     suoyin += 1
#
# # 7.利用for循环和range找出100以内所有的偶数并将这些偶数添加到一个新列表中。
# lis = []
# for i in range(100):
#     if i % 2 == 0:
#         lis.append(i)
# print(lis)
#
# # 8.利用for循环和range找出50以内能被3整除的数，并将这些数插入到一个新列表中。
# lis = []
# for i in range(1, 50):
#     if i % 3 == 0:
#         lis.append(i)
# print(lis)
#
# # 10.利用for循环和range从100~1，倒序打印。
# lis = []
# for i in range(100,0,-1):
#     lis.append(i)
# print(lis)
#
# # 11.利用for循环和range打印100~10，倒序将所有的偶数添加到一个新列表中，然后在对列表的元素进行筛选，将能被4整除的数留下来。
# lis = []
# l2 = []
# for i in range(100,9):
#     if i%2 == 0:
#         lis.append(i)
# for n in lis:
#     if n % 4 == 0:
#         l2.append(n)
# print(l2)
#
# # 12.利用for循环和range，将1-30的数字依次添加到一个列表中，并循环这个列表，将能被3整除的数改成*。
# lis = []
# for i in range(1, 31):
#     lis.append(i)
# for n in lis:
#     if n % 3 == 0:
#         lis[n - 1] = "*"
# print(lis)
#
# # 13.查找列表li中的元素，移除每个元素的空格，并找出以"A"或者"a"开头，并以"c"结尾的所有元素，并添加到一个新列表中,最后循环打印这个新列表。
# li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", " aqc"]
# l2 = []
# for i in li:
#     n = i.strip()
#     if n.startswith("A"):
#         if n.endswith("c"):
#             l2.append(n)
#     elif n.startswith("a"):
#         if n.endswith("c"):
#             l2.append(n)
# for t in l2:
#     print(t)

# # 14.开发敏感词语过滤程序，提示用户输入评论内容，如果用户输入的内容中包含特殊的字符：
# # 敏感词列表 li = ["苍老师", "东京热", "武藤兰", "波多野结衣"]
# # 则将用户输入的内容中的敏感词汇替换成等长度的*（苍老师就替换***），并添加到一个列表中；如果用户输入的内容没有敏感词汇，则直接添加到上述的列表中。
l2 = []
li = ["苍老师", "东京热", "武藤兰", "波多野结衣"]
n = 0
nu = input("请输入评论内容:")
for i in li:
    if i in nu:
        nu = nu.replace(i, len(i)*"*")
l2.append(nu)
print(l2)

# 15.有如下列表（选做题）
li = [1, 3, 4, "alex.txt.txt", [3, 7, 8, "BaoYuan"], 5, "RiTiAn"]
# 循环打印列表中的每个元素，遇到列表则再循环打印出它里面的元素。
# 我想要的结果是：
# 1
# 3
# 4
# alex.txt.txt
# 3
# 7
# 8
# baoyuan
# 5
# ritian
# num = 0
# while num < len(li):
#     if type(li[num]) == list:
#         w = li[num]
#         print(li[num])
#         del li[num]
#         for i in w[::-1]:
#             li.insert(num, i)
#     num += 1
# for b in li:
#     if type(b) == str:
#         print(b.lower())
#     else:
#         print(b)
# 方法二
# for i in li:
#     if type(i) == list:
#         for em in i:
#             if type(em) == str:
#                 print(em.lower())
#             else:
#                 print(em)
#     elif type(i) == str:
#         print(i.lower())
#     else:
#         print(i)

# 端午作业:
#
# 1.day4之前所有内容,进行笔记整理
#
# 2.整理day4之前的所有的知识点画思维导图
#
# 3.继续向后预习
