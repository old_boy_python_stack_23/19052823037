# 1. 看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
#
#    ```
class Foo(object):
    a1 = 1

    def __init__(self, num):
        self.num = num

    def show_data(self):
        print(self.num + self.a1)


obj1 = Foo(666)
obj2 = Foo(999)
print(obj1.num)  # 666
print(obj1.a1)  # 1

obj1.num = 18
obj1.a1 = 99

print(obj1.num)  # 18
print(obj1.a1)  # 99

print(obj2.a1)  # 1
print(obj2.num)  # 999
print(obj2.num)  # 999
print(Foo.a1)  # 1
print(obj1.a1)  # 99


#    ```
#
# 2. 看代码写结果，注意返回值。
#
#    ```
class Foo(object):

    def f1(self):
        return 999

    def f2(self):
        v = self.f1()
        print('f2')
        return v

    def f3(self):
        print('f3')
        return self.f2()

    def run(self):
        result = self.f3()
        print(result)


obj = Foo()

v1 = obj.run()  # f3 f2 999

print(v1)  # None


#    ```
#
# 3. 看代码写结果
#
#    ```
class Foo(object):
    def __init__(self, num):
        self.num = num


v1 = [Foo for i in range(10)]
v2 = [Foo(5) for i in range(10)]
v3 = [Foo(i) for i in range(10)]

print(v1)  # [Foo, Foo, Foo, Foo, Foo, Foo, Foo, Foo, Foo, Foo] class
print(v2)  # [Foo(5),Foo(5),Foo(5),Foo(5),Foo(5),Foo(5),Foo(5),Foo(5),Foo(5),Foo(5),] 都是地址
print(v3)  # [Foo(0),Foo(1),Foo(2),Foo(3),Foo(4),Foo(5),Foo(6),Foo(7),Foo(8),Foo(9),] 都是地址


#    ```
#
# 4. 看代码写结果
#
#    ```
class StarkConfig(object):

    def __init__(self, num):
        self.num = num

    def changelist(self, request):
        print(self.num, request)


config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list:
    print(item.num)
'''
1
2
3
'''


#    ```
#
# 5. 看代码写结果：
#
#    ```
class StarkConfig(object):

    def __init__(self, num):
        self.num = num

    def changelist(self, request):
        print(self.num, request)


config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list:
    item.changelist(666)
'''
1 666
2 666
3 666
'''


#    ```
#
# 6. 看代码写结果：
#
#    ```
class Department(object):
    def __init__(self, title):
        self.title = title


class Person(object):
    def __init__(self, name, age, depart):
        self.name = name
        self.age = age
        self.depart = depart


d1 = Department('人事部')
d2 = Department('销售部')

p1 = Person('武沛齐', 18, d1)
p2 = Person('alex', 18, d1)
p3 = Person('安安', 19, d2)

print(p1.name)
'武沛齐'
print(p2.age)
'18'
print(p3.depart)
'd2'  # 地址
print(p3.depart.title)
"销售部"


#    ```
#
# 7. 看代码写结果：
#
#    ```
class Department(object):
    def __init__(self, title):
        self.title = title


class Person(object):
    def __init__(self, name, age, depart):
        self.name = name
        self.age = age
        self.depart = depart

    def message(self):
        msg = "我是%s,年龄%s,属于%s" % (self.name, self.age, self.depart.title)
        print(msg)


d1 = Department('人事部')
d2 = Department('销售部')

p1 = Person('武沛齐', 18, d1)
p2 = Person('alex', 18, d1)
p1.message()
"我是武沛齐,年龄18,属于人事部"
p2.message()
"我是alex,年龄18,属于人事部"


#    ```
#
# 8. 看代码写结果：
#
#    ```
class A:
    def f1(self):
        print('in A f1')


class B(A):
    def f1(self):
        print('in B f1')


class C(A):
    def f1(self):
        print('in C f1')


class D(B, C):
    def f1(self):
        super(B, self).f1()
        print('in D f1')


obj = D()
obj.f1()
"""
in C f1
in D f1
"""


#    ```
#
# 9. 看代码写结果：
#
#    ```
class A:
    def f1(self):
        print('in A f1')


class B(A):
    def f1(self):
        super().f1()
        print('in B f1')


class C(A):
    def f1(self):
        print('in C f1')


class D(B, C):
    def f1(self):
        super().f1()
        print('in D f1')


obj = D()
obj.f1()
"""
in C f1
in B f1
in D f1
"""

#    ```
#
# 10. 程序设计题：
#
#     ```
#     运用类完成一个扑克牌类(无大小王)的小游戏：
#     用户需要输入用户名，以下为用户可选选项:
#         1. 洗牌
#         2. 随机抽取一张
#         3. 指定抽取一张
#         4. 从小到大排序
#         5. 退出
#
#     1. 洗牌：每次执行的结果顺序随机。
#     2. 随机抽取一张：显示结果为：太白金星您随机抽取的牌为：黑桃K
#     3. 指定抽取一张：
#         用户输入序号（1~52）
#         比如输入5，显示结果为：太白金星您抽取的第5张牌为：黑桃A
#     4. 将此牌从小到大显示出来。A -> 2 -> 3 .......-> K
#
#     提供思路：
#         52张牌可以放置一个容器中。
#         用户名，以及盛放牌的容器可以封装到对象属性中。
#     ```

import random

l1 = ['红桃', '黑桃', '方块', '梅花']
l2 = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']
l3 = [i + k for i in l1 for k in l2]


class Poker:
    def __init__(self, username, pokerpai=None):
        self.username = username
        self.poker = pokerpai

    def riffle(self):
        random.shuffle(l3)
        self.poker = l3
        return self.poker

    def random_selection(self):
        random.choice(l3)
        print(f"{self.username}您随机抽取的牌为：", random.choice(l3))

    def appoint(self):
        num = input("请输入抽取的牌的序号(1-52):").strip()
        print(f"{self.username}您抽取的第{num}张牌是:", l3[int(num) - 1]) if int(num) in range(1, 53) else print("输入格式错误")

    def sorte(self):
        ll = []
        l2 = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']
        for i in l2:
            for m in self.poker:
                ll.append(m) if i in m else 1
        for n in ll:
            print(n, end=",")

    def drop_out(self):
        exit(0)


def star():
    username = input("请输入用户名:").strip()
    obj = Poker(username)
    dic = {1: obj.riffle, 2: obj.random_selection, 3: obj.appoint, 4: obj.sorte, 5: obj.drop_out}
    while True:
        function = input("""
1. 洗牌
2. 随机抽取一张
3. 指定抽取一张
4. 从小到大排序
5. 退出
请输入选择的功能:""").strip()
        dic[int(function)]() if int(function) in range(1, 6) else print("输入格式错误!")


if __name__ == '__main__':
    star()
