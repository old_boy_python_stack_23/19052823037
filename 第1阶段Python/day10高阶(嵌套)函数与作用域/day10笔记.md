# 函数进阶

1. 函数的传参：形参角度：第三种传参方式。

   动态参数：  *args  **kwargs

   ```python
   # def eat(food1,food2,food3):
   #     print(f'我请你吃：{food1},{food2},{food3}')
   # eat('蒸羊羔','蒸熊掌','蒸鹿尾')
   
   # 当给函数传入的参数数目不定时，之前的传参方式解决不了问题。
   # 万能参数，动态参数。 *args
   def eat(food1,food2,food3):
       print(f'我请你吃：{food1},{food2},{food3}')
   eat('蒸羊羔','蒸熊掌','蒸鹿尾','烧花鸭','烧企鹅')
   
   def eat(*args):  # 将实参角度：定义一个函数时，* 所有的位置参数聚合到一个元组中。
       print(args)
       print(f'我请你吃：{args}')
   eat('蒸羊羔','蒸熊掌','蒸鹿尾','烧花鸭','烧企鹅')
   
   # **kwargs
   def func(**kwargs):  # 函数的定义时：**将实参角度所有的关键字参数聚合成了一个字典，给了kwargs.
       print(kwargs)
   func(name='alex',age=84,hobby='唱跳rap篮球')
   
   # *args,**kwargs 万能参数
   def func(*args,**kwargs):
       print(args,kwargs)
   ```

   练习：写一个函数，求传入函数中的不定个数的数字实参的和。

   ```python
   def func(lis):
       num1 = 0 
       for i in lis:
           num1 += i
       return num1
   ```

   

2. *的魔性用法

   ```python
   # 函数中
   def func(*args,**kwargs):
       print(args)  # (1, 2, 3,'太白', 'wusir', '景女神')
       print(kwargs)
   l1 = [1, 2, 3]
   l2 = ['太白', 'wusir', '景女神']
   # func(l1,l2)
   # func(*l1,*l2)  # 当函数的执行时：*iterable 代表打散。
   func(*[1, 2, 3],*(11,22),*'fdsakl')  # 当函数的执行时：*iterable 代表打散。
   def func(*args,**kwargs):
       print(args)
       print(kwargs)
   func(**{'name':"alex"},**{'age': 73,'hobby': '吹'})
   #当函数的执行时：**dict 代表打散。
   
   
   # 函数外：处理剩余元素
   a,b,*c = [1,2,3,4,5]
   a,*c,b, = [1,2,3,4,5]
   a,*c = range(5)
   a,*c,b = (1,2,3,4,5,6)
   print(a,c,b)
   ```

   

3. 形参角度的最终顺序

   ```python
   *args的位置
   *args不能放在位置参数前面，a,b取不到值
   def func(*args,a,b,sex='man',):
       print(a)
       print(b)
       print(sex)
       print(args)
       # print(kwargs)
   func(1,2,4,5,6)
   
   args如果想要接收到值之前，肯定要改变sex默认参数。
   def func(a,b,sex='man',*args):
       print(a)
       print(b)
       print(sex)
       print(args)
       # print(kwargs)
   func(1,2,4,5,6)
   
   def func(a,b,*args,sex='man'):
       print(a)
       print(b)
       print(sex)
       print(args)
       # print(kwargs)
   func(1,2,4,5,6)
   func(1,2,4,5,6,sex='women')
   
   **kwargs
   位置参数,*args,默认参数，**kwargs
   def func(a,b,*args,sex='man',**kwargs,):
       print(a)
       print(b)
       print(sex)
       print(args)
       print(kwargs)
   func(1,2,4,5,6,name='太白',age=18)
   
   ```

   

4. 函数的传参：形参角度：第四种传参方式（了解）

   ```python
   位置参数,*args,默认参数，仅限关键字参数,**kwargs
   def func(a,b,*args,sex='man',c,**kwargs,):
       print(a)
       print(b)
       print(sex)
       print(c)
       print(args)
       print(kwargs)
   func(1,2,4,5,6,67,c=666,name='太白',age=18,)
   ```

   

5. 从空间角度研究函数

   **全局名称空间**： py文件运行时开辟的，存放的是执行的py文件（除去函数内部）的所有的变量与值（地址）的对应关系，整个py文件结束之后，才会消失。

   **临时（局部）名称空间**： 函数执行时，在内存中临时开辟的一个空间，存放的函数中的变量与值的对应关系，随着函数的结束而消失。

   **内置名称空间：**input,print,内置函数等。

   ![TIM图片20190618163607](D:\Python_study\笔记图片\TIM图片20190618163607.png)

   ![V05C~N6C[ECT6X11%CC8[76](D:\Python_study\笔记图片\V05C~N6C[ECT6X11%CC8[76.png)

   ![Z[GM0O~}JA4)D2R]MZMF($O](D:\Python_study\笔记图片\Z[GM0O~}JA4)D2R]MZMF($O.png)

   

6. 取值顺序加载顺序

   加载顺序：上面这三个空间，谁先加载到内存。

   内置名称空间 ----》 全局名称空间 ----》 （函数执行时）临时名称空间

   取值顺序：（就近原则）

7. 作用域

   全局作用域：全局名称空间，内置名称空间。

   局部作用域：局部名称空间。

8. 内置函数：globals，locals

   ```python
   """
   此文件研究的是内置函数 globals locals
   """
   # name = 'alex'
   # l1 = [1, 2, 3]
   #
   # def func():
   #     age = '18'
   #
   # print(globals()) # 全局作用域所有的内容
   # print(locals())  # 当前位置
   
   
   # name = 'alex'
   # l1 = [1, 2, 3]
   
   # def func():
   #     age = '18'
   #     oldboy = '老男孩教育'
   #     print(globals()) # 全局作用域所有的内容
   #     print(locals())  # 当前位置的变量与值的对应关系
   #
   # func()
   
   # name = 'alex'
   # l1 = [1, 2, 3]
   #
   # def func():
   #     age = '18'
   #     oldboy = '老男孩教育'
   #     def inner():
   #         name_class = 'python23期'
   #         print(globals()) # 全局作用域所有的内容
   #         print(locals())  # 当前位置的变量与值的对应关系
   #     inner()
   # func()
   ```

   

9. 高阶函数（嵌套函数）

   ```python
   # 例1：
   # def func1():
   #     print('in func1')
   #     print(3)
   # def func2():
   #     print('in func2')
   #     print(4)
   # func1()
   # print(1)
   # func2()
   # print(2)
   '''
   in func1
   3
   1
   in func2'
   4
   2
   '''
   
   # 例2：
   def func1():
       print('in func1')
       print(3)
   
   def func2():
       print('in func2')
       func1()
       print(4)
       
   print(1)
   func2()
   print(2)
   
   '''
   1
   in func2
   in func1
   3
   4
   2
   '''
   # # 例3：
   #
   def fun2():
       print(2)
       def func3():
           print(6)
       print(4)
       func3()
       print(8)
   
   print(3)
   fun2()
   print(5)
   
   '''
   3 2 4 6 8 5
   
   '''
   ```

   