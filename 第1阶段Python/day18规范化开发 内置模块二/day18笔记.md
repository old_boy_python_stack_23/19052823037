# 软件的开发规范和常用模块2

## 开发规范

**什么是开发规范?为什么要有开发规范呢?**

现在包括之前写的一些程序，所谓的'项目'，都是在一个py文件下完成的，代码量撑死也就几百行，你认为没问题，挺好。但是真正的后端开发的项目，系统等，少则几万行代码，多则十几万，几十万行代码，你全都放在一个py文件中行么？

　　软件开发，规范你的项目目录结构，代码规范，遵循PEP8规范等等，让你更加清晰滴，合理的开发。

那么接下来我们以博客园系统的作业举例，将我们之前在一个py文件中的所有代码，整合成规范的开发。

此时我们是将所有的代码都写到了一个py文件中，如果代码量多且都在一个py文件中，那么对于代码结构不清晰，不规范，运行起来效率也会非常低。所以我们接下来一步一步的修改：

1. **程序配置.**

 ![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627195238498-16036376.png)

   你项目中所有的有关文件的操作出现几处，都是直接写的register相对路径，如果说这个register注册表路径改变了，或者你改变了register注册表的名称，那么相应的这几处都需要一一更改，这样其实你就是把代码写死了，那么怎么解决？ 我要统一相同的路径，也就是统一相同的变量，在文件的最上面写一个变量指向register注册表的路径，代码中如果需要这个路径时，直接引用即可。

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627195428974-1647271964.png)

![img](file:///C:/Users/%E9%87%91%E9%91%AB/AppData/Roaming/Typora/typora-user-images/1556090366348.png?lastModify=1558485274)

![img](file:///C:/Users/%E9%87%91%E9%91%AB/AppData/Roaming/Typora/typora-user-images/1556090384268.png?lastModify=1558485274)

1. **划分文件。**

**![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627195557254-2044245249.png)**

一个项目的函数不能只是这些，我们只是举个例子，这个小作业函数都已经这么多了，那么要是一个具体的实际的项目，函数会非常多，所以我们应该将这些函数进行分类，然后分文件而治。在这里我划分了以下几个文件：

**settings.py**: 配置文件，就是放置一些项目中需要的静态参数，比如文件路径，数据库配置，软件的默认设置等等

类似于我们作业中的这个：

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627195701873-1742658361.png)

![img](file:///C:/Users/%E9%87%91%E9%91%AB/AppData/Roaming/Typora/typora-user-images/1556091812222.png?lastModify=1558485274)

**common.py**:公共组件文件，这里面放置一些我们常用的公共组件函数，并不是我们核心逻辑的函数，而更像是服务于整个程序中的公用的插件，程序中需要即调用。比如我们程序中的装饰器auth，有些函数是需要这个装饰器认证的，但是有一些是不需要这个装饰器认证的，它既是何处需要何处调用即可。比如还有密码加密功能，序列化功能，日志功能等这些功能都可以放在这里。

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627195740552-202150048.png)

![img](file:///C:/Users/%E9%87%91%E9%91%AB/AppData/Roaming/Typora/typora-user-images/1556092295873.png?lastModify=1558485274)

**src.py**:这个文件主要存放的就是核心逻辑功能，你看你需要进行选择的这些核心功能函数，都应该放在这个文件中。

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627195852599-1632353067.png)

**start.py**:项目启动文件。你的项目需要有专门的文件启动，而不是在你的核心逻辑部分进行启动的，有人对这个可能不太理解，我为什么还要设置一个单独的启动文件呢？你看你生活中使用的所有电器基本都一个单独的启动按钮，汽车，热水器，电视，等等等等，那么为什么他们会单独设置一个启动按钮，而不是在一堆线路板或者内部随便找一个地方开启呢？ 目的就是放在显眼的位置，方便开启。你想想你的项目这么多py文件，如果src文件也有很多，那么到底哪个文件启动整个项目，你还得一个一个去寻找，太麻烦了，这样我把它单独拿出来，就是方便开启整个项目。

那么我们写的项目开启整个项目的代码就是下面这段： ![img](file:///C:/Users/%E9%87%91%E9%91%AB/AppData/Roaming/Typora/typora-user-images/1556094510743.png?lastModify=1558485274)

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627195943247-1675298998.png)

你把这些放置到一个文件中也可以，但是没有必要，我们只需要一个命令或者一个开启指令就行，就好比我们开启电视只需要让人很快的找到那个按钮即可，对于按钮后面的一些复杂的线路板，我们并不关心，所以我们要将上面这个段代码整合成一个函数，开启项目的''按钮''就是此函数的执行即可。

 ![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627195955922-561531940.png)

这个按钮要放到启动文件start.py里面。

除了以上这几个py文件之外还有几个文件，也是非常重要的：

**类似于register文件**：这个文件文件名不固定，register只是我们项目中用到的注册表，但是这种文件就是存储数据的文件，类似于**文本数据库**，那么我们一些项目中的数据有的是从数据库中获取的，有些数据就是这种文本数据库中获取的，总之，你的项目中有时会遇到将一些数据存储在文件中，与程序交互的情况，所以我们要单独设置这样的文件。

**log文件**：log文件顾名思义就是存储log日志的文件。日志我们一会就会讲到，日志主要是供开发人员使用。比如你项目中出现一些bug问题，比如开发人员对服务器做的一些操作都会记录到日志中，以便开发者浏览，查询。

至此，我们将这个作业原来的两个文件，合理的划分成了6个文件，但是还是有问题的，如果我们的项目很大，你的每一个部分相应的你一个文件存不下的，比如你的src主逻辑文件，函数很多，你是不是得分成：src1.py src2.py？

你的文本数据库register这个只是一个注册表，如果你还有个人信息表，记录表呢？ 如果是这样，你的整个项目也是非常凌乱的： ![img](file:///C:/Users/%E9%87%91%E9%91%AB/AppData/Roaming/Typora/typora-user-images/1556177303579.png?lastModify=1558485274)

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627200111055-2143297104.png)

**3. 划分具体目录**

上面看着就非常乱了，那么如何整改呢？ 其实非常简单，原来你就是30件衣服放在一个衣柜里，那么你就得分类装，放外套的地方，放内衣的地方，放佩饰的地方等等，但是突然你的衣服编程300件了，那一个衣柜放不下，我就整多个柜子，分别放置不同的衣物。所以我们这可以整多个文件夹，分别管理不同的物品，那么标准版本的目录结构就来了：

![img](file:///C:/Users/%E9%87%91%E9%91%AB/AppData/Roaming/Typora/typora-user-images/1556177548922.png?lastModify=1558485274)**为什么要设计项目目录结构？**

"设计项目目录结构"，就和"代码编码风格"一样，属于个人风格问题。对于这种风格上的规范，一直都存在两种态度:

1. 一类同学认为，这种个人风格问题"无关紧要"。理由是能让程序work就好，风格问题根本不是问题。
2. 另一类同学认为，规范化能更好的控制程序结构，让程序具有更高的可读性。

我是比较偏向于后者的，因为我是前一类同学思想行为下的直接受害者。我曾经维护过一个非常不好读的项目，其实现的逻辑并不复杂，但是却耗费了我非常长的时间去理解它想表达的意思。从此我个人对于提高项目可读性、可维护性的要求就很高了。"项目目录结构"其实也是属于"可读性和可维护性"的范畴，我们设计一个层次清晰的目录结构，就是为了达到以下两点:

1. 可读性高: 不熟悉这个项目的代码的人，一眼就能看懂目录结构，知道程序启动脚本是哪个，测试目录在哪儿，配置文件在哪儿等等。从而非常快速的了解这个项目。
2. 可维护性高: 定义好组织规则后，维护者就能很明确地知道，新增的哪个文件和代码应该放在什么目录之下。这个好处是，随着时间的推移，代码/配置的规模增加，项目结构不会混乱，仍然能够组织良好。

所以，我认为，保持一个层次清晰的目录结构是有必要的。更何况组织一个良好的工程目录，其实是一件很简单的事儿。

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627200156992-1307350927.png)

上面那个图片就是较好的目录结构。

## 按照项目目录结构,规范博客园系统

接下来，我就带领大家把具体的代码写入对应的文件中，并且将此项目启动起来，一定要跟着我的步骤一步一步去执行：

1. **配置start.py文件**

我们首先要配置启动文件，启动文件很简答就是将项目的启动执行放置start.py文件中，运行start.py文件可以成功启动项目即可。 那么项目的启动就是这个指令run() 我们把这个run()放置此文件中不就行了？

![img](file:///C:/Users/%E9%87%91%E9%91%AB/AppData/Roaming/Typora/typora-user-images/1556178621604.png?lastModify=1558485274)

 ![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627200213698-168341436.png)

这样你能执行这个项目么？肯定是不可以呀，你的starts.py根本就找不到run这个变量，肯定是会报错的。

NameError: name 'run' is not defined 本文件肯定是找不到run这个变量也就是函数名的，不过这个难不倒我们，我们刚学了模块， 另个一文件的内容我们可以引用过来。但是你发现import run 或者 from src import run 都是报错的。为什么呢？ 骚年，遇到报错不要慌！我们说过你的模块之所以可以引用，那是因为你的模块肯定在这个三个地方：内存，内置，sys.path里面，那么core在内存中肯定是没有的，也不是内置，而且sys.path也不可能有，因为sys.path只会将你当前的目录（bin）加载到内存，所以你刚才那么引用肯定是有问题的，那么如何解决？内存，内置你是左右不了的，你只能将core的路径添加到sys.path中，这样就可以了。

```
import sys
sys.path.append(r'D:\lnh.python\py project\teaching_show\blog\core')
from src import run
run()
```

这样虽然解决了，但是你不觉得有问题么？你现在从这个start文件需要引用src文件，那么你需要手动的将src的工作目录添加到sys.path中，那么有没有可能你会引用到其他的文件？比如你的项目中可能需要引用conf，lib等其他py文件，那么在每次引用之前，或者是开启项目时，全部把他们添加到sys.path中么？

```
sys.path.append(r'D:\lnh.python\py project\teaching_show\blog\core')
sys.path.append(r'D:\lnh.python\py project\teaching_show\blog\conf')
sys.path.append(r'D:\lnh.python\py project\teaching_show\blog\db')
sys.path.append(r'D:\lnh.python\py project\teaching_show\blog\lib')
```

这样是不是太麻烦了？ 我们应该怎么做？我们应该把项目的工作路径添加到sys.path中，用一个例子说明：你想找张三，李四，王五，赵六等人，这些人全部都在一栋楼比如在汇德商厦，那么我就告诉你汇德商厦的位置：北京昌平区沙河镇汇德商厦。 你到了汇德商厦你在找具体这些人就可以了。所以我们只要将这个blog项目的工作目录添加到sys.path中，这样无论这个项目中的任意一个文件引用项目中哪个文件，就都可以找到了。所以：

```
import sys
sys.path.append(r'D:\lnh.python\py project\teaching_show\blog')
from core.src import run
run()
```

上面还是差一点点，你这样写你的blog的路径就写死了，你的项目不可能只在你的电脑上，项目是共同开发的，你的项目肯定会出现在别人电脑上，那么你的路径就是问题了，在你的电脑上你的blog项目的路径是上面所写的，如果移植到别人电脑上，他的路径不可能与你的路径相同， 这样就会报错了，所以我们这个路径要动态获取，不能写死，所以这样就解决了：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
import sys
import os
# sys.path.append(r'D:\lnh.python\py project\teaching_show\blog')
print(os.path.dirname(__file__))
# 获取本文件的绝对路径  # D:/lnh.python/py project/teaching_show/blog/bin
print(os.path.dirname(os.path.dirname(__file__)))
# 获取父级目录也就是blog的绝对路径  # D:/lnh.python/py project/teaching_show/blog
BATH_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BATH_DIR)
from core.src import run
run()
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

那么还差一个小问题，这个starts文件可以当做脚本文件进行直接启动，如果是作为模块，被别人引用的话，按照这么写，也是可以启动整个程序的，这样合理么？这样是不合理的，作为启动文件，是不可以被别人引用启动的，所以我们此时要想到 `__name__`了：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
import sys
import os
# sys.path.append(r'D:\lnh.python\py project\teaching_show\blog')
# print(os.path.dirname(__file__))
# 获取本文件的绝对路径  # D:/lnh.python/py project/teaching_show/blog/bin
# print(os.path.dirname(os.path.dirname(__file__)))
# 获取父级目录也就是blog的绝对路径  # D:/lnh.python/py project/teaching_show/blog
BATH_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BATH_DIR)
from core.src import run

if __name__ == '__main__':
    run()
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

这样，我们的starts启动文件就已经配置成功了。以后只要我们通过starts文件启动整个程序，它会先将整个项目的工作目录添加到sys.path中，然后在启动程序，这样我整个项目里面的任何的py文件想引用项目中的其他py文件，都是你可以的了。

1. **配置settings.py文件。**

接下来，我们就会将我们项目中的静态路径，数据库的连接设置等等文件放置在settings文件中。

我们看一下，你的主逻辑src中有这样几个变量：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
status_dic = {
    'username': None,
    'status': False,
}
flag = True
register_path = r'D:\lnh.python\py project\teaching_show\blog\register'
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

我们是不是应该把这几个变量都放置在settings文件中呢？不是！setttings文件叫做配置文件，其实也叫做配置静态文件，什么叫静态？ 静态就是一般不会轻易改变的，但是对于上面的代码status_dic ，flag这两个变量，由于在使用这个系统时会时长变化，所以不建议将这个两个变量放置在settings配置文件中，只需要将register_path放置进去就可以。

```
register_path = r'D:\lnh.python\py project\teaching_show\blog\register'
```

![img](file:///C:/Users/%E9%87%91%E9%91%AB/AppData/Roaming/Typora/typora-user-images/1558061233366.png?lastModify=1558485274)

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627200852339-224767952.png)

但是你将这个变量放置在settings.py之后，你的程序启动起来是有问题，为什么？

```
with open(register_path, encoding='utf-8') as f1:
NameError: name 'register_path' is not defined
```

因为主逻辑src中找不到register_path这个路径了，所以会报错，那么我们解决方式就是在src主逻辑中引用settings.py文件中的register_path就可以了。

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627200926512-525478573.png)

这里引发一个问题：为什么你这样写就可以直接引用settings文件呢？我们在starts文件中已经说了，刚已启动blog文件时，我们手动将blog的路径添加到sys.path中了，这就意味着，我在整个项目中的任何py文件，都可以引用到blog项目目录下面的任何目录：bin,conf,core,db,lib,log这几个，所以，刚才我们引用settings文件才是可以的。

1. **配置common.py文件**

接下来，我们要配置我们的公共组件文件，在我们这个项目中，装饰器就是公共组件的工具，我们要把装饰器这个工具配置到common.py文件中。先把装饰器代码剪切到common.py文件中。这样直接粘过来，是有各种问题的：

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627200951219-833929356.png)

![img](file:///C:/Users/%E9%87%91%E9%91%AB/AppData/Roaming/Typora/typora-user-images/1558064310800.png?lastModify=1558485274)

所以我们要在common.py文件中引入src文件的这两个变量。

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627201012493-1293565326.png)

可是你的src文件中使用了auth装饰器，此时你的auth装饰器已经移动位置了，所以你要在src文件中引用auth装饰器，这样才可以使用上。

![img](https://img2018.cnblogs.com/blog/988316/201906/988316-20190627201043334-1780124117.png)

OK，这样你就算是将你之前写的模拟博客园登录的作业按照规范化目录结构合理的完善完成了，最后还有一个关于README文档的书写。

#### 关于README的内容

**这个我觉得是每个项目都应该有的一个文件**，目的是能简要描述该项目的信息，让读者快速了解这个项目。

它需要说明以下几个事项:

1. 软件定位，软件的基本功能。
2. 运行代码的方法: 安装环境、启动命令等。
3. 简要的使用说明。
4. 代码目录结构说明，更详细点可以说明软件的基本原理。
5. 常见问题说明。

我觉得有以上几点是比较好的一个`README`。在软件开发初期，由于开发过程中以上内容可能不明确或者发生变化，并不是一定要在一开始就将所有信息都补全。但是在项目完结的时候，是需要撰写这样的一个文档的。

可以参考Redis源码中[Readme](https://github.com/antirez/redis#what-is-redis)的写法，这里面简洁但是清晰的描述了Redis功能和源码结构。

## **一、time模块**

表示时间的三种方式：

　　时间戳：数字（计算机能认识的）

　　时间字符串：t='2012-12-12'

　　结构化时间：time.struct_time(tm_year=2017, tm_mon=8, tm_mday=8, tm_hour=8, tm_min=4, tm_sec=32, tm_wday=1, tm_yday=220, tm_isdst=0)像这样的就是结构化时间

```python
 1 import time
 2 # 对象：对象.方法
 3 # ----------------------------------
 4 # 1.时间戳(数字)：给计算机的看的
 5 print(time.time())#当前时间的时间戳
 6 print(time.localtime())#结构化时间对象
 7 s=time.localtime() #当前的结构化时间对象（utc时间）
 8 print(s.tm_year)
 9 s2=time.gmtime()  #这个和localtime只是小时不一样
10 print(s2)
11 
12 
13 #-----------------------------------
14 # 2.时间的转换
15 print(time.localtime(15648461))#把时间戳转换成结构化时间
16 t='2012-12-12' #这是一个字符串时间
17 print(time.mktime(time.localtime()))#将结构化时间转换成时间戳
18 print(time.strftime("%Y-%m-%d",time.localtime()))#将结构化时间转换成字符串时间
19 print(time.strftime('%y/%m/%d  %H:%M:%S'))#小写的y是取得年的后两位
20 print(time.strptime('2008-03-12',"%Y-%m-%d"))#将字符串时间转换成结构化时间
```



```python
 1 %y 两位数的年份表示（00-99）
 2 %Y 四位数的年份表示（000-9999）
 3 %m 月份（01-12）
 4 %d 月内中的一天（0-31）
 5 %H 24小时制小时数（0-23）
 6 %I 12小时制小时数（01-12）
 7 %M 分钟数（00=59）
 8 %S 秒（00-59）
 9 %a 本地简化星期名称
10 %A 本地完整星期名称
11 %b 本地简化的月份名称
12 %B 本地完整的月份名称
13 %c 本地相应的日期表示和时间表示
14 %j 年内的一天（001-366）
15 %p 本地A.M.或P.M.的等价符
16 %U 一年中的星期数（00-53）星期天为星期的开始
17 %w 星期（0-6），星期天为星期的开始
18 %W 一年中的星期数（00-53）星期一为星期的开始
19 %x 本地相应的日期表示
20 %X 本地相应的时间表示
21 %Z 当前时区的名称
22 %% %号本身
23 
24 python中时间日期格式化符号：
```





![img](https://images2017.cnblogs.com/blog/1184802/201708/1184802-20170808161710870-117632695.png)



**time.strftime('格式定义'，‘结构化时间’)   结构化时间参数若不传，则显示当前时间**

```python
`print``(time.strptime(``'2008-03-12'``,``"%Y-%m-%d"``))``print``(time.strftime(``'%Y-%m-%d'``))``print``(time.strftime(``"%Y-%m-%d"``,time.localtime(``15444``)))`
```

![img](https://images2017.cnblogs.com/blog/1184802/201708/1184802-20170808162722214-56553075.png)

![img](https://images.cnblogs.com/OutliningIndicators/ExpandedBlockStart.gif)

```python
1 print(time.asctime(time.localtime(150000)))
2 print(time.asctime(time.localtime()))
3 # time.ctime（时间戳）如果不传参数，直接返回当前时间的格式化字符串
4 print(time.ctime())
5 print(time.ctime(150000))
```

## **二、random模块**



```python
 1 import random
 2 # ----------------------------
 3 # 1.随机小数
 4 print(random.random()) #大于0且小于1之间的随机小数
 5 print(random.uniform(1,3))  #大于1且小于3的随机小数
 6 
 7 # ----------------------------
 8 # 2.随机整数
 9 print(random.randint(1,5)) #大于1且小于等于5之间的整数
10 print(random.randrange(1,10,2))  #大于等于1且小于3之间的整数（且是所有的奇数）
11 
12 # ----------------------------
13 # 3.随机选择一个返回
14 print(random.choice([1,'23',[4,5]]))
15 # ----------------------------
16 # 4.随机选择返回多个
17 print(random.sample([1,'23',[4,5]],2))  #列表元素任意两个组合
18 # ----------------------------
19 
20 
21 # ----------------------------
22 # 5.打乱列表顺序
23 item=[1,5,2,3,4]
24 random.shuffle(item)  #打乱次序
25 print(item)
```

```python
 1 # 验证码小例子(这个只是产生随机的四位数字)
 2 # 方法一、
 3 # l=[]
 4 # for i in range(4):
 5 #     l.append(str(random.randint(0,9)))
 6 # print(''.join(l))
 7 # print(l)
 8 
 9 
10 # 方法二
11 # print(random.randint(1000,9999))
12 
13 
14 # 验证码升级版
15 # 要求：首次要有数字，其次要有字母，一共四位，可以重复
16 # chr(65-90)#a-z
17 # chr(97-122)#A-Z
18 
19 方法一
20 # num_list = list(range(10))
21 # new_num_l=list(map(str,num_list))#['0','1'...'9']
22 # l=[] #用来存字母
23 # for i in range(65,91):
24 #     zifu=chr(i)
25 #     l.append(zifu)  #['A'-'Z']
26 # new_num_l.extend(l) #要把上面的数字和下面的字母拼在一块
27 # print(new_num_l)
28 # ret_l=[] #存生成的随机数字或字母
29 # for i in range(4): #从new_num_l里面选数字选择四次就放到了ret_l里面)
30 #     ret_l.append(random.choice(new_num_l))
31 # # print(ret_l)
32 # print(''.join(ret_l)) #拼成字符串
33 
34 方法二
35 # import random
36 # def myrandom():
37 #     new_num_l=list(map(str,range(10)))
38 #     l=[chr(i) for i in range(65,91)]
39 #     new_num_l.extend(l)
40 #     ret_l=[random.choice(new_num_l) for i in range(4)]
41 #     return ''.join(ret_l)
42 # print(myrandom())
43 
44 方法三
45 import random
46 l=list(str(range(10)))+[chr(i) for i in range(65,91)]+[chr(j) for j in range(97,122)]
47 print(''.join(random.sample(l,4)))
```



 

## **三、os模块**



```python
 1 os.getcwd() 获取当前工作目录，即当前python脚本工作的目录路径
 2 os.chdir("dirname")  改变当前脚本工作目录；相当于shell下cd
 3 os.curdir  返回当前目录: ('.')
 4 os.pardir  获取当前目录的父目录字符串名：('..')
 5 os.makedirs('dirname1/dirname2')    可生成多层递归目录
 6 os.removedirs('dirname1')    若目录为空，则删除，并递归到上一级目录，如若也为空，则删除，依此类推
 7 os.mkdir('dirname')    生成单级目录；相当于shell中mkdir dirname
 8 os.rmdir('dirname')    删除单级空目录，若目录不为空则无法删除，报错；相当于shell中rmdir dirname
 9 os.listdir('dirname')    列出指定目录下的所有文件和子目录，包括隐藏文件，并以列表方式打印
10 os.remove()  删除一个文件
11 os.rename("oldname","newname")  重命名文件/目录
12 os.stat('path/filename')  获取文件/目录信息
13 os.sep    输出操作系统特定的路径分隔符，win下为"\\",Linux下为"/"
14 os.linesep    输出当前平台使用的行终止符，win下为"\t\n",Linux下为"\n"
15 os.pathsep    输出用于分割文件路径的字符串 win下为;,Linux下为:
16 os.name    输出字符串指示当前使用平台。win->'nt'; Linux->'posix'
17 os.system("bash command")  运行shell命令，直接显示
18 os.popen("bash command)  运行shell命令，获取执行结果
19 os.environ  获取系统环境变量
20 
21 
22 os.path
23 os.path.abspath(path) 返回path规范化的绝对路径 os.path.split(path) 将path分割成目录和文件名二元组返回 os.path.dirname(path) 返回path的目录。其实就是os.path.split(path)的第一个元素 os.path.basename(path) 返回path最后的文件名。如何path以／或\结尾，那么就会返回空值。
24                         即os.path.split(path)的第二个元素
25 os.path.exists(path)  如果path存在，返回True；如果path不存在，返回False
26 os.path.isabs(path)  如果path是绝对路径，返回True
27 os.path.isfile(path)  如果path是一个存在的文件，返回True。否则返回False
28 os.path.isdir(path)  如果path是一个存在的目录，则返回True。否则返回False
29 os.path.join(path1[, path2[, ...]])  将多个路径组合后返回，第一个绝对路径之前的参数将被忽略
30 os.path.getatime(path)  返回path所指向的文件或者目录的最后访问时间
31 os.path.getmtime(path)  返回path所指向的文件或者目录的最后修改时间
32 os.path.getsize(path) 返回path的大小
```



注意：os.stat('path\filename') 获取文件\目录信息的结构说明

```python
`stat 结构:` `st_mode: inode 保护模式``st_ino: inode 节点号。``st_dev: inode 驻留的设备。``st_nlink: inode 的链接数。``st_uid: 所有者的用户``ID``。``st_gid: 所有者的组``ID``。``st_size: 普通文件以字节为单位的大小；包含等待某些特殊文件的数据。``st_atime: 上次访问的时间。``st_mtime: 最后一次修改的时间。``st_ctime: 由操作系统报告的``"ctime"``。在某些系统上（如Unix）是最新的元数据更改的时间，<br>在其它系统上（如Windows）是创建时间（详细信息参见平台的文档）。` `ststat 结构`
```

## **四、sys模块**

sys模块是与python解释器交互的一个接口

```python
 1 import sys
 2 print(sys.argv) #实现从程序外部向程序传递参数。（在命令行里面输打开路径执行）
 3 name=sys.argv[1] #命令行参数List,第一个元素是程序的本身路径
 4 password = sys.argv[2]
 5 if name=='egon' and password == '123':
 6     print('继续执行程序')
 7 else:
 8    exit()
 9 
10 sys.exit()#退出程序，正常退出时exit(0)
11 print(sys.version)#获取python解释的版本信息
12 print(sys.maxsize)#最大能表示的数，与系统多少位有关
13 print(sys.path)#返回模块的搜索路径，初始化时使用PYTHONPATH环境变量的值
14 print(sys.platform)#返回操作系统平台名称
```

 

## **五、序列化模块**

1.什么是序列化-------将原本的字典，列表等内容转换成一个字符串的过程就叫做序列化

2.序列化的目的

　　1.以某种存储形式使自定义对象持久化

　　2.将对象从一个地方传递到另一个地方

　　3.使程序更具维护性

json

　　Json模块提供了四个功能：dumps、loads、dump、load

```python
 1 import json
 2 dic={'k1':'v1','k2':'v2','k3':'v3'}
 3 print(type(dic))
 4 str_dic = json.dumps(dic) #将字典转换成字符串，转换后的字典中的元素是由双引号表示的
 5 print(str_dic,type(str_dic))#{"k1": "v1", "k2": "v2", "k3": "v3"} <class 'str'>
 6 
 7 
 8 dic2 = json.loads(str_dic)#将一个字符串转换成字典类型
 9 print(dic2,type(dic2))#{'k1': 'v1', 'k2': 'v2', 'k3': 'v3'} <class 'dict'>
10 
11 list_dic = [1,['a','b','c'],3,{'k1':'v1','k2':'v2'}]
12 str_dic = json.dumps(list_dic) #也可以处理嵌套的数据类型
13 print(type(str_dic),str_dic) #<class 'str'> [1, ["a", "b", "c"], 3, {"k1": "v1", "k2": "v2"}]
14 list_dic2 = json.loads(str_dic)
15 print(type(list_dic2),list_dic2) #<class 'list'> [1, ['a', 'b', 'c'], 3, {'k1': 'v1', 'k2': 'v2'}]
```



```python
 1 import json
 2 f=open('json_file','w')
 3 dic = {'k1':'v1','k2':'v2','k3':'v3'}
 4 json.dump(dic,f)# #dump方法接收一个文件句柄，直接将字典转换成json字符串写入文件
 5 f.close()
 6 
 7 f = open('json_file')
 8 dic2 = json.load(f)  #load方法接收一个文件句柄，直接将文件中的json字符串转换成数据结构返回
 9 f.close()
10 print(type(dic2),dic2)
```



pickle

json 和 pickle 模块

　　json：用于字符串和python数据类型之间进行转换

　　pickle：用于python特有的类型和python的数据类型进行转换



```python
 1 # --------------------------
 2 import pickle
 3 # dic= {'k1':'v1','k2':'v2','k3':'v3'}
 4 # str_dic=pickle.dumps(dic)
 5 # print(str_dic)  #打印的是bytes类型的二进制内容
 6 #
 7 # dic2 = pickle.loads(str_dic)
 8 # print(dic2)  #有吧字典给转换回来了
 9 
10 import time
11 struct_time  = time.localtime(1000000000)
12 print(struct_time)
13 f = open('pickle_file','wb')
14 pickle.dump(struct_time,f)
15 f.close()
16 
17 f = open('pickle_file','rb')
18 struct_time2 = pickle.load(f)
19 print(struct_time.tm_year)
```



 

shelve

shelve也是python提供给我们的序列化工具，比pickle用起来更简单一些。
shelve只提供给我们一个open方法，是用key来访问的，使用起来和字典类似。



```python
 1 import shelve
 2 f = shelve.open('shelve_file')
 3 f['key'] = {'int':10, 'float':9.5, 'string':'Sample data'}  #直接对文件句柄操作，就可以存入数据
 4 f.close()
 5 
 6 import shelve
 7 f1 = shelve.open('shelve_file')
 8 existing = f1['key']  #取出数据的时候也只需要直接用key获取即可，但是如果key不存在会报错
 9 f1.close()
10 print(existing)
11 
12 shelve
```



 这个模块有个限制，它不支持多个应用同一时间往同一个DB进行写操作。所以当我们知道我们的应用如果只进行读操作，我们可以让shelve通过只读方式打开DB

```python
1 import shelve
2 f = shelve.open('shelve_file', flag='r')
3 existing = f['key']
4 f.close()
5 print(existing)
```

由于shelve在默认情况下是不会记录待持久化对象的任何修改的，所以我们在shelve.open()时候需

```python
import shelve
f1 = shelve.open('shelve_file')
print(f1['key'])
f1['key']['new_value'] = 'this was not here before'
f1.close()

f2 = shelve.open('shelve_file', writeback=True)
print(f2['key'])
f2['key']['new_value'] = 'this was not here before'
f2.close()

设置writeback
```



writeback方式有优点也有缺点。优点是减少了我们出错的概率，并且让对象的持久化对用户更加的透明了；但这种方式并不是所有的情况下都需要，首先，使用writeback以后，shelf在open()的时候会增加额外的内存消耗，并且当DB在close()的时候会将缓存中的每一个对象都写入到DB，这也会带来额外的等待时间。因为shelve没有办法知道缓存中哪些对象修改了，哪些对象没有修改，因此所有的对象都会被写入。