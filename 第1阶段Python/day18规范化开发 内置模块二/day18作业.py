# _*_coding:utf-8_*
# 1. 写函数，让用户输入用户名密码，将密码转化成密文，然后构建一个字典，字典的键为用户名，
# 值为其对应的密码，将这个字典以json字符串的形式写入文件中。
# import json
# import hashlib
# def userip():
#     user_input = input("请输入用户名")
#     password_inpu = input("请输入密码")
#     ret = hashlib.md5()
#     ret.update(password_inpu.encode('utf-8'))
#     print(ret.hexdigest())
#     dic = {user_input:ret.hexdigest()}
#     print(dic)
#     tb =json.dumps(dic)
#     with open("register",mode="a",encoding='utf-8') as f:
#         f.write(tb)
#
# userip()
# 2. 利用递归寻找文件夹中所有的文件，并将这些文件的绝对路径添加到一个列表中返回（面试题，有点难，可先做其他）。
import os
lis = []
def func(paths):
    if os.path.isdir(paths): # 第一次判定看参数是否是文件夹,如果不是下面的内容不执行
        ret = os.listdir(paths)
        for i in ret:
            path1 = os.path.join(paths,i)
            if os.path.isfile(path1):# 如果是文件,添加路径到lis
                lis.append(path1)
            else:
                # 如果是文件夹,生成此文件夹的新路径
                func(path1) # 按照新路径进行递归执行
                # paths = os.path.dirname(paths) #最深层递归结束后要逐层返回路径
        return
    else:
        print('您输入的参数不是文件夹请重新输入')
func("D:\Python_study\day18规范化开发 内置模块二")
print(lis)
# # 3. 写函数：用户输入某年某月，判断这是这一年的第几天（需要用Python的结构化时间）。
# import time
# def tm_yday(time1):
#     # time1 = input("请输入 年 月 日 例如:2019/06/28")
#     # ft = time.strftime("%Y/%m/%d")
#     st = time.strptime(time1,'%Y/%m/%d')
#     print(st[7])
# tm_yday("2018/08/20")
# #    结构化时间可以通过这样取值：
# #
# import time
# ret = time.localtime()
# print(ret)  # time.struct_time(tm_year=2019, tm_mon=6, tm_mday=28, tm_hour=15, tm_min=50, tm_sec=47, tm_wday=4, tm_yday=179, tm_isdst=0)
# 2019
# print(ret.tm_year)  # 2019


# # 4. 写函数，生成一个4位随机验证码（包含数字大小写字母）。
# import random
# def code_ma():
#     code = ""
#     for i in range(4):
#         num = random.randint(0, 9)
#         alf = chr(random.randint(97, 122))
#         upper = chr(random.randint(65, 90))
#         add = random.choice([num, alf,upper])
#         code = "".join([code, str(add)])
#
#     return code
# print(code_ma())