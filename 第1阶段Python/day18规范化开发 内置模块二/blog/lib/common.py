#_*_coding:utf-8_*_
from core import src

def wrapper(f):  # 装饰器 登录验证功能
    def inner(*args, **kwargs):
        if src.dic_status["status"]:
            ret = f(*args, **kwargs)
            return ret
        else:
            if src.login():
                ret = f(*args, **kwargs)
                return ret

    return inner