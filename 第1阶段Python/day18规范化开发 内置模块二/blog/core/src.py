#_*_coding:utf-8_*
import os
from lib import common
from conf import settings
dic_status = {"username": None, "status": False}
def register():
    while True:
        username_register = input("请输入注册用户名:").strip()
        password_register = input("请输入注册密码:").strip()
        if username_register.upper() == "Q" or password_register.upper() == "Q":
            return
        elif username_register.isalnum() and len(password_register) in range(6, 15):
            with open(settings.register_path, mode="r", encoding="utf-8") as f:
                dic11 = {line.strip().split("|")[0]: line.strip().split("|")[1] for line in f}
            if username_register not in dic11:
                with open(settings.register_path, mode="a", encoding="utf-8") as f1:
                    f1.write(f"\n{username_register}|{password_register}")
                    print("注册成功")
                    return
            else:
                print("用户名已存在,请重新输入")
        else:
            print("用户名或密码格式错误,请重新输入")


def login():  # 登陆功能 三次退出此功能 return False 成功 return True
    count = 0
    while True:
        if count < 3:
            username_input = input("请输入用户名:").strip()
            user_password = input("请输入密码:").strip()
            with open(settings.register_path, mode='r', encoding="utf-8") as f2:
                dic1 = {line.strip().split("|")[0]: line.strip().split("|")[1] for line in f2}
            if username_input in dic1 and user_password == dic1[username_input]:
                dic_status["username"] = username_input
                dic_status["status"] = True
                print("登陆成功")
                return True
            else:
                print(f"用户名或密码错误!剩余登陆次数为:{2 - count}")
        else:
            print("超过三次退出")
            return False
        count += 1


def writeing():  # 手动输入功能
    file_input = input("请输入文件名:")
    content_input = input("请输入内容:")
    with open(f"{file_input}", mode='w', encoding='utf-8') as f3:
        f3.write(f"{content_input}")
    print("输入成功")
    return


def lead():  # 导入md文件功能 输入文件名 or 路径
    file1_input = input("请输入文件名:").strip()  # 文件找不到怎么办 加个判断 怎么实现
    with open(file1_input, mode='r', encoding='utf-8') as f4:
        content = f4.readline()
    with open(file1_input.split("\\")[-1].split('.')[0]+".text 选课系统 作业设计", mode='a', encoding='utf-8') as f5:
        f5.write(content)
    print("导入成功")
    return


dic2 = {1: writeing, 2: lead}


@common.wrapper
def enter_page():
    print(f"欢迎{dic_status['username']}进入文章页面")
    while True:
        choice2 = input('''请选择
    1, 直接写入内容
    2, 导入md文件''').strip()
        if choice2.isdigit():
            if 0 < int(choice2) < 3:
                dic2[int(choice2)]()
                break
        else:
            print('输入格式错误请重新输入！')


@common.wrapper
def comment():
    print(f"欢迎{dic_status['username']}进入评论页面")
    oslistdir = os.listdir('D:\Python_study\day18规范化开发 内置函数二\\选课系统\db\comment')
    comment_article = """
评论区：
-----------------------------------------"""
    while True:
        print(os.listdir('D:\Python_study\day18规范化开发 内置函数二\\选课系统\db\comment'))  # ['函数的初识.text 选课系统 作业设计', '函数的进阶.md']
        comment_choice = input("请选择要评论的文章序号:").strip()
        if comment_choice.isdigit():
            if int(comment_choice) in range(len(oslistdir) + 1):
                with open(f"rD:\Python_study\day18规范化开发 内置函数二\\选课系统\db\comment/{oslistdir[int(comment_choice) - 1]}", mode="r+", encoding="utf-8") as fb:
                    # print(comment_article in fb.read())
                    comment_comment = input("请输入评论内容:")
                    lis = ["苍老师", "东京热", "武藤兰", "波多野结衣"]
                    for i in lis:
                        if i in comment_comment:
                            comment_comment = comment_comment.replace(i, len(i) * '*')
                    if comment_article in fb.read():
                        fb.write(f'''
          (用户名){dic_status["username"]}
          {comment_comment}''')
                    else:
                        fb.write(f'''
    {comment_article}
          (用户名){dic_status["username"]}
          {comment_comment}''')
        else:
            print("输入错误,请重新输入")


@common.wrapper
def diary():
    print(f"欢迎{dic_status['username']}进入日记页面")


@common.wrapper
def collection():
    print(f"欢迎{dic_status['username']}进入收藏页面")


@common.wrapper
def cancel():
    dic_status['username'] = None
    dic_status['status'] = False


@common.wrapper
def secede():
    print("成功退出")
    exit(0)


dic = {1: login, 2: register, 3: enter_page, 4: comment, 5: diary, 6: collection, 7: cancel, 8: secede}
def run():
    while True:
        print("""
1.请登录
2.请注册
3.进入文章页面
4.进入评论页面
5.进入日记页面
6.进入收藏页面
7.注销账号
8.退出整个程序""")
        choice = input("请输入序号").strip()
        if choice.isdigit():
            if 0 < int(choice) < 9:
                dic[int(choice)]()
        else:
            print("输入格式错误，请重新输入")