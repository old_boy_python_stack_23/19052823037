# _*_coding:utf-8_*
import sys
import os
BATH_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BATH_DIR)
from core import src

if __name__ == '__main__':
    src.run()
