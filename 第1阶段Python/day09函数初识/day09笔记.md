# 函数初识

## 什么是函数

```python
获取任意一个字符串的元素的个数
s1 = 'fdskjlgfdgfdjkslgdfjkjafdsajk'
count = 0
for i in s1:
    count += 1
print(count)


# 获取列表的元素的个数
l1 = [1, 2, 3]
count = 0
for i in l1:
    count += 1
print(count)


# 面向过程编程
1. 代码重复。
2. 代码可可读性不高。

初识函数
l1 = [1, 2, 3]
def new_len():
    count = 0
    for i in l1:
        count += 1
    print(count)
new_len()


函数：函数是以功能为导向，一个函数封装一个功能。登录，注册，文件的改的操作.....
函数减少代码的重复性，增强了代码的可读性。
```



## 函数的结构

```python
l1 = [1, 2, 3]

def new_len():
    count = 0
    for i in l1:
        count += 1
    print(count)
new_len()

def 关键字： 定义一个函数。紧跟一个空格。
new_len函数名：与变量命名规范一致。一定要具有可描述性。 
():结构需要，传参使用。
    : 分割符。
tab键：四个空格。缩进。函数体


```

## 函数的调用

```python
l1 = [1, 2, 3]

def new_len():

count = 0

for i in l1:

count += 1

print(count)

函数的执行写多少次，执行多少次。

new_len() # 函数名() 函数的执行者。调用者。

new_len()  # 函数名() 函数的执行者。

new_len()  # 函数名() 函数的执行者。

new_len()  # 函数名() 函数的执行者。

new_len()  # 函数名() 函数的执行者。

   l1 = [1, 2, 3]
   def new_len():
       count = 0
       for i in l1:
           count += 1
       print(count)

   for i in range(10):
       print(111)

   for i in range(3):
       new_len()
```

## 函数的返回值

一个函数就是封装一个功能，这个功能一般都会有一个最终结果的，比如你写一个登录函数，最终登录成功与否是不是需要返回你一个结果？还有咱们是不是都用过len这个函数，他是获取一个对象的元素的总个数，最终肯定会返回一个元素个数这样的结果：

s1 = 'abfdas'

print(len(s1))  # 6

 函数的返回值用return表示：

1.**结束函数。**

```python
l1 = [1, 2, 3]	
def new_len():	
    print(111)
    print(222)
    if 1 == 1:
        return
    print(333)
    print(444)
new_len()
```



2.**函数中没有return或者只写一个return,函数的执行者得到的是None。**

```python 
# l1 = [1, 2, 3]
# def new_len():
#     count = 0
#     for i in l1:
#         count += 1
#     return
# print(new_len())
```



3.**函数中return后面是单个值，函数的执行者得到的是这个值（不改变值的类型）。** 

```python
def func():
	print(111)
	# return 100
	# return [1, 2, 3]
	return {'name': '太白'}
ret = func()
print(ret, type(ret))
```



4. **函数中return后面是多个值,函数的执行者得到的是一个元组。**

   ```python
   # def func():
   #     print(111)
   #     return 1, '23期', [22, 33]
   # ret = func()
   # print(ret, type(ret))  # (1, '23期', [22, 33])
   
   # def func():
   #     print(111)
   #     # return 1, '23期', [22, 33]
   # a,b,c = func()
   # print(a,b,c)
   
   
   def func():
       print(111)
       # return 1+1+2
       return 2 > 1
   ret = func()
   print(ret)
   ```

   

## 函数的传参

我们上面研究了，函数的结构，函数的执行，以及函数的返回值。对函数有一个初步的了解，那么接下来就是一个非常重要的知识点，函数的参数。函数是以功能为导向的，上面我们写的函数里面的代码都是写死的，也就是说，这个函数里面的更改起来很麻烦，试想一下，我们使用探探，陌陌等软件，可不可以进行筛选，比如选择性别，年龄等，导出结果？ 再拿我们之前学过的len 这个len是不是可以获取字符串的总个数？是不是可以获取列表的总个数？你更改了len函数内部的代码了？没有吧？你看下面的例子：

```
s1 = 'sfdas'
l1 = [1, 3, 7]
print(len(s1))  # 5
print(len(l1))  # 3
```

```
# # 函数的传参：函数的拓展性
#
#
# def new_len(a):  # 定义函数时：参数：形参。
#     count = 0
#     for i in a:
#         count += 1
#     return count
# l1 = [1, 2, 3]
# s1 = 'fdsjaklsfjgfds'
# # print(new_len(l1)) # 函数的调用者：参数 实参。
# print(new_len(s1)) # 函数的调用者：参数 实参。
# print(len(s1))
```

实参角度：

1. 位置参数。
2. 关键字参数。
3. 混合参数。

形参角度：

1. 位置参数。
2. 默认参数。
3. 动态参数。
4. 仅限关键字参数。（了解）



**实参角度：**

1. 位置参数。

   ```
   # 1. 位置参数。： 从左至右，按照顺序，一一对应
   def meet(sex,age,job,):
       print('左划一下')
       print('右划一下')
       print(f'寻找性别{sex},年龄{age}岁,{job}')
       print('聊天')
       print('约吗')
       print('约....')
   meet('女','18~25','讲师')
   
   # 写一个函数，接收两个数字的参数，将较大的数字返回。
   # def comp(a,b):
   #     if a > b:
   #         return a
   #     else:
   #         return b
   # ret = comp(1,2000)
   # print(ret)
   
   # 三元运算符：简单的if else。
   # a1 = 1
   # b2 = 2
   # ret = a1 if a1 > b2 else b2
   # # print(ret)
   #
   # def comp(a,b):
   #     # ret = a if a > b else b
   #     # return ret
   #     return a if a > b else b
   # ret = comp(1,2000)
   # # print(ret)
   ```

2. 关键字参数。一一对应

   ```
   def meet(sex,age,job,hight,weight,):
       print('左划一下')
       print('右划一下')
       print(f'寻找性别{sex},年龄{age}岁,身高{hight},体重{weight},工作{job}')
       print('聊天')
       print('约吗')
       print('约....')
   
   # meet(sex='女',job='学生',weight=120,hight=170,age='18~25')
   ```

3. 混合参数。关键字参数一定在位置参数后面，一一对应。

   ```
   # def meet(sex,age,job,hight,weight,):
   #     print('左划一下')
   #     print('右划一下')
   #     print(f'寻找性别{sex},年龄{age}岁,身高{hight},体重{weight},工作{job}')
   #     print('聊天')
   #     print('约吗')
   #     print('约....')
   #
   # meet('男',27,'ITC语言',weight=120,hight=175,)
   ```

形参角度：

1. 位置参数：与实参角度位置参数一样。

   ```
   # def meet(sex,age,job):
   #     print('左划一下')
   #     print('右划一下')
   #     print(f'寻找性别{sex},年龄{age}岁,{job}')
   #     print('聊天')
   #     print('约吗')
   #     print('约....')
   # meet('女','18~25','讲师')
   ```

2. 默认参数: 一定在位置参数后面，不传参数即沿用默认的参数。

   ```
   # open('文件的改',encoding='utf-8')
   def meet(age,job,sex='女'):
       print('左划一下')
       print('右划一下')
       print(f'寻找性别{sex},年龄{age}岁,{job}')
       print('聊天')
       print('约吗')
       print('约....')
   # meet('18~25','幼师')
   # 更改默认参数
   # meet('18~25','幼师',sex='laddy_boy')
   meet('18~25','幼师','laddy_boy')
   
   ```

   

1. ## 今日总结

   1. 函数的初始：函数是以功能为导向，一个函数封装一个功能。登陆，注册......

   2. 函数解决了代码的重复性的问题。

   3. 函数的结构

   4. *** 函数的调用：函数名（）

   5. ***函数的返回值：

      pass

   6. *** 函数的传参。实参角度，形参角度。

2. ## 预习内容

   <https://www.cnblogs.com/jin-xin/articles/10839389.html>

