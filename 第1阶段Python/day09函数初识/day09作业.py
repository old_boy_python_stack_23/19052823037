# _*_coding:utf-8_*_
# 1. 整理函数相关知识点,写博客。
# 2. 写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新列表返回给调用者。
# def return_lis(lis):
#     num = 0
#     new_lis = []
#     for i in lis:
#         if num % 2 != 0:
#             new_lis.append(i)
#         num += 1
#     return new_lis
#
#
# t = (1, 2, 5, 6, 8, 3, 9)
# w = return_lis(t)
# print(w)


# 3. 写函数，判断用户传入的对象（字符串、列表、元组）长度是否大于5。
# user_input = "挂号费"
# def five_len(user_input):
#     return "是" if len(user_input) > 5 else "否"
# w = five_len(user_input)
# print(w)

# 4. 写函数，检查传入列表的长度，如果大于2，那么仅保留前两个长度的内容，并将新内容返回给调用者。
# def length(lis):
#     return lis[:2] if len(lis) > 2 else None
# lis = [123,46135,1456]
# w = length(lis)
# print(w)
# 5. 写函数，计算传入函数的字符串中,[数字]、[字母] 以及 [其他]的个数，并返回结果。
# def num(st):
#     zm = 0
#     num1 = 0
#     qt = 0
#     for i in st:
#         if i.isdigit():
#             num1 += 1
#         elif i.isalpha():
#             zm += 1
#         else:
#             qt += 1
#     return f"数字{num1},字母{zm},其他{qt}"
# wn = "46513543fsdfsf你好啊,. ;"
#
# w = num(wn)
# print(w)
# 6. 写函数，接收两个数字参数，返回比较大的那个数字。
# def compare(a,b):
#     return a if a > b else b
# w = compare(30,20)
# print(w)

# 7. 写函数，检查传入字典的每一个value的长度,如果大于2，那么仅保留前两个长度的内容，并将新内容返回给调用者。
# dic = {"k1": "v1v1", "k2": [11,22,33,44]}
# #    PS:字典中的value只能是字符串或列表
# def  dic_value(dic):
#     for keys in dic:
#         if len(dic[keys]) > 2:
#             dic[keys] = dic[keys][:2]
#     return dic
# w = dic_value(dic)
# print(w)
# # 二
# def dic_value1(dic):
#     for k,v in dic.items():
#         if len(v) > 2:
#             dic[k] = v[:2]
#     return dic
# 8. 写函数，此函数只接收一个参数且此参数必须是列表数据类型，此函数完成的功能是返回给调用者一个字典，
# 此字典的键值对为此列表的索引及对应的元素。例如传入的列表为：[11,22,33] 返回的字典为 {0:11,1:22,2:33}。
# def new_dic(lis):
#     num = 0
#     dic = {}
#     for i in lis:
#         dic[num] = lis[num]
#         num += 1
#     return dic
# lis = [11,22,33,44,55,66,77]
# w = new_dic(lis)
# print(w)
# 9. 写函数，函数接收四个参数分别是：姓名，性别，年龄，学历。用户通过输入这四个内容，然后将这四个内容传入到函数中，
# 此函数接收到这四个内容，将内容追加到一个student_msg文件中。
# def student_msg(name, sex, age, edu):
#     with open("student_msg", mode="a", encoding="utf-8") as f:
#         f.write(f"{name} {sex} {age} {edu}\n")
#
#
# name = input("请输入姓名")
# sex = input("请输入性别(不输入默认为男性)")
# age = input("请输入年龄")
# edu = input("请输入学历")
# student_msg(name, sex, age, edu)


# 10. 对第9题升级：支持用户持续输入，Q或者q退出，性别默认为男，如果遇到女学生，则把性别输入女。
def student_msg(name, age, edu, sex="男"):
    with open("student_msg", mode="a", encoding="utf-8") as f:
        f.write(f"{name} {sex.strip()} {age} {edu}\n")


while True:
    msg = input("退出请输入输入Q or q,继续随便输入")
    if msg.isalpha():
        if msg.upper() == "Q":
            break
    sex = input("请输入性别 (男性可以不输入):")
    if sex == "女":
        student_msg(name=input("请输入姓名"), age=input("请输入年龄"), edu=input("请输入学历"),sex=sex)
    else:
        student_msg(name=input("请输入姓名"), age=input("请输入年龄"), edu=input("请输入学历"))



# 11. 写函数，用户传入修改的文件名，与要修改的内容，执行函数，完成整个文件的批量修改操作（**选做题**）。
# def alter(file_name, old_msg,new_msg):
#     import os
#     with open(f"{file_name}", mode="r", encoding="utf-8") as f1,\
#     open(f"{file_name}_副本",mode="w",encoding="utf-8") as f2:
#         for line in f1:
#             new_line = line.replace(f'{old_msg}', f'{new_msg}')
#             f2.write(new_line)
#     os.remove(f"{file_name}")
#     os.rename(f"{file_name}_副本", f"{file_name}")
# alter(file_name=input("输入文件命名"),old_msg=input("输入旧信息"),new_msg=input("输入新信息"))
