#
# 1.有变量name = " aleX leNb " 完成如下操作：
#
# - 移除 name 变量对应的值两边的空格,并输出处理结果
# name = " aleX leNb "
# name1 = name.strip(
# print(name1)
# - 将 name变量对应的值中所有的空格去除掉,并输出处理结果
# print(name.replace(' ',''))
# - 判断 name 变量是否以 "al" 开头,并输出结果（用两种方式 切片+字符串方法）
# print(name.strip().startswith('al'))
# print(name.strip()[0:2] == 'al')
# - 判断name变量是否以"Nb"结尾,并输出结果（用两种方式 切片+字符串方法）
# print(name.strip().endswith('Nb'))
# print(name.strip()[-2:] == 'Nb')
# - 将 name 变量对应的值中的 所有的"l" 替换为 "p",并输出结果
# print(name.strip().replace('l','p'))
# - 将name变量对应的值中的第一个"l"替换成"p",并输出结果
# print(name.strip().replace('l','p',1))
# - 将 name 变量对应的值根据 "l" 分割,并输出结果
# print(name.strip().split('l'))
# - 将 name 变量对应的值全部变大写,并输出结果
# print(name..upper())
# - 将 name 变量对应的值全部变小写,并输出结果
# print(name..lower())
# - 请输出 name 变量对应的值的第 2 个字符?
# print(name.[1])
# - 请输出 name 变量对应的值的前 3 个字符?
# print(name.[0:3])
# - 请输出 name 变量对应的值的后 2 个字符?
# print(name.[-2:])
# 2.有字符串
# s = "123a4b5c"
# - 通过对s切片形成新的字符串 "123"
# print(s[0:3])
# - 通过对s切片形成新的字符串 "a4b"
# print(s[3:6])
# - 通过对s切片形成字符串s5,s5 = "c"
# s5 = s[-1]
# print(s5)
#     - 通过对s切片形成字符串s6,s6 = "2ab"
#     s6 = s[1:6:2]
# print(s6)
# - 通过对s切片形成字符串s6,s6 = "cba"
# s6 = s[:-6:-2]
# print(s6)
#
# 3.使用while循环字符串 s="你好世界" 中每个元素。
# count = 0
# while count < len(s):
#     print(s[count])
#     count += 1
#
# 4.使用while循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"(提示使用字符串方法中的格式化)
# s = "321"
# count = 0
# while count < len(s):
#     print("倒计时%s秒"%(s[count]))
#     count += 1
# else:
#     print("出发")
#
# 5.使用for循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"(提示使用字符串方法中的格式化)
#
# s = "321"
# for i in s:
#     m = "倒计时%s秒"
#     print(m%(i))
# print("出发")
#
# 6.实现一个整数加法计算器(两个数相加)：
#
# 如：content = input("请输入内容:") 用户输入：5+9或5+ 9或5 + 9（含空白），然后进行分割转换最终进行整数的计算得到结果。(列表也支持索引)
# content = input("请输入内容:")
# content1 = content.replace(' ','')
# li = content1.split("+")
# print(int(li[0]) + int(li[-1]))
#
# 7.计算用户输入的内容中有几个 s 字符？
#
# 如：content = input("请输入内容：") # 如abcassfsqfsafqsdzacsad
# content = input("请输入内容：")
# n = 0
# for i in content:
#     if i == "s":
#         n += 1
# print(n)
# print(content.count("s"))
#
#
# 8.使用while循环分别正向和反向对字符串 message = "伤情最是晚凉天，憔悴厮人不堪言。" 进行打印。
# message = "伤情最是晚凉天，憔悴厮人不堪言。"
# countent = 0
# while countent < len(message):
#     print(message[countent])
#     countent += 1
#
# countent = 0
# while countent < len(message):
#     countent += 1
#     print(message[-countent])
#
#
# 9.获取用户输入的内容，并计算前四位"a"出现几次,并输出结果。
# m = input("请输入内容")
# n = 0
# for i in m[0:4]:
#     if i == "a":
#         n += 1
# print(n)
#
#
#
# 10.制作趣味模板程序需求：等待⽤户输⼊名字、地点、爱好，根据⽤户的名字和爱好进⾏任意现实 如：敬爱可亲的xxx，最喜欢在xxx地⽅⼲xxx (字符串格式化)
# name = input("请输入姓名")
# site = input("请输入地点")
# hobby = input("请输入爱好")
# print("敬爱可亲的%s,最喜欢在%s地方干%s"%(name,site,hobby))
#
#
#
# 11.判断⼀句话是否是回⽂. 回⽂: 正着念和反着念是⼀样的. 例如, 上海⾃来⽔来⾃海上
# h = input("请输入回文:")
# if h == h[::-1]:
#     print("是回文")
# else:
#     print("不是回文")
#
# 12.输⼊⼀个字符串，要求判断在这个字符串中⼤写字⺟，⼩写字⺟，数字，其他各出现了多少次，并输出出来
# n = 0
# m = 0
# t = 0
# d = 0
# b = input("请输入一个字符串")
# for i in b:
#     if i.isupper() == True:
#         n += 1
#     elif i.islower() == True:
#         m += 1
#     elif i.isdecimal() == True:
#         t += 1
#     else:
#         d += 1
# print("大写字母{},小写字母{},数字{},其他{}"。format(n,m,t,d)
#
#
# 13.用户可持续输入（用while循环），用户使用的情况：
# 输入A，则显示走大路回家，然后在让用户进一步选择：
# 是选择公交车，还是步行？
# 选择公交车，显示10分钟到家，并退出整个程序。
# 选择步行，显示20分钟到家，并退出整个程序。
# 输入B，则显示走小路回家，并退出整个程序。
# 输入C，则显示绕道回家，然后在让用户进一步选择：
# 是选择游戏厅玩会，还是网吧？
# 选择游戏厅，则显示 ‘一个半小时到家，爸爸在家，拿棍等你。’并让其重新输入A，B,C选项。
# 选择网吧，则显示‘两个小时到家，妈妈已做好了战斗准备。’并让其重新输入A，B,C选项。
# while True:
#     K = input("请输入A or B or C:")
#     if K == "A":
#         M = input("请输入公交 or 步行:")
#         if M == "公交":
#             print("十分钟到家")
#             exit(0)
#         elif M == "步行":
#             print("20分钟到家")
#             exit(0)
#     elif K == "B":
#         print("走小路回家")
#         exit(0)
#     elif K == "C":
#         print("绕道回家")
#         L = input("请输入游戏厅 or 网吧")
#         if L == "游戏厅":
#             print('一个半小时到家，爸爸在家，拿棍等你。')
#             # continue
#         elif L == "网吧":
#             print('两个小时到家，妈妈已做好了战斗准备。')
#             # continue
