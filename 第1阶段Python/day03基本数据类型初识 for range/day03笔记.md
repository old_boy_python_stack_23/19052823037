## 1.整型 int

​	在python3中所有的整数都是int类型.但在python2中如果数据量比较大,会使用long类型

而python3中不存在long类型

### 可以进行的操作:

​		bit_length().计算整数在内存中占用的二进制码的长度

```python
num = 13456845
print(num.bit_length())

十进制和二进制的计算
十进制转化成二进制:除二取余，倒序排列
# 14  余   0  # 十进制
#商
# 7        1
# 3        1
# 1        1
二进制转十进制的转换原理：从二进制的右边第一个数开始，每一个乘以2的n次方，n从0开始，每次递增1。然后得出来的每个数相加即是十进制数。
```



## 2.布尔值

bool # 布尔值 -- 用于条件使用

True  真

False 假

```python
# print(bool(-10))       # 0 是 False             非0的都是True
# print(bool(""))       # 空的字符串是 False 空str tupy list..   非空的就时True
# print(type(str(True))) 布尔值可以转换成字符串
# print(int(False))      # True 转化成数字 1   False 转换成数字 0
```



## 3.字符串 str

​	用于存储数据, 数据量比较少

​	把字符连成串.在python中使用     '    "    ''' 引起来的内容被称为字符串

### 索引和切片

**索引就是下标 切记 下标是从0开始的**

字符串，列表，元组  --  都是有索引（下标）

索引是准确的定位某个元素

从左向右 0,1,2,3

从右向左 -1，-2，-3，-4

**支持索引的都支持切片**   **[索引]**

切片长度可以超出范围，索引长度不能超出范围(会报错)  [起始位置：终止位置]

步长： 决定要走的方向，决定走的步子多大  [起始位置：终止位置：步长]

 起始位置 + 步长

```python
alex.txt
#       01234567  (索引)     #从左向右数数
#      -4-3-2-1 （索引） # 从右向左数数
# 格式 [star:end:step]

ws2 = "python最牛B"
print(s2[0:3])  # 从0获取到3. 不包含3. 结果: pyt 
print(s2[6:8])  # 结果 最牛 
print(s2[6:9])  # 最大是8. 但根据顾头不顾腚, 想要取到8必须给9 
print(s2[6:10])  # 如果右边已经过了了最大值. 相当于获取到后 
print(s2[4:])   # 如果想获取到后. 那么最后一个值可以不给. 
print(s2[-1:-5])    # 从-1 获取到 -5 这样是获取不不到任何结果的. 从-1向右数. 你怎么数 也数不不到-5 
print(s2[-5:-1])    # 取到数据了了. 但是. 顾头不顾腚. 怎么取后⼀一个呢? 
print(s2[-5:])  # 什么都不写就是最后了
print(s2[:-1])  # 这个是取到倒数第一个
print(s2[:])    # 原样输出 
# 跳着取, 步长
print(s2[1:5:2])    # 从第第一个开始取, 取到第5个,每2个取1个, 结果: yh, 分析: 1:5=> ytho => yh 
print(s2[:5:2])     # 从头开始到第五个. 每两个取一个 
print(s2[4::2])     # 从4开始取到后. 每两个取一个
print(s2[-5::2])    # 从-5取到后.每两个取一个
print(s2[-1:-5])    # -1:-5什什么都没有. 因为是从左往右获取的. 
print(s2[-1:-5:-1])  # 步⻓长是-1. 这时就从右往左取值了
print(s2[-5::-3])   # 从倒数第5个开始. 到开始. 每3个取一个, 结果oy 
```

​	步长: 如果是整数 从左向右取 如果是负数 从右向左取 默认是1

### 字符串的操作方法

```python
# 全部大写
name = "meet"
name1 = name.upper()
print(name1)
# 全部小写
name = "MEET"
name1 = name.lower()
print(name1)
# upper主要用于验证码登陆
yzm = "o98K"
input_yzm = input("请输入验证码（o98K）：")
if yzm.upper() == input_yzm.upper():
    print("正确")
else:
    print("错误")

# 以什么开头
name = alex.txt
print(name.startswith('a'))  # 就是以a开的头

# 以什么结尾
name = "zhuxiaodidi"
print(name.endswith("i"))   # 就是以i结尾

# count # 统计
name = "zhudidi"
print(name.count("zhu"))   # 查询某个内容出现的次数

# 替换  ****
name = "alexnbnsnsn"
name1 = name.replace('n','s')  # 替换
name1 = name.replace('n','s',2)  # 替换  2是替换的次数
print(name1)

# 除去头尾两边的空格和换行符 \n  脱   *****
name = alex.txt
name1 = name.strip()  # 可以写想要去掉的内容
print(name1)
if name == alex.txt:
    print(666)

name = alex.txt
print(name.strip())

# 分割    *****
name = alex.txt
print(name.split("w"))
# 默认是以空格分割 ,也可以自己制定分割
# 分割后返回的内容是一个列表
```



### 第三种字符串格式化

```python
name = "alex{}wusir{}"
name1 = name.format('结婚了',"要结婚了")  # 按照位置顺序去填充的

name = "alex{1}wusir{0}"
name1 = name.format('结婚了',"要结婚了")    # 按照索引位置去填充

name = "alex{a}wusir{b}"
name1 = name.format(a="结婚了",b="马上结婚")  # 指名道姓 关键字 填充
print(name1)
```





### is 系列  -- 判断

```python
name = "②"
print(name.isdigit())  # 判断是不是阿拉伯数字 有bug 上面就是特例 不建议使用

name = "666"
print(name.isdecimal()) # 判断是不是十进制  -- 推荐用它来判断是不是数字

name = "alexs你好"
print(name.isalpha())    # 判断的是中文和字母

name = "alex666"
print(name.isalnum())     # 判断的是不是字母，中文和阿拉伯数字
```



## 4.for循环(迭代)

​	使用for循环可以遍历(获取)字符串中的每一个字符

``` python
# 语法
for i(变量) in 可迭代对象:
    pass or ...
# pass 和 ... 都是占位符,仅起到占位的作用,并不会做任何操作
# 可迭代对象: 可以一个一个往外取值的对象
s19 = "⼤大家好, 我是VUE, 前端的⼩小朋友们. 你们好么?" 
# ⽤用while循环 
index = 0 
while index < len(s19):    
    print(s19[index])   # 利利⽤用索引切⽚片来完成字符的查找    
    index = index + 1
# for循环, 把s19中的每⼀一个字符拿出来赋值给前⾯面的c 
for c in s19:    
    print(c)
'''    in有两种⽤用法:
1. 在for中. 是把每⼀一个元素获取到赋值给前⾯面的变量量.
2. 不不在for中. 判断xxx是否出现在str中. 
'''
print('VUE' in s19)
# 练习, 计算在字符串"I am sylar, I'm 14 years old, I have 2 dogs!" 
s20 = "I am sylar, I'm 14 years old, I have 2 dogs!" 
count = 0 
for c in s20:
    if c.isdigit():
        count = count + 1
print(count)
```

## 5 range

python 3 中 打印range() 是他本身

python 2 中打印range() 是list 



