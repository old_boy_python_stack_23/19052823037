# # # 1.
# # # 猜数字，设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了, 然后继续让用户输入;
# # # 如果比66小，则显示猜测的结果小了, 然后继续让用户输入;
# # # 只有等于66，显示猜测结果正确，然后退出循环。
# #
# while True:  # 让程序循环起来方便多次输入测试
#     n = 66
#     m = int(input("请输入一个数字"))
#     if m > n:
#         print("大了")
#     elif m < n:
#         print("小了")
#     else:
#         print("结果正确")
#         break  # 结果正确终止循环
# #
# #
# # # 2.
# # # 在上一题的基础，设置：给用户三次猜测机会，如果三次之内猜测对了，则显示猜测正确，退出循环，如果三次之内没有猜测正确，则自动退出循环，并显示‘大笨蛋’
# count = 1
# while count <= 3:
#     num = int(input("请输入一个数字:"))
#     if num < 66:
#         print("小了")
#     elif num == 66:
#         print("正确")
#         break
#     else:
#         print("大了")
#     count += 1
# else:
#     print("大笨蛋")
# # #
# # # 3.判断下列逻辑语句的True, False
# 1 > 1 or 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6
# True
# not 2 > 1 and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6
# False
# print(1 > 1 or 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6)
# print(not 2 > 1 and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6)
#
# # # 4.求出下列逻辑语句的值。
# 8 or 3 and 4 or 2 and 0 or 9 and 7
# 8
# 0 or 2 and 3 and 4 or 6 and 0 or 3
# 4
# print(0 or 2 and 3 and 4 or 6 and 0 or 3)
# #
# # # 5.下列结果是什么？
# 6 or 2 > 1
# 6
# 3 or 2 > 1
# 3
# 0 or 5 < 4
# False
# 5 < 4 or 3
# 3
# 2 > 1 or 6
# True
# 3 and 2 > 1
# True
# 0 and 3 > 1
# 0
# 2 > 1 and 3
# 3
# 3 > 1 and 0
# 0
# 3 > 1 and 2 or 2 < 3 and 3 and 4 or 3 > 2
# 2
#
# #
# # # 6.使用while循环输出
# # # 1
# # # 2
# # # 3
# # # 4
# # # 5
# # # 6
# # # 8
# # # 9
# # # 10
# # 方法一
# n = 1
# while n <= 10:
#     if n != 7:
#         print(n)
#     n += 1
# # 方法二
# for i in range(1, 11):
#     if i != 7:
#         print(i)
# #
# # # 7.求1 - 100的所有数的和
# # 方法一
# sum = 0
# for i in range(1, 101):
#     sum = sum + i
# print(sum)
#
# # 方法二
# count = 1
# sum = 0
# while count <= 100:
#     sum += count
#     count += 1
# print(sum)
# #
# # # 8.输出1 - 100内的所有奇数(奇数就是除以2余数不为0)
# b = 1
# s = 0
# while b <= 100:
#     if b % 2 == 1:
#         print(b, end='')  # 为了测试具体的数字是否符合要求
#         s += b
#     b = b + 1
# print('\n')  # 换行测试时为了方便看,让结果输出到同一行了
# print(s)
# #
# #
# # # 9.输出1 - 100内的所有偶数(偶数就是除以2余数不为1)
# b = 1
# s = 0
# while b <= 100:
#     if b % 2 == 0:
#         print(b, end='')  # 为了测试具体的数字是否符合要求
#         s += b
#     b = b + 1
# print('\n')  # 换行测试时为了方便看,让结果输出到同一行了
# print(s)
# #
# #
# # # 10.求1 - 2 + 3 - 4 + 5...99的所有数的和
# # 方法一
# a = 1
# s = 0
# n = 0
# while a < 100:
#     if a % 2 == 1:
#         s += a
#         a += 1
#     elif a % 2 == 0:
#         n += a
#         a += 1
# print(s)
# print(n)
# print(s - n)
#
# # 二
#
# a = 1
# s = 0
# while a < 100:
#     if a % 2 == 1:
#         s += a
#         a += 1
#     elif a % 2 == 0:
#         s += a
#         a -= 1
# print(s)
#
#
#
#
#
#
# # 11.简述ASCII、Unicode、utf - 8编码英文和中文都是用几个字节?
# ASCII
# 英文
# 一个字节
# 无中文
# Unicode
# 英文
# 2
# 个字节
# 中文
# 4
# 个字节
# utf - 8
# 英文
# 1
# 个字节
# 中文
# 3
# 个字节
#
# #
# #
# # 12.
# # 简述位和字节的关系？
# 8 bit = 1 byte
#
# # 13.
# # "老男孩"使用GBK占几个字节, 使用Unicode占用几个字节?
# 使用GBK占6个字节
# 使用Unicode占12字节
# #
# #
# # 14.
# # 猜年龄游戏升级版
# # 要求：允许用户最多尝试3次，每尝试3次后，如果还没猜对，就问用户是否还想继续玩，如果回答Y，就继续让其猜3次，以此往复，如果回答N，就退出程序，如何猜对了，就直接退出。
# w = 1
# n = 66
# while True:
#     m = int(input("请输入一个数字"))
#     if m > n:
#         print("大了")
#     elif m < n:
#         print("小了")
#     else:
#         print("结果正确")
#         break  # 结果正确终止循环
#     if w % 3 == 0:
#         while True:
#             Q = input("是否继续,继续输入Y,退出回复N")
#             if Q == "N":
#                 exit(0)
#             elif Q == "Y":
#                 break
#             else:
#                 print("输入错误,请重新输入")
#     w += 1
# #
# #
# # 15. ⽤户登陆（三次输错机会）且每次输错误时显示剩余错误次数（提示：使⽤字符串格式化）
# n = 1
# while n <= 3:
#     name = input("请输入用户名")
#     password = input("请输入密码")
#     if name == "yangsen" and password == "love":
#         print("登陆成功")
#         break
#     else:
#         print("用户名或密码错误,剩余错误次数%s" % (3 - n))
#     n += 1
# func = "jfdklnfn"
# print("nihao%s"%(func))
# 字符串中一个%sord...对应外边%()里的一个数据类型