## 1.字符串格式化输出

​	% 占位符:

​	声明占位的类型 %s -- 字符串  %d/%i -- 整型  %% 转义 成为普通的%

​	%() 不能多，不能少，一一对应

​	f"{}" 大括号里的内容一般都放变量 字符串单引号

​	3.6版本及以上才能使用# 

```python
msg = '''
a = "------------------- info ----------------------"
b = %s
c = %s
d = %s
e = %s
f = %s
g = "------------------- end ------------------------"
'''
name = input("name:")                 
age = input("age:")
sex = input("sex:")
job = input("job:")
hobby = input("hobby:")
print(msg)

	两种输出方式

print(msg%(input("name:"),input("age:"),input("sex:"),input("job:"),input("hobby:")))

# 字符串格式化的时候 不能少 不能多   （占的数量和填充的数量要一致）
# 填充的时候 内容和占的位置是要一一对应的


msg = '%s，学习进度5%%'
print(msg%(input("name:")))

# %s -- 占字符串的位置
# %d -- 占整型的位置
# %% -- 转义（把占位转换成普通的%号）

name = input("name")
print(f"alex.txt{name},{'aaa'}")

# f字符串拼接 -- 3.6版本及以上才能使用

msg = '''
------------------- info ----------------------
name: %s
age:  %s
sex : %s
job:  %s
hobby: %s
------------------- end ------------------------
'''

print(msg%(alex.txt,'20','nan','it','cnb'))

msg = f'''
------------------- info ----------------------
name: {input("name:")}
age:  {input("age:")}
sex : {input("sex:")}
job:  {input("job:")}
hobby: {input("hobby:")}
------------------- end ------------------------
'''
print(msg)
```



## 2.while 循环

​	while 关键字 条件:   （死循环）

​	    循环体

​	条件终止循环

​	break   终止当前的循环

​	continue 跳出本次循环，继续下次循环   伪装成循环体中最后一行# 

```python
while True: # 死循环
    循环体
	break
    continue
else:
```





## 3.运算符

​	算数运算符

```python
+ - * / **幂运算 //取整   %取余(模)  
```



赋值运算符

```python
= += -= *= /= **= %= //=
    a = 1
    a += 1  # a = a + 1
    a -= 1  # a = a - 1
    a *= 1  # a = a * 1
    a /= 1    # a = a / 1
    a **= 1   # a = a ** 1
    a %= 1    # a = a % 1
```



逻辑运算符

```python
and (与) -- 和   or (或)   not (非) -- 不是

    1 and 0  # and是两边都是真的时候才是真，只要有一边是假就取假
    0 and 1  
    print(1 and 9)   #and 运算 两边都是真的时候取and后边的内容
    print(False and 0) #and 运算 两边都是假的时候取and前边的内容

    print(3 > 2 and 9)  3>2 视为一个整体来看

    or

    print(1 or 0)  # 只要有一个是真就取真
    print(1 or 4)   # or 两个都是真的时候，取or前面的内容
    print(False or 0) # or 两个都是假的时候，取or后面的内容

    print(3>2 or 4)

	运算顺序
    () > not > and > or
	练习
    print(3 and 9 or 8 and 7 or 0)
    print(0 and False or False and 9 and 4>3)
    print(True and False or 3>4 and not True)
```



比较运算符

```python
== # 等于
!= # 不等于
>  # 大于
<  # 小于 
>= # 大于等于
<= # 小于等于
```



成员运算符

```python
in 在   not in 不在# 
```



## 4.编码

ASCII码 -- 不支持中文

GBK --  国标：

​	英文 1个字节

​	中文 2个字节

Unicode -- 万国码：

​	英文 2个字节

​	中文 4个字节

utf-8  -- 最流行的编码方式

​	英文 1个字节

​	欧洲 2个字节

​	亚洲 3个字节# 

单位转换：

​	1Byte = 8bit

​	1024B = 1KB

​	1024KB = 1MB

​	1024MB = 1GB

​	1024GB = 1TB

