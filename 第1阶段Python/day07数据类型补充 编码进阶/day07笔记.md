# 数据类型补充

## str 

​	**定义方式**

​		s = "alex"

​		str(1235456)

​	

​	**首字母大写**

​		s = "alex wusir"

​		s1 = s.capitalize()

​		s2 = s.title

​	**统计出现的次数**

​		a = "alex awusair"

​		print(count("a"))

​		大小写转换

​		s = "alex"

​		print(s.swapcase())

​	**查找**

​		s = "alex taibai"

​		print(s.find("c"))    # find找不到 返回-1

​		print(s.index("C"))  #  查不到就报错

## list

​		li = list("12341") 定义方式

​	**统计**

​		count

​	**查看**

​		.index()

​	**翻转**

​		li.reverse()

​		print(li)

​	**排序**

​		li.sort() # 升序

​		li.sort(reverse=True) 降序

​		print(li)

## tuple元组

tu = tuple("546413546")

统计

.count()

查找

tu.index("  ")

## dict 字典

dic = dict(k = 1,k1 = 123)  # 定义方式

print(dic)

随机删除

dic.popitem()  # python3.6版本默认删除最后一个键值对

python3.5之前 随机删除  #   宝哥翻车了

## 批量创建字典

 dic = {}

dic1.fromkeys("123",1)

print(dic1)

第一个参数是可迭代对象

第二个参数是每个键对应的值  ---- 用的都是同一个内存地址

## set 集合

```python
s = set("1234")  # 定义方式
print(s)
```

## 数据类型转换

```python
str - int # 字符串中必须都是十进制的数,才能进行转换
s = "abc"
n = int(s)
print(n,type(n))

int - str
n = 123
s = str(n)
print(s,type(s))

str - list
s = "123"
li = list(s)
print(li)

面试题:
把字符串转化成列表
print(s.split())
把列表转换成字符串
print(''.join(li))

list - str
li = ["12","3"]
s = str(li)
print(s,type(s))
print(''.join(li))  #join 不能有数字

list - tuple
li = [1,2,3]
tu = tuple(li)
print(tu)

tuple - list
tu = (1,2,3)
li = list(tu)
print(li)

set - list
s = {12,3,4}
print(list(s))

list - set
li = [1,2,3]
s = set(li)
print(s)
```



## 总结1：

```python
str -- int  # 字符中必须都是十进制的数，才能进行转换
s = "abc"
n = int(s)
print(n,type(n))

str -- list
s = "123"
li = list(s)
print(li)
# 面试题：
把字符串转化成列表
print(s.split())
把列表转化成字符串
print(''.join())


list -- str
li = ["3231","2323"]
s = str(li)
print(s,type(s))
print(''.join(li))# join 不能有数字

list -- tuple 
li = [1,2,3,4]
tu = tuple(li)
print(tu)

tuple -- list
tu = (1,2,3,4)
li = list(tu)
print(li)

set -- list
s = {32,4,5,5}
print(list(s))

list -- set
li =[3,2,5]
s = set(li)
print(s)
```





**总结1**：

​	字符串 -- 数字：字符串中必须都是十进制的数字

​	数字 -- 字符串：直接传唤

​	列表 -- 字符串：‘’.join() -- 可迭代对象中不能出现数字

​	字符串 -- 数字：split

​	出字典外，容器数据类型之间可以直接相互转换

## 总结2

​	str, int, bool, list, tuple, dict, set

有序：

​	str,int,bool list tuple

无序：

​	dict ,set

可变：

​	list, dict, set

不可变：

​	str, int, tupule, bool



# 常见的坑

```python
li = [1,2,3,4]  # [1,3,4]
# 索引值是奇数的删除
for i in range(4):
    if i % 2 == 1:
        li.pop(i)   # 会报错
print(li)

# 面试题:
li = [1,2,3,4,5]   #[1,3,4,5]
# 索引值是奇数的删除
for i in range(4):
    if i % 2 == 1:
        li.pop(i)   # 结果不对
print(li)

li = [1,2,3,4,5]
for i in range(len(li)-1,-1,-1):
    if i % 2 == 1:
        li.pop(i)   # 倒序删除
print(li)

# 偷换感念
li = [1,2,3,4,5]
new_li = []
for i in range(len(li)):
    if i % 2 == 1:
        new_li.append(li[i])
for em in new_li:
    li.remove(em)
print(li)

li = [1,2,3,4,5]
for i in range(len(li)-1,-1,-1):
    if i % 2 == 1:
        del li[i]   # 倒序删除
print(li)

li = [1,2,3,4,5]
del li[1::2]
print(li)

# 使用for删除列表的时候从左向右删除,会报错.结果不对

dic = {"k1":"v1","k2":"v2"}
for i in dic:
    dic["k3"] = "v3"
print(dic)

# 面试题:
li = [1,2,3,4]
for i in li:
    li.append(i)
print(li)

# 不能在遍历字典本身中改变大小,要批量删除字典的键值对

dic = {"k1":"v1","k2":"v2","k3":"v3"}
for i in dic:
    dic["k7"] = "1"
print(dic)

li = []
for i in dic:
    if i == "k1" or i == "k3":
        li.append(i)
for em in li:
    dic.pop(em)
print(dic)

```



# 编码进阶

硬盘中存储的是  字节

```python
s = "少年"
s1 = s.encode("gbk")以GBK进行编码
s2 = s1.decode("gbk")以GBK进行解码 # 用什么编码就用什么解码
print(s1)  # b'\xc9\xd9\xc4\xea'
print(s2)

python2 内存使用的是ascii
python3 内存使用的是Unicode
```























