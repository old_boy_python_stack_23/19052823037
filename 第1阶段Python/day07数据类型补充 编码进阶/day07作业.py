
# # 1.整理当天的笔记,将今天课上代码自己练习一下
# # 已完成
# # 2.用户输入一个数字，判断一个数是否是水仙花数。
# # 水仙花数是一个三位数, 三位数的每一位的三次方的和还等于这个数. 那这个数就是一个水仙花数, 例如: 153 = 1**3 + 5**3 + 3**3
# num = input("请输入一个数字:")
# s = 0
# for i in num:
#     s += int(i)**3
# print(s)
# print("是水仙花数") if s == int(num) else print("不是")
#
#
#
# # 3.请说出下面a,b,c三个变量的数据类型。
# a = ('太白金星')  # 字符串
# b = (1,)          # 元组
# c = ({'name': 'barry'}) # 字典
#
# 4.按照需求为列表排序：
# l1 = [1, 3, 6, 7, 9, 8, 5, 4, 2]
# # 从大到小排序
# l1.sort(reverse=True)
# print(l1)
# # 从小到大排序
# l1.sort
# print(l1)
# # 反转l1列表
# l1.reverse()
# print(l1)
#
# # 5.看代码写结果：
# dic = dict.fromkeys('abc',[])
# dic['a'].append(666)
# dic['b'].append(111)
# print(dic)
# ('a':[666,111],'b':[666,111],'c':[666,111])
#
# # 6.完成彩票36选7的功能. 从36个数中随机的产生7个数. 最终获取到7个不重复的数据作为最终的开奖结果.
# # 随机数:
# # from random import randint
# # randint(0, 20) # 0 - 20 的随机数
# li = set({})
# from random import randint
# while len(li) < 7:
#     num = randint(0, 36)
#     li.add(num)
# print(li)
#
# # 7.字符串和字节转换
# # s1 = '太白金星'
# # 将s1转换成utf-8的bytes类型。
# print(s1.encode("utf-8"))  #b'\xe5\xa4\xaa\xe7\x99\xbd\xe9\x87\x91\xe6\x98\x9f'
# # 将s1转化成gbk的bytes类型。
# print(s1.encode("gbk"))   #b'\xcc\xab\xb0\xd7\xbd\xf0\xd0\xc7'
# b = b'\xe5\xae\x9d\xe5\x85\x83\xe6\x9c\x80\xe5\xb8\x85'
# # b为utf-8的bytes类型，请转换成gbk的bytes类型。
# b1 = b.decode("utf-8")
# print(b1.encode("gbk"))  # b'\xb1\xa6\xd4\xaa\xd7\xee\xcb\xa7'
#
# # 8.把列表中所有姓周的⼈的信息删掉(升级题：此题有坑, 请慎重):
# lst = ['周⽼⼆', '周星星', '麻花藤', '周扒⽪']
# #   结果: lst = ['麻花藤']
#
# for i in range(len(lst)-1,-1,-1):
#     if "周" in lst[i]:
#         lst.remove(lst[i])
# print(lst)
#
# 9.⻋牌区域划分, 现给出以下⻋牌. 根据⻋牌的信息, 分析出各省的⻋牌持有量. (升级题)
cars = ['鲁A32444','鲁B12333','京B8989M','⿊C49678','⿊C46555','沪 B25041']
locals = {'沪':'上海', '⿊':'⿊⻰江', '鲁':'⼭东', '鄂':'湖北', '湘':'湖南','京':'北京'}
# 结果: {'⿊⻰江':2, '⼭东': 2, '北京': 1,'上海':1}
dic = {}
l1 = []
for i in cars:
    if i[0] in locals.keys():
        l1.append(i[0])
        num = l1.count(i[0])
        dic[locals[i[0]]] = num
print(dic)
#
#
# # 10.⼲掉主播. 现有如下主播收益信息:
# zhubo = {'卢本伟':122000, '冯提莫':189999, '⾦⽼板': 99999, '吴⽼板': 25000000, 'alex': 126}
# # 1. 计算主播平均收益值
# val = zhubo.values()
# money = 0
# for i in val:
#     money += i
# # print(money)
# mean = (money/len(zhubo))
# print(mean)
#
# # 2. ⼲掉收益⼩于平均值的主播
# baibai = []
# for i in zhubo.keys():
#     if zhubo[i] < mean:
#         baibai.append(i)
# print(baibai)
# for i in baibai:
#     zhubo.pop(i)
# print(zhubo)
# # 3. ⼲掉卢本伟
# zhubo.pop("卢本伟")
# print(zhubo)