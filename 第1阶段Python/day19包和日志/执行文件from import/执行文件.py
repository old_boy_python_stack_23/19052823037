# from ... import ...
# 通过这种方式不用设置__init__文件
# from aaa import m1
# m1.func()

# from aaa.bbb.m2 import func1
# func1()
# from aaa.bbb import m2
# m2.func1()

# from a.b.c import d.e.f
# c的. 的前面一定是包
# import 的后面一定是名字,并且不能 再有点

# from aaa.bbb.m2.func1 import a  # 错误的
# from aaa.bbb import m2
# m2.func1()

import json
