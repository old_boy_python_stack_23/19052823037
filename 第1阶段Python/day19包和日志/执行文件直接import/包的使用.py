# 第一类: 执行文件 通过 import 导入包以及包内的功能
# 创建一个aaa的包,自行创建一个__init__py文件
# 回忆 :创建一个tbjx模块发生的三件事:
'''
    1. 将该tbjx文件加载到内存.
    2. 创建一个以tbjx命名的名称空间.
    3. 通过tbjx. 的方式引用tbjx模块的所有的名字.
'''

# 创建一个包,也会发生三件事:
'''
    1. 将该aaa包内 __init__py文件加载到内存.
    2. 创建一个以aaa命名的名称空间.
    3. 通过aaa. 的方式引用__init__的所有的名字.
'''
import aaa
# print(aaa.x)
# aaa.f1()

# print(aaa.m1)
# print(aaa.m1.a)

# 我想要引用 aaa包的m1文件的a变量
# 错误示例1:
import aaa
# 1. aaa的 __init__ 里面 写import m1
# 2. print(aaa.m1.a)
# print(aaa.m1.a)

# 报错原因: No module named 'm1'
# 分析报错原因: 模块找不到 内存,内置,sys.path三个地方找不到.
# m1 不在内存,不在内置,sys.path 会主动加载执行文件(包的使用.py)的当前目录.

# 解决方式:
import aaa
# 1. 在执行文件写入 import aaa
# 2. aaa的 __init__ 里面 写 from aaa import m1
# 3. 然后在执行文件  aaa.m1.a
# print(aaa.m1.a)

# aaa.m1.func1()


import aaa
# 如何在当前文件中,引用 aaa包的bbb包.
# 1. 在执行文件写入 import aaa
# 2. aaa的 __init__ 里面 写 from aaa import bbb
# 3. 然后在执行文件  aaa.bbb
# print(aaa.bbb)


# 如何在当前文件中,引用 aaa包的bbb包 的 变量 name.
# 1. 在执行文件写入 import aaa
# 2. aaa的 __init__ 里面 写 from aaa import bbb
# 3. 然后在执行文件  aaa.bbb
# print(aaa.bbb)


import aaa
# print(aaa.bbb.name)

# 如何在当前文件中,引用 aaa包的bbb包 的 mb文件的函数func.

# 1. 在执行文件写入 import aaa
# 2. 在aaa包的__Init__ 写上 from aaa import bbb  (这样写 bbb包的__init__里面所有的名字都能引用)
# print(aaa.bbb.name)
# 3. 在bbb包的__Init__ 写上 from aaa.bbb import mb
# aaa.bbb.mb.func3()

# 首先 无论从哪里引用模块,import 或者 from  ... import ...
# 最开始的模块或者包名一定是内存,内置,sys.path中能找到的.(可参考bbb包中的 __init__)

# 直接import 为了让我们会使用 包里面的 __init__






# 第二类: 执行文件 通过 from ... import... 导入包以及包内的功能