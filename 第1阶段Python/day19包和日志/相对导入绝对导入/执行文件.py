# import nb
# nb.f1()
# nb.f2()

# 由于nb模块增加了很多很多功能,所以我们nb这个文件就要划整一个包,
# 无论对nb模块有任何操作,对于使用者来说不应该改变,极少的改变对其的调用.
#
# import sys
# sys.path.append(r'D:\s23\day19\相对导入绝对导入\dir')
#
# import nb
#
# nb.f1()
# nb.f2()
# nb.f3()
# nb.f4()
# nb.f5()
# nb.f6()

# 我将原包名改成了大写的NB
import sys
sys.path.append(r'D:\s23\day19\相对导入绝对导入\dir')
import NB as nb

nb.f1()
nb.f2()
nb.f3()
nb.f4()
nb.f5()
nb.f6()
nb.f7()