# from nb.m1 import f1,f2
# from nb.m2 import f3,f4
# from nb.m3 import f5,f6

# 目前为止,今天所看到的引用模块的方式都是绝对导入
# 相对导入: . ..  ......


from .m1 import f1,f2
from .m2 import f3,f4
from .m3 import f5,f6
from .dsb.ly import f7