# import logging
# logging.basicConfig(
#     level=logging.DEBUG,
# )
# # logging.debug('debug message')
# # logging.info('info message')
# # logging.warning('warning message')
# # logging.error('error message')
# # logging.critical('critical message')

# 应用:
# def func():
#     print('in func')
#     logging.debug('正常执行')
# func()

# try:
#     i = input('请输入选项:')
#     int(i)
# except Exception as e:
#     logging.error(e)
# print(11)

# low版的日志:缺点: 文件与屏幕输入只能选择一个.
import logging
logging.basicConfig(
    # level=logging.DEBUG,
    level=30,
    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
    filename=r'test.log',
)
# logging.debug('调试模式')  # 10
# logging.info('正常模式')  # 20
logging.warning('警告信息')  # 30
# logging.error('错误信息')  # 40
# logging.critical('严重错误信息')  # 50