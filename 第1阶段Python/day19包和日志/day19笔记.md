##  包

####  什么是包



```
#官网解释
Packages are a way of structuring Python’s module namespace by using “dotted module names”
包是一种通过使用‘.模块名’来组织python模块名称空间的方式。

#具体的：包就是一个包含有__init__.py文件的文件夹，所以其实我们创建包的目的就是为了用文件夹将文件/模块组织起来

#需要强调的是：
　　1. 在python3中，即使包下没有__init__.py文件，import 包仍然不会报错，而在python2中，包下一定要有该文件，否则import 包报错

　　2. 创建包的目的不是为了运行，而是被导入使用，记住，包只是模块的一种形式而已，包的本质就是一种模块
```



#### 为何要使用包

```
包的本质就是一个文件夹，那么文件夹唯一的功能就是将文件组织起来
随着功能越写越多，我们无法将所以功能都放到一个文件中，于是我们使用模块去组织功能，而随着模块越来越多，我们就需要用文件夹将模块文件组织起来，以此来提高程序的结构性和可维护性
```

#### 注意事项



```
#1.关于包相关的导入语句也分为import和from ... import ...两种，但是无论哪种，无论在什么位置，在导入时都必须遵循一个原则：凡是在导入时带点的，点的左边都必须是一个包，否则非法。可以带有一连串的点，如item.subitem.subsubitem,但都必须遵循这个原则。但对于导入后，在使用时就没有这种限制了，点的左边可以是包,模块，函数，类(它们都可以用点的方式调用自己的属性)。

#2、import导入文件时，产生名称空间中的名字来源于文件，import 包，产生的名称空间的名字同样来源于文件，即包下的__init__.py，导入包本质就是在导入该文件

#3、包A和包B下有同名模块也不会冲突，如A.a与B.a来自俩个命名空间
```



#### 包的使用

　　**示例文件**



```
glance/                   #Top-level package

├── __init__.py      #Initialize the glance package

├── api                  #Subpackage for api

│   ├── __init__.py

│   ├── policy.py

│   └── versions.py

├── cmd                #Subpackage for cmd

│   ├── __init__.py

│   └── manage.py

└── db                  #Subpackage for db

    ├── __init__.py

    └── models.py
```



　　**文件内容**

```


#policy.py
def get():
    print('from policy.py')

#versions.py
def create_resource(conf):
    print('from version.py: ',conf)

#manage.py
def main():
    print('from manage.py')

#models.py
def register_models(engine):
    print('from models.py: ',engine)

包所包含的文件内容

文件内容
```



执行文件与示范文件在同级目录下

　　**包的使用之import** 

```
1 import glance.db.models
2 glance.db.models.register_models('mysql') 
```

单独导入包名称时不会导入包中所有包含的所有子模块，如



```
#在与glance同级的test.py中
import glance
glance.cmd.manage.main()

'''
执行结果：
AttributeError: module 'glance' has no attribute 'cmd'

''' 
```



解决方法：

```
1 #glance/__init__.py
2 from . import cmd
3 
4 #glance/cmd/__init__.py
5 from . import manage
```

执行：

```
1 #在于glance同级的test.py中
2 import glance
3 glance.cmd.manage.main()
```

　　**8.4.4 包的使用之from ... import ...**

**需要注意的是from后import导入的模块，必须是明确的一个不能带点，否则会有语法错误，如：from a import b.c是错误语法**

```
1 from glance.db import models
2 models.register_models('mysql')
3 
4 from glance.db.models import register_models
5 register_models('mysql')
```

　　**8.4.5 from glance.api import \***

在讲模块时，我们已经讨论过了从一个模块内导入所有*，此处我们研究从一个包导入所有*。

此处是想从包api中导入所有，实际上该语句只会导入包api下__init__.py文件中定义的名字，我们可以在这个文件中定义__all___:



```
1 #在__init__.py中定义
2 x=10
3 
4 def func():
5     print('from api.__init.py')
6 
7 __all__=['x','func','policy']
```



此时我们在于glance同级的文件中执行from glance.api import *就导入__all__中的内容（versions仍然不能导入）。



```
#在__init__.py中定义
x=10

def func():
    print('from api.__init.py')

__all__=['x','func','policy']
```



此时我们在于glance同级的文件中执行from glance.api import *就导入__all__中的内容（versions仍然不能导入）。

练习：



```
#执行文件中的使用效果如下，请处理好包的导入
from glance import *

get()
create_resource('a.conf')
main()
register_models('mysql')
```

```
#在glance.__init__.py中
from .api.policy import get
from .api.versions import create_resource

from .cmd.manage import main
from .db.models import  register_models

__all__=['get','create_resource','main','register_models']
```



　　**绝对导入和相对导入**

我们的最顶级包glance是写给别人用的，然后在glance包内部也会有彼此之间互相导入的需求，这时候就有绝对导入和相对导入两种方式：

绝对导入：以glance作为起始

相对导入：用.或者..的方式最为起始（只能在一个包中使用，不能用于不同目录内）

例如：我们在glance/api/version.py中想要导入glance/cmd/manage.py



```
1 在glance/api/version.py
2 
3 #绝对导入
4 from glance.cmd import manage
5 manage.main()
6 
7 #相对导入
8 from ..cmd import manage
9 manage.main()
```



测试结果：注意一定要在于glance同级的文件中测试

```
1 from glance.api import versions 
```

　　**8.4.7 包以及包所包含的模块都是用来被导入的，而不是被直接执行的。而环境变量都是以执行文件为准的**

比如我们想在glance/api/versions.py中导入glance/api/policy.py，有的同学一抽这俩模块是在同一个目录下，十分开心的就去做了，它直接这么做

```
1 #在version.py中
2 
3 import policy
4 policy.get()
```

没错，我们单独运行version.py是一点问题没有的，运行version.py的路径搜索就是从当前路径开始的，于是在导入policy时能在当前目录下找到

但是你想啊，你子包中的模块version.py极有可能是被一个glance包同一级别的其他文件导入，比如我们在于glance同级下的一个test.py文件中导入version.py，如下



```
 1 from glance.api import versions
 2 
 3 '''
 4 执行结果:
 5 ImportError: No module named 'policy'
 6 '''
 7 
 8 '''
 9 分析:
10 此时我们导入versions在versions.py中执行
11 import policy需要找从sys.path也就是从当前目录找policy.py,
12 这必然是找不到的
13 '''
```



　　**8.4.8 绝对导入与相对导入总结**



```
绝对导入与相对导入

# 绝对导入: 以执行文件的sys.path为起始点开始导入,称之为绝对导入
#        优点: 执行文件与被导入的模块中都可以使用
#        缺点: 所有导入都是以sys.path为起始点,导入麻烦

# 相对导入: 参照当前所在文件的文件夹为起始开始查找,称之为相对导入
#        符号: .代表当前所在文件的文件加,..代表上一级文件夹,...代表上一级的上一级文件夹
#        优点: 导入更加简单
#        缺点: 只能在导入包中的模块时才能使用
　　　　  #注意:
　　　　　　　　1. 相对导入只能用于包内部模块之间的相互导入,导入者与被导入者都必须存在于一个包内
　　　　　　　　2. attempted relative import beyond top-level package # 试图在顶级包之外使用相对导入是错误的,言外之意,必须在顶级包内使用相对导入,每增加一个.代表跳到上一级文件夹,而上一级不应该超出顶级包
```

# 日志
## low版日志
```
# import logging
# logging.basicConfig(
#     level=logging.DEBUG,
# )
# # logging.debug('debug message')
# # logging.info('info message')
# # logging.warning('warning message')
# # logging.error('error message')
# # logging.critical('critical message')

# 应用:
# def func():
#     print('in func')
#     logging.debug('正常执行')
# func()

# try:
#     i = input('请输入选项:')
#     int(i)
# except Exception as e:
#     logging.error(e)
# print(11)

# low版的日志:缺点: 文件与屏幕输入只能选择一个.
import logging
logging.basicConfig(
    # level=logging.DEBUG,
    level=30,
    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
    filename=r'test.log',
)
# logging.debug('调试模式')  # 10
# logging.info('正常模式')  # 20
logging.warning('警告信息')  # 30
# logging.error('错误信息')  # 40
# logging.critical('严重错误信息')  # 50
```
## 标准版日志
```
# import logging
#
# # 创建一个logging对象
# logger = logging.getLogger()
#
# # 创建一个文件对象
# fh = logging.FileHandler('标配版.log', encoding='utf-8')
#
# # 创建一个屏幕对象
# sh = logging.StreamHandler()
#
# # 配置显示格式
# formatter1 = logging.Formatter('%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s')
# formatter2 = logging.Formatter('%(asctime)s %(message)s')
# fh.setFormatter(formatter1)
# sh.setFormatter(formatter2)
#
# logger.addHandler(fh)
# logger.addHandler(sh)
#
# # 总开关
# logger.setLevel(10)
#
# fh.setLevel(10)
# sh.setLevel(40)
#
# logging.debug('调试模式')  # 10
# logging.info('正常模式')  # 20
# logging.warning('警告信息')  # 30
# logging.error('错误信息')  # 40
# logging.critical('严重错误信息')  # 50
```
## 旗舰版日志
```
"""
logging配置
"""


import logging.config

# 定义三种日志输出格式 开始

standard_format = '[%(asctime)s][%(threadName)s:%(thread)d][task_id:%(name)s][%(filename)s:%(lineno)d]' \
                  '[%(levelname)s][%(message)s]' #其中name为getlogger指定的名字

simple_format = '[%(levelname)s][%(asctime)s][%(filename)s:%(lineno)d]%(message)s'

id_simple_format = '[%(levelname)s][%(asctime)s] %(message)s'

# 定义日志输出格式 结束
logfile_name = 'login.log'  # log文件名
logfile_path_staff = r'D:\s23\day19\日志模块\旗舰版日志文件夹\staff.log'
logfile_path_boss = r'D:\s23\day19\日志模块\旗舰版日志文件夹\boss.log'

# log配置字典
# LOGGING_DIC第一层的所有的键不能改变

LOGGING_DIC = {
    'version': 1,  # 版本号
    'disable_existing_loggers': False,  #　固定写法
    'formatters': {
        'standard': {
            'format': standard_format
        },
        'simple': {
            'format': simple_format
        },
        'id_simple':{
            'format': id_simple_format
        }
    },
    'filters': {},
    'handlers': {
        #打印到终端的日志
        'sh': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',  # 打印到屏幕
            'formatter': 'id_simple'
        },
        #打印到文件的日志,收集info及以上的日志
        'fh': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',  # 保存到文件
            'formatter': 'standard',
            'filename': logfile_path_staff,  # 日志文件
            'maxBytes': 5000,  # 日志大小 5M
            'backupCount': 5,
            'encoding': 'utf-8',  # 日志文件的编码，再也不用担心中文log乱码了
        },
        'boss':
            {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',  # 保存到文件
                'formatter': 'id_simple',
                'filename': logfile_path_boss,  # 日志文件
                'maxBytes': 1024*1024*5,  # 日志大小 5M
                'backupCount': 5,
                'encoding': 'utf-8',  # 日志文件的编码，再也不用担心中文log乱码了
            },
    },
    'loggers': {
        #logging.getLogger(__name__)拿到的logger配置
        '': {
            'handlers': ['sh', 'fh', 'boss'],  # 这里把上面定义的两个handler都加上，即log数据既写入文件又打印到屏幕
            'level': 'DEBUG',
            'propagate': True,  # 向上（更高level的logger）传递
        },
    },
}


def md_logger():
    logging.config.dictConfig(LOGGING_DIC)  # 导入上面定义的logging配置
    logger = logging.getLogger()  # 生成一个log实例
    return logger
    # logger.debug('It works!')  # 记录该文件的运行状态

dic = {
    'username': '小黑'
}


def login():
    # print('登陆成功')
    md_logger().info(f"{dic['username']}登陆成功")

login()
```