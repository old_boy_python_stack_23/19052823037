# class Foo:
#     f = "类的静态变量"
#
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age
#
#     def say_hi(self):
#         print('hi,%s' % self.name)
#
#
# obj = Foo('egon', 73)
#
#
# # print(hasattr(obj, "name"))
# # print(hasattr(obj, 'say_hi'))
# class A:
#     class_name = 'python23期'
#     __dsb = '走了'
#
#     def __init__(self, name, age):
#         self.name = name
#         self.__age = age
#
#
# print(type(obj))


# from types import FunctionType
# from types import MethodType

# class A:
#     static_field = '静态属性'
#
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age
#
#     def func(self):
#         print('in A func')
#
#
# obj = A('MC骚Q', 18)
# print(hasattr(obj, 'name'))
# print(getattr(obj, 'name'))
# setattr(obj, 'hobby', '玩')
# print(hasattr(obj, 'hobby'))
# print(getattr(obj, 'hobby'))
# # delattr(obj,'name')
# print(hasattr(obj, 'name'))
# print(getattr(obj, 'name', None))
# if hasattr(obj, 'name'):
#     print(getattr(obj, 'name'))
# class B:
#     static = 'B类'
#
# def func1():
#     print('in func1')
#
#
# def func2():
#     print('in func2')
#
#
# def func3():
#     print('in func3')
#
#
# #
# # l1 = [func1, func2, func3]
# l1 = [f'func{i}' for i in range(1, 4)]
# import sys

# print(sys.modules)
# this_modules = sys.modules[__name__]
# print(this_modules)
# print(getattr(this_modules, 'func1'))
# getattr(this_modules, 'func1')()
# cls = getattr(this_modules,"B")
# obj = cls()
# print(obj.static)

# class Auth:
#     lis = [(1, 'login'), (2, "register"), (3, 'exit')]
#
#     def login(self):
#         print('登录函数')
#
#     def register(self):
#         print('注册函数')
#
#     def exit(self):
#         print('退出...')
#
#
# while 1:
#     func_name = input('请输入选择:').strip()
#     obj = Auth()
#     print(getattr(obj, 'lis')[int(func_name) - 1])
#     if getattr(obj, func_name, None):
#         getattr(obj, func_name)()
#     else:
#         print('请重新输入')
# if func_name == 'login':
#     obj.login()
#
# elif func_name == 'register':
#     obj.register()
#
# elif func_name == 'exit':
#     obj.exit()


# 一个对象之所以可以使用len（）函数，根本原因是这个对象中含有__len__的方法
# class S:
#     def __init__(self, name, age, sex):
#         self.name = name
#         self.age = age
#         self.sex = sex
#
#     def __repr__(self):
#         return 'xxx'
#
#     def __str__(self):
#         return ",mm"
#
#
# obj = S('XIAOZHI', 13212, 14546)
# OBJ1 = S(132123, 56413613, 1231321)
# print(str(obj))
# print("ciduixiangshi%r" % obj)
class A:
    def __init__(self):
        print('in __init__')
    def __new__(cls, *args, **kwargs):
        print('in __new__')
        object1 = object.__new__(cls)
        return object1

obj = A()
