# _*_coding:utf-8_*_
# 定制旗舰版日志
import os

# 引用logging.config 功能
import logging.config


# 定义两种种日志输出格式 开始
def md_logger():
    # 定制标准的日志输出格式
    standard_format = '[%(asctime)s][%(threadName)s:%(thread)d][task_id:%(name)s][%(filename)s:%(lineno)d]' \
                      '[%(levelname)s][%(message)s]'  # 其中name为getlogger指定的名字
    # 定制简单的(打印到屏幕)日志输出格式
    simple_format = '[%(levelname)s][%(asctime)s][%(filename)s:%(lineno)d]%(message)s'
    # 定义日志输出格式 结束

    # 获取日志写入文件路径
    logfile_path_staff = os.path.dirname(os.path.dirname(__file__))

    zuihou_path = os.path.join(logfile_path_staff, 'log', 'logging')  # 更改调用日志 只需要改这里的路径就行了 具体的配置 需要详细去配置了

    # log配置字典
    # LOGGING_DIC第一层的所有的键不能改变

    LOGGING_DIC = {
        'version': 1,  # 版本号
        'disable_existing_loggers': False,  # 固定写法
        'formatters': {
            'standard': {
                'format': standard_format  # 标准版
            },
            'simple': {
                'format': simple_format  # 简单版
            },
        },
        'filters': {},
        'handlers': {
            # 打印到终端的日志
            'sh': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',  # 打印到屏幕
                'formatter': 'simple'
            },
            # 打印到文件的日志,收集info及以上的日志
            'fh': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',  # 保存到文件
                'formatter': 'standard',
                'filename': zuihou_path,  # 日志文件
                'maxBytes': 1024 * 1024 * 5,  # 日志大小 5M
                'backupCount': 5,
                'encoding': 'utf-8',  # 日志文件的编码，再也不用担心中文log乱码了
            },
        },
        'loggers': {
            # logging.getLogger(__name__)拿到的logger配置
            '': {
                'handlers': ['sh', 'fh'],  # 这里把上面定义的两个handler都加上，即log数据既写入文件又打印到屏幕
                'level': 'DEBUG',
                'propagate': True,  # 向上（更高level的logger）传递
            },
        },
    }

    logging.config.dictConfig(LOGGING_DIC)  # 导入上面定义的logging配置
    logger = logging.getLogger()  # 生成一个log实例
    return logger
