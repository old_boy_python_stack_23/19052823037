# _*_coding:utf-8_*
import hashlib
import pickle
from lib import common
import os

dic_status = {"username": None, "status": False, "role": None}
# student = []
dic_student = {}  # 把上边这个裂变换成字典会不会好点?   obj.name : obj


# 定义一个学生类 封装 学生属性 方法
class Student:
    def __init__(self, name):
        self.name = name
        self.courses = []

    def show_courses(self):
        '''查看可选课程'''
        with open(f'{starts.BATH_DIR}\\db\\Course', mode='rb') as f:
            while 1:
                try:
                    course_obj = pickle.load(f)
                    print(course_obj.name, course_obj.price, course_obj.period)
                except Exception:
                    break

    def select_course(self):
        '''选择课程'''
        course = []  # Course类容器，从文件中读取后添加到这里方便后续的操作

        with open(f'{starts.BATH_DIR}\\db\\Course', mode='rb') as f:
            while 1:
                try:
                    course_obj = pickle.load(f)
                    course.append(course_obj)
                except Exception:
                    break
        for i in range(len(course)):
            print(i + 1, course[i].name)
        choice = input("请输入您要选择的课程序号").strip()
        self.courses.append(course[int(choice) - 1])
        for k, v in dic_student.items():
            with open(f'{starts.BATH_DIR}\\db\\Student_', mode='ab') as f1:
                pickle.dump(v, f1)
        os.remove(f'{starts.BATH_DIR}\\db\\Student')
        os.rename(f'{starts.BATH_DIR}\\db\\Student_', f'{starts.BATH_DIR}\\db\\Student')

    def show_selected_course(self):
        '''查看所选课程'''
        for m in self.courses:
            print(m.name)

    def exit(self):
        '''退出'''
        exit(0)


# 定义一个管理员类封装管理员属性 方法
class Manager:
    def __init__(self, name):
        self.name = name

    def create_course(self):
        '''创建课程'''
        name = input("请输入课程名称:").strip()
        price = input("请输入课程价格").strip()
        period = input("请输入课程周期").strip()
        sname = Course(name, price, period)
        with open(f'{starts.BATH_DIR}\\db\\Course', mode='ab') as fex:
            pickle.dump(sname, fex)
        common.md_logger().info(f"您创建了新的课程{name}")

    def create_student(self):
        '''创建学生'''
        student_name = input('请输入学生姓名').strip()
        student_password = input("请输入初始密码").strip()
        ret = hashlib.md5()
        ret.update(student_password.encode('utf-8'))

        with open(f'{starts.BATH_DIR}\\db\\UserInfo', mode='a', encoding="utf-8") as f:
            f.write(f"{student_name}|{ret.hexdigest()}|Student\n")
        student = Student(student_name)
        with open(f'{starts.BATH_DIR}\\db\\Student', mode='ab') as fex:
            pickle.dump(student, fex)
        common.md_logger().info(f"您创建了新的学生{student_name}")

    def show_courses(self):
        '''查看可选课程'''
        with open(f'{starts.BATH_DIR}\\db\\Course', mode='rb') as f:
            while 1:
                try:
                    course_obj = pickle.load(f)
                    print(course_obj.name, course_obj.price, course_obj.period)
                except Exception:
                    break

    def show_students(self):
        '''查看所有学生'''
        with open(f'{starts.BATH_DIR}\\db\\Student', mode='rb') as f:
            while 1:
                try:
                    student_obj = pickle.load(f)
                    print(student_obj.name)
                except Exception:
                    break

    def show_students_courses(self):
        '''查看所有学生选课情况'''
        with open(f'{starts.BATH_DIR}\\db\\Student', mode='rb') as f:
            while 1:
                try:
                    student_obj = pickle.load(f)
                    # print(student_obj)
                    print(student_obj.name, [i.name for i in student_obj.courses])

                except Exception:
                    break

    def exit(self):
        '''退出'''
        exit(0)


# 课程类
class Course:
    def __init__(self, name, price, period):
        self.name = name
        self.price = price
        self.period = period
        self.teacher = None


# 将所有用户的信息读取到内存 以字典的形式显现出来方便减少循环的判断
from bin import starts

user_dic = {}  # 用户名 : 密码
role_dic = {}  # 用户名 : 权限

with open(f'{starts.BATH_DIR}\\db\\UserInfo', mode='r', encoding="utf-8") as fex:
    for i in fex.readlines():
        user_dic[i.strip().split('|')[0]] = i.strip().split('|')[1]
        role_dic[i.strip().split('|')[0]] = i.strip().split('|')[2]


# 登陆函数
def login():
    count = 0
    while True:
        if count < 3:
            username_input = input("请输入用户名:").strip()
            user_password = input("请输入密码:").strip()
            ret = hashlib.md5()
            ret.update(user_password.encode('utf-8'))
            try:
                if user_dic[username_input] == ret.hexdigest():
                    dic_status['username'] = username_input
                    dic_status['status'] = True
                    dic_status['role'] = role_dic[username_input]
                    print("登陆成功")
                    return True
                else:
                    print("用户名或密码错误")
            except Exception:
                print("用户不存在")
            count += 1
        else:
            print('登陆超过三次,退出')
            return False


# 程序入口
def main():
    '''程序入口'''
    login()
    if dic_status["role"] == "Manager":
        obj = Manager(f"{dic_status['username']}")
        while 1:
            dic_chioce = {1: 'create_course', 2: 'create_student', 3: 'show_courses', 4: 'show_students',
                          5: 'show_students_courses', 6: 'exit'}
            print("1: 创建课程\n2: 创建学生\n3: 查看可选课程\n4: 查看所有学生\n5: 查看所有学生选课情况\n6: 退出\n")
            try:
                user_chioce = input("请输入选择的功能序号").strip()
                if hasattr(obj, dic_chioce[int(user_chioce)]):
                    getattr(obj, dic_chioce[int(user_chioce)])()
            except Exception:
                print("输入格式错误")
    elif dic_status['role'] == "Student":
        with open(f'{starts.BATH_DIR}\\db\\Student', mode='rb') as f:
            try:
                while 1:
                    obj0 = pickle.load(f)
                    dic_student[obj0.name] = obj0
            except Exception:
                pass
        obj = dic_student[dic_status["username"]]
        while 1:
            dic_chioce1 = {1: "show_courses", 2: "select_course", 3: "show_selected_course", 4: "exit"}
            print("1: 查看可选课程\n2: 选择课程\n3: 查看所选课程\n4: 退出")
            try:
                user_chioce = input("请输入选择的功能序号").strip()
                if hasattr(obj, dic_chioce1[int(user_chioce)]):
                    getattr(obj, dic_chioce1[int(user_chioce)])()
            except Exception:
                print("输入格式错误")