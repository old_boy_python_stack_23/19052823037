# python之 迭代器

## 关键字：global，nonlocal

```python
# count = 0
#
# def func():
#     count += 1
# func()
# UnboundLocalError: local variable 'count' referenced before assignment
# 解释器认为：如果你在局部作用域对一个变量进行修改了，
# 你在局部作用域已经定义好这个变量了。

```

**global**

1. 可以在局部作用域声明一个全局变量。

   ```python
   # 这是剪切
   # def func():
   #
   #     global name
   #     name = 1
   #
   #     print(globals())
   #     # print(locals())
   #     name += 1
   #     print(globals())
   #
   #
   # func()
   # # print(name)
   # print(globals())
   
   ```

2. 可以修改全局变量。

   ```python
   # count = 0
   #
   # def func():
   #     global count
   #     count += 1
   #
   # print(count)
   # func()
   # print(count)
   ```

**nonlocal**

1. 不能操作全局变量。
2. 可以对父级作用域的变量进行修改，并且在当前作用域创建（复制）一份此变量。

```python
# 这是复制
# def func():
#     count = 0
#     def inner():
#         nonlocal count
#         count += 1
#         print(count)
#         print(locals())
#     inner()
#     print(locals())
# func()
# UnboundLocalError: local variable 'count' referenced before assignment
# 解释器认为：如果你在局部作用域对一个变量进行修改了，
# 你在局部作用域已经定义好这个变量了。
```

## 默认参数的坑

```python
# def func(a,b=False):
#     print(a)
#     print(b)
# func(1,True)
# 当你的默认参数如果是可变的数据类型，你要小心了。

# def func(a,l=[]):
#     l.append(a)
#     return l
# print(func(1))  # [1,]
# print(func(2))  # [2,]
# print(func(3))  # [3,]

```

## 函数名的应用

+ 函数名的内存地址

```python
# a = 1
# b = 2
# c = a + b
# print(c)
# def func():
#     print(666)
#
# print(func)  # func = <function func at 0x00000000003F1EA0>
# 1,函数名指向的是函数的内存地址,加上()就执行这个函数。
# func()
```

+ 函数名可以作为一个变量赋值给其他变量

```python
# age1 = 12
# age2 = age1
# age3 = age2
# print(age3)
# def func():
#     print(666)
#
# f1 = func
# f2 = f1
# f2()
# f1()

```

+  函数名可以作为容器类类型的元素。

```python
# a = 1
# b = 2
# c = 3
# l = [a,b,c]
# print(l)

# def func1():
#     print('in func1')
#
# def func2():
#     print('in func2')
#
# def func3():
#     print('in func3')
#
# l = [func1, func2, func3]
# # print(l)
# for i in l:
#     i()

```

+ 函数名可以作为函数的实参。

```python
# a = 1
# def func(argv):
#     print(argv)
# func(a)

# def func1():
#     print('in func1')
#
# def func2(argv):
#     argv()
#     print('in func2')
#
# func2(func1)
```

+ 函数名可以作为函数的返回值

```python
# b = 666
#
# def func1():
#     print('in func1')
#
# def func2(argv):
#     print('in func2')
#     return argv
# ret = func2(b)
# print(ret)



# def func1():
#     print('in func1')
#
# def func2(argv):
#     # argv = func1 : function 43543
#     print('in func2')
#     return argv
# ret = func2(func1)  # func1 : function 43543
# ret()

# def wrapper():
#
#     def inner():
#         print(666)
#     return inner
# # ret = wrapper()
# # ret()
# wrapper()()
```



## python新特性：f-string格式化输出

```python
# 1,不区分大小写
# num = input('>>>')
# s = F'python{num}'
# print(s)

# 可以加入表达式

# s1 = 'alex'
# s2 = f'我的名字{s1.upper()}'
# print(s2)

# l1 = ['太白', 18, 176]
# s1 = f'我的名字{l1[0]}，我的年龄{l1[1]}，我的身高{l1[2]}'
# print(s1)

# 可以结合函数
# def func(a,b):
#     return a + b

# s1 = f'最终的结果{func(1,3)}'
# print(s1)

# 不能放一些特殊的字符 ! , : { } ;
# print(f"替换结果{{{73}}}")
# print(f'{12,}')
```

## 迭代器

1. **可迭代对象**

   字面意思分析：可以重复的迭代的实实在在的东西。	

   list,dict(keys(),values(),items()),tuple,str,set,range, 文件句柄（待定）

   专业角度: 内部含有`'__iter__'`方法的对象，就是可迭代对象。

   内置函数：dir()
   	print(dir(str))

   **判断一个对象是否是可迭代对象：**
   	print('__iter__' in dir(str))

   **优点：**

   1. 直观。	

   2. 操作方法较多。

   **缺点：**

   1. 占内存。

   2. 不能迭代取值（索引，字典的key）。

2. **迭代器**

   字面意思：可以重复迭代的工具。

   专业角度： 内部含有`'__iter__'`并且含有`"__next__"`方法的对象，就是迭代器

   可迭代对象转化成迭代器：

   ```python
   l1 = [1, 2, 3, 4, 5]
   # 内置函数iter()
   obj = iter(l1)
   ```

   ```python
   # 迭代器可以迭代取值。利用next()进行取值
   l1 = [1, 2, 3, 4, 5]
   # 内置函数iter()
   obj = iter(l1)
   # print(obj)
   print(next(obj))
   print(next(obj))
   print(next(obj))
   print(next(obj))
   print(next(obj))
   print(next(obj))
   
   ```

   **迭代器优点：**

   1. 非常节省内存。
   2. 惰性机制。

   **迭代器缺点：**

   1. 不直观。
   2. 操作不灵活。
   3. 效率相对低。

   **特性：**

   ```python
   l1 = [22, 33, 44, 55, 66, 77]
   obj = iter(l1)
   
   for i in range(3):
       print(next(obj))
   
   for i in range(2):
       print(next(obj))
   ```

   利用while循环，模拟for循环内部循环可迭代对象的机制。

   1. 先要将可迭代对象转化成迭代器。

   2. 利用next对迭代器进行取值。

   3. 利用异常处理try一下防止报错。

   

3. **可迭代对象与迭代器的对比**

    **可迭代对象：**

   ​    是一个私有的方法比较多，操作灵活（比如列表，字典的增删改查，字符串的常用操作方法等）,比较直观，但是占用内存，而且不能直接通过循环迭代取值的这么一个数据集。

   ​    **应用**：当你侧重于对于数据可以灵活处理，并且内存空间足够，将数据集设置为可迭代对象是明确的选择。

   ​    **迭代器：**

   ​    是一个非常节省内存，可以记录取值位置，可以直接通过循环+next方法取值，但是不直观，操作方法比较单一的数据集。

   ​    **应用**：当你的数据量过大，大到足以撑爆你的内存或者你以节省内存为首选因素时，将数据集设置为迭代器是一个不错的选择。（可参考为什么python把文件句柄设置成迭代器）。