# 1. 完成下列功能:
#    1. 创建一个人类Person,再类中创建3个静态变量(静态字段)
#           animal = '高级动物'
#           soul = '有灵魂'
#           language = '语言'
class Person:
    animal = '高级动物'
    soul = '有灵魂'
    language = '语言'

    #    2. 在类中定义三个方法,吃饭,睡觉,工作.
    def eat(self):
        print(f"{self.name}在吃饭")

    def sleep(self):
        pass

    def work(self):
        pass

    #    3. 在此类中的__init__方法中,给对象封装5个属性:国家,姓名,性别,年龄,  身高.
    def __init__(self, c, n, s, a, h):
        self.country = c
        self.name = n
        self.sex = s
        self.age = a
        self.high = h


#    4. 实例化四个人类对象:
#           第一个人类对象p1属性为:中国,alex,未知,42,175.
#           第二个人类对象p2属性为:美国,武大,男,35,160.
#           第三个人类对象p3属性为:你自己定义.
#           第四个人类对象p4属性为:p1的国籍,p2的名字,p3的性别,p2的年龄,p3  的身高.
p1 = Person("中国", 'alex', '未知', '42', 175)
print(p1.__dict__)
p2 = Person('美国', '武大', '男', '35', 160)
print(p2.__dict__)
p3 = Person('日本', '小黑', '未知', '40', 155)
print(p3.__dict__)
p4 = Person(p1.country, p2.name, p3.sex, p2.age, p3.high)
print(p4.__dict__)

#    5. 通过p1对象执行吃饭方法,方法里面打印:alex在吃饭.
p1.eat()
#    6. 通过p2对象执行吃饭方法,方法里面打印:武大在吃饭.
p2.eat()
#    7. 通过p3对象执行吃饭方法,方法里面打印:(p3对象自己的名字)在吃饭.
p3.eat()
#    8. 通过p1对象找到Person的静态变量 animal
print(p1.animal)
#    9. 通过p2对象找到Person的静态变量 soul
print(p2.soul)
#    10. 通过p3对象找到Person的静态变量 language
print(p3.language)


# 2. 通过自己创建类,实例化对象
#    在终端输出如下信息
#    小明，10岁，男，上山去砍柴
#    小明，10岁，男，开车去东北
#    小明，10岁，男，最爱大保健
#    老李，90岁，男，上山去砍柴
#    老李，90岁，男，开车去东北
#    老李，90岁，男，最爱大保健
#    老张…
class Human:
    def __init__(self, n, a, s='男', d=['上山去砍柴', '开车去东北', '最爱大保健']):
        self.name = n
        self.age = a
        self.sex = s
        self.do = d


h1 = Human('小明', '10岁')
for i in (f"{h1.__dict__['name']},{h1.__dict__['age']},{h1.__dict__['sex']},{i}" for i in h1.__dict__['do']):
    print(i)
h2 = Human('老李', '90岁')
for i in (f"{h2.__dict__['name']},{h2.__dict__['age']},{h2.__dict__['sex']},{i}" for i in h2.__dict__['do']):
    print(i)


# 3. 设计一个汽车类。
#
#    要求：
#
#    汽车的公共属性为：动力驱动，具有四个或以上车轮，主要用途载运人员或货物。
#    	汽车的功能：run,transfer.
#
#    具体的汽车的不同属性：颜色，车牌，类型（越野，轿车，货车等）。

class Car:
    car = '动力驱动，具有四个或以上车轮，主要用途载运人员或货物。'

    def __init__(self, c, l, t):
        self.color = c
        self.license = l
        self.type = t

    def run(self):
        pass

    def transfer(self):
        pass

# 4. 模拟英雄联盟写一个游戏人物的类（升级题）.
#    要求:
#
#    1. 创建一个 Game_role的类.
#    2. 构造方法中给对象封装name,ad(攻击力),hp(血量).三个属性.
#    3. 创建一个attack方法,此方法是实例化两个对象,互相攻击的功能:
#           例: 实例化一个对象 盖伦,ad为10, hp为100
#           实例化另个一个对象 剑豪 ad为20, hp为80
#           盖伦通过attack方法攻击剑豪,此方法要完成 '谁攻击谁,谁掉了多少血,  还剩多少血'的提示功能.
class Game_role:
    def __init__(self,n,a,h):
        self.name = n
        self.ad = a
        self.hp = h
    def attack(self,na):
        na.hp = na.hp - self.ad
        pass
gailun = Game_role('盖伦',10,100)
jianhao = Game_role('剑豪',20,80)
gailun.attack(jianhao)
print(jianhao.hp)
print(f"{gailun.name}攻击了{jianhao.name},{jianhao.name}掉了{gailun.ad}血")
