class A:
    company = 'oldboy'  # 静态变量(静态字段)
    __ipone = "4613"

    def __init__(self, name, age):  # 特殊方法
        self.name = name  # 对象属性(普通字段)
        self.__age = age  # 私有对象属性(私有普通字段段)

    def func1(self):  # 普通方法
        print("in func1")

    def __func(self):  # 私有方法
        print('私有方法func')

    @classmethod  # 类方法
    def class_func(cls):
        '''定义类方法,至少有一个cls参数'''
        print("类方法")

    @staticmethod  # 静态方法
    def static_func():
        '''定义静态方法,无默认参数'''

        print("静态方法")

    @property
    def prop(self):
        pass


# 类有两种成员
# 公有成员 在任何地方都能访问
# 私有成员 只有在类的内部才能访问
#
#
obj = A('naem', 'age')
A.static_func(obj)


# 类的约束


class B:
    school_name = 'oldboy'


class A(B):
    class_name = 'fiashgouiqgh'
    __naem = 'alex'

    def func(self):
        print("fiahdfn")

    def __func1(self):
        print("能不能访问")

    def func2(self):
        self.__func1()


obj = A()
print(obj.class_name)
print(obj.__name)
print(A.__naem)
obj.__func1()
obj.func2()


class Student:
    count = 0

    def __init__(self, name):
        self.namae = name
        self.fucn()

    @classmethod
    def fucn(cls):
        cls.count += 1


sq = Student("申强")
ly = Student("立业")


class A:
    def __init__(self, name, hight, weight):
        self.name = name
        self.hight = hight
        self.weight = weight

    def bmi(self):
        return self.weight / self.hight ** 2


obj = A('杨森第三次考核', 1.78, 105)
print(obj.bmi())


class A:
    def __init__(self, name, aaa):
        self.name = name
        self.aaa = aaa

    @property
    def aaa(self):
        print("get的时候需运行我")

    @aaa.setter
    def aaa(self, v):
        print('修改的时候执行我')

    @aaa.deleter
    def aaa(self):
        print("删除的时候执行我")


obj = A('ALEX')
# obj.aaa
# del obj.aaa
obj.aaa = "dsb"
# print(obj.aaa)
nim = input(">>>")
int(nim)


class S:
    pass


class A(S):
    pass


class B(A):
    pass


isinstance()
s1 = "oghfdg"
type(s1)  # 判断的是对象从属于那个类
# type到底是什么?
# type 元类 python 中一切皆对象 一个类也是一个对象
# 异常处理
dic = {
    1: 111,
    2: 666,
    3: 555
}
while 1:
    try:
        num = input(">>>")
        int(num)
        print(dic[int(num)])
    except KeyError as e:
        print('选项超出范围,请重新输入')
    except ValueError as e:
        print("请输入数字")
    finally:
        pass

assert 1 == 2
print(123)
print(1.001 ** 365)


class A:
    company_name = '老男孩教育'  # 静态变量
    __ipone = "ashgoiih"  # 私有静态变量      静态变量也叫静态字段

    def __init__(self, name, age):  # 特殊方法
        self.name = name  # 对象属性
        self.__age = age

    def func(self):  # 普通方法
        pass

    def __func(self):  # 私有方法
        pass
