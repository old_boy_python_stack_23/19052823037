# 内置模块

## 序列化模块 (非常非常重要)

序列化:将一个数据结构(list,dict....)转化成一个特殊的序列(特殊的字符串)的过程.

```python
# l1 = [1, 2, 3]
# ret = str(l1)
# print(ret,type(ret))
# print(eval(ret))  # 不让用

# 文件存取时,遇到的矛盾.
# dic ={'username': '太白', 'password': 123}

# dic = {1: {'username': '太白', 'password': 123,'status': False},
#        2:{'username': 'alex', 'password': 123,'status': False}
#        }

# 这个字典能放在全局么?
# with open('register.json',encoding='utf-8',mode='w') as f1:
#     f1.write(str(dic))
# with open('register',encoding='utf-8') as f1:
#     ret = f1.read()
#     print(ret,type(ret))
#
# # 数据结构 --- > str() 存储在文件, 读取出来时,反转不回去.
# # 网络传输.
# l1 = [i for i in range(100000)]
# # 凡是数据通过网络传出去最终的格式必须bytes
# s1 = str(l1)
# b1 = s1.encode('utf-8')
# print(b1)  # b1可以发送出去
#
# s2 = b1.decode('utf-8')
# print(s2,type(s2))
# s2 转化不成列表了.

# 我们现在要解决的问题: 如果有一种特殊的字符串,这个字符串可以与任何的数据结构互相转换.
```

序列化模块分类:

```python
# 序列化模块: 将一中数据结构转化成特殊的序列(特殊的字符串,bytes)并且还可以反转回去.

    # json模块: 是所有语言公认的一种序列.最最常用的
    #所以支持的python数据结构有限: int str bool dict list(tuple),None,float

    # None  ---> Null ----> NUll
    # dict  --->
    # pickle模块: 只能python语言中使用的,序列化模块:
    # 支持python所有的数据类型以及对象.
    # shevle模块(不讲): 课下了解(只能是文件存取).
```

json序列化:

两对四个方法:

dumps,loads 主要用于网络传输,可以用于文件的存取.

```python
import json
# dumps,loads 主要用于网络传输,可以用于文件的存取.
# dic = {'username': '太白', 'password': 123,'status': False}
# ret = json.dumps(dic)
# # print(ret,type(ret))

# ret_dict = json.loads(ret)
# print(ret_dict)

# 特殊的参数
# dic = {'username': '太白', 'password': 123,'status': False}
# # print(dic)
# ret = json.dumps(dic,ensure_ascii=False,sort_keys=True)
# print(ret,type(ret))
# import json
# dic = {'username': '太白', 'password': 123,'status': False}
# s_dict = json.dumps(dic)
# with open('jsonlx.json',encoding='utf-8',mode='w') as f1:
#     f1.write(s_dict)
#
# with open('jsonlx.json',encoding='utf-8') as f2:
#     content = f2.read()
#     print(json.loads(content))
```

多个数据如何存储到一个文件中?

```python
# 错误演示:
# import json
# dic1 = {'username': '太白', 'password': 123,'status': False}
# dic2 = {'username': 'alex', 'password': 123,'status': False}
# dic3 = {'username': 'ly', 'password': 123,'status': False}
# with open('jsonmore.json',encoding='utf-8',mode='a') as f1:
#     # f1.write(json.dumps(dic1))
#     # f1.write(json.dumps(dic2))
#     # f1.write(json.dumps(dic3))
#     f1.write(f'{json.dumps(dic1)}{json.dumps(dic2)}{json.dumps(dic3)}')

#
# with open('jsonmore.json',encoding='utf-8') as f1:
#     ret = json.loads(f1.read())
#     print(ret)

# 正确做法:

import json
# dic1 = {'username': '太白', 'password': 123,'status': False}
# dic2 = {'username': 'alex', 'password': 123,'status': False}
# dic3 = {'username': 'ly', 'password': 123,'status': False}
# with open('jsonmore.json',encoding='utf-8',mode='a') as f1:
#     f1.write(f'{json.dumps(dic1)}\n{json.dumps(dic2)}\n{json.dumps(dic3)}')

# with open('jsonmore.json',encoding='utf-8') as f1:
#     for line in f1:
#         ret = json.loads(line)
#         print(ret,type(ret))

```

pickle序列化:

dumps,loads  只能是网络传输

```python
import pickle

# dumps,loads  只能是网络传输
# l1 = ['wusir', '太白', '小黑', 666]
# ret = pickle.dumps(l1)
# # print(ret)
#
# l2 = pickle.loads(ret)
# print(l2,type(l2))
```

dump load 数据结构存取文件.

```python
import pickle
# l1 = ['wusir', '太白', '小黑', 666]
# with open('pickle练习.pickle',mode='wb') as f1:
#     pickle.dump(l1,f1)

# with open('pickle练习.pickle', mode='rb') as f1:
#     ret = pickle.load(f1)
#     print(ret,type(ret))

# 多个数据写入文件
l1 = ['wusir', '太白', '小黑1', 666]
l2 = ['wusir', '太白', '小黑2', 666]
l3 = ['wusir', '太白', '小黑3', 666]
# with open('pickle练习1.pickle',mode='wb') as f1:
# #     pickle.dump(l1,f1)
# #     pickle.dump(l2,f1)
# #     pickle.dump(l3,f1)
#
# with open('pickle练习1.pickle', mode='rb') as f1:
#     ret1 = pickle.load(f1)
#     ret2 = pickle.load(f1)
#     ret3 = pickle.load(f1)
#     print(ret1,ret2,ret3)
```



## os模块

```python
# 目录: 文件夹.
# 工作目录,当前目录,父级目录: day17

import os
# print(os.getcwd())  # D:\s23\day17 绝对路径  ***
# os.chdir(r'D:\s23\day9')
# print(os.getcwd())
# print(os.curdir)
# print(os.pardir)

# 和文件夹相关  ***
import os
# os.makedirs('dirname1/dirname2/dirname3/dirname4')  # 多级目录
# os.removedirs('dirname1/dirname2/dirname3/dirname4') # 截止到有文件的那层
# os.mkdir(r'd:\abc') # 单级目录
# os.rmdir('abc')
# print(os.listdir(r'D:\s23\day15'))

#
# os.remove()  删除一个文件  ***
# os.rename("oldname","newname")  重命名文件/目录  ***
# print(os.stat(r'D:\s23\day17\01 昨日内容回顾.py'))

# path 和路径相关  ***
# print(os.path.abspath('04 os模块.py'))  # D:\s23\day17\01 昨日内容回顾.py
# print(os.path.split(os.path.abspath('01 昨日内容回顾.py')))  # ('D:\\s23\\day17', '01 昨日内容回顾.py')
# print(os.path.dirname(r'D:\s23\day9\01 初始函数.py'))  # 获取父级目录
# print(os.path.dirname(os.path.abspath('01 昨日内容回顾.py')))
# print(__file__)  # 动态获取当前文件的绝对路径
# 获取当前文件的爷爷级的目录
# print(os.path.dirname(os.path.dirname(__file__)))
# print(os.path.basename(r'D:\s23\day9\01 初始函数.py'))  # 获取文件名
# print(os.path.exists(r'D:\s23\day9\02 初始函数.py'))
# 判断是否是绝对路径
# print(os.path.isabs(r'D:\s23\day9\01 初始函数.py'))
# print(os.path.isabs(r'day17/01 昨日内容回顾.py'))
# 判断该路径是否是一个文件路径
# print(os.path.isfile(r'D:\s23\day9\01 初始函数.py'))
# print(os.path.isfile(r'D:\s23\day9'))
# print(os.path.isdir(r'D:\s23\day17\dirname1\dirname2'))
# print(os.path.exists(r'D:\s23\day17\dirname1\dirname2'))
# 判断是否是一个目录(文件夹)
# print(os.path.isdir(r'D:\s23\day17\02 序列化模块.py'))
# D:\s23\day16\评论文章
# path = os.path.join('D:','s23','day20','随便')
# print(path)
# par_dir = os.path.dirname(__file__)
# print(par_dir)  # D:/s23/day17
# path = r'D:\s23\day17\db\lydata'
# path = par_dir + '\db' +'\lydata'
# path = os.path.join(par_dir,'db','lydata')
# with open(path,encoding='utf-8',mode='a') as f1:
#     f1.write('李业在红浪漫消费998元')

# print(os.path.getatime('D:\s23\day17\db\lydata'))
# print(os.path.getmtime('D:\s23\day17\db\lydata'))
# print(os.path.getsize('D:\s23\day17\db\lydata'))

# print(os.stat(r'D:\s23\day17\01 昨日内容回顾.py'))
```



## sys模块

```python
import sys
# sys.path  ***
# print(sys.version)  # 版本

# for i in range(3):
#     print(i)
# # exit()  # 强制退出
# # quit()
# for i in range(5):
#     print(i)

# print(sys.platform)

```



## hashlib模块

加密模块, 摘要算法,散列算法,等等.它是一堆加密算法的集合.

liye|zmdsb

太白|123

明文形式存储的,带来安全隐患.

hashlib如何加密?

1. 将一个bytes类型的数据   通过hashlib进行加密返回  一个等长度的16进制数字.
2. 过程不可逆.
3. 相同的bytes类型的数据通过相同的加密方法得到的数字绝对相同.
4.  不相同的bytes类型的数据通过相同的加密方法得到的数字绝对不相同.

撞库:  111111, 123456, 000000,19980123,

{'202cb962ac59075b964b07152d234b70': 123456}

hashlib 的用途:

1. 密码加密.
2. 文件一致性校验.

**密码加密:**

```python


# 密码加密
# md5
# ret = hashlib.md5()
# ret.update('123'.encode('utf-8'))
# s = ret.hexdigest()
# print(s,type(s))

# ret = hashlib.md5()
# ret.update('123'.encode('utf-8'))
# s = ret.hexdigest()
# print(s,type(s))
#
# ret = hashlib.md5()
# ret.update('223'.encode('utf-8'))
# s = ret.hexdigest()
# print(s,type(s))

# ret = hashlib.md5()
# ret.update('22fdslkafjdsklfdsjalfaklfjdslkfjdslkfjdsalf;dsajkfldsjf3'.encode('utf-8'))
# s = ret.hexdigest()
# print(s,type(s))

# 撞库

# ret = hashlib.md5()
# ret.update('123456*@qwe'.encode('utf-8'))
# s = ret.hexdigest()
# print(s,type(s))

# 加固定盐
# ret = hashlib.md5('xxx教育'.encode('utf-8'))
# ret.update('123456'.encode('utf-8'))
# s = ret.hexdigest()
# print(s,type(s))

# 加动态的盐
# username = input('输入用户名:').strip()
# password = input('输入密码').strip()
# ret = hashlib.md5(username[::2].encode('utf-8'))
# ret.update(password.encode('utf-8'))
# s = ret.hexdigest()
# print(s)

# sha系列: 安全系数高,耗时高.
# 加盐,加动态盐
# ret = hashlib.sha512()
# ret.update('123456fdklsajflsdfjsdlkafjafkl'.encode('utf-8'))
# s = ret.hexdigest()
# print(s,type(s))
#
```

**文件的一致性校验:**

```python
# low版
# import hashlib
# ret = hashlib.md5()
# with open('MD5文件校验',mode='rb') as f1:
#     content = f1.read()
#     ret.update(content)
# print(ret.hexdigest())
#
# ret = hashlib.md5()
# with open('MD5文件校验1',mode='rb') as f1:
#     content = f1.read()
#     ret.update(content)
# print(ret.hexdigest())

# ret = hashlib.md5()
# with open(r'D:\s23\day17\python-3.7.4rc1-embed-win32.zip',mode='rb') as f1:
#     content = f1.read()
#     ret.update(content)
# print(ret.hexdigest())
# d9c18c989c474c7629121c9d59cc429e
# d9c18c989c474c7629121c9d59cc429e


# 分步update
# s1 = '老男孩教育 最好的python 讲师 是 太白'
#
# # 1
# ret = hashlib.md5()
# ret.update(s1.encode('utf-8'))
# print(ret.hexdigest())
#
#
# # 2
# ret = hashlib.md5()
# ret.update('老男孩教育'.encode('utf-8'))
# ret.update(' 最好的python'.encode('utf-8'))
# ret.update(' 讲师 是'.encode('utf-8'))
# ret.update(' 太白'.encode('utf-8'))
# print(ret.hexdigest())  # 90c56d265a363292ec70c7074798c913



# 高达上版
import hashlib
def md5_file(path):
    ret = hashlib.md5()
    with open(path,mode='rb') as f1:
        while 1:
            content = f1.read(1024)
            if content:
                ret.update(content)
            else:
                return ret.hexdigest()


print(md5_file(r'D:\s23\day17\python-3.7.4rc1-embed-win32.zip'))
```

