# 装饰器

## 开放封闭原则

软件面世时,不可能吧所有的功能都设计好,当前的未来一两年功能给你上线,定期更新迭代.对于软件之前写的源代码一般都不会修改,对函数里面的代码以及函数的调用方式.

开放原则: 在源码不改变的情况下,增加一些额外的功能

封闭原则: 不要改变源代码

python中装饰器: 完美诠释的开放封闭原则

装饰器 就是个函数 : 他要装饰一个函数,在不改变原函数以及调用方式的前提下,给其增加一个额外的功能



## 装饰器初识

```python
# 1 李业，在一家xx科技有限公司工作，主管安排了一个任务，
# 写一个代码测试怼怼哥写的函数的执行效率。
# import time
# def index():
#     time.sleep(2)
#     print('欢迎访问博客园首页')


# print(time.time())
# start_time = time.time()
# index()
# end_time = time.time()
# print(f'此函数的执行效率{end_time-start_time}')

# 2. 主管让你测试小邓，李大象,重复代码太多。
#
# def func1():
#     time.sleep(2)
#     print('欢迎访问日记首页')
#
#
# def func2():
#     time.sleep(1)
#     print('欢迎访问评论首页')

# start_time = time.time()
# func1()
# end_time = time.time()
# print(f'此函数的执行效率{end_time-start_time}')
#
# start_time = time.time()
# func2()
# end_time = time.time()
# print(f'此函数的执行效率{end_time-start_time}')

# 3.  整合到函数中

# def func1():
#     time.sleep(2)
#     print('欢迎访问日记首页')
#
#
# def func2():
#     time.sleep(1)
#     print('欢迎访问评论首页')


# def test_time(x):
#     start_time = time.time()
#     x()
#     end_time = time.time()
#     print(f'此函数的执行效率{end_time-start_time}')

# test_time(func1)
# test_time(func2)


# 4. 怼怼哥这个函数在实际项目中被500执行，主管要求：在被执行此函数时，
# 同时要测试一下被执行函数的效率。


# def index():
#     time.sleep(2)
#     print('欢迎访问博客园首页')
#
# # index()
# def test_time(x):
#     start_time = time.time()
#     x()
#     end_time = time.time()
#     print(f'此函数的执行效率{end_time-start_time}')
#
# test_time(index)

# 版本4的问题： 开放原则满足了，封闭原则：不改变原函数的源码，以及调用方式。
# 违反了封闭原则：改变了函数的调用方式。


# 版本5： 不能改变原函数的调用方式（闭包）：


# def index():
#     time.sleep(2)
#     print('欢迎访问博客园首页')
#
# # index()
#
# # def func1():
# #     time.sleep(2)
# #     print('欢迎访问日记首页')
#
# def test_time(x):  # x = index
#     def inner():
#         start_time = time.time()
#         x()
#         end_time = time.time()
#         print(f'此函数的执行效率{end_time-start_time}')
#     return inner
#
# index = test_time(index)
# index()

# 语法糖 @加上装饰器函数的名


# def f():
#     print(666)
#
#
# f = '太白'
# print(f)


# def test_time(x):  # x = index
#     def inner():
#         start_time = time.time()
#         x()
#         end_time = time.time()
#         print(f'此函数的执行效率{end_time-start_time}')
#     return inner
#
#
# # @test_time  # index = test_time(index)
# def index():
#     time.sleep(2)
#     print('欢迎访问博客园首页')

# index = test_time(index)
# index()

# def func1():
#     time.sleep(2)
#     print('欢迎访问日记首页')

# @test_time
# def func2():
#     time.sleep(1)
#     print('欢迎访问评论首页')

# func2 = test_time(func2)
# func3 = test_time(func3)
# func2()
'''100行代码'''
# index()


'''10行代码'''
# index()


'''50行代码'''
# index()


# 版本6：被装饰函数有返回值

# def test_time(x):  # x = index
#     def inner():
#         start_time = time.time()
#         ret = x()
#         # print(F'ret: {ret}')
#         end_time = time.time()
#         print(f'此函数的执行效率{end_time-start_time}')
#         return ret
#     return inner
#
#
# @test_time  # index = test_time(index)
# def index():
#     time.sleep(0.5)
#     print('欢迎访问博客园首页')
#     return True
#
# print(index())  # inner()
# 你应该是让True返回给index()这样才完美了，但是现在index是inner，所以你要是完全不改变原函数的使用，
# 你print(index()) ---> True


# 版本7： 被装饰函数带参数，无论加不加装饰器，你的实参'太白金星'应该传给形参n,。
# 但版本6不能实现传参，index('太白金星') ==  inner('太白金星')
#
# def test_time(x):  # x = index
#     def inner(*args,**kwargs):
#         # 函数的定义：* ** 聚合。
#         # args = ('苹果')
#         #args = (1, 3)
#         start_time = time.time()
#         ret = x(*args,**kwargs)
#         # 函数的执行：* ** 打散。
#         # ret = x(*('苹果'))  ==x('苹果',)
#         # ret = x(*(1, 3))  ==x(1,3)
#         # print(F'ret: {ret}')
#         end_time = time.time()
#         print(f'此函数的执行效率{end_time-start_time}')
#         return ret
#     return inner
#
#
# # @test_time  # index = test_time(index)
# def index(n):
#     time.sleep(0.5)
#     print(f'欢迎{n}访问博客园首页')
#     return True
#
# # @test_time  # index = test_time(index)
# def func2(a,b):
#     time.sleep(0.5)
#     print(f'最终结果：{a+b}')
#     return a + b
#
#
# print(index('苹果'))  # inner('苹果')
# print(func2(1,3)) # == inner(1,3)


# def warpper(f):
#     def inner(*args,**kwargs):
#         '''被装饰函数之前的操作'''
#         # print(666)
#         ret = f(*args,**kwargs)
#         '''被装饰函数之后的操作'''
#         # print('执行完毕了')
#         return ret
#     return inner
#
# @warpper
# def func():
#     print(111)
# #
# func()
# func()
# func()
# func()
# func()


# 装饰器的应用：在不改变原函数的源码以及调用方式前提下，为其增加额外的功能。
# 登陆认证，打印日志等。
```



## 模拟博客园登陆

```python
flag = False

def auth(f):
    def inner(*args,**kwargs):
        if flag:
            '''被装饰函数之前的操作'''
            # 登陆认证：
            # 输入用户名，输入密码

            ret = f(*args,**kwargs)
            '''被装饰函数之后的操作'''
            return ret
        else:
            pass
    return inner

@auth
def article():
    print('欢迎访问文章页面')


@auth
def diary():
    print('欢迎访问日记页面')


@auth
def comment():
    print('欢迎访问评论页面')


article()
diary()
comment()
```

