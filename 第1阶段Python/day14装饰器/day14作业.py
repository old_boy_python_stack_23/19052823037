# 1.整理今天的笔记以及课上代码，完善昨天没有写完的作业，先把上午留的装饰器认证登录的完成。
def login():
    count = 0
    while True:
        if count < 3:
            username_input = input("请输入用户名:")
            user_password = input("请输入密码:")
            with open("register", mode="r", encoding="utf-8") as f:
                line1 = f.readlines()
            l = [i.strip().split('|') for i in line1]  # [['alex', 'taibai250'], ['太白', 'alex520']]
            for i in l:
                if username_input == i[0]and user_password == i[1]:
                    print("登陆成功")
                    return True
            else:
                print("用户名或密码错误")
        else:
            print("超过三次退出")
            return False
        count += 1


flag = False

def auth(f):
    def inner(*args, **kwargs):
        global flag
        if flag:
            ret = f(*args, **kwargs)
            return ret
        else:
            flag = login()
            if flag:
                ret = f(*args, **kwargs)
                return ret
    return inner


@auth
def article():
    print('欢迎访问文章页面')


@auth
def diary():
    print('欢迎访问日记页面')


@auth
def comment():
    print('欢迎访问评论页面')


article()
diary()
comment()
# 2.将课上模拟博客园登录的装饰器的认证的代码完善好，整清楚。


# 3.看代码写结果：
#
#
# def wrapper(f):
#     def inner(*args, **kwargs):
#         print(111)
#         ret = f(*args, **kwargs)
#         print(222)
#         return ret
#
#     return inner
#
#
# def func():
#     print(333)
#
#
# print(444)
# func()
# print(555)
# 444
# 333
# 555
# 4.
# 编写装饰器, 在每次执行被装饰函数之前打印一句’每次执行被装饰函数之前都得先经过这里, 这里根据需求添加代码’。
# def wapper(f):
#     def inner(*args, **kwargs):
#         print("每次执行被装饰函数之前都得先经过这里, 这里根据需求添加代码")
#         ret = f(*args, **kwargs)
#         return ret
#
#     return inner
# @wapper
# def func():
#     pass
# func()

# 5.
# 为函数写一个装饰器，把函数的返回值 + 100
# 然后再返回。
# def wrapper(f):
#     def inner():
#         ret = f() +100
#         return ret
#     return inner
#
#
# @wrapper
# def func():
#     return 7
#
#
# result = func()
# print(result)

# # 6.请实现一个装饰器，通过一次调用是函数重复执行5次。
# def wrapper(f):
#     def inner():
#         for i in range(5):
#             ret = f()
#         return ret
#     return inner
# @wrapper
# def func():
#     print("niaho")
# func()
# 7.请实现一个装饰器，每次调用函数时，将函数名以及调用此函数的时间节点写入文件中。

# 可用代码：
import time

struct_time = time.localtime()
print(time.strftime("%Y-%m-%d %H:%M:%S", struct_time))  # 当前时间节点
#
# def wrapper():
#     pass
#
# def func1(f):
#     print(f.__name__)
#
# func1(wrapper)
# 函数名通过： 函数名.__name__获取。
import time


def moon(f):
    def inner():
        struct_time = time.localtime()
        ret = f()
        func_name = f.__name__
        s = (time.strftime("%Y-%m-%d %H:%M:%S", struct_time))
        with open("register", mode="a", encoding="utf-8") as w:
            w.write(f"\n{s}")
            w.write(func_name)
        return ret

    return inner


@moon
def test1():
    pass


test1()
