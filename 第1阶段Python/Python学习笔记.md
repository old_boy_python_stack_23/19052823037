# 一、计算机基础

## 计算机基础知识

**一、为何要学习计算机基础？**

​           好多人觉得自己有点基础就都想着直接敲代码，觉得基础知识很容易，很简单，就不怎么用心去学。然而，我觉得基础知识很重要。就像盖一栋楼房一样，你先要打好地基，再去盖房。  　　

　　　Python是一门编程语言，即通俗一点说就是语言。

　　　我们都知道世界上有很多种语言，比如：汉语，英语，阿拉伯语等等众多的语言。要想用这些语言去和人沟通，如果你想和英国人说话，你必须得会英语吧。而我们的计算机也有它自己的语言，你要想让计算机帮助我们做事情，你就需要和它沟通吧。那你就得懂得计算机语言吧，也就是编程。当然计算机也有很多语言，比如：C，Java，PHP，Python，C#等。所以我们就先从学习基础知识开始。也有人说学习基础理论知识很枯燥，但是，那还是得学。你要把它当成自己的兴趣，一点一点的去投入它，相信你会是很棒的。

　　  程序用编程语言来写程序，最终开发的结果就是一个软件。就像大家都知道的QQ，腾讯视频，酷狗音乐等一系列软件。这些软件要想运行必须得有系统控制它吧。当然，有人会问：为什么要用操作系统呢？当然，很久以前的那些程序员确实是在没有操作环境下，编程语言是操作硬件来编写的。你可能觉得没问题，但是其实问题很严重。如果一直像以前那样会严重影响效率的。操作系统是出现在硬件之上的，是用来控制硬件的。所以，我们开发时只需要调用操作系统为我们提供的简单的接口就可以了。

![img](https://images2015.cnblogs.com/blog/1184802/201707/1184802-20170711155432368-610830318.png)

  

　　　如上图所示，我把计算机的系统分为了上面三大块。硬件，操作系统，应用程序。要想学习软件知识，我想那些硬件的知识或多或少还是得了解点的，现在我们就来谈谈硬件一类的知识。

**二、计算机硬件介绍**

​    	1. 硬件的目的：为了运行软件给它的一些指令。我们可以优先从硬件中提取出这三个主要的东西，分别是： CPU，内存，硬盘

​        2. 在现实生活中，人脑是用来计算的，在计算机中，用来计算的是什么呢？当然是CPU了。多数CPU都有两种模式，即内核态与用户态。这里的即内核态与用户态将会在下面的内容中讲到。

​       3.如果我们把计算机理解为人的大脑，我们可以总结为几句话：

​           　　　　**CPU是人的大脑，负责运算**

　　  　　　　**内存是人的记忆，负责临时存储**

　　  　　　　**硬盘是人的笔记本，负责永久存储**

　　　　　　  **输入设备是人的耳朵或眼睛，负责接受外部的信息传给CPU**

　　　　　　  **以上所有的设备都通过总线连接，总线相当于人的神经**

**![img](https://images2015.cnblogs.com/blog/1184802/201707/1184802-20170711223701806-856435351.png)**

​                                                                                            总线示意图

**三、处理器（寄存器及内核态与用户态切换）**

　　 1.计算机的大脑是CPU，它从内存中取指令-▶解码-▶执行，然后在取指令，解码，执行，周而复始，直至整个程序被执行完成。

　　　2. 寄存器是一个存储设备，最快的一种存储设备就是寄存器。

​      3.寄存器的分类

　　　　　　①通用寄存器：用来保存变量和临时结果的。

　　　　　　②程序计数器：它保存了将要取出的下一条指令的内存地址。在指令取出后，程序计算器就被更新以便执行后期的指令

　　　　　　③堆栈指针：它指向内存中当前栈的顶端。该栈包含已经进入但是还没有退出的每个过程中的一个框架。在一个过程的堆栈框架中保存了有关的输入参数、局部变量以及那些没有保存在寄存器中的临时变量

　　　　　　④程序状态字寄存器（Program Status Word,简称PSW）：这个寄存器包含了条码位(由比较指令设置)、CPU优先级、模式（用户态或内核态），以及各种其他控制位。用户通常读入整个PSW，但是只对其中少量的字段写入。在系统调用和I/O中，PSW非常重要。

　　  4.内核态与用户态

​       多数CPU都有两种模式，即内核态与用户态。  　　　　　　

　　　　①当cpu处于内核状态时，运行的是操作系统，能控制硬件（可以获取所有cpu的指令集）　　　　　

　　　　②当cpu处于用户太状态时，运行的是用户软件，不能控制硬件（可以获取所有cpu的指令集中的一个子集，该子集不包括操作硬件的指令集）

　　　这里有些人可能会含糊什么是内核态，什么是用户态？下面我来解释一下：

　　　　内核态:当cpu在内核态运行时，cpu可以执行指令集中所有的指令，很明显，所有的指令中包含了使用硬件的所有功能，（操作系统在内核态下运行，从而可以访问整个硬件）所以，归根结底通俗一点的话也就是上面①解释的那样

　　　　用户态:用户程序在用户态下运行，仅仅只能执行cpu整个指令集的一个子集，该子集中不包含操作硬件功能的部分，因此，一般情况下，在用户态中有关I/O和内存保护（操作系统占用的内存是受保护的，不能被别的程序占用），当然，在用户态下，将PSW中的模式设置成内核态也是禁止的。

　　5.内核态与用户态切换

​      用户态下工作的软件是不能之间操作硬件的，但是像我们的一些软件，比如暴风音影啊一类的软件，我们要想从磁盘中读取一个电影文件，那就得从用户态切换成内核态，为此，用户程序必须使用系统调用（system call），系统调用陷入内核并调用操作系统，TRAP指令把用户态切换成内核态，并启用操作系统从而获得服务。

**四、存储器系列，L1缓存，L2缓存，内存（RAM），EEPROM和闪存，CMOS与BIOS电池**

​       1.  计算机中第二重要的就是存储了，所有人都意淫着存储：速度快（这样cpu的等待存储器的延迟就降低了）+容量大+价钱便宜。然后同时兼备三者是不可能的，所以有了如下的不同的处理方式

![img](https://images2015.cnblogs.com/blog/1184802/201707/1184802-20170711234003400-1240723557.png)

由上图可以很清楚的看见寄存器存储是速度非常快的，但是它的容量却很少。下来就是高速缓存了。我就不一一介绍了，我相信大家应该可以看得懂这个图。

　2.寄存器即L1缓存：用与cpu相同材质制造，与cpu一样快，因而cpu访问它无时延，典型容量是：在32位cpu中为32*32，在64位cpu中为64*64，在两种情况下容量均<1KB。

   3.高速缓存即L2缓存：主要由硬件控制高速缓存的存取，内存中有高速缓存行按照0~64字节为行0，64~127为行1。。。最常用的高速缓存行放置在cpu内部或者非常接近cpu的高速缓存中。当某个程序需要读一个存储字时，高速缓存硬件检查所需要的高速缓存行是否在高速缓存中。

   4.内存：主存通常称为随机访问存储RAM，就是我们通常所说的内存，容量一直在不断攀升，所有不能再高速缓存中找到的，都会到主存中找，**主存是易失性存储，断电后数据全部消失**

   5.EEPROM（Electrically Erasable PROM，电可擦除可编程ROM）和闪存（flash memory）也是非易失性的。还有一类存储器就是**CMOS，它是易失性的，**许多计算机利用CMOS存储器**来保持当前时间和日期**。CMOS存储器和递增时间的电路由一小块**电池驱动**，所以，即使计算机没有加电，时间也仍然可以正确地更新，除此之外**CMOS还可以保存配置的参数，比如，哪一个是启动磁盘等**，之所以采用CMOS是因为它耗电非常少，一块工厂原装电池往往能使用若干年，但是当电池失效时，相关的配置和时间等都将丢失。

**五、磁盘**

1.磁盘由磁头，磁道，扇区组成的。

2.磁道：每个磁头可以读取一段换新区域。把一个戈丁手臂位置上所以的磁道合起来，组成一个柱面

3.每个磁道划成若干扇区，扇区典型的值是512字节。

4.数据都存放于一段一段的扇区，即磁道这个圆圈的一小段圆圈，从磁盘读取一段数据需要经历寻道时间和延迟时间，那么什么是寻道时间和延迟时间呢？

​    **平均寻道时间：**机械手臂从一个柱面随机移动到相邻的柱面的时间成为寻到时间，找到了磁道就以为着招到了数据所在的那个圈圈，但是还不知道数据具体这个圆圈的具体位置

​    **平均延迟时间：**机械臂到达正确的磁道之后还必须等待旋转到数据所在的扇区下，这段时间成为延迟时间

 

![img](https://images2015.cnblogs.com/blog/1184802/201707/1184802-20170712081400493-13407149.png)

 

**六、磁带**

1.有些人会想磁带是用来干什么的呢？当然，它也是内存之一，是用来存储东西的，它的存储量是相当大的，而且价钱也便宜。当遇上火灾等紧急情况时，可以用磁带来存储我们的重要文件。常常用来做备份（常见于大型数据库系统中）。但是，它也有缺点，就是运行速度特别慢，效率低。

2.cpu和存储器并不是操作系统唯一需要管理的资源，I/O设备也是非常重要的一环。**I/O设备一般包括两个部分：设备控制器和设备本身**。

**控制器的功能**：通常情况下对设备的控制是非常复杂和具体的，控制器的任务就是为操作系统屏蔽这些复杂而具体的工作，提供给操作系统一个简单而清晰的接口

**设备本身**：有相对简单的接口且标准的，这样大家都可以为其编写驱动程序了。要想调用设备，必须根据该接口编写复杂而具体的程序，于是有了控制器提供设备驱动接口给操作系统。必须把设备驱动程序安装到操作系统中。

**七、总线**

　　　　北桥即PCI桥：连接高速设备

　　　  南桥即ISA桥：连接慢速设备

![img](https://images2015.cnblogs.com/blog/1036857/201701/1036857-20170118183358656-1969770652.png)

 

**八、操作系统的启动流程**

　　1.计算机加电

　　2.BIOS开始运行，检测硬件：cpu、内存、硬盘等

　　3.BIOS读取CMOS存储器中的参数，选择启动设备

　　4.从启动设备上读取第一个扇区的内容（MBR主引导记录512字节，前446为引导信息，后64为分区信息，最后两个为标志位）

　　5.根据分区信息读入bootloader启动装载模块，启动操作系统

　　6.然后操作系统询问BIOS，以获得配置信息。对于每种设备，系统会检查其设备驱动程序是否存在，如果没有，系统则会要求用户按照设备驱动程序。一旦有了全部的设备驱动程序，操作系统就将它们调入内核。然后初始有关的表格（如进程表），穿件需要的进程，并在每个终端上启动登录程序或GUI

**九、应用程序的启动流程**

　　1.双击快捷方式

　　2.告诉操作系统一个文件路径

　　3.操作系统从硬盘读取文件到内存中

　　4.cpu从内存中读取数据执行

## 计算机的发展历史及多道技术

**一、操作系统发展史及多道技术**

　　1.第一代计算机（1940~1955）：真空管和穿孔卡片

　　　　特点：没有操作系统的概念，所有的程序设计都是直接操控硬件。

　　　　优点：每个人独享，可以自己调试代码，找到bug。

　　　　缺点：浪费计算机资源。

　　2.第二代就算机（1955~1965）：晶体管和批处理系统

　　　　特点：把代码都赞到一块，让一个CPU共享，但是还是一个一个的去运行，还是顺序算法（串行）
　　　　优点：批处理，节省了机时。
　　　　缺点：有人的参与了，搬过来搬过去的麻烦，拖慢程序运行的速度

　　3.第三代计算机（1965~1980）：集成电路芯片和多道程序设计

　　　　第三代计算机的操作系统广泛应用了第二代计算机的操作系统没有的关键技术：多道技术

　　　　多道技术:（指的是多道/个程序）
 　　　　　　1.空间上的复用：内存要支持同时跑进多个程序
 　　　　　　2.时间上的复用：多个程序要让它们能切换（什么时候要切？一个程序占用的时间过长要切；当CPU遇到IO阻塞时，等待的时间要切）

　　4.第四代计算机（1980~至今）：个人计算机

备注： 在这里需要补充下 用网易云上 嵩天老师的 计算机发展到08年进入到新时代了 以上是按照硬件来划分计算机的

**二、为什么要使用操作系统呢？**

程序员无法把所有的硬件操作细节都了解到，管理这些硬件并且加以优化使用是非常繁琐的工作，这个繁琐的工作就是操作系统来干的，有了他，程序员就从这些繁琐的工作中解脱了出来，只需要考虑自己的应用软件的编写就可以了，应用软件直接使用操作系统提供的功能来间接使用硬件。

**三、操作系统的两大作用**

　　1.为应用程序提供如何使用硬件资源的抽象

　　2.把多个程序对硬件的竞争变得有序化（管理应用程序）

**四、计算机语言的分类**

机器语言：
 　　　　特点：用计算机能看懂的0和1去写程序
 　　　　优点：程序运行速度快
 　　　　缺点：开发效率低
汇编语言：
 　　　　特点：用一些英文标签代替一串二进制数字去写程序
 　　　　优点：比机器语言好一点，操作系统内使用大量汇编语言（操作系统不需要网络，则速度越来越好）
     　　   比如：关于进程的调用代码，就是用汇编语言写的
 　　　　缺点：开发效率低
高级语言：
 　　　　特点：用人能读懂的（英文）字符去写程序
 　　　　优点：开发效率高
 　　　　缺点：运行速度慢，必须经过翻译才能让计算机识别，导致运行速度慢
　　以上得出结论：开发效率从低到高，运行速度从低到高，学习难度由难到易。

## 计算机网络知识简单介绍

一、网络基础

1.网络指的是什么？

　　计算机与计算机之间通过物理链接介质（网络设备）连接到一起。

　　计算机与计算机之间基于网络协议通信（网络协议就相当于计算机界的英语）

2.osi七层协议：

互联网协议按照功能不同分为osi七层或tcp/ip五层或tcp/ip四层

 ![img](https://images2015.cnblogs.com/blog/1184802/201707/1184802-20170713144722697-1296783623.png)

每层运行常见物理设备：

![img](https://images2015.cnblogs.com/blog/1184802/201707/1184802-20170717142255425-1238644025.png)

 

3.五层模型讲解

**物理层：**由来：计算机和计算机之间要想通信，就必须接入internet，言外之意就是计算机之间必须完成组网。

　　　    　功能：主要基于电器特性发送高低电压，高电压对应数字1，低电压对应数字0（提供电信号）

**数据链路层：**

　　　　   1.由来：单纯的电信号0和1没有任何意义，必须规定电信号多少位

　　　　   2.功能：定义了电信号的分组方式

　　　　   3.以太网协议ethernet:早期的时候各个公司都有自己的分组方式，后来形成了统一的标准，即以太网协议ethernet

　　　　   4.以太网协议ethernet规定：

　　　　　　1.一组电信号构成一个数据包，叫做“帧”

　　　　　　2.每一数据帧分成：报头head和数据data两部分

　　　　　　　　　　head包含：发送者/源地址，6个字节

　　　　　　　　　　                   接受者/目标地址，6个字节

　　　　　　　　　　                   数据类型：6个字节

　　　　　　　　　　data包含：数据包的具体内容（最短64字节，最长1500字节）

　　　　5.mac地址：ethernet规定接入internet的设备都必须具备网卡，发送端和接收端的地址便是指网卡的地址，即mac地址。

　　　　6.广播：有了mac地址，同一网络内的两台机器就可以通信了（一台主机通过arpmac协议获取另外一台主机的mac地址）

**网络层：**

　　　1.由来：有了ethernet，mac地址，广播的发送方式，计算机与计算机之间就可以通信了，问题是世界范围的互联网由一个个彼此隔离的小的局域网组成的，那么发送一条消息全世界都能收到，这样会导致效率很低了。所以，必须找到一种方法来区分计算机是在局域网还是不在局域网里。如果在同一个局域网里，就采用广播的方式发送，如果不是，就采用路由的方式。

　　　2.功能：引入一套新的地址用来区分不同的广播域/子网，这套地址即网络地址。

　　   3. IP地址：规定网络地址的协议叫ip地址，广泛采用V4版本即ipv4，它规定网络地址由32位二进制表示

​               范围：0.0.0.0-255.255.255.255

　　　　 一个ip地址通常写成四段十进制数，例：172.16.10.1

　　   4.子网掩码：所谓”子网掩码”，就是表示子网络特征的一个参数。它在形式上等同于IP地址，也是一个32位二进制数字，它的网络部分全部为1，主机部分全部为0。比如，IP地址172.16.10.1，如果已知网络部分是前24位，主机部分是后8位，那么子网络掩码就是11111111.11111111.11111111.00000000，写成十进制就是255.255.255.0。

　　　　知道”子网掩码”，我们就能判断，任意两个IP地址是否处在同一个子网络。方法是将两个IP地址与子网掩码分别进行AND运算（两个数位都为1，运算结果为1，否则为0），然后比较结果是否相同，如果是的话，就表明它们在同一个子网络中，否则就不是。

　　   5.arp协议：就是讲IP地址解析成mac地址。

**传输层：**

　　 **1.** 由来：我们通过ip地址和mac地址找到了一台特定的主机，如何标识这台主机上的应用程序，答案就是端口。端口即应用程序与网卡关联的编号。

　　   2. 功能：建立端口到端口的通信。

　　　3.TCP：通过双向链接，客户端向服务端发送消息后，等待服务端回复消息后才算发送成功。

　　　　　　缺点：速度慢

　　　　　　优点：可靠（可靠在对方要回应一个包确保发送成功）

　　　4.UDP： 没有链接，直接发送。

 

　　　　　　 缺点：不可靠

　　　　　　 优点：速度快

　　　 5.三次握手（连接）和四次挥手（断开）：

　　　　　　三次握手的目的：建立双向通信链路。SYN代表客户端向服务端发送的一个请求，ACK代表服务端向客户端发送的回应。

　　　　　　三次握手就像谈恋爱确定关系一样，四次挥手就像分手一样。此处只是打比方而已。

 

![img](https://images2015.cnblogs.com/blog/1184802/201707/1184802-20170717145741222-809718773.png)

**应用层：**

　　　由来：用户使用的都是应用程序，均工作于应用层，互联网是开发的，大家都可以开发自己的应用程序，数据多种多样，必须规定好数据的组织形式 

   　　功能：规定应用程序的数据格式。

**URI（uri）地址:例如**

　　　　**http://www.cnblogs.com/haiyan123**

以上网址有三部分构成：

　　　协议部分：http://    应用层的协议

　　    域名：www.cnblogs.com

　　　资源：haiyan123

# 二、Python基础	

## 1、windows环境下python安装

1. Python3 环境说明

Python3 可应用于多平台包括 Windows、Linux 和 Mac OS X。

- Unix (Solaris, Linux, FreeBSD, AIX, HP/UX, SunOS, IRIX, 等等。)
- Python 同样可以移植到 Java 和 .NET 虚拟机上。

2. Python3 下载

Python3 最新源码，二进制文档，新闻资讯等可以在 Python 的官网查看到：

Python 官网：<https://www.python.org/>

你可以在以下链接中下载 Python 的文档，你可以下载 HTML、PDF 和 PostScript 等格式的文档。

Python文档下载地址：<https://www.python.org/doc/>

3. Python 安装

Python 已经被移植在许多平台上（经过改动使它能够工作在不同平台上）。

您需要下载适用于您使用平台的二进制代码，然后安装 Python。

如果您平台的二进制代码是不可用的，你需要使用C编译器手动编译源代码。

编译的源代码，功能上有更多的选择性， 为 Python 安装提供了更多的灵活性。

以下是各个平台安装包的下载地址：

![img](https://www.runoob.com/wp-content/uploads/2018/07/F2135662-1078-4EE2-BEBB-353F8D8E521F.jpg)

**Source Code** 可用于 Linux 上的安装。

以下为不同平台上安装 Python3 的方法。

4. Unix & Linux 平台安装 Python3:

以下为在 Unix & Linux 平台上安装 Python 的简单步骤：

- 打开WEB浏览器访问 <https://www.python.org/downloads/source/>
- 选择适用于 Unix/Linux 的源码压缩包。
- 下载及解压压缩包 **Python-3.x.x.tgz**，**3.x.x** 为你下载的对应版本号。
- 如果你需要自定义一些选项修改 *Modules/Setup*

5. Window 平台安装 Python:

以下为在 Window 平台上安装 Python 的简单步骤。

打开 WEB 浏览器访问 <https://www.python.org/downloads/windows/> ，一般就下载 executable installer，x86 表示是 32 位机子的，x86-64 表示 64 位机子的。

![img](https://www.runoob.com/wp-content/uploads/2018/07/A0ADAB69-1DA6-409B-AF85-DA2FC7E0B57F.png)

记得勾选 **Add Python 3.6 to PATH**。

![img](https://www.runoob.com/wp-content/uploads/2018/07/20180226150011548.png)

按 **Win+R** 键，输入 cmd 调出命令提示符，输入 python:

![img](https://www.runoob.com/wp-content/uploads/2018/07/20170707155742110.png)

6. MAC 平台安装 Python:

MAC 系统都自带有 Python2.7 环境，你可以在链接 <https://www.python.org/downloads/mac-osx/> 上下载最新版安装 Python 3.x。

你也可以参考源码安装的方式来安装。

7. 环境变量配置

程序和可执行文件可以在许多目录，而这些路径很可能不在操作系统提供可执行文件的搜索路径中。

path(路径)存储在环境变量中，这是由操作系统维护的一个命名的字符串。这些变量包含可用的命令行解释器和其他程序的信息。

Unix或Windows中路径变量为PATH（UNIX区分大小写，Windows不区分大小写）。

在Mac OS中，安装程序过程中改变了python的安装路径。如果你需要在其他目录引用Python，你必须在path中添加Python目录。

1. 在 Unix/Linux 设置环境变量

- 在 csh shell输入：

  ```
  setenv PATH "$PATH:/usr/local/bin/python"
  ```

  按下Enter

- 在 bash shell (Linux) 输入 :

  ```
  export PATH="$PATH:/usr/local/bin/python" 
  ```

  按下Enter

- 在 sh 或者 ksh shell 输入: 

  ```
  PATH="$PATH:/usr/local/bin/python" 
  ```

  按下 Enter。

**注意:** /usr/local/bin/python 是 Python 的安装目录。

2. 在 Windows 设置环境变量

在环境变量中添加Python目录：

**在命令提示框中(cmd) :** 输入 

```
path=%path%;C:\Python 
```

按下"Enter"。

**注意:** C:\Python 是Python的安装目录。

也可以通过以下方式设置：

- 右键点击"计算机"，然后点击"属性"
- 然后点击"高级系统设置"
- 选择"系统变量"窗口下面的"Path",双击即可！
- 然后在"Path"行，添加python安装路径即可(我的D:\Python32)，所以在后面，添加该路径即可。 **ps：记住，路径直接用分号"；"隔开！**
- 最后设置成功以后，在cmd命令行，输入命令"python"，就可以有相关显示。

![img](https://www.runoob.com/wp-content/uploads/2013/11/201209201707594792.png)

3. Python 环境变量

下面几个重要的环境变量，它应用于Python：

| 变量名        | 描述                                                         |
| :------------ | :----------------------------------------------------------- |
| PYTHONPATH    | PYTHONPATH是Python搜索路径，默认我们import的模块都会从PYTHONPATH里面寻找。 |
| PYTHONSTARTUP | Python启动后，先寻找PYTHONSTARTUP环境变量，然后执行此变量指定的文件中的代码。 |
| PYTHONCASEOK  | 加入PYTHONCASEOK的环境变量, 就会使python导入模块的时候不区分大小写. |
| PYTHONHOME    | 另一种模块搜索路径。它通常内嵌于的PYTHONSTARTUP或PYTHONPATH目录中，使得两个模块库更容易切换。 |

8. 运行Python

   有三种方式可以运行Python：

   1、交互式解释器：

   你可以通过命令行窗口进入python并开在交互式解释器中开始编写Python代码。

   你可以在Unix，DOS或任何其他提供了命令行或者shell的系统进行python编码工作。

   $ python # Unix/Linux 

   或者 

   2.C:>python # Windows/DOS

   以下为Python命令行参数：

   | 选项   | 描述                                                   |
   | :----- | :----------------------------------------------------- |
   | -d     | 在解析时显示调试信息                                   |
   | -O     | 生成优化代码 ( .pyo 文件 )                             |
   | -S     | 启动时不引入查找Python路径的位置                       |
   | -V     | 输出Python版本号                                       |
   | -X     | 从 1.6版本之后基于内建的异常（仅仅用于字符串）已过时。 |
   | -c cmd | 执行 Python 脚本，并将运行结果作为 cmd 字符串。        |
   | file   | 在给定的python文件执行python脚本。                     |

   

   3、在 Cloud Studio 中运行 Python3 程序

> Python 的 3.0 版本，常被称为 Python3000，或简称 Py3k。相对于 Python 的早期版本，这是一个较大的升级。为了不带入过多的累赘，Python 3.0 在设计的时候没有考虑向下相容。许多针对早期 Python 版本设计的程序都无法在 Python 3.0 上正常执行。Cloud Studio 为我们提供的 Python 开发环境用的是 Python2.7 版本。通过下面的步骤，可以让你在 Cloud Studio 上运行 Python3 编写的程序

- step1：登录[腾讯云开发者平台](https://studio.dev.tencent.com/)，选择 PHP + Python + Java 开发环境，此时，我看在终端输入命令 `python --version` 可以看到，当前使用的python解释器版本是 2.7.12

  ![img](https://static.runoob.com/images/ad/3870103.jpg)

  ![img](https://static.runoob.com/images/ad/68425849.jpg)

- step2：安装 Python3，执行一下命令，安装 Python3 并查看解释器是否正常工作

```
sudo apt-get install python3
python3 --version
```

出现以下画面则说明 Python3 已经成功安装，你可以通过 python3 命令使用 Python3 解释器来运行你的 Python3 程序。至此，Python3 已经安装完毕，你可以在 Cloud Studio 上运行 Python3 程序 ![img](https://static.runoob.com/images/ad/50512769.jpg)

## 2、python简介和入门

### python的应用和历史

python2:

c,Java,C# 源码 不统一 找了这三个大牛一块完善,但是造成了功能重复 维护难度大

python3:

后来龟叔自己重写了python,就是python3 源码统一 维护难度降低了

### python是编程语言

分类:

编译型 : 只翻译一次 

​	优点 : 运行速度快 

​	缺点 : 开发速度慢

​	代表语言: C,C++

解释型 : 逐行翻译

​	优点 : 开发速度快

​	缺点 : 运行速度慢

​	代表语言: python

### python 的优缺点

**优点:** 

1. 优雅 明确 简单 易入门
2. 开发效率高 强大的第三方库
3. 高级语言 
4. 可移植性
5. 可扩展性
6. 可嵌入性

**缺点:**

1. 相对于C运行慢
2. 代码不能加密
3. 不能利用多线程

### python的种类

- Cpython   官方推荐安装  转换成	C的字节码
- Jython     转换成Java的字节码
- Ironpython   转换成C#(.NET)的字节码
- PyPy  转换成 动态编译 两块 开发快   运行快

### 写一个python程序

print("hello world") 

print() 打印     打印到屏幕

前期 排bug专用   

程序中所有的符号都是英文的

### 变量

作用: 作为一个临时存储的中间值   给复杂难记的的起一个容易记住的名字

   a = 1

1. 起了个变量名 a
2. 有个值           1
3. 将值赋给变量名 =

a = 2 # 修改的是内存中的指向

变量的命名规则

1. 字母数字下划线组成
2. 不能以数字开头 更不能是纯数字
3. 不能使用python中的关键字
4. 不能使用中文和拼音(太low)
5. 区分大小写
6. 变量名要有意义
7. 推荐写法
   1. 驼峰提: AlexAge
   2. 下划线: Alex_Age

### 常量

ALEX_AGE = 50 全部大写的变量名解释常量  常量不建议修改

### 注释

给一些不太能够理解的写一个描述 增加程序的可读性

两种

​	单行(当行)注释: #只能注释一行,不能换行 **注释后的代码是不执行的**

​	多行注释: """ """     ''' '''      本质上是字符串

### 基础数据类型初识

**整型--数字**

```python
+ - * / ** // % 
```



**字符串**

​	字符串 +  字符串拼接 都是字符串的时候才能加

​	字符串 *  只能和数字相乘	

**布尔值**

​	True

​	False

列表

元祖

字典

### 用户交互  (input)

input("请输入一个内容") #  **python3版本中** input 获取到的内容全都是字符串

查看是什么类型  -- type()

### 流程控制语句if

if - 如果 - 选择 5种

```python
# 1. 单纯if
if 条件:
    结果
# 2.二选一
if 条件:
    结果
else:
    结果
# 3.多选一 # 没有else的时候 选择0个或1个,有else的时候选且只选一个
if 条件:
    结果
elif 条件:
    结果
elif 条件:
    结果
else:
    结果
# 4. 多选 可以选所有,也可以一个都不选
if 条件:
    结果
if 条件:
    结果
if 条件:
    结果
# 5. 嵌套
if 条件:
    if 条件:
        if 条件:
            	结果
```



### 字符串格式化输出

​	1.   % 占位符:

​	声明占位的类型 %s -- 字符串  %d/%i -- 整型  %% 转义 成为普通的%

​	%() 不能多，不能少，一一对应

​	2.   f"{}" 大括号里的内容一般都放变量 字符串单引号

​	3.6版本及以上才能使用# 

 3. .formate()  待补充

    

```python
msg = '''
a = "------------------- info ----------------------"
b = %s
c = %s
d = %s
e = %s
f = %s
g = "------------------- end ------------------------"
'''
name = input("name:")
age = input("age:")
sex = input("sex:")
job = input("job:")
hobby = input("hobby:")
print(msg)

	两种输出方式

print(msg%(input("name:"),input("age:"),input("sex:"),input("job:"),input("hobby:")))

# 字符串格式化的时候 不能少 不能多   （占的数量和填充的数量要一致）
# 填充的时候 内容和占的位置是要一一对应的


msg = '%s，学习进度5%%'
print(msg%(input("name:")))

# %s -- 占字符串的位置
# %d -- 占整型的位置
# %% -- 转义（把占位转换成普通的%号）

name = input("name")
print(f"alex.txt{name},{'aaa'}")

# f字符串拼接 -- 3.6版本及以上才能使用

msg = '''
------------------- info ----------------------
name: %s
age:  %s
sex : %s
job:  %s
hobby: %s
------------------- end ------------------------
'''

print(msg%(alex.txt,'20','nan','it','cnb'))

msg = f'''
------------------- info ----------------------
name: {input("name:")}
age:  {input("age:")}
sex : {input("sex:")}
job:  {input("job:")}
hobby: {input("hobby:")}
------------------- end ------------------------
'''
print(msg)
```



### while 循环

定义一个变量 count：计数，控制循环范围

```python
while 条件：
	代码块（循环体）
```

当条件成立时执行下面的流程、模块

终止循环

break打断的是当前本层循环，终止掉循环，毁灭性的

continue 停止当前循环，继续执行下一次循环，暂时性的

![1559204659206](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1559204659206.png)

遇到continue，终止下面的程序运行，回到while继续循环

while else

```python
while 条件:
    代码块1
else：#当且仅当当条件为假的时候执行
	代码块2
```



### 运算符

算数运算符

```python
+ - * / **幂运算 //取整   %取余(模)  
```



赋值运算符

```python
= += -= *= /= **= %= //=
    a = 1
    a += 1  # a = a + 1
    a -= 1  # a = a - 1
    a *= 1  # a = a * 1
    a /= 1    # a = a / 1
    a **= 1   # a = a ** 1
    a %= 1    # a = a % 1
```



逻辑运算符

```python
and (与) -- 和   or (或)   not (非) -- 不是

    1 and 0  # and是两边都是真的时候才是真，只要有一边是假就取假
    0 and 1  
    print(1 and 9)   #and 运算 两边都是真的时候取and后边的内容
    print(False and 0) #and 运算 两边都是假的时候取and前边的内容

    print(3 > 2 and 9)  3>2 视为一个整体来看

    or

    print(1 or 0)  # 只要有一个是真就取真
    print(1 or 4)   # or 两个都是真的时候，取or前面的内容
    print(False or 0) # or 两个都是假的时候，取or后面的内容

    print(3>2 or 4)

	运算顺序
    () > not > and > or
	练习
    print(3 and 9 or 8 and 7 or 0)
    print(0 and False or False and 9 and 4>3)
    print(True and False or 3>4 and not True)
```



比较运算符

```python
== # 等于
!= # 不等于
>  # 大于
<  # 小于 
>= # 大于等于
<= # 小于等于
```



成员运算符

```python
in 在   not in 不在# 
```



### 编码

ASCII码 -- 不支持中文

GBK --  国标：

​	英文 1个字节

​	中文 2个字节

Unicode -- 万国码：

​	英文 2个字节

​	中文 4个字节

utf-8  -- 最流行的编码方式

​	英文 1个字节

​	欧洲 2个字节

​	亚洲 3个字节# 

单位转换：

​	1Byte = 8bit

​	1024B = 1KB

​	1024KB = 1MB

​	1024MB = 1GB

​	1024GB = 1TB

## 3、Python之数据类型

### **int**  整数

在python3中所有的整数都是int类型.但在python2中如果数据量比较大,会使用long类型

而python3中不存在long类型

```python 
+ - * /  //取整   %取余   **幂运算
# 可进行的操作
.bit_length() # 计算整数在内存中占用的二进制码的长度
num = 13456845
print(num.bit_length())
# 补充点 二进制 十进制和二进制相互转化
十进制转化成二进制:除二取余，倒序排列
    
二进制转十进制的转换原理：从二进制的右边第一个数开始，每一个乘以2的n次方，n从0开始，每次递增1。然后得出来的每个数相加即是十进制数。
```



### bool 布尔 

判断 if while

没有操作

- 类型转换
- 结论1：想转化成什么类型就用这个类型符号括起来 列如：字符串转整数 int（str）
- 结论2：True => 1 False => 0
- 结论3：可以当做False来使用的数据：0  ""  []  {}  set()  None 所有的空都是False

### str 字符串    "    "

1. 字符：单一文字符

2. 字符串：有序的字符序列

   字符串是由    '  '      "  "    '''   '''  括起来的内容  

   索引：一排数字，反应第某个位置的字符 索引的下标从0 开始，使用 [  ]来获取数据

   切片：[star​：end：​step]

   ​	顾头不顾尾

   ​	step=n 步长 如果是+ 从左往右，如果是 - 从右往左 每n个取1个

3. 常用操作方法：

   

   ```python
   1. upper() #转化成大写. 忽略大小写的时候
   2. strip() #去掉左右两端空白(空格 制表符\t 换行符\n)   用户输入的内容都要去空白
   3. replace(old, new) #字符串替换
   4. split() 切割  #结果是list
   5. startswith() #判断是否以xxx开头
   6. find() index() index 找不到报错#查找
   7. isdigit() #判断是否是数字组成
   8. len()  #求长度. 内置函数
   ```

### **list 列表 [ ]**

 可以存放大量的数据

1. 介绍

   列表是python的基础数据类型之⼀ ,其他编程语⾔也有类似的数据类型. 比如JS中的数
   组, java中的数组等等. 它是以[ ]括起来, 每个元素⽤' , '隔开⽽且可以存放各种数据类型:

   ```python
   lst = [1, '哈哈', "吼吼", [1,8,0,"百度"], ("我","叫", "元", "组"), "abc", {"我叫":"dict字典"},{"我叫集合","集合"}]
   ```

   列表相比于字符串. 不仅可以存放不同的数据类型. ⽽且可以存放⼤量的数据. 32位
   python可以存放: 536870912个元素, 64位可以存放: 1152921504606846975个元素.⽽且列
   表是有序的(按照你保存的顺序),有索引, 可以切⽚⽅便取值.

2. 列表的索引和切片

   ```python
   lst = ["麻花藤", "王剑林", "⻢芸", "周鸿医", "向华强"]
   print(lst[0]) # 获取第⼀个元素
   print(lst[1])
   print(lst[2])
   lst[3] = "流动强" # 注意. 列表是可以发⽣改变的. 这⾥和字符串不⼀样
   print(lst) # ['麻花藤', '王剑林', '⻢芸', '流动强', '向华强']
   s0 = "向华强"
   s0[1] = "美" # TypeError: 'str' object does not support item assignment 不允许改变
   print(s0)
   ```

   列表的切片:

   ```python
   lst = ["麻花藤", "王剑林", "⻢芸", "周鸿医", "向华强"]
   print(lst[0:3]) # ['麻花藤', '王剑林', '⻢芸']
   print(lst[:3]) # ['麻花藤', '王剑林', '⻢芸']
   print(lst[1::2]) # ['王剑林', '周鸿医'] 也有步⻓
   print(lst[2::-1]) # ['⻢芸', '王剑林', '麻花藤'] 也可以倒着取
   print(lst[-1:-3:-2]) # 倒着带步⻓
   ```

3. 列表的增删改查

   1. 增, 注意, list和str是不⼀样的. lst可以发⽣改变. 所以直接就在原来的对象上进⾏了操作

     ```python
     # .append() 末尾添加  .insert(place,content)在place处插入content,原来的后移  .extend([])迭代添加 
     lst = ["麻花藤", "林俊杰", "周润发", "周芷若"]
     print(lst)
     lst.append("wusir")#在列表末尾添加一个信息
     print(lst)
     
     lst = []
     while True:#循环录入信息
     	content = input("请输⼊你要录⼊的员⼯信息, 输⼊Q退出:")
         if content.upper() == 'Q':
             break
     	lst.append(content)
     	print(lst)
         
     lst = ["麻花藤", "张德忠", "孔德福"]
     lst.insert(1, "刘德华") # 在1的位置插⼊刘德华. 原来的元素向后移动⼀位
     print(lst)
     # 迭代添加
     lst = ["王志⽂", "张⼀⼭", "苦海⽆涯"]
     lst.extend(["麻花藤", "麻花不疼"]) # 迭代添加
     print(lst) #['王志⽂', '张⼀⼭', '苦海⽆涯', '麻花藤', '麻花不疼']
     ```

   2. 删除

      pop, remove, clear, del

      ```python
      lst = ["麻花藤", "王剑林", "李嘉诚", "王富贵"]
      print(lst)
      deleted = lst.pop() # 删除最后⼀个
      print("被删除的", deleted)# 被删除的 王富贵
      print(lst)# ['麻花藤', '王剑林', '李嘉诚']
      el = lst.pop(2) # 删除索引为2的元素
      print(el)
      print(lst)
      lst.remove("麻花藤") # 删除指定元素
      print(lst)
      # lst.remove("哈哈") # 删除不存在的元素会报错
      # # print(lst)
      lst.clear()  # 清空list
      print(lst)
      # 切⽚删除
      del lst[1:3]
      print(lst)
      ```

   3.修改

   ```python
   # 索引切片修改
   # 修改
   lst = ["太⽩", "太⿊", "五⾊", "银王", "⽇天"]
   lst[1] = "太污" # 把1号元素修改成太污
   print(lst)
   lst[1:4:3] = ["麻花藤", "哇靠"] # 切⽚修改也OK. 如果步⻓不是1, 要注意. 元素的个数
   print(lst)
   lst[1:4] = ["李嘉诚个⻳⼉⼦"] # 如果切⽚没有步⻓或者步⻓是1. 则不⽤关⼼个数
   print(lst)
   ```

   

4. 查询,列表是一个刻迭代对象,所以可以进行for循环

   ```python
   for el in lst:
       print(el)
   ```

5. 其他操作

   ```python
   lst = ["太⽩", "太⿊", "五⾊", "银王", "⽇天", "太⽩"]
   c = lst.count("太⽩") # 查询太⽩出现的次数
   print(c)
   lst = [1, 11, 22, 2]
   lst.sort() # 排序. 默认升序
   print(lst)
   lst.sort(reverse=True) # 降序
   print(lst)
   lst = ["太⽩", "太⿊", "五⾊", "银王", "⽇天", "太⽩"]
   print(lst)
   lst.reverse()
   print(lst)
   l = len(lst) # 列表的⻓度
   print(l)
   ```

4. 列表的嵌套

   采用降维操作,一层一层的看就好

   ```python
   lst = [1, "太⽩", "wusir", ["⻢⻁疼", ["可⼝可乐"], "王剑林"]]
   # 找到wusir
   print(lst[2])
   # 找到太⽩和wusir
   print(lst[1:3])
   # 找到太⽩的⽩字
   print(lst[1][1])
   # 将wusir拿到. 然后⾸字⺟⼤写. 再扔回去
   s = lst[2]
   s = s.capitalize()
   lst[2] = s
   print(lst)
   # 简写
   lst[2] = lst[2].capitalize()
   print(lst)
   # 把太⽩换成太⿊
   lst[1] = lst[1].replace("⽩", "⿊")
   print(lst)
   # 把⻢⻁疼换成⻢化疼
   lst[3][0] = lst[3][0].replace("⻁", "化")
   print(lst[3][0])
   lst[3][1].append("雪碧")
   print(lst)
   
   ```
### tuple 元组 ( ) 

元组: 俗称不可变的列表.⼜被成为只读列表, 元组也是python的基本数据类型之⼀, ⽤⼩括号括起来, ⾥⾯可以放任何数据类型的数据, 查询可以. 循环也可以. 切片也可以. 但就是不能改

```python
tu = (1, "太⽩", "李⽩", "太⿊", "怎么⿊")
print(tu)
print(tu[0])
print(tu[2])
print(tu[2:5]) # 切⽚之后还是元组
# for循环遍历元组
for el in tu:
 print(el)
# 尝试修改元组
# tu[1] = "⻢⻁疼" # 报错 'tuple' object does not support item assignment
tu = (1, "哈哈", [], "呵呵")
# tu[2] = ["fdsaf"] # 这么改不⾏
tu[2].append("麻花藤") # 可以改了. 没报错
tu[2].append("王剑林")
print(tu)
```



关于不可变, 注意: 这⾥元组的不可变的意思是⼦元素不可变. ⽽⼦元素内部的⼦元素是可
以变, 这取决于⼦元素是否是可变对象.元组中如果只有⼀个元素. ⼀定要添加⼀个逗号, 否则就不是元组

```python
tu = (1,)
print(type(tu))
#元组也有count(), index(), len()等⽅法. 可以⾃⼰测试使⽤
```

### for 循环 和range

range可以帮我们获取到⼀组数据. 通过for循环能够获取到这些数据.

python 3 中 打印range() 是他本身

python 2 中打印range() 是list 

```python
for num in range(10):
 print(num)
for num in range(1, 10, 2):
 print(num)
for num in range(10, 1, -2): # 反着来, 和切⽚⼀样
print(num)
```



### **dict 字典   { key : value }** 

以key：value的形式存储数据,中间用 " , " 隔开

1. 字典是什么

   可变的

   dict   {}  "键":"值"    别的语言称为键值对数据

   dic = {"key":"value",1:2,3:4}

   键: 必须是可哈希(不可变的数据类型),并且是唯一的

   dic = {"key":1,1:123,False:True,(1,2,3,):"ABC",2:[1,2,3]}

   字典是无序的,python3.5 版本以上, 默认了自己定义的顺序, python3.5以下随机显示

   enumerate()对列表 字典进行枚举 括号中两个参数,前面是对向 后边是起始数字

2. 字典能干啥

   字典是存储大量数据, 字典比列表还要大

   字典查询找值的时候能够方便,快速

   字典能够将数据进行关联 

   例如 dic = {'a':'b','b':'c','c':'d','d':'e'}

   字典比较消耗内存,最长用的数据 字符串,列表.字典

   有明确对应关系时,推荐使用字典  字典一要使用明白     json

   list((1,2,3,4,5,6))  元组转列表

   tuple([1,2,3,4])  l列表转元组

   目前所学的知识点不能转换字典

3. 字典怎么用

   增

   ```python
   dic = {}
   dic.setdefault("c","aaa") # 先去字典中查看要添加的键存不存在,如果存在,不添加;如果不存在,添加
   dic["s"] = "huasghkdah"
   ```

   删

   ```python
   dic.pop("键") # 通过键删除 pop又返回值
   字典没有remove
   del dic 删除整个字典
   del dic["键"]
   dic.clear()
   ```

   改

   ```python
   dic = {"a":"b","C":"c"}
   dic["b"] = "这是值"    键在字典中就是修改 不在 则是添加
   dic2 = {"1":"2","a":"abc"}
   dic2.update(dic) # update()里的级别高,会把相同的key对应的值覆盖,没有相同的key就天加
   ```

   查

   ```python
   dic = {"a":1,"C":"c"}
   print(dic["d"])  #如果有键在字典中,返回对应的值,没有则报错
   get - 获取
   print(dic.get("c","你是不是个傻子")) # 获取值的时候可以指定返回的内容,键不存在的时候不报错 返回None
   区别
   dic["a"]+5
   dic.get("c")+5
   print 都打印出来
   dic.setdefault("键")
   ```

   其他

   ```python
   for i in dic.keys():# 高仿列表 可迭代 but 没索引
       print(i)
   for i in dic:
       print(i)
   dic.item() # 
   dic.keys()
   dic.values()
   ```

   解构

   ```python
   a,b = b,a
   a,b = 1,2
   a,b = (34,36)
   a,b = [1,4]
   a,b = {"s":1,"a":1}# 字典拆包后的结果是键赋值给了a和 b 的变量
   
   ```

   

4. 字典嵌套

   ```python
   # dic = {
   #     101:{1:["周杰伦","林俊杰"],2:{"汪峰":["国际章",{"前妻1":["熊大","熊二"]},
   #                                   {"前妻2":["葫芦娃","木吒"]}]}},
   #     102:{1:["李小龙","吴京","李连杰"],2:{"谢霆锋":["张柏芝","王菲"]},
   #          3:["alex","wusir","大象","奇奇"]},
   #     103:{1:["郭美美","干爹"],2:{"王宝强":{"马蓉":"宋哲"}}},
   #     201:{1:["凹凸曼","皮卡丘"],2:{"朱怼怼":{"杨幂":"刘恺威"}}}
   # }
   
   # print(dic[101][2]["汪峰"][2]["前妻2"][0])
   
   #['国际章', {'前妻1': ['熊大', '熊二']}, {'前妻2': ['葫芦娃', '木吒']}]
   #{'前妻2': ['葫芦娃', '木吒']}
   #['葫芦娃', '木吒']
   
   # home1 = dic[102][3][2]
   # {1: ['李小龙', '吴京', '李连杰'],
   # 2: {'谢霆锋': ['张柏芝', '王菲']},
   # 3: ['alex', 'wusir', '大象', '奇奇']}
   # print(home1)
   
   # print(dic[103][2]["王宝强"]["马蓉"])
   ```

   

### set 集合（可以理解为数学上的集合）



**1. 什么是集合?**

set  {1,2,3}

s = {1,2,3,"123",False,(1,2,3,4)}

集合就是一个没有值的字典,遵循:唯一,无序,元素要求可哈希(不可变)

print(s)

集合是无序的

集合是可变的



**2.集合怎么用?**

```python
# 增:
s.update("3456")  # 迭代添加
print(s)
s.add("怼怼")
print(s)

# 删:
s = {1,2,3,"123",False,(1,2,3,4)}
print(s)
s.pop()   #随机删除
s.remove(3) # 通过元素删除
s.clear()    # 清空
del s        #删除整个集合
print(s)

# 改:
删了,再加

# 查:
for

# 天然去重   --  唯一

# 其他操作:
s1 = {1,2,3,4,5,6,7}
s2 = {3,4,5,6}
print(s1 - s2)  #差集
print(s1 | s2)  #并集   (合集)
print(s1 & s2)  #交集
print(s1 ^ s2)  #对称差集  -- 反交集
print(s1 > s2)  # 超集   -- 父集
print(s1 < s2)  # 子集

# 冻结集合(可变 转换成 不可变)  -- 更不常用
f_s = frozenset({1,2,3,4,5})

dic = {f_s:"1"}
print(dic)
```

**3.集合在哪用?**

```python
1.去重
li = [1,2,3,4,5,2,2,2,33,3,3,2,2,1,]
print(list(set(li)))

2面试题:
li = [1,2,3,4,2,1,3] #一行代码去重
print(list(set(li)))
```



## 4、文件操作

## 5、小数据池和深浅拷贝



### 数据的比较

1. 不同数据类型之间最好不要进行比较

2. 如果输入的数据和变量进行比较的时候，因为input输入的类型都是字符串，所以要将输入的数据转化为和变量相同的数据类型，方法事例如下：

   ![1559116062987](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1559116062987.png)



**报错**

typeError :must be str,not int 类型错误,必须是字符串,不能是整数.常见于前面是字符串,与后面的整数相加

## 

**交互**：s = input（“提示语”）input输入的结果为字符串

**



**Pycharm解释器选择**

![1559266513353](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1559266513353.png)

![1559266533053](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1559266533053.png)

![1559266544443](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1559266544443.png)

![1559266558361](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1559266558361.png)

![1559266566957](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1559266566957.png)



**迭代**

```
for 变量 in 可迭代对象:
	循环体
	
```



**git常用命令**

1. git init   指第一次初始化一个文件夹，要确保此文件夹下生成.git隐藏文件以后都不用再初始化了
2. git add . 
   1. add 相当于一个动作，要添加动作后的名词至本地git仓库
   2. “. ”的含义是把所有此文件夹下的增删改操作都添加到本地仓库
3. git commit -m "你本次操作的记录备注信息"
   1. commit 在git语法中做提交操作，也是一个动作
   2. -m "你本次操作的记录备注信息"
4. git push -u origin master
   1. 提交本地仓库到远程仓库
   2. 切记！！！不要删除远程仓库（码云）内的任何东西
   3. 任何增删改在本地操作，操作完成之后按顺序执行：
      1. git add .                              git add .
      2. git commit -m "day02"        git commit -m "  备注 "
      3. git push -u origin master     git push -u origin master

# 三、Python之函数

# 四、Python之常用模块

# 五、 Python之模块和包

# 六、Python之面对对象

# 七、Python之网络编程

# 八、数据库

# 九、前端

# 十、Python Web框架

# 十一、版本控制 -- git

# 十二、爬虫

# 十三、前端框架VUE

# 十四、量化投资与Python

# 十五、算法

# 十六、设计模式

# 十七、restful framework

# 十八、权限管理

