# _*_coding:utf-8_*_
# 1.将今天考试的卷子,自己在做一遍(不要有任何的参考),然后没有做出来的题记录下来在参考笔记和资料
# 2.完成一个商城购物车的程序。
# 商品信息在shopping.txt文件中存储的，存储形式：
# name price
# 电脑 1999
# 鼠标 10
# 游艇 20
# 美女 998
# with open("shopping.txt",mode="w",encoding="utf-8") as f:
#     f.write('''name price
# 电脑 1999
# 鼠标 10
# 游艇 20
# 美女 998
# 充气娃娃 6699
# 宝哥最爱 11''')
# 要求:
# # 1，用户先给自己的账户充钱：比如先充3000元。
# meney = input("请输入充值金额")
# # 2，读取商品信息文件将文件中的数据转化成下面的格式：
# # goods = [{"name": "电脑", "price": 1999},
# # {"name": "鼠标", "price": 10},
# # {"name": "游艇", "price": 20},
# # {"name": "美女", "price": 998},]
# lis = []
# with open("shopping.txt", mode="r", encoding="utf-8") as f:
#     w = f.readline()
#     for i in f.readlines():
#         dic = {}
#         for s in range(len(w.strip().split())):
#             dic[w.strip().split()[s]] = i.strip().split()[s]
#         lis.append(dic)
#     print(lis)

# # 3，页面显示 序号 + 商品名称 + 商品价格，如：
# # # 1 电脑 1999
# # # 2 鼠标 10
# with open("shopping.txt", mode="r", encoding="utf-8") as f:
#     w = f.readline()
#     n = 1
#     for i in f.readlines():
#         print(n, i.strip().split()[0], i.strip().split()[1])
#         n += 1

# # 4，用户输入选择的商品序号，然后打印商品名称及商品价格,并将此商品，添加到购物车(自己定义购物车)，用户还可继续添加商品。
# shopping_car = []
# while True:
#     user_input = input("请输入商品序号:")
#     dic = {}
#     with open("shopping.txt", mode="r", encoding="utf-8") as f:
#         w = f.readline()
#         m = f.readlines()
#         dic[m[int(user_input) - 1].strip().split()[0]] = m[int(user_input) - 1].strip().split()[1]
#         shopping_car.append(dic)
#         print(dic)

# # 5，如果用户输入的商品序号有误，则提示输入有误，并重新输入。
# with open("shopping.txt", mode="r", encoding="utf-8") as f:
#     w = f.readline()
#     m = f.readlines()  # ['电脑 1999\n', '鼠标 10\n', '游艇 20\n', '美女 998']
#     shopping_car = []
#     while True:
#         user_input = input("请输入商品序号:")
#         if user_input.isdigit():
#             if int(user_input) in range(1, len(m) + 1):
#                 dic = {}
#                 dic[m[int(user_input) - 1].strip().split()[0]] = m[int(user_input) - 1].strip().split()[1]
#                 shopping_car.append(dic)
#                 print(dic)
#         else:
#             print("输入有误,请重新输入")
# 6，用户输入n为购物车结算，依次显示用户购物车里面的商品，数量及单价，若充值的钱数不足，则让用户删除某商品，直至可以购买，若充值的钱数充足，则可以直接购买。
# 7，用户输入Q或者q退出程序。
# 8，退出程序之后，依次显示用户购买的商品，数量，单价，以及此次共消费多少钱，账户余额多少，并将购买信息写入文件。
# 1题必须要重视,因为这样就能知道你哪部分的知识点是没有掌握的!
# 完成2-3要求为C。
# 完成2-4要求为 C+。
# 完成2-6要求为B。
# 完成全部要求并且没有BUG为A 或者A +
with open("shopping.txt", mode="r", encoding="utf-8") as f:
    w = f.readline()
    m = f.readlines()  # ['电脑 1999\n', '鼠标 10\n', '游艇 20\n', '美女 998']
while True:
    meney = input("请输入充值金额")
    if meney.isdigit():
        meney = int(meney)
        print("充值成功,余额为{}".format(meney))
        break
    else:
        print("输入错误，请重新输入")
biaoji = 0
n = 1
print("序号", "商品", "单价")
for i in m:
    print(n, i.strip().split()[0], i.strip().split()[1])
    n += 1
shopping_car = []  # 购物车 将选中的商品加入到里边 字典格式
shopping_car2 = []  # 多次购物时为了使下一次不重复付费,需要一个载体承接
shopping_money1 = 0  # 消费的总额,单次是加零 多次是多次的和
while True:
    user_input = input("请输入商品序号(输入n or N 购物车结算,输入q or Q 退出程序):")
    if user_input.upper() == "N":
        while True:
            hk = []
            for sale in shopping_car:
                md = (list(sale)[0], sale[list(sale)[0]], shopping_car.count(sale))  # 获取的是目标商品,数量单价 接下来判重
                hk.append(md)
            shopping_car1 = list(set(hk))  # shopping_car1 对购物车去重  输出时的购物车数据
            shopping_money = 0  # 购物车内的物品价值总额
            surplus_noney = int(meney) - shopping_money
            print("商品", "单价", "数量")
            for shopping in shopping_car1:
                shopping_money += int(shopping[1]) * int(shopping[2])
                print(shopping[0], shopping[1], shopping[2])
            print("商品的总价格是:{}".format(shopping_money))
            buy_input = input("请输入是否购买:是or否")
            if buy_input == "是":
                if int(shopping_money) <= int(meney):  # 判断钱是否够,够了可以买钱减少,不够输入删除商品
                    meney -= shopping_money
                    print("恭喜购物成功,余额为{};如需退出程序请输入q 或者 Q".format(meney))
                    import copy
                    shopping_car_copy = copy.deepcopy(shopping_car1)
                    shopping_car2.append(shopping_car_copy)
                    shopping_car.clear()
                    biaoji += 1
                    break
                else:
                    xuhao = 1  # 先输出购物车中的信息,方便让用户选择删除
                    for dele in shopping_car:
                        print(xuhao, dele)
                        xuhao += 1
                    while True:
                        shopping_lose = input("您目前余额{}金额不足,请输入要删除的商品序号:".format(meney))
                        if shopping_lose.isdigit():
                            shopping_car.pop(int(shopping_lose) - 1)
                            break
                        else:
                            print("输入错误，请重新输入！")
                biaoji += 1
            elif buy_input == "否":
                print("继续添加物品请输入商品序号,退出请输入q or Q")
                break
            else:
                print("输入有误,请重新输入!")
        shopping_money1 += shopping_money
    elif user_input.upper() == "Q":
        if biaoji == 0:
            exit(0)
        else:
            print("商品", "单价", "数量")
            for shopping2 in shopping_car2:
                for shopping in shopping2:
                    print(shopping[0], shopping[1], shopping[2])
                    with open("buy_message.txt", mode="a+", encoding="utf-8") as z:
                        z.write("{} {} {}\n".format(shopping[0], shopping[1], shopping[2]))
            print("此次消费了{}".format(shopping_money1))
            print("账户剩余{}".format(meney))
            with open("buy_message.txt", mode="a+", encoding="utf-8") as z:
                z.write("此次消费了{}账户剩余{}\n".format(shopping_money1, meney))
            exit(0)
    elif user_input.isdigit():
        if int(user_input) in range(1, len(m) + 1):
            dic = {}
            dic[m[int(user_input) - 1].strip().split()[0]] = m[int(user_input) - 1].strip().split()[1]
            shopping_car.append(dic)
            print(dic)
        else:
            print("输入序号不对.请重新输入!")
    else:
        print("输入有误,请重新输入")