# 1. 简答题：
#    1. 面向对象的三大特性是什么？
"""
封装 继承 多态
"""
#    2. 什么是面向对象的新式类？什么是经典类？
"""
# 经典类: 在python2中，class Dad: 不会继承object，这样的类叫做经典类（它叫经典类，不是因为它经典，而是因为它比较老）
# 新式类: 在python3中，python会默认继承object类（一切皆对象）class Dad  就相当于python2中的  class Dad（object）

"""

#    3. 面向对象为什么要有继承？继承的好处是什么？
"""
# 增加耦合性
# 减少重复代码
# 时代吗更规范化,合理化
"""
#    4. 面向对象中super的作用。
"""
# 用来调用父类的方法
"""

# 2. 代码题(通过具体代码完成下列要求)：
#
#    ```
# class A:
#     def func(self):
#         print('in A')
#
#
# class B:
#     def func(self):
#         print('in B')
#
#
# class C(A, B):
#     def func(self):
#         print('in C')


#    ```
#
# ​		可以改动上上面代码，完成下列需求：对C类实例化一个对象产生一个c1，然后c1.func()
#
# ​		1. 让其执行C类中的func
"""
class A:
    def func(self):
        print('in A')


class B:
    def func(self):
        print('in B')


class C(A, B):
    def func(self):
        print('in C')
c1 = C()
c1.func()
"""

# ​		2. 让其执行A类中的func
"""
class A:
    def func(self):
        print('in A')


class B:
    def func(self):
        print('in B')


class C(A, B):
    pass


c1 = C()
c1.func()
"""

# ​		3. 让其执行B类中的func
# class A:
#     def func(self):
#         print('in A')
#     pass
#
#
# class B:
#     def func(self):
#         print('in B')
#
#
# class C(A, B):
#
#     def func(self):
#         # print('in C')
#         super(A, self).func()
#
#
# c1 = C()
# c1.func()
# ​		4. 让其既执行C类中的func，又执行A类中的func
# class A:
#     def func(self):
#         print('in A')
#
#
# class B:
#     def func(self):
#         print('in B')
#
#
# class C(A, B):
#
#     def func(self):
#         print('in C')
#         super().func()
#
#
# c1 = C()
# c1.func()
# ​		5. 让让其既执行C类中的func，又执行B类中的func
# class A:
#     def func(self):
#         print('in A')
#
#
# class B:
#     def func(self):
#         print('in B')
#
#
# class C(A, B):
#
#     def func(self):
#         print('in C')
#         super(A, self).func()
#
#
# c1 = C()
# c1.func()


#
#
# 3. 下面代码执行结果是什么？为什么？
#
#    ```
# class Parent:
#     def func(self):
#         print('in Parent func')
#
#     def __init__(self):
#         self.func()
#
#
# class Son(Parent):
#     def func(self):
#         print('in Son func')
#
#
# son1 = Son()
"""
in Son func
就近原则
"""


#    ```
#
# 4.
#
# ```
# class A:
#     name = []
#
#
# p1 = A()
# p2 = A()
#
# p1.name.append(1)
# P3 = A()
# # p1.name，p2.name，A.name 分别是什么？
# '''
# [1]
# [1]
# [1]
# '''
#
# p1.age = 12
# # p1.age，p2.age，A.age 分别又是什么？为什么？
# 12
# 报错
# 报错
#
# 5. 写出下列代码执行结果：
#
# ```
# class Base1:
#     def f1(self):
#         print('base1.f1' )
#
#     def f2(self):
#         print('base1.f2' )
#
#     def f3(self):
#         print('base1.f3' )
#         self.f1()
#
#
# class Base2:
#     def f1(self):
#         print('base2.f1' )
#
#
# class Foo(Base1, Base2):
#     def f0(self):
#         print('foo.f0')
#         self.f3()
#
#
# obj = Foo()
# obj.f0()
# foo.f0
# base1.f3
# base1.f1

# ```
#
# 6. 看代码写结果：
#
# ```
class Parent:
    x = 1


class Child1(Parent):
    pass


class Child2(Parent):
    pass


# print(Parent.x, Child1.x, Child2.x)  # 1 1 1
#
# Child2.x = 2
# print(Parent.x, Child1.x, Child2.x)  # 1 1 2

# Child1.x = 3
# print(Parent.x, Child1.x, Child2.x)  # 1 3 1
# ```
#
# 7. 有如下类：
#
# ```
class A:
    pass


class B(A):
    pass


class C(A):
    pass


class D(A):
    pass


class E(B, C):
    pass


class F(C, D):
    pass


class G(D):
    pass


class H(E, F):
    pass


class I(F, G):
    pass


class K(H, I):
    pass


# ```
print(K.mro())
# 如果这是经典类，请写出他的继承顺序。
#
# 如果这是新式类，请写出他的继承顺序，并写出具体过程。
'''
mro(K(H, I)) = [K] + merge(mro(H), mro(I)), [H, I]
=[K, H, E, B, F, C, D, A], [K, I, F, C, G, D, A][H, I]
[k, ] = [H, E, B, F, C, D, A], [I, F, C, G, D, A][H, I]
[k, H, ] = [E, B, F, C, D, A], [I, F, C, G, D, A][I]
[k, H, E, B, I, F, C, G] = [D, A], [D, A]
[k, H, E, B, I, F, C, G, D, A] = [D, A], [D, A]
= [k, H, E, B, I, F, C, G, D, A]
# 1           
mro(H(E, F)) = [H] + merge(mro(E), mro(F)), [E, F]
=[H] + merge([E, B, C, A], [F, C, D, A]), [E, F]
=[H, E] + merge([B, C, A], [F, C, D, A]), [, F]
=[H, E, B, F, ] + merge([C, A], [C, D, A])
=[H, E, B, F, C, D, A] + merge([C, A], [C, D, A])

mro(E(B, C)) = [E] + merge(mro(B), mro(C)), [B, C]
=[E] + merge([B, A], [C, A]), [B, C]
=[E, B, C, A]
mro(B(A)) = [B] + [A] == > [B, A]

mro(F(C, D)) = [F] + merge(mro(C), mro(D)), [C, D]
=[F] + merge([C, A], [D, A]), [C, D]
=[F, C, A] + [F, D, A], [C, D]

# 2 
mro(I(F, G)) = [I] + merge(mro(F), mro(G)), [F, G]
=[I] + merge([F, C, A] + [F, D, A], [C, D], [G, D, A]), [F, G]
[I, ] = +merge([F, C, A] + [F, D, A], [C, D], [G, D, A]), [F, G]
[I, F, ] = +merge([C, A] + [D, A], [C, D], [G, D, A]), [G]
[I, F, C, ] = +merge([A] + [D, A], [D], [G, D, A]), [G]
[I, F, C, G, ] = +merge([A] + [D, A], [D], [D, A])
[I, F, C, G, D, ] = +merge([A] + [A], [A])
=[I, F, C, G, D, A]

mro(F(C, D)) = [F] + merge(mro(C), mro(D)), [C, D]
=[F] + merge([C, A], [D, A]), [C, D]
=[F, C, A] + [F, D, A], [C, D]

mro(G(D)) = [G] + merge(mro(D))
=[G] + merge([D, A])
=[G, D, A]
'''
