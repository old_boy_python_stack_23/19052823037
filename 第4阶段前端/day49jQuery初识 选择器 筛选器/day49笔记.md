### 内容回顾

#### DOM事件

##### 事件的绑定

```
方式一 行内绑定
<a onclick="函数名()"></a>
function 函数名(){
    函数体
}

方式二 匿名函数绑定
某对象.onclick = function(){
    函数体
}

方式三 函数名绑定
某对象.onclick = 函数名
function 函数名(){
    函数体
}
```

##### 常见事件

```
onclick()      单机事件 
onmouseover()  鼠标悬浮
onmouseout()   鼠标离开
onscroll()     滚动轴移动
onChange()     文本区域内内容变化
onfocus()      获取焦点
onblur()       失去焦点
```

#### BOM

和浏览器的操作相关的

##### window对象

```
定时器方法
setInterval("fn()",n)  每隔n毫秒就执行fn函数一次
setTimeout("fn()",n)   n毫秒之后执行fn函数一次

innerHeight   浏览器显示网页内部内容的高度,会随着浏览器窗口变化
innerWidth    浏览器显示网页内部内容的宽度,会随着浏览器窗口变化
```

##### location对象 地址栏  

```
属性 :href
查看页面的url连接,href = '新的url',跳转到新的页面

方法:reload()
刷新页面
```

##### history对象

```
history.back()     go(-1)   //回到上一页
history.go(0)               //刷新
history.forward()  go(1)    //去下一页
```

#### 应用和示例

```
红绿灯
随机字体颜色
关闭顶部广告栏
select框的联动
随着滚动轴移动显示回到顶部
```

### 今日内容

#### jquery介绍

##### jquery的优势

```
1.js代码对浏览器的兼容性做的更好了
2.隐式循环
3.链式操作
```

##### jquery是什么?

```
高度封装了js代码的模块
	封装了dom节点
	封装了操作dom节点的简便方法
```

##### jquery的导入

```js
https://code.jquery.com/jquery-3.4.1.js 未压缩版
https://code.jquery.com/jquery-3.4.1.min.js 压缩版
下载:保存在本地文件里
引入:<script src="jquery3.4.1.min.js"></script>
```

##### $和jQuery的关系

```
$就是jQuery名字的简写,实际上是一回事儿
```

##### jquery对象和dom对象的关系和转换

```
jquery封装了dom
dom转成jquery : jQuery(dom对象)  $(dom对象)
jquery转成dom : jq对象[index]
```

#### jquery选择器

##### 基础选择器

```js
#id选择器   .类选择器  标签选择器   *通用选择器
$('#city')
k.fn.init [ul#city]0: ul#citylength: 1__proto__: Object(0)
$('.box')
k.fn.init [div.box, prevObject: k.fn.init(1)]0: div.boxlength: 1prevObject: k.fn.init [document]__proto__: Object(0)
$('li')
k.fn.init(4) [li, li, li, li, prevObject: k.fn.init(1)]0: li1: li2: li3: lilength: 4prevObject: k.fn.init [document]__proto__: Object(0)
$('a')
k.fn.init(2) [a, a, prevObject: k.fn.init(1)]0: a1: alength: 2prevObject: k.fn.init [document]__proto__: Object(0)
$('*')
k.fn.init(16) [html, head, meta, title, body, div.box, ul#city, li, li, a, li, a, li, p, script, script, prevObject: k.fn.init(1)]

div.c1交集选择器      div,p并集选择器
$('div.box')
k.fn.init [div.box, prevObject: k.fn.init(1)]0: div.boxlength: 1prevObject: k.fn.init [document]__proto__: Object(0)
$('div,p,a')
k.fn.init(4) [div.box, a, a, p, prevObject: k.fn.init(1)]                           
```

##### 层级选择器

```js
空格 后代选择器    >子代选择器 +毗邻选择器  ~弟弟选择器
$('div li')
$('div>ul>li')
$('.baidu+li')
k.fn.init [prevObject: k.fn.init(1)]
$('.baidu~li')
k.fn.init(3) [li, li, li, prevObject: k.fn.init(1)]
```

##### 属性选择器

```js
$('[属性名]')         必须是含有某属性的标签
$('a[属性名]')        含有某属性的a标签
$('选择器[属性名]')    含有某属性的符合前面选择器的标签
$('选择器[属性名="aaaa"]')  属性名=aaa的符合选择器要求标签
$('选择器[属性名$="xxx"]')  属性值以xxx结尾的
$('选择器[属性名^="xxx"]')  属性值以xxx开头的
$('选择器[属性名*="xxx"]')  属性值包含xxx
$('选择器[属性名1][属性名2="xxx]')  拥有属性1,且属性二的值='xxx',符合前面选择器要求的
```

#### jquery筛选器

##### 基础筛选器

```js
$('选择器:筛选器')
$('选择器:first')
作用于选择器选择出来的结果
first      找第一个
last       最后一个
eq(index)  通过索引找
even       找偶数索引
odd        找奇数索引
gt(index)  大于某索引的
lt(index)  小于某索引的
not(选择器) 不含 符合选择器 要求的
has(选择器) 后代中含有该选择器要求的(找的不是后代,找的是本身)
```

##### 表单筛选器

###### type筛选器

```
$(':text')
$(':password')
$(':radio')
$(':checkbox')
$(':file')
$(':submit')
$(':reset')
$(':button')
注意 : date type的input是找不到的
```

###### 状态筛选器

```js
enabled
disabled
checked
selected

$(':disabled')
jQuery.fn.init [input, prevObject: jQuery.fn.init(1)]
$(':enabled')
jQuery.fn.init(15) [input, input, input, input, input, input, input, input, input, input, input, select, option, option, option, prevObject: jQuery.fn.init(1)]
$(':checked')
jQuery.fn.init(4) [input, input, input, option, prevObject: jQuery.fn.init(1)]
$(':selected')
$(':checkbox:checked')
jQuery.fn.init(2) [input, input, prevObject: jQuery.fn.init(1)]
$('input:checkbox:checked')
jQuery.fn.init(2) [input, input, prevObject: jQuery.fn.init(1)]
```

#### jquery的筛选器方法

```
找兄弟 :$('ul p').siblings()
找哥哥
$('ul p').prev()             找上一个哥哥
$('ul p').prevAll()          找所有哥哥
$('ul p').prevUntil('选择器') 找哥哥到某一个地方就停了
找弟弟 : next()  nextAll()   nextUntil('选择器')
找祖宗 : parent()  parents()   parentsUntil('选择器')
找儿子 : children()
```

```
筛选方法
first()
last()
eq(index)
not('选择器')   去掉满足选择器条件的
filter('选择器')交集选择器,在所有的结果中继续找满足选择器要求的
find('选择器')  后代选择器 找所有结果中符合选择器要求的后代
has('选择器')   通过后代关系找当代 后代中有符合选择器要求的就把当代的留下
```

