### 动画

```
show系列
	show hide toggle
slide系列
	fadeOut slideDown slideToggle(时间，回调函数)
fade淡入淡出系列
	fadeOut fadeIn fadeToggle
动画的停止
	stop 永远在动画开始之前，停止动画

```

### 事件

#### 事件绑定

```js
// bind 参数都是选传的,接收参数e.data
    $('button').bind('click',{'a':'b'},fn)
    function fn(e) {
        console.log(e.data)
        console.log(e.data.a)
    }

    $('button').bind('click',fn)
    function fn(e) {
        console.log('wahaha')
    }
// 简写的事件名称当做方法名
    $('button').click({'a':'b'},fn)
    function fn(e) {
        console.log(e.data)
        console.log(e.data.a)
    }

    $('button').click(fn)
    function fn(e) {
        console.log('wahaha')
    }
```

解除绑定

```js
$("button").unbind("click")
```

各种事件

```
click(function(){...})   // 单机事件

blur(function(){...})    // 失去焦点
focus(function(){...})   // 获得焦点

change(function(){...})  // input框鼠标离开时内容改变触发

keyup(function(){...})   // 按下的键盘触发事件 27对应的是esc键 获取键盘编号 e.keyCode
mouseover/mouseout       // 如果给父元素绑定了over事件,那么这个父元素如果有子元素,每一次进入子元素也							   触发父元素的over事件
mouseenter/mouseleave = hover(function(){...})  //鼠标的悬浮
```

#### 事件冒泡

```js
<style>
        .outer{
            width: 300px;
            height: 300px;
            background-color: red;
        }
        .inner{
            width: 100px;
            height: 100px;
            background-color: forestgreen;
        }
    </style>

<body>
    <div class="outer">
        <div class="inner"></div>
    </div>
</body>

<script>
    $('.outer').click(
        function () {
            alert('outer')
        }
    )
    $('.inner').click(
        function (e) {
            alert('inner')
            // e.stopPropagation() //阻止事件冒泡方法1
            return false    //阻止事件冒泡方法2
        }
    )
</script>
```

#### 事件委托

```js
$('div').on('click','button',{'a':'b'},function (event) {
        console.log(event.data)
        alert('不许点')
    })
相当于把button元素的点击事件委托给了父元素div
后添加进来的button也能拥有click事件
```

### 页面的加载

```js
document.onload = function(){
   //js 代码 
}
window.onload = function () {
            $('button').click(function () {
            alert('不许点')
            })
}
onload要等到所有的文档 音视频都加在完毕才触发
onload只能绑定一次

//jquery的方式,只等待文档加载完毕之后就可以执行,在同一个html页面上可以多次使用
$(document).ready(
     function () {
       //文档加在完毕之后能做的事情 
})

//jquery的方式(简写)*****
$(function () {
	//文档加在完毕之后能做的事情        
  })

//示例
$(function () {
     $('button').click(function () {
          alert('不许点')
     })
})

//jquery中等待所有的资源包括视频 音频都加载完才执行function中的代码,可以使用多次
$(window).ready(
      function () {
           alert('123')
})
```

### each

```js
<body>
    <ul>
        <li>选项一</li>
        <li>选项二</li>
        <li>选项三</li>
    </ul>
</body>
<script>
    $('li').each(
        function (ind,dom) {      //主动传ind是每项的索引,dom是每一项的标签对象
            console.log(ind,dom)
        }
    )
</script>
```





小作业

1.form表单的验证提示信息用focus和blur事件完成

2.表格操作 使用事件委托

3.小米购物车

4.模态对话框