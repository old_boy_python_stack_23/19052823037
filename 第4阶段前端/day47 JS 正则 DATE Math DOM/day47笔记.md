### 今日内容

#### JS

##### 正则的用法

```js
创建一个正则:
var reg = RegExp('正则表达式')  //注意,写在字符串中所有带\的元字符都会被转义,应该写作\\
var reg2 = /正则表达式/  //内部的元字符就不会转义了
reg.test('待检测的字符串') //如果字符串中含有符合表达式规则的内容就返回true,否则返回false

在字符串中应用正则
var exp = 'alex3714'
exp.match(/\d/)    //只匹配从左到右的第一个
exp.match(/\d/g)   //匹配所有符合规则的 返回一个数组
var exp2 = 'Alex is a big sb'
exp2.match(/a/) //只匹配小写a
exp2.match(/a/i) //i表示不区分大小写 A也会被匹配出来
exp2.match(/a/ig) //不区分大小写并匹配所有

exp.search(/正则表达式/i) //不区分大小写,从exp字符串中找出符合条件的子串的第一个索引位置
exp.split(/正则表达式/i,n) //不区分大小写,根据正则切割,返回前n个结果
exp.replace(/正则/gi,'新的值') //i表示不区分大小写,g表示替换所有,将符合正则条件的内容替换成新的值

小问题1
var reg2 = /\d/g     //正则表示要匹配多个值
reg2.test('a1b2c3')  //多次test会的到true true true false 继续test会循环之前的结果

小问题2
var reg3 = /\w{5,10}/
reg3.test() //如果什么都不写,那么默认test中传递undefined参数,刚好可以符合9位字符串的规则
```

##### Date对象

```js
创建对象:
var dt = new Date() //获取到当前时间
dt.toLocalString() //转换成'2019/8/13 10:18:12'
dt.getFullYear() //年
dt.getMonth() //月 1月是0
dt.getday()   //周中天 周日是0
dt.getDate()  //月中天 1号是1
dt.getHours() //时 从0开始
dt.getMinutes() //分 从0开始
dt.getSeconds() //秒 从0开始

自定义时间:
var dt2 = new Date('2018/1/1 12:12:12')   1月1日
var dt2 = new Date(2018,1,1);             2月1日
```

##### Math对象

```

```

##### 面向对象(了解)

```js
function Student(name,age){
    this.stu_name = name
    this.stu_age  = age
}
Student.prototype.show = function(){
    console.log(this.stu_name,this.stu_age)
}
var stu = Student('alex',84) // 实例化
stu.stu_name  // 查看属性
stu.stu_age
stu.show()    // 调用方法

object.prototype.show = function(){
    console.log(this.stu_name,this.stu_age)
}
```

#### DOM

什么是dom

```
document object modle 文档对象模型
整个文档就是一颗树
树当中的每一个节点都是一个对象 : 这个对象中维系着 属性信息 文本信息 关系信息
在页面上还有一些动作效果:
	根据人的行为改变的 点击 鼠标悬浮
	自动在改变的
```

js是怎么给前端的web加上动作的呢?

```
1.找到对应的标签
2.给标签绑定对应的事件
3.操作对应的标签
```

##### 查找标签

###### 直接查找

```
var a = document.getElementById('baidu')           //直接返回一个元素对象
console.log(a)
var sons = document.getElementsByClassName('son')  //返回元素组成的数组
console.log(sons)
sons[0] //获取到一个标签对象  
var divs = document.getElementsByTagName('div')    //返回元素组成的数组
console.log(divs)
```

###### 间接查找

```
找父亲
var a = document.getElementById('baidu')          
console.log(a)
var foodiv = a.parentNode          //获取到父节点的对象

找儿子
var foo= document.getElementById('foo')
foo.children     //获取所有的子节点,返回一个数组
foo.firstElementChild //获取第一个子节点
foo.lastElementChild //获取最后一个子节点

找兄弟
var son1 = document.getElementById('son1')
console.log(son1)
var son2 = document.getElementById('son2')
console.log(son2)
son1.nextElementSibling     //找下一个兄弟 返回一个对象
son1.previousElementSibling //找上一个兄弟 返回一个对象
```

##### 操作本身的标签

###### 标签的创建

```
var obj = document.createElement('标签名')   // a div ul li span
obj就是一个新创建出来的标签对象
```

###### 标签的添加

```
父节点.appendChild(要添加的节点) //添加在父节点的儿子们之后
父节点.insertBefore(要添加的节点,参考儿子节点) //添加在父节点的某个儿子之前
```

###### 标签的删除

```
父节点.removeChild(要删除的子节点)
子节点1.parentNode.removeChile(子节点2)
```

###### 标签的替换

```
父节点.replaceChild(新标签,旧儿子)
```

###### 标签的复制

```
节点.cloneNode()     //只克隆一层
节点.cloneNode(true) //克隆自己和所有的子子孙孙
注意 :如果克隆出来的标签的id是重复的,那么修改之后才能应用到页面上
```

##### 节点的属性操作

```
节点对象.getAttribute('属性名')
节点对象.setAttribute('属性名','属性值')
节点对象.removeAttribute('属性名')
```

##### 节点的文本操作

```
节点对象.innerText = '只能写文字'
节点对象.innerHTML = '可以放标签'
```

##### 节点的值操作

##### 节点的类操作

#### BOM

操作的是浏览器