# CSS层叠了类选择器样式表

#### css选择器优先级

```css
行内>id选择器>类选择器>标签选择器>继承
1000  100    10       1       0
所有的值可以累加但是不进位
优先级如果相同,写在后面的生效
div.a{
    background-color: green !important; 提高样式的优先级到最高
}
```

#### 颜色表示

```css
rgb表示法:
	rgb :red green blue 光谱三原色
	rgb(255,255,255) 白色
	rgb(0,0,0) 黑色
16进制的颜色表示法
	#000000-#FFFFFF
	#000 - #FFF
单词表示法:
	# red green 
rgba表示法:
	#a表示的是透明度 0-1,0是完全透明,1是不透明
https://htmlcolorcodes.com/zh/yanse-ming/
```

#### 字体

```
p{
   font-family: 'Microsoft Yahei','楷体','仿宋';
   font-weight: bold;
   font-size: 12px;
}
```

#### 文本

```
text-align 文字的水平对齐
	left 左对齐 默认对齐方式
    center 居中
    reght 右对齐
text-decoration 文本装饰
    none;         没有下划线
    line-through; 中划线
    overline;     上划线
    underline;    下划线

text-indent 文本缩进
	text-indent: 2em; em单位是一个相对单位,相对当前字体大小的像素是1em

line-height	行高,设置行高=容器高度,文字自动垂直居中
	line-height: 200px;
	
color:设置字体颜色
```

#### 背景图片

```
background-color: red;   在没有背景图片覆盖的范围显示背景颜色
background-image: url('timg.jpg'); 设置背景图片
height: 400px;
width: 400px; 
background-repeat: no-repeat; 设置图片不重复   repeat-x水平重复   repeat-y垂直重复
background-position: right top; 图片的位置    左中右x 上中下y
/*left center right /top center bottom*/
background-attachment: fixed; 在窗口中固定图片的位置
background:red url("timg.jpg") no-repeat left center;  把所有的设置综合写在background中
```

#### 边框的设置

```
            width: 100px;
            height: 100px;
            /*border-color: tomato green gray yellow;*/
            /*border-width: 1px 3px 5px 7px;*/
            /*border-style: solid dotted dashed double;*/
            /*一个值:上下左右
            四个值:遵循顺时针上右下左
            三个值:上 右左 下
            两个值:遵循上下 左右
            */
            /*border-top-style:solid ;*/  单独设置顶线的样式(left,bottom,right)
            /*border-left-style:solid ;*/ 单独设置左边线的样式
            /*border-top-color:red;*/     单独设置顶线颜色
            border:yellow solid  10px;    统一设置边框的颜色 样式 宽度
```

#### 块和行内的概念\转换

```
对于行内标签来说不能设置宽和高
有些时候需要行内标签也设置宽和高,需要进行行内-->块,行内->行内块
display的属性值 : block块  inline行内 inline-block行内快 none
display: block;        独占一行并且可以设置宽高
display: inline-block;  既可以设置宽高又不会独占一行 行内\块转行内快
display: inline;        表示一个行内元素,不能设置宽高
display: none;          不仅不显示内容,连位置也不占了
```

#### 盒模型

```
border  : 边框的宽度
padding : 内边距的距离
content : width height
背景包含的部分:padding + conntent
计算一个盒子的总大小: 宽width+2padding+2border 高height+2padding+border
外边距 margin
    上下的盒子如果设置bottom和top会导致塌陷,取两个设置的最大值作为外边距
margin: 0 auto;表示左右居中(父盒子宽度,子盒子宽度)

分方向的设置
border-top border-right  border-bottom border-left
padding-top padding-right  padding-bottom padding-left
margin-top margin-right  margin-bottom margin-left

给图形设置圆角
border-radius: 5px;
```

#### 列表样式

```
在css中去掉列表的样式
ul,li{
   list-style: none;
}
```

#### 浮动

```
float:left /right
浮动的元素会脱离标准文档流,原本块级元素就不再独占整行了
并且在原页面中也不占位置了,之后的元素会挤到页面上影响页面的排版
清除浮动 clear:both
伪元素清除法:
        .clearfix:after{
            content: '';
            clear: both;
            display: block;
        }
<body>
<div class="father clearfix">
<div class="box"></div>
<div class="box"></div>
</div>
<div class="main">
    主页的内容
</div>
</body>
```

#### overflow

```
内容多余（溢出）标签的大小
visible 可见(默认)
hidden 超出部分隐藏
scroll 超出显示滚动条
```

#### 定位

```
position : relative /absolute /fixed
top:10px;
right:10px;
bottom:10px;
left:10px;
相对定位 :相对自己原来的位置移动,移动之后还占据原来的位置
绝对定位 :绝对定位是相对于整个html页面,不会占据原来的位置,层级的提升
		如果我们设置了绝对定位的元素 的父元素 没有设置position,那么我们对元素的所有设置都是基于整个页面
		如果设置了position,那么子盒子的absolute位置会根据父盒子的位置定位
		父相子绝:子元素设置的绝对位置会对应着祖级元素的相对位置
固定定位 :固定是相对于整个窗口的
```

#### z-index

```
用法说明:
z-index 值表示谁压着谁，数值大的压盖住数值小的，
只有定位了的元素，才能有z-index,也就是说，不管相对定位，绝对定位，固定定位，都可以使用z-index，而浮动元素不能使用z-index
z-index值没有单位，就是一个正整数，默认的z-index值为0如果大家都没有z-index值，或者z-index值一样，那么谁写在HTML后面，谁在上面压着别人，定位了元素，永远压住没有定位的元素。
从父现象：父亲的有优先级不高，儿子的优先级再高也没用。父亲怂了，儿子再牛逼也没用
```

#### opacity盒子透明度

```
opacity: 0.5;调整d4对应的整个标签的透明度是50%
.d4{
    opacity: 0.5;
}
<div class="d4">
    你好你好娃哈哈
	<img src="timg.jpg" alt="">
</div>
```