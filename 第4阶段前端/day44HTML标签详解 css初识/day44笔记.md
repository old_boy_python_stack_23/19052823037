今日内容

### html - 块级标签

#### 列表

##### ul 无序列表

```html
属性type:disc(默认)\square(实心方形)\circle(空心圆)\none(不显示样式)
<!--设置不显示任何样式-->
<ul type="none">
    <li>手机</li>
    <li>电脑</li>
    <li>其他电器</li>
</ul>
<!--设置显示实心方块-->
<ul type="square">
    <li>旧电脑</li>
    <li>旧家具</li>
    <li>旧手机</li>
</ul>
<!--设置显示空心圆-->
<ul type="circle">
    <li>旧电脑</li>
    <li>旧家具</li>
    <li>旧手机</li>
</ul>
<!--默认显示实心圆 disc-->
<ul>
    <li>旧电脑</li>
    <li>旧家具</li>
    <li>旧手机</li>
</ul>
```

##### ol 有序列表

```html
属性type:1(默认)\a(小写字母)\A(大写字母)\I(罗马数字)
属性start:开始位置
<!--默认显示1,2,3序号,可以指定开始的值-->
<ol>
    <li>长头发</li>
    <li>旧家电</li>
    <li>破剪刀</li>
</ol>
<!--设置根据a,b,c显示,可以指定开始的值-->
<ol type="a" start="2">
    <li>长头发</li>
    <li>旧家电</li>
    <li>破剪刀</li>
</ol>
<!--设置根据A,B,C显示,可以指定开始的值-->
<ol type="A" start="26">
    <li>长头发</li>
    <li>旧家电</li>
    <li>破剪刀</li>
</ol>
<!--设置根据罗马数字,可以指定开始的值-->
<ol type="I">
    <li>长头发</li>
    <li>旧家电</li>
    <li>破剪刀</li>
</ol>
```

##### dl 定义列表

```html
<dl>
    <dt>title</dt> <!--dt表示标题--> 
    <dd>alex</dd>  <!--dd表示内容--> 
    <dd>wusir</dd>
    <dd>太白</dd>
    <dt>title</dt>
    <dd>alex</dd>
    <dd>wusir</dd>
    <dd>太白</dd>
</dl>
```

#### 表格

```html
 标准表格:thead tbody的内容
 tr表示每一行
 th表示thead中的每一个元素
 td表示tbody中的每一个元素
 <table border="1" cellpadding="10px" cellspacing="5px">
        <thead>
            <tr>
                <th>第一列</th>
                <th>第二列</th>
                <th>第三列</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2">第一个值</td>

                <td>qqxing</td>
            </tr>
            <tr>
                <td>第二个值</td>
                <td>wahaha</td>
                <td rowspan="2">ab钙</td>
            </tr>
            <tr>
                <td>第二个值</td>
                <td>wahaha</td>

            </tr>
        </tbody>
    </table>
```

```html
 不写thead只写tbody 就没有表头样式
 <table border="1" cellpadding="10px" cellspacing="5px">
        <tbody>
            <tr>
                <td colspan="2">第一个值</td>

                <td>qqxing</td>
            </tr>
            <tr>
                <td>第二个值</td>
                <td>wahaha</td>
                <td rowspan="2">ab钙</td>
            </tr>
            <tr>
                <td>第二个值</td>
                <td>wahaha</td>

            </tr>
        </tbody>
    </table>
```



#### 排版标签 p div hr

```
p : 段落标签,会自动在段落上下加上空白来分开段落
p标签是一个文本级标签,内部不可以再嵌套块级元素
div:什么样式都没有的块级标签-- 用的最多
hr:单闭合标签,显示一条分割线
```

#### Form表单

```html
form : 表单标签
	action : 提交到的地址,把表单中的数据提交到对应的地址上
input :
	type种类:text,password,radio,checkbox,submit
    name\value :把数据提交到后台的,提交的是input标签的name值和value值
    对于选择框 : checked属性表示默认选中
    readonly :对于输入框的禁止修改 可以提交
    disabled :对于所有元素不能修改 也不能提交
button :
	input的submit类型和button放在form表单中都表示提交
<form action="http://127.0.0.1:9000">
    <input type="text" name='username' value = 'alexander' placeholder="请输入用户名或密码" readonly>
    <input type="password" name="password" value="3714" disabled>
    <!--注意:如果设置了readonly,只能给输入框的模式设置,那么不能修改,但是可以提交-->
    <!--注意:如果设置了disabled,可以给所有的表单元素设置,那么不能修改,也不能提交-->
    <input type="radio" name="sex" value="1" checked disabled> 男
    <input type="radio" name="sex" value="2" disabled> 女
    <!--checked 默认选项 -->
    <input type="checkbox" name="hobby" value="a" checked> 抽烟
    <input type="checkbox" name="hobby" value="b" checked> 喝酒
    <input type="checkbox" name="hobby" value="c"> 抠脚
    <input type="submit" value="表单按钮">
    <button>提交按钮</button>
    <!--注意 input的submit类型和button放在form表单中都表示提交 注意button的type不能是button-->
</form>
```

```html
input的其他type:
reset hidden button file date
注意: 如果是file类型的input标签需要修改表单的enctype类型Multipart/form-data
<form>
	<input type="hidden">
	<input type="reset">  
	<input type="button" value="普通按钮">
	<input type="file">
	<input type="date">
</form>
```

```html
lable:
     for属性,点击lable中的内容,让for标示的id对应的元素获得焦点
<label for="user">姓名 : </label>
<input type="text" name='username' id="user" >
```

```html
文本框:
<textarea name="" id="" cols="30" rows="10"></textarea>
```

```html
select选择框:
	multiple:设置多选框
    size:选择框显示大小
option选项:
	selected:默认选中
<select name="city" id="a" size="3">
        <option value="1" selected>北京</option>
        <option value="2">上海</option>
        <option value="3">天津</option>
</select>
<select name="" id="" multiple>
        <option value="" selected>抽烟</option>
        <option value="" selected>喝酒</option>
        <option value="" >烫头</option>
</select>
<!--注意:默认是单选-->
<!--注意:使用multiple属性设置为多选:按住鼠标下拉,按住ctrl选,按住shift选-->
```



### CSS介绍

现在的互联网前端分三层：

- - - HTML：超文本标记语言。从**语义**的角度描述页面**结构**。
    - CSS：层叠样式表。从**审美**的角度负责页面**样式**。
    - JS：JavaScript 。从**交互**的角度描述页面**行为**

  - CSS：Cascading Style Sheet，层叠样式表。CSS的作用就是给HTML页面标签添加各种样式，**定义网页的显示效果**。简单一句话：CSS将网页**内容和显示样式进行分离**，提高了显示功能。

css的最新版本是css3，**我们目前学习的是css2.1**

接下来我们要讲一下为什么要使用CSS。

#### HTML的缺陷：

- - 1. 不能够适应多种设备
    2. 要求浏览器必须智能化足够庞大
    3. 数据和显示没有分开
    4. 功能不够强大

#### CSS 优点：

- - 1. 使数据和显示分开
    2. 降低网络流量
    3. 使整个网站视觉效果一致
    4. 使开发效率提高了（耦合性降低，一个人负责写html，一个人负责写css）

  - 比如说，有一个样式需要在一百个页面上显示，如果是html来实现，那要写一百遍，现在有了css，只要写一遍。现在，html只提供数据和一些控件，完全交给css提供各种各样的样式。

### css样式

#### 样式的引入

```css
<body>
<!--引入方式一 :行内引入-->
<!--<div style="color:red">wahaha</div>-->
<body>
注意行内引入是在body标签内<_>
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!--引入方式二:内接引入-->
    <!--<style>-->
        <!--div{-->
            <!--color: blue;-->
        <!--}-->
    <!--</style>-->

    <!--引入方式三: 外接样式-链接式 :link导入一个连接 重点重点重点*****-->
    <!--<link rel="stylesheet" href="index.css">-->
	
    <!--引入方式四: 外接样式-导入式 :style导入一个@import url('路径')-->
    <!--<style>-->
        <!--@import url("index.css");-->
    <!--</style>-->
	<!--区别：使用链接方式时，会在装载页面主体部分之前装载css文件，这样显示出来的网页从一开始就是带有样式。而导入式，会在整个页面装载完成后再装载css文件，对于有的浏览器来说，在一些情况下，如果网页文件的体积比较大，则会出现先显示无样式页面，闪一下之后再出现设置样式后的效果。-->
</head>
```

####  最简单的基本样式

```css
<style>
        div{
            color: red;
            width: 200px;
            height: 200px;
            background-color: yellow;
             /*background-color: yellow; 注释css样式*/ 
        }
</style>
```

####  选择器

##### 基本选择器

```css
标签\类\id\通用选择器:
标签名直接选择标签,#选择id, .表示class,*表示所有标签
<style>
标签选择器：
        span{
            color:green
        }
        div{
            color: #c4db69
        }
        a{
            color: red;
        }


1.样式的继承 : 子元素会继承父元素的样式(但是a标签除外)
2.要想对a标签进行样式设置,必须直接找到a标签的位置,对a单独设置
3.样式之间的重叠部分是有优先级的,继承下来的样式的优先级为0(最低)

id选择器：
#div1{
   color: cornflowerblue;
}
类选择器：
.sp{
     color: orangered;
}

通用选择器:所有的标签都会被选中
*{
   color: yellow;
}
</style>
```

##### 高级选择器

- 后代

```css
后代选择器: 找的是子子孙孙
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
       div span{
           color: tomato;
        }
    </style>
</head>
<body>
    <div>
        我是div标签的content
        <span>西红柿色1</span>
        <p>
            在div-p标签中
           <span>西红柿色2</span>
        </p>
    </div>
    <span>我只是一个单纯的span标签</span>
</body>

子代选择器>：支找儿子这一代的
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
       div>span{
           color: tomato;
        }
    </style>
</head>
<body>
    <div>
        我是div标签的content
        <span>西红柿色1</span>
        <p>
            在div-p标签中
           <span>西红柿色2</span>
        </p>
    </div>
    <span>我只是一个单纯的span标签</span>
</body>
```

```css
毗邻选择器：+  和我同级并且离我最近的兄弟
    <style>
       span+a{
           color: tomato;
        }
    </style>
    <div>
        <a>我是a0标签</a>
        <span>span标签</span>
        <a>我是a1标签</a>  <!--变色-->
        <a>我是a2标签</a>
    </div>
弟弟选择器：~  所有和我同级的兄弟们
    <style>
       span~a{
           color: tomato;
        }
    </style>
    <div>
        <a>我是a0标签</a>
        <span>span标签</span>
        <a>我是a1标签</a>   <!--变色-->
        <a>我是a2标签</a>   <!--变色-->
    </div>
```



```css
属性选择器 [属性]/[属性='值']
<style>
       /*a[href]{*/
            /*color: green;*/
        /*}*/
        /*a[href='http://www.taobao.com']{*/
            /*color: lightpink;*/
        /*}*/
        input[type='text']{
            background-color: lightblue;
        }
 </style>
 <body>
     <div>
        <a href="http://www.taobao.com">我是a0标签</a>
        <span>span标签</span>
        <a href="http://www.jd.com">我是a1标签</a>
        <a href="http://www.mi.com">我是a2标签</a>
        <a>没有href属性</a>
    </div>
    <input type="text">
    <input type="password">
 </body>

```

```css
并集,\交集

<style>
    ul,ol,span{
       background-color: gainsboro;
    }
</style>
<body>
<ul>
    <li>u-first</li>
</ul>
<ol>
    <li>o-first</li>
</ol>
</body>

交集选择器
<style>
    div.box1.box2{
            background-color: red;
            width: 200px;
            height: 200px;
    }
</style>
<body>
    <div class="box1 box2">box1box2</div>
    <div class="box1">box1</div>
    <div>aaa</div>
    <span class="box1">span标签</span>
</body>
```

```css
伪类选择器
a:link {color: #FF0000}		/* 表示没有被访问的a标签的样式 */
a:visited {color: #00FF00}	/* 表示访问过后的a标签的样式 */
a:hover {color: #FF00FF}	/* 鼠标移动到链接上 */
a:active {color: #0000FF}	/* 表示鼠标摁住的时候a标签的样式 */
input: focus /* 光标聚集时的操作 */
通用: hover 
<style>
        a:link{
            color:tomato;
        }
        a:visited{
            color: gray;
        }
        a:active{
            color: green;
        }
        input:focus{
            background-color: aquamarine;
        }
        div{
            width: 100px;
            height: 100px;
        }
        div:hover{
            background-color: pink;
        }
</style>
<body>
<a href="http://www.jd.com">京东</a>
<a href="http://www.xiaohuar.com">校花</a>
<input type="text">
<div></div>
</body>
```

```css
伪元素选择器
first-letter /*只能用于行级元素 是第一个字母的样式*/
xx：before    /*在xx标签之后进行操作 添加content：“xxx” xxx的内容 样式是xxx的样式*/
xx：after     /*在xx标签之前进行操作 添加content：“xxx” xxx的内容 样式是xxx的样式*/

<style>
        p:first-letter{
            color: green;
        }
        p:before{
            content: '**';
            /*color: pink;*/
        }
        p:after{
            content: '.....';
            color: lightblue;
        }
</style>
<body>
<div><p>春江水暖鸭先知</p></div>
</body>
```

