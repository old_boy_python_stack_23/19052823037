# bootstrap



### 一、bootstrap的作用

就是已经把class给写好，我们要做的就是把那些class记住干嘛用的，直接使用就可以了，

最早起源于twitter,bs2全部浏览器兼容。bs3ie7以上兼容，ie7及ie7以下都不兼容。依赖jquery。

### 二、排版

#### 主标题：

或者`<div class="h1"></div>`

H1：36px

H2: 30px

H3: 24px

H4: 18px

H5: 14px

H6: 12px;

#### 副标题：

`<small></small>`

#### 突出某一块的内容

`<p>我是一块内容<span class="lead">要突出的部分</span></p>`

`<p>这是一段文字，要<strong>粗体的部分</strong></p>`

`<i>斜体</i><em>斜体</em>`

#### 字体颜色

`<p class="text-muted">字体减弱效果</p>`

`<p class="text-primary">字体变蓝效果</p>`

`<p class="text-success">字体变绿的效果</p>`

`<p class="text-danger">字体变红的效果</p>`

`<p class="text-info">字体变深蓝的效果</p>`

`<p class="text-warning">字体变屎黄色的效果</p>`

#### 字体的大小写

`.text-uppercase` 英文字体大写

`.text-lowercase`英文字体小写

#### 缩略语

 `<p><abbr class='text-danger' title='这个是口头语'>我去！</abbr>你到底去不去!</p>`

#### 引用

`<blockquote><p>这是一段引用的话<footer class='text-right'>引用谁的话<cite>倾斜</cite></footer></p></blockquote>`

#### 图片

`.img-rounded`图片的圆角

`.img-circle`50%的圆角

`.img-thumbnail`加个照片的框

#### 列表样式

##### 去掉li的默认样式

```html
<ul class="list-unstyled">
<li>我是第一条</li>
<li>我是第二条</li>
<li>我是第三条</li>
</ul>
```

##### 内联列表

列表横着显示

```html
<ul class="list-unstyled list-inline">
    <li>我是内联列表第一条</li>
    <li>我是内联列表第二条</li>
    <li>我是内联列表第三条</li>
</ul>
```

##### 水平定义列表

```html
<dl class="dl-horizontal">
    <dt>我：</dt>
    <dd>下午考试</dd>
    <dt>所有人：</dt>
    <dd>特别开心</dd>
</dl>
```

##### 贴代码段

```html
<code>我是一行代码</code>
<pre>我是一段代码</pre>

<pre> 多行代码段
    <p class="text-info">获取id<small>--获取用户的id标签</small></p>
    documentElementById()
    <p class="text-info">获取class</p>
    documentElementsByClass()
</pre>

<pre>
    &lt;div&gt;我是一个div&lt;/div&gt;
</pre>

<kbd>ctrl+c</kbd> 黑色风格代码块
```

##### 文字的对齐方式

```html
<p class="text-left">左对齐</p>
<p class="text-center">中间对齐</p>
<p class="text-right">右对齐</p>
```

#### 背景色

`.bg-primary`蓝色

`.bg-success` 绿色

`.bg-info`浅蓝色

`.bg-danger`红色

`.bg-warning`屎黄色

### 三、容器

`.container` 类用于固定宽度并支持响应式布局的容器。左右留白

`.container-fluid` 类用于 100% 宽度，占据全部视口（viewport）的容器。

### 四、表格

 `.table-striped` 类可以给 `<table>` 之内的每一行增加斑马条纹样式。

`.table-bordered` 类为表格和其中的每个单元格增加边框。

`.table-hover` 类可以让 `<tbody>` 中的每一行对鼠标悬停状态作出响应。

`.table-condensed` 类可以让表格更加紧凑，单元格中的内补（padding）均会减半

`.table-responsive` 响应式表格

状态就是背景颜色

 `active` 点击的状态灰色

`.success`成功的绿色

`.info`信息蓝色

`.danger`红色危险

`show/hide` 显示和隐藏

### 五、按钮

`.btn`按钮的颜色

`.btn-default`按钮的形状

想让一个a标签变成一个按钮

`<a href="#" class="btn btn-default" role="button">按钮</a>`

`<button class="btn btn-default" type="submit">按钮</button>`

按钮颜色

`.btn-success`绿色

`.btn-danger`红色

`.btn-warning`屎黄色

`btn-link` 直接变成链接

 `.btn-primary`蓝色

按键尺寸

`.btn-lg`大 `.btn-sm`小  `.btn-xs`超小 `.btn-block`变成块级，宽度是父级的100%

按钮的状态

`.active`按钮被按下的颜色

`disabled`按键禁用

### 六、栅格系统

把屏幕分成了12等份，可以通过div，让他变成其中的几等份。

响应式，响应了几个状态，屏幕的宽度

超小的屏幕 手机(<768px) `col-xs-*`

小屏幕 平板(>=768px) `col-sm-*`

中等屏幕 笔记本(>=992px) `col-md-*`

大屏幕 大桌面 (>=1200px) `col-lg-*`

响应式就是根据在每种尺寸不同的屏幕上，都规定了占的份数

```html
<div class="row">
  <div class="col-xs-12 col-sm-6 col-md-8 col-lg-5">A</div>
  <div class="col-xs-6 col-sm-6 col-md-4 col-lg-7">B</div>
</div>
上面的代码说明在一行内：
1、在xs也就是手机屏幕上，A独占一行
2、在sm 也就是平板屏幕上，A、B各占6份
3、在md 也就是笔记本状态下，A占8份，B占4份
4、在lg 也就是大桌面的情况下 A占5份，B占7份
```

### 七、表单

首先是一个`<form></form>`在这个里面嵌套一个`<div class='form-group'></div>`给他分组，方便给状态，然后在这个`div`里再写一个`label`和`input`在`input`里添加控制类`.form-control`

```html
<form class="container">
  <div class="form-group">
    <label for="">用户名：</label>
    <input type="text" class="form-control">
  </div>
</form>
```

注意`label`中包裹`input`会造成不能占用父的宽度的100%，必须不能包裹，分开写

复选框的父级标签的`div` `class为` `.checkbox` 这个可以包裹

```html
<div class="checkbox">
  <label>
    <input type="checkbox">点我有惊喜！
  </label>
</div>
```

单选框的父标签的`div`  `class`为`.radio`

禁止点击在`div`里类再添加一个`disabled`

```html
<div class="radio disabled">
        <label><input type="radio" name="a">星期一</label>
    </div>
    <div class="radio">
        <label><input type="radio" name="a">星期二</label>
    </div>
```

#### 表单的一行显示

只要在最外面的`form`中添加类`.form-inline`就可以了，所有的标签都在一行内显示。

#### 文字在同一个`Inut`框中

```html
<form action="" class="container">
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-addon">用户名：</div>
            <input type="text" class="form-control">
        </div>
    </div>
</form>
```

#### 文字和`input`框在同一行

和之前不同的是，一是`label`里添加`col-xx control-label`,再就是给input再增加一个父级`<div class='col-xxx'>`

```html
<form action="" class="container">
    <div class="form-group">
        <label for="" class="control-label col-sm-2 col-md-2">用户名：</label>
        <div class="col-md-10 col-sm-10">
            <input type="text" class="form-control">
        </div>
    </div>
</form>
```

#### `input`框内加图标文字

就是在`form-group`的`div`的类名里增加一个类`has-feedback`标签内再增加一个`span`标签，类名用字体图标的类名，和类名`form-control-feedback`

```html
<form action="" class="container">
    <div class="form-group has-feedback">
        <label for="">密码</label>
        <input type="text" class="form-control">
        <span class="glyphicon glyphicon-qrcode form-control-feedback" aria-hidden="true"></span>
    </div>
</form>
```

#### 状态

在分组里加，如果想让颜色不一样，就再添加个分组

`has-success`绿色边框

`has-error`红色

`has-warning`屎黄色

### 八、常用的组件

#### 带图标的按钮

就是一个按钮里再放一个`span`他的类用字体图标。

```html
<button type="button" class="btn btn-default btn-success">
    <span class="glyphicon glyphicon-hand-up"></span>
    我是一个按键呀
</button>
```

这里注意：如果只是单纯的装饰用`aria-hidden="true"`，如果还需要给这个图标比如加事件等操作的时候，就要设为`false`

#### 下拉菜单

最外面的div要有`.dropdown`，下面包裹一个`button`，除了有`button`的类名外，还需要增加一个`dropdown-toggle`，还需要多一个属性`data-toggle="dropdown"`，设置一个id，方便和平级的`ul`的`aria-labelledby`= "id名"进行关联。包裹一个`<span class="caret"></span>`

平级添加一个`ul`>`li` `ul`类名为`dropdown-menu`

```html
<div class="container">
    <div class="dropdown">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="dd">
            下拉菜单
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dd">
            <li><a href="#">星期一</a></li>
            <li><a href="#">星期二</a></li>
            <li><a href="#">星期三</a></li>
            <li><a href="#">星期四</a></li>
        </ul>
    </div>
</div>
```

向上的弹出，就是把最外面`div`的类`dropdown`改为`dropup`就改变了方向，想让下拉的选项,向右突出缩进，就是给`li`增加一个类`.dropdown-header`

##### 分隔线

`<li role="separator" class="divider"></li>`

##### 禁用某个选项

`<li class="disabled"></li>`

#### 横向按钮组

```html
<div class="container">
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-default"><a href="#">左</a></button>
        <button type="button" class="btn btn-default"><a href="#">中</a></button>
        <button type="button" class="btn btn-default"><a href="#">右</a></button>
    </div>
</div>
```

#### 按钮组的尺寸

直接给按键组增加一个尺寸类

`<div class="btn-group btn-group-sm" role="group">`

`.btn-group-lg`大

`.btn-group-sm`中

`.btn-group-xs`小

#### 横向按钮组带下拉菜单

```html
<div class="h1 text-warning">横向菜单带下拉框</div>
<div class="btn-group btn-group-lg">
    <button class="btn btn-default"><a href="#">131313A</a></button>
    <button class="btn btn-default"><a href="#">424224B</a></button>
    <button class="btn btn-default"><a href="#">1414141D</a></button>
    <div class="btn-group btn-group-lg" role="group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="tt">1313131F
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#">星期一</a></li>
            <li><a href="#">星期一</a></li>
            <li><a href="#">星期一</a></li>
        </ul>
    </div>
</div>
```

纵向的只要在最外面的div里加上类`.btn-group-vertical`

#### 徽章

可以在a标签内嵌套一个`span`也可以在`button`里嵌套一个`span`标签，`span`的类为``

```html
<div class="container">
    <a href="#">我是a标签的徽章<span class=badge>40</span></a>
</div>
<div class="container">
    <button type="button" class="btn btn-default btn-success">我是一个按钮<span class="badge">10</span></button>
</div>
```

#### 进度条

最外层`.progress`	包裹`span`类名`sr-only`40%，父级div 类名`progress-bar`

```html
<div class="container">
    <div class="progress">
        <div class="progress-bar" style="width: 0%; min-width:2em;" >0%
            <span class="sr-only"></span>
        </div>
    </div>
</div>
```

条纹 `.progress-bar-striped`

颜色 `.progress-bar-success`

条纹动起来`.active`

##### 同一行中有多种形态的进度条

```html
<div class="container">
    <div class="progress">
        <div class="progress-bar progress-bar-success" style="width: 20%; min-width: 2em;">
            20%
            <span class="sr-only"></span>
        </div>
        <div class="progress-bar progress-bar-warning" style="width: 30%; min-width: 2em;">
            30%
            <span class="sr-only"></span>
        </div>
        <div class="progress-bar progress-bar-danger active progress-bar-striped" style="width: 30%; min-width: 2em;">
            30%
            <span class="sr-only"></span>
        </div>
    </div>
</div>
```

#### 导航标签页

`ul`的类`.nav .nav-tabs` 包裹着`li`普通的

`.nav-pills`胶囊

将横状的导航变成竖状的`.nav-stacked`

```html
<div class="h2">导航栏</div>
<div class="container">
    <ul class="nav nav-pills">
        <li class="active"><a href="javascript:void(0)">Home</a></li>
        <li><a href="javascript:void(0)">Case</a></li>
        <li><a href="javascript:void(0)">About Me</a></li>
    </ul>
</div>
```

jquery

```html
<script >
    $(".nav-pills li").on({"click":function () {
            $(".nav-pills li").removeClass("active");
            $(this).addClass("active");
        }});
</script>
```

带下拉框的导航

```html
<div class="container">
    <ul class="nav nav-tabs">
        <li><a href="javascript:void(0)">Home</a></li>
        <li><a href="javascript:void(0)">Case</a></li>
        <li class="dropdown">
            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                About Me
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu text-info">
                <li><a href="javascript:void(0)">骄傲单身狗</a></li>
                <li><a href="javascript:void(0)">骄傲单身狗</a></li>
                <li><a href="javascript:void(0)">骄傲单身狗</a></li>
            </ul>

        </li>
    </ul>
</div>
```

固定在头部的菜单

需要在最外面加一个`nav`标签，类名添加`.navbar .navbar-fixed-top`， 在底部就是`.navbar .navbar-fixed-to`

反色(黑白色)

`.navbar-inerse`

#### 标签加提醒

```html
<div class="h3">我是一个好人<span class="label label-danger">New</span></div>
```

#### 警告框

```html
<div class="container">
    <div class="alert alert-danger">你已经成功的登陆了</div>
</div>
```

警告框中的链接

```html
<div class="container">
    <div class="alert alert-success">
        你在账号在别处登陆了
        <a href="#" class="alert-link">点击查看</a>
    </div>
</div>
```

### 九、插件

#### 模态框

主要就是按钮和模态框的关联是通过按钮的这两个属性`data-toggle="modal" data-target="#c1"`和模态框进行关连的。

```html
<button type="button" class="btn btn-default btn-success" data-toggle="modal" data-target="#c1">按钮</button>

<div class="modal fade" tabindex="-1" role="dialog" id="c1">
    <form action="" method="post">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">用户登陆</h4>
            </div>
            <div class="modal-body clearfix">

                    <div class="form-group clearfix">
                        <label for="" class="col-sm-2 control-label">用户名：</label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="请输入用户名" class="form-control">
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label for="" class="col-sm-2 control-label">密码：</label>
                        <div class="col-sm-10">
                            <input type="password" placeholder="请输入密码" class="form-control">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="submit" class="btn btn-primary">提交</button>
            </div>
        </div><!-- /.modal-content -->
    </div>
    </form>
    <!-- /.modal-dialog -->
</div><!-- /.modal 
```