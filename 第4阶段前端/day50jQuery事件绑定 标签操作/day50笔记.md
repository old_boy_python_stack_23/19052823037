### （上次内容补充）

```js
// 1.不要用for(i in li_list){}的方式循环一个jq对象
for(let i=0;i<li_list.length;i++){   //let 声明的变量的作用域只在for循环中
        console.log(i)
}
```

### 今日内容

```
事件的绑定
jquery操作标签
	操作文本 : <>文本内容<>
	操作标签 : 添加 删除 修改 克隆 
	操作属性 : 通用属性 类 css样式 value 盒子模型 滚动条  
```

#### 事件的绑定

```js
<body>
    <button>点击1</button>
    <button>点击2</button>
</body>

<script src="jquery.3.4.1.js"></script>
<script>
    $('button').click(
        function () {
            alert('你点了我一下!')
        }
    )
</script>
```

#### 标签的文本操作

```js
text()
$('li:first').text()         // 获取值
$('li:first').text('wahaha') // 设置值

$('li:last').html()          // 获取值
$('li:last').html('qqxing')  // 设置值

$('li:first').html('<a href="http://www.mi.com">爽歪歪</a>')   //a标签

var a = document.createElement('a')
a.innerText = '我是AD钙'
$('li:last').html(a)     //a 是dom对象

var a2 = document.createElement('a')
var jqobj = $(a2)
jqobj.text('乳娃娃')
$('li:last').html(jqobj)   //jqobj是jquery对象
```

#### 标签的操作

##### 增加

```
父子关系:
	追加儿子 :(父).append(子) (子).appendTo(父)
	头部添加 :(父).prepend(子) (子).prependTo(父)
兄弟关系:
	添加哥哥 :参考点.before(要插入的)     要插入的.insertbefore(参考点)
	添加弟弟 :参考点.after(要插入的)      要插入的.insertAfter(参考点)
如果被添加的标签原本就在文档树中,就相当于移动
```



```js
例子append
    var li = document.createElement('li')
    var jq = $(li).text('张艺兴')
    $('ul').append(jq)
    
    var li = document.createElement('li')
    var jq = $(li).text('张艺兴')
    $('ul').append(jq[0])
   
    var li = document.createElement('li')
    var jq = $(li).text('张艺兴')
    $('ul').append('<li>鹿晗</li>')
    
例子appendTo
	var li = document.createElement('li')
    var jq = $(li).text('张艺兴')
    jq.appendTo('ul')
    
    var dom_ul = document.getElementsByTagName('ul')[0]
    var li = document.createElement('li')
    var jq = $(li).text('张艺兴')
    jq.appendTo(dom_ul)
    
    var li = document.createElement('li')
    var jq = $(li).text('张艺兴')
    jq.appendTo($('ul'))
   
对已经存在的内容进行添加 -- 移动
   $('li:first').appendTo('ul')
```



```js
$('li:last').prependTo('ul')
$('ul').prepend('<li>李东宇</li>')
$('<li>邓帅</li>').prependTo('ul')
```



```javascript
$('#l2').before('<li>李东宇</li>')
$('<li>李东宇222</li>').insertBefore('#l2')

$('#l2').after('<li>邓帅</li>')
$('<li>邓帅222</li>').insertAfter('#l2')
```

##### 删除 修改 克隆

```js
删除 : remove detach empty
remove : 删除标签和事件,被删掉的对象做返回值
detach : 删除标签,保留事件,被删掉的对象做返回值
empty : 清空内容标签,自己被保留

修改 : replaceWith replaceAll
replaceWith : a.replaceWith(b)  用b替换a
replaceAll : a.replaceAll(b)    用a替换b

复制 : clone
var btn = $(this).clone()      //克隆标签但不能克隆事件
var btn = $(this).clone(true)  //克隆标签和事件
```

```js
var obj = $('button').remove()
obj是button标签,但是事件已经被删除了

var  a = document.createElement('a')
a.innerText = 'wahaha'
$(a).replaceAll('p')
$('p').replaceWith(a)
```

#### 标签的属性操作

##### 通用属性

```
attr
获取属性的值
$('a').attr('href')
设置/修改属性的值
$('a').attr('href','http://www.baidu.com')
设置多个属性值
$('a').attr({'href':'http://www.baidu.com','title':'baidu'})

removeAttr
$('a').removeAttr('title') //删除title属性
```

```
如果一个标签只有true和false两种情况,适合用prop处理
$(':checkbox:checked').prop('checked') //获取值
$(':checkbox:checked').prop('checked',false) //表示取消选中
如果设置/获取的结果是true表示选中,false表示取消选中
```

#### 类的操作

```
添加类   addClass
$('div').addClass('red')        //添加一个类
$('div').addClass('red bigger') //添加多个类

删除类   removeClass
$('div').removeClass('bigger')  //删除一个类
$('div').removeClass('red bigger')

转换类   toggleClass             //有即删 无即加
$('div').toggleClass('red')      
$('div').toggleClass('red bigger')
```

#### value值的操作

```
$(input).val()
$(':text').val('值')
$(':password').val('值')

对于选择框 : 单选 多选 下拉选择
设置选中的值需要放在数组中 : 
	$(':radio').val([1])
	$(':radio').val([1,2,3])
```

#### css样式

```
css('样式名称','值')
css({'样式名1':'值1','样式名2':'值2'})

$('div').css('background-color','red')           //设置一个样式
$('div').css({'height':'100px','width':'100px'}) //设置多个样式
```

#### 滚动条

```
scrollTop()
scrollLeft()

$(window).scrollLeft()
$(window).scrollTop()
```

#### 盒子模型

```
内容宽高 : width和height

内容+padding : innerWidth和innerHeight

内容+padding+border :outerWidth和outerHeight

内容+padding+border+margin : outerWidth(true)和outerHeight(true)

设置值:变得永远是content的值
```

#### 表单操作

```
$(':submit').click(
    function () {
        return false
    }
)
如果返回false不提交
如果返回true不提交
```