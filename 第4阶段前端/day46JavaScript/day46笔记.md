# JavaScript初识

#### javascript介绍

```
 能够处理逻辑 
​ 可以和浏览器交互
​ 不够严谨
```

```
javascript包含:
	ECMAscript js的一种标准化规范 标出了一些基础的js语法
	DOM document object model 文本对象模型 主要操作文档中的标签
	BOM browser object model 浏览器对象模型 主要用来操作浏览器
```

#### js引入和script标签

```
方式一:在html页写js代码
    <script>
        alert('hello,world')
    </script>
方式二: 引入js文件
     <script src="first.js"></script> 
```

#### 结束符和注释

```js
结束符
; 是js代码中的结束符

单行注释
// alert('hello,world');
多行注释
/*多行注释*/

```

#### 变量

```
变量名 : 数字\字母\下划线\$
创建变量的关键字var :var a = 1;
创建变量的时候可以不指定数据类型
创建变量但不赋值 :var a;   a创建出来就是一个undefined未定义
```

#### 输入输出

```js
弹出框alert
	alert('hello') 弹出框中的内容是"hello"
弹出输入框
	var inp = prompt('问句') 弹出输入框,输入的内容会被返回给inp
控制台输出
    console.log(变量或值)
```

#### 基础的数据类型

##### 查看类型

```js
typeof 变量;
typeof(变量);
```

##### 数字number

```
整数 var a = 1
小数 var b = 1.237
保留小数 b.toFixed(2)  //1.24
```

##### 字符串String

```
var s1 = '单引号'
var s2 = '双引号都是字符串的表达方式'
```

###### string 类型的常用方法

```
属性 : .length
方法:
.trim() 去空白
a.concat('abc') a拼接'abc'串
.charAt(索引)  给索引求字符
.indexOf(字符) 给字符求索引
.slice(start,end) 顾头不顾尾,可以用负数,取子串
.toLowerCase()   全部变小写	
.toUpperCase()   全部变大写
.split(',',2)    根据(第一个参数)分隔符切割,切前多少个结果
```

##### Boolean 布尔值

```js
Boolean()
true  : [] {} 
false : undefined null NaN 0 '' 
```

##### null 空和undefined未定义

```
null 表示空  boolean值为false
undefined 没有定义 创建变量但是不赋值 boolean值为false
```

#### 内置对象类型

##### 数组 Array

```js
创建:
var arr = ['a','b','c'];
var arr2 = new Array();
索引操作:
arr[0] 查看
arr2[0] = 'alex' 赋值操作
```

###### Array常用的属性和方法

```js
属性:length
方法:
.push(ele)	尾部追加元素
.pop()	弹出并返回尾部的元素
.unshift(ele)	头部插入元素
.shift()	头部移除元素

.slice(start, end)	切片//直接切出来对象里面的元素没了
.reverse() //在原数组上改的	反转
.join(seq) //a1.join('+')，seq是连接符	将数组元素连接成字符串
.concat(val, ...) //连个数组合并,得到一个新数组，原数组不变	连接数组
.sort()   //排序
.splice() //参数：1.从哪删(索引), 2.删几个  3.删除位置替换的新元素(可多个元素)	删除元素，并向数组添加新元素。
```

##### 自定义对象



#### 数据类型之间的转换

```js
string --> int
	parseInt('123') //123
	parseInt('123abc') //123
	parseInt('abc') //NaN  not a number
string --> float
	parseFloat('1.233') 
float/int --> String
	var num = 123
	String(123)
	var str = num.toString()
任意类型 --> Boolean
	Boolean(数据)

字符串和数字相加 --> 字符串
字符串和数字相减 --> 数字
```

#### 运算符

##### 赋值运算符

```
= += -= *= /= %=
```

##### 比较运算符

```
> < >= <= 
== !=  不比较类型
===  !== 比较类型和值(常用)
```

##### 算数运算符

```
+ - * / % ** 

++ --
var a = 1
undefined
var b = a++    // a=2  b=1
var c = ++a    // a=3  c=3
```

##### 逻辑运算符

```js
&& 逻辑与  ||逻辑或  !逻辑非
true && true //true and
true || false //true or
!true        //false not
```

#### 流程控制

```
特点:
所有的代码块都是用{}标识的
所有的条件都是用()标识的
```

##### 条件判断

###### if语句

```js
if (true) {
   //执行操作
}else if(true){
    //满足条件执行            
}else if(true){
   //满足条件执行        
}else{
  //满足条件执行
}
```

###### case语句

```js
//case表示一个条件 满足这个条件就会走进来 遇到break跳出。如果某个条件中不写 break，那么直到该程序遇到下一个break停止
var err_type = 'info'
    switch(err_type) {
        case 'warining':
            console.log('警告');
            break;
        case 'error':
            console.log('错误');
            break;
        default:
            console.log('没错')
    }
```

##### 循环语句

###### while

```js
var i = 1; //初始化循环变量
while(i<=9){ //判断循环条件
    console.log(i);
    i = i+1; //更新循环条件
}
```

###### for

```js
//方式一:
for(var i = 1;i<=10;i++){
        console.log(i)
}

//方式二:
var arr = [1,2,3,4,5]
for (n in arr){
        console.log(n)
}
```

三元运算符

```js
var 结果 = boolean表达式 ? 为true返回的值:为false返回的值 
```

#### 函数

```js
function 函数名(参数){
    函数体
    return 返回值
}
函数名(参数)
//注意 : 传递的参数可以和定义的个数不一样,但是不要这么写
//      返回值只能有一个
```

```js
//arguments伪数组
function add(){
    console.log(arguments);
}
add(1,2,3,4)

function add(a,b){
    console.log(arguments);
}
add(1,2,3,4)
```

###### 匿名函数

```js
var add = function(){
    console.log('hello,world');
} 
//add()调用
```

###### 自调用函数

```js
(function(a,b){
    console.log(a,b)
})(1,2)
```

