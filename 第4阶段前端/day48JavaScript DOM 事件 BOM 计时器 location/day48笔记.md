### 内容回顾

#### js的基础语法

##### Math

```
floor   ceil   max    min   random   abs    round
```

##### Date

```
创建Date对象 new Date()   得到的当前时间的对象
对象.tolocalstring()     '2019/8/14 8:41'
对象.getFullYear()        2019
对象.getMonth()           7 0-11
对象.getDate()            14 1-31
对象.getDay()             3  0-6
对象.getHours()           8
对象.getMinutes()         41
对象.getSeconds()         23

自己创建任意时间对象
new Date('2018/7/14 10:23:56')
new Date('7/14/18 10:23:56')
```

##### RegExp

```
创建一个正则对象
reg = new RegExp('\\d')
reg2 = /\d/
reg.test(待匹配的字符串)  # 匹配true 不匹配返回false

# i表示不区分大小写    g表示匹配所有符合条件的项
string.search(/\d/i)    # 查找第一个符合正则要求的子串的索引位置
string.match(/\d/gi)    # 可以查所有符合条件的项
string.replace(/正则/g,新的值) # 用新的值替换正则匹配到的项
string.split(/正则/)    # 默认就全部切割
```

##### 面向对象(了解)

```
function 类名(参数){
    this.属性名 = 值;
}
类名.prototype.方法名 = function(参数){
    this表示self
}
var obj = new 类名(参数)
obj.方法名(参数)
obj.属性名
```

#### DOM

##### dom树

```
整个文档就是一棵树
整个文档是从上到下依次加载的 
当加载到script标签的时候,会有一个特殊的变量提升的解析方法,导致我们后定义的函数可以提前被调用
每一个节点描述 : 父标签 子标签 兄弟节点
               属性
               文本
               标签名
```

##### 查找节点

###### 直接查找 

```
var 节点对象 = document.getElementById('标签的id')   返回一个
var 节点对象 = document.getElementsByClassName('标签的类名') 返回一组
var 节点对象 = document.getElementsByTagName('标签名')  返回一组
```

###### 间接查找

```
找父亲   节点.parentNode 返回一个
找兄弟   节点.previousElementSilblings 返回上一个    节点.nextElementSilblings 返回下一个
找儿子   节点.firstElementChild    节点.lastElementChild
		节点.children 返回一组
```

##### 操作节点

```
标签的创建  节点对象 = document.createElement('标签名')
添加标签      父节点.appendChild(子节点)     放在所有儿子的最后  
             父节点.insertBefore(子节点,参考儿子节点) 放在参考点之前
删除节点      父节点.removeChild(子节点)
替换         父节点.replaceChild(新的节点,旧)
克隆         节点.cloneNode(true)   true表示拷贝这个标签的子子孙孙
```

##### 文本操作

```
innerText   只识别文本,所有的标签也当做文本识别
innerHtml   可以识别标签
```

##### 属性操作

```
节点对象.getAttribute('属性名')
节点对象.setAttribute('属性名','属性值')
节点对象.removeAttribute('属性名')
```

```
节点对象.classList.add('类名')
节点对象.classList.remove('类名')
节点对象.classList.contains('类名')
节点对象.classList.toggle('类名')
```

```
节点对象.value = 值
节点对象.value查看值
```

```
节点对象.style.样式属性 = 值    设置样式 所有带中线的方法都要改变成驼峰命名
```

### 今日内容

#### DOM

##### 事件

```
绑定方式:
<button id="btn">点击一下</button>
方式一:
    var btn = document.getElementById('btn')
    btn.onclick = function () {
        alert('点我干嘛')
    }
方式二:
	btn.onclick = clik
    function clik() {
        alert('不要点')
    }
方式三:
	<button id="btn" onclick="clik()">点击一下</button>
	function clik() {
        alert('不要点')
    }
```

#### BOM

browser object model

##### 定时器

###### setInterval

```
每隔一段时间就完成某个操作
var tid = setInterval(fn,n)  每隔n毫秒就调用一次fn函数
var tid = setInterval("fn()",n) 

clearInterval(tid)   清除定时器
```

###### setTimeOut

```
在设定时间之后执行一次来完成某个操作
var tid = setTimeout(fn,n)       n毫秒之后只调用一次fn函数
var tid = setTimeout("fn()",n) 

clearTimeout(tid)   清除定时器
```

##### location对象

window的子对象  window.location

``` 
属性:
window.location.href 查看当前网页的url
window.location.href = 'http://www.baidu.com'   修改当前网页的url,修改之后会跳转
方法:
window.location.reload()  刷新页面
```

#### 练习

##### onscroll事件

```
window.onscoll 在页面的滚动条滚动的时候触发的事件
document.documentElement.scrollTop 针对获取浏览器的垂直滚动条的位置
```

##### select的onchange事件

```
select对象.options.selectedindex 被选中的选项在options中的索引位置
```