# day79爬虫初识

## 一、爬虫概述

- 什么是爬虫：
  - 就是通过编写程序模拟浏览器上网，然后让其去互联网中爬取数据的过程
- 爬虫的分类
  - 通用爬虫：爬取一整张页面源码数据。搜索引擎（抓取系统==》内部封装的一套爬虫程序）重点使用的是该种形式的爬虫。
  - 聚焦爬虫：抓取的是页面中指定的局部数据。
  - 增量式爬虫：监测网站数据更新的情况。抓取的是网站最新更新出来的数据。
- 爬虫安全性的探究
  - 风险所在
    - 爬虫干扰了被访问网站的正常运营；
    - 爬虫抓取了受到法律保护的特定类型的数据或信息
  - 如何规避风险
    - 严格遵守网站设置的robots协议；
    - 在规避反爬虫措施的同时，需要优化自己的代码，避免干扰被访问网站的正常运行；
    - 在使用、传播抓取到的信息时，应审查所抓取的内容，如发现属于用户的个人信息、隐私或者他人的商业秘密的，应及时停止并删除
- 反爬机制：应用在网站中
- 反反爬策略：应用在爬虫程序中
- 第一个反爬机制：
  - robots协议：纯文本的协议
    - 特点：防君子不防小人

## 二、http&https

- 什么是http协议（全名超文本传输协议）
  - 就是服务器端和客户端进行数据交互的某种形式
- https就是安全（数据加密）的http协议
- 常用的头信息
  - user-agent：请求载体的身份标识
  - Connection：‘close’
  - content-type
- https的加密方式
  - 对称秘钥加密
  - 非对称秘钥加密
  - 证书秘钥加密（https）

## 三、requests模块的基本使用

- requests模块
- 概念：基于网络请求的模块
- 作用：用来模拟浏览器发请求，从而实现爬虫
- 环境安装：pip install requests
- 编码流程：
  - 指定url地址
  - 发起请求
  - 获取响应数据
  - 持久化存储

爬取搜狗首页的页面源码数据

```python
import requests
#1.指定url
url = 'http://www.sogou.com/'
#2.请求发送：get返回的是一个响应对象
response = response.get(url = url)
#3.获取相应数据：text返回的是字符串形式的相应数据
page_text = response.text
#4.持久化储存
with open('.sogou.html','w',encoding='utf-8') as fp:
    fp.write(page_text)
```



- 实现一个建议的网页采集器
  - 请求参数德 动态化

```python
url = 'https://www.sogou.com/web'
#请求参数的动态化
wd = input('enter a key word:')
params = {
    'query':wd
}

response = requests.get(url=url,params=params)

page_text = response.text
fileName = wd+'.html'
with open(fileName,'w',encoding='utf-8') as fp:
    fp.write(page_text)
print(fileName,'爬取成功！')
```

上述代码问题：

- 乱码问题
  - response.encoding = 'xxx'  (编码格式)
- 数据丢失
  - 反爬机制：UA检测
  - 反反爬策略：UA伪装

```python
#乱码问题的解决
url = 'https://www.sogou.com/web'
#请求参数的动态化
wd = input('enter a key word:')
params = {
    'query':wd
}

response = requests.get(url=url,params=params)

#将响应数据的编码格式手动进行指定
response.encoding = 'utf-8'

page_text = response.text
fileName = wd+'.html'
with open(fileName,'w',encoding='utf-8') as fp:
    fp.write(page_text)
print(fileName,'爬取成功！')
```

```python
#UA伪装操作
url = 'https://www.sogou.com/web'
#请求参数的动态化
wd = input('enter a key word:')
params = {
    'query':wd
}

#UA伪装
headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'
}
response = requests.get(url=url,params=params,headers=headers)


#将响应数据的编码格式手动进行指定
response.encoding = 'utf-8'

page_text = response.text
fileName = wd+'.html'
with open(fileName,'w',encoding='utf-8') as fp:
    fp.write(page_text)
print(fileName,'爬取成功！')
```

- 动态加载的数据
  - 通过另一个网络请求请求到的数据



- 爬取豆瓣电影中动态加载出的电影详情数据

```python
url = 'https://movie.douban.com/j/chart/top_list'
#参数动态化
params = {
    'type': '5',
    'interval_id': '100:90',
    'action': '',
    'start': '0',
    'limit': '200',
}
response = requests.get(url=url,params=params,headers=headers)
#json()返回的是序列化好的对象
movie_list = response.json()
for movie in movie_list:
    print(movie['title'],movie['score'])
```

- 总结：对一个陌生的网站进行数据爬取的时候，首先要确定的一点就是爬取的数据是否为动态加载出来的
  - 是:需要通过抓包工具捕获到动态加载数据对应的数据包，从中提取出url和请求参数。
  - 不是：直接对浏览器地址栏的url发起请求即可



**如何检测爬取的数据是不是动态加载出来的？**

- 通过抓包工具进行局部搜索就可以验证数据是否为动态加载
  - 搜索到：不是动态加载
  - 搜索不到：是动态加载
    - 如何定位动态加载的数据在哪呢？
      - 通过抓包工具进行全局搜索进行定位

动态爬取示例

- 爬取肯德基的餐厅位置信息http://www.kfc.com.cn/kfccda/storelist/index.aspx

```python
url = 'http://www.kfc.com.cn/kfccda/ashx/GetStoreList.ashx?op=keyword'
data = {
    'cname': '',
    'pid': '',
    'keyword': '上海',
    'pageIndex': '1',
    'pageSize': '10',
}
address_dic = requests.post(url=url,data=data,headers=headers).json()
for dic in address_dic['Table1']:
    print(dic['addressDetail'])
```

- 需求 https://www.fjggfw.gov.cn/Website/JYXXNew.aspx 福建省公共资源交易中心

  提取内容:

  工程建设中的中标结果信息/中标候选人信息

  1. 完整的html中标信息
  2. 第一中标候选人
  3. 中标金额
  4. 中标时间
  5. 其它参与投标的公司

  实现思路

  - 确认爬取的数据都是动态加载出来的
  - 在首页中捕获到ajax请求对应的数据包，从该数据包中提取出请求的url和请求参数
  - 对提取到的url进行请求发送，获取响应数据（json）
  - 从json串中提取到每一个公告对应的id值
  - 将id值和中标信息对应的url进行整合，进行请求发送捕获到每一个公告对应的中标信息数据

```python
post_url = 'https://www.fjggfw.gov.cn/Website/AjaxHandler/BuilderHandler.ashx'
headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36',
    'Cookie': '_qddac=4-3-1.4euvh3.x4wulp.k1hj8mnw; ASP.NET_SessionId=o4xkycpib3ry5rzkvfcamxzk; Hm_lvt_94bfa5b89a33cebfead2f88d38657023=1570520304; __root_domain_v=.fjggfw.gov.cn; _qddaz=QD.89mfu7.7kgq8w.k1hj8mhg; _qdda=4-1.4euvh3; _qddab=4-x4wulp.k1hj8mnw; _qddamta_2852155767=4-0; _qddagsx_02095bad0b=2882f90558bd014d97adf2d81c54875229141367446ccfed2b0c8913707c606ccf30ec99a338fed545821a5ff0476fd6332b8721c380e9dfb75dcc00600350b31d85d17d284bb5d6713a887ee73fa35c32b7350c9909379a8d9f728ac0c902e470cb5894c901c4176ada8a81e2ae1a7348ae5da6ff97dfb43a23c6c46ec8ec10; Hm_lpvt_94bfa5b89a33cebfead2f88d38657023=1570520973'
}
data = {
    'OPtype': 'GetListNew',
    'pageNo': '1',
    'pageSize': '10',
    'proArea': '-1',
    'category': 'GCJS',
    'announcementType': '-1',
    'ProType': '-1',
    'xmlx': '-1',
    'projectName': '',
    'TopTime': '2019-07-10 00:00:00',
    'EndTime': '2019-10-08 23:59:59',
    'rrr': '0.7293828344656237',
}
post_data = requests.post(url=post_url,headers=headers,data=data).json()
for dic in post_data['data']:
    _id = int(dic['M_ID'])
    detail_url = 'https://www.fjggfw.gov.cn/Website/AjaxHandler/BuilderHandler.ashx?OPtype=GetGGInfoPC&ID={}&GGTYPE=5&url=AjaxHandler%2FBuilderHandler.ashx'.format(_id)
    company_data = requests.get(url=detail_url,headers=headers).json()['data']
    company_str = ''.join(company_data)
    print(company_str)
```

