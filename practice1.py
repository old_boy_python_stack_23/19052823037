# msg = '''
# a = "------------------- info ----------------------"
# b =  %s
# c =  %s
# d =  %s
# e =  %s
# f =  %s
# g = "------------------- end ------------------------"
# '''
# # name = input("name:")
# # age = input("age:")
# # sex = input("sex:")
# # job = input("job:")
# # hobby = input("hobby:")
#
# print(msg%(input("name:"),input("age:"),input("sex:"),input("job:"),input("hobby:")))

# msg = '''
# a = "------------------- info ----------------------"
# b = %s
# c = %s
# d = %s
# e = %s
# f = %s
# g = "------------------- end ------------------------"
# '''
# name = input("name:")
# age = input("age:")
# sex = input("sex:")
# job = input("job:")
# hobby = input("hobby:")
# print(msg%(name,age,sex,job,hobby))

# print("我是%s"%(input("请输入name:")))

# print(f"我是{input('请输入name')}")

# noinspection PyUnresolvedReferences

# from sympy import *
# a = Symbol('n')
# b = Symbol('b')
# c = Symbol('c')
# d = Symbol('d')
# e = Symbol('e')
# s = Symbol('s')
# print(solve([c-a-b,b-a-d,d-a-e,d-a-e,2*e-a-c],[b,c,d,e]))
# print((solve([c-a-b,b-a-d,d-a-e,d-a-e,2*e-a-c],[b,c,d,e])[c])**2+solve([c-a-b,b-a-d,d-a-e,d-a-e,2*e-a-c],[b,c,d,e])[b]**2)
# def add(x,y,f):
#     return f(x) + f(y)
# res = add(3,-6,abs)
# print(res)

# def func():
#     print(123)
# func1 = [func,func,func]
# print (func1)
#
# def func():
#     print("hahaha")
# def test(func):
#     print("a")
#     return func()
# print(test)
#
# @test  # 从这里可以看出@test等价于 test(xxx()),但是这种写法你得考虑python代码的执行顺序
# def xxx():
#     print('Hello world!')
#
# dic = {"1":1,"2":2,"3":3,"4":4}
# # 删除"2"  2种方式
# dic.pop("2")
# del dic["2"]
# # 添加 "5":5 2种方式
# dic["5"] = 5
# dic.setdefault("5",5)
# # 修改: "1":10086
# dic["1"] = 10086
# # 查看"3"的值
# print(dic["3"])
# print(dic.get("3"),"没有值")
#
#
# li = ["苍老师", "东京热", "武藤兰", "波多野结衣"]
# # new_li = []
# # my_input = input(">>>")   #***你好啊,波多野结衣你也挺不错
# # for em in li:
# #     if em in my_input:
# #         my_input = my_input.replace(em,len(em) * "*")   #***你好啊,*****你也挺不错
# # new_li.append(my_input)
# # print(new_li)
# l2=[]
# n = 0
# nu = input("请输入评论内容:")
# for i in li:
#     if i in nu:
#         nu = nu.replace(i,len(i)*"*")
# l2.append(nu)
# print(l2)
#
# def func(arg):
#     v1 = arg()
#     print(v1)    # 烧脑进行时
# def show():
#     print(6666)
#
# func(show)

#
# a = 1000
# b = 1000
# print(a == b)
#
# print(id(a))
# print(id(b))
#
# print(a is b)
# 小数据池
# 只在cmd环境中适应,python中的规则没记,cmd环境 数字是-5 - -  256

# def calc(n):
#     v = int(n/2)
#     print(v)
#     if v > 0:
#         calc(v)
#     print(n)
#
# calc(10)
# print(int(0/2))

# a,b = {"s":1,"k":2}
# print(a,b)

# dic = {"a":"b","c":"d","e":"f"}
# for em in dic:
#
#     g = 1
# print(g)
# print(em)


# a = 1000
# b = 1000
# print(id(a))
# print(id(b))
#
# is 判断内存地址是否相同
#  == 判断等号两边的值是否相同
# 驻留机制 字符串 数字 字母下划线组成的
# 代码块 : 一个文件, 一个模块, 一个函数 一个类 ,终端中每一行都是一个代码块
# 在Python中是用字典的方式存储

# l1 = [1,2,3,4,[5,6,7,]]
# l2 = l1.copy()
# l1[-1].append(8)
# print(l2)  #[1,2,3,4,[5,6,7,8]]


# dic = {"a":"b","c":"d","e":"f","g":"h"}
# dic.popitem()
# print(dic)
# dic.popitem()
# print(dic)
# dic.popitem()
# print(dic)

# li = [1,2,3,4,5]
# for i in li:
#     if i % 2 == 1:
#         li.remove(i)
# print(li)

# s = "少年"
# s1 = s.encode("gbk")#以GBK进行编码
# s2 = s1.decode("gbk")#以GBK进行解码 # 用什么编码就用什么解码
# print(s1)
# print(s2)


# he = [122000, 189999, 99999, 25000000, 126]
# n = 0
# for i in he:
#     n += i
# print(n)

# a = 10
# b = 20
# c = a if a > int(b) else b
# print(c)

# a = 14
# b=20
# a,b=b,a
# print(a,b)
# import copy
# a = [1,2,3,[11,22,33],4]
# b = copy.copy(a)
# b[3].append(5)
# print(a,b)
# a = [1,1,2,2,2,3,5,5,5,5,5,4]
# a = list(set(a))
# print(a)

# s = "老男孩"
# s1 = s.encode("gbk")
# s2 = s1.decode("gbk")
# s3 = s2.encode("utf-8")
# s4 = s3.decode("utf-8")
#
# print(s3)
# print(s4)
# import copy
# a = [1,2,3,[4,5],6]
# b = a
# c = copy.copy(a)
# d = copy.deepcopy(a)
# b.append(10)
# c[3].append(11)
# d[3].append(12)
# print(a)
# print(b)
# print(c)
# print(d)


# a = "gfdhsahhnba"
# print(a[-1:-4:-1])
# v1 = [1,2,3,4,5]
# v2 = [v1,v1,v1]
# v2[1][0] = 111
# v2[2][0] = 222
# print(v1)
# print(v2)


# v = "k1:v1|k2:v2|k3:v3"
# dic = {}
# w = v.split("|")
# for i in w:
#     # print(i.split(":"))
#    dic[i.split(":")[0]] = i.split(":")[1]
# print(dic)
# lis = [1, 23, 45, 6, 48465465, 45646, 1, 23, 6, 5465, 465456465, 456413, 513153435, 35546543484, 891343265]
# dic = set({})
# for i in lis:
#     dic[i] = dic.get(i, 0) + 1
# print(dic)

# dic = {"a":"b"}
# print(dic.items().isdisjoint("a"))
# m = input(">>>")
# if int(m) == False:
#     print("成功")
# 2.完成一个商城购物车的程序。
#
# 商品信息在shopping.txt文件中存储的，存储形式：
# name price
# 电脑 1999
# 鼠标 10
# 游艇 20
# 美女 998

# 要求:
# 1，用户先给自己的账户充钱：比如先充3000元。
# 2，读取商品信息文件将文件中的数据转化成下面的格式：
# goods = [{"name": "电脑", "price": 1999},
# {"name": "鼠标", "price": 10},
# {"name": "游艇", "price": 20},
# {"name": "美女", "price": 998},]
# 3，页面显示 序号 + 商品名称 + 商品价格，如：
# 1 电脑 1999
# 2 鼠标 10
# 4，用户输入选择的商品序号，然后打印商品名称及商品价格,并将此商品，
# 添加到购物车(自己定义购物车)，用户还可继续添加商品。

# 5，如果用户输入的商品序号有误，则提示输入有误，并重新输入。
# 6，用户输入n为购物车结算，依次显示用户购物车里面的商品，数量及单价，
# 若充值的钱数不足，则让用户删除某商品，直至可以购买，若充值的钱数充足，则可以直接购买
# 7，用户输入Q或者q退出程序。
# 8，退出程序之后，依次显示用户购买的商品，数量，单价，以及此次共消费多少钱，
# 账户余额多少，并将购买信息写入文件。


# tp = input("请输入充值的数额,输入Q退出:")
# if tp == "Q" or tp == "q":
#     pass
#     exit()
# if tp.isdecimal():
#     if int(tp) > 0:
#         print("充值成功,账户余额为%s" % tp)
# goods = []
# with open("shopping.txt","r",encoding="utf-8") as f:
#     em = f.readline().strip().split()
#     for i in f.readlines():
#         dic = {}
#         li = i.strip().split(" ")
#         for le in range(len(li)):
#             dic[em[le]] = li[le]
#         goods.append(dic)
# # print(goods)
#     for i in range(len(goods)):
#         print(i+1,goods[i]['name'],goods[i]['price'])
# car ={}    #车
# balance = 0       #余额
# while 1:
#     no = input("请输入商品序号:(Q,退出,N结算)")
#     if no.isdigit() and 0 < int(no) <= len(goods):
#         no = int(no) - 1
#         name1 = goods[no]['name']  #商品名字
#         price1 = goods[no]['price'] #商品价格
#         if name1 not in car:            #加入购物车
#             car[name1] = {'price':price1,'num':1}
#         else:
#              car[name1]['num'] += 1
#         print(car)
#     elif no.upper() == 'Q':
#         print('退出')
#         car.clear()
#         break
#     elif no.upper() == "N":
#         for i in car:
#             print(i,car[i]['price'],car[i]['num'])  #买的东西
#             balance += int(car[i]['price'] )* int(car[i]['num'])  #价格
#         while int(tp )< balance:
#             print('余额不足')
#             for i in car:
#                 print(i,car[i]['price'],car[i]['num'])
#                 del_shop = input('请输入您要删除的商品：（Q退出）')
#                 if car.get(del_shop):
#                     car[del_shop]['num'] -= 1
#                     balance = car[del_shop]['price'] * car[del_shop]['num']
#                     if car[del_shop]['num'] == 0:
#                         car.pop(del_shop)
#                 elif del_shop.upper() == 'Q':
#                     exit()
#                 else:
#                     print('输入有误 请重新输入')
#             else:
#                 tp -= balance
#                 print('有钱')
#         else:
#             print('输入有误重新输入')
#         if car:
#             with open('shopping', 'a', encoding='utf-8') as f:
#                 for i in car:
#                     print(i, car[i]['price'], car[i]['num'])
#                     f.write('购买了：%s,单价：%s,数量：%s' % (i, car[i]['price'], car[i]['name']))
#             print('消费了：%s，账户余额：%s' % (balance, tp))

# with open("shopping.txt", mode="r", encoding="utf-8") as f:
#     w = f.readline()
#     m = f.readlines()  # ['电脑 1999\n', '鼠标 10\n', '游艇 20\n', '美女 998']
# while True:
#     meney = input("请输入充值金额")
#     if meney.isdigit():
#         meney = int(meney)
#         print("充值成功,余额为{}".format(meney))
#         break
#     else:
#         print("输入错误，请重新输入")
# biaoji = 0
# n = 1
# print("序号", "商品", "单价")
# for i in m:
#     print(n, i.strip().split()[0], i.strip().split()[1])
#     n += 1
# shopping_car = []  # 购物车 将选中的商品加入到里边 字典格式
# shopping_car2 = []  # 多次购物时为了使下一次不重复付费,需要一个载体承接
# shopping_money1 = 0  # 消费的总额,单次是加零 多次是多次的和
# while True:
#     user_input = input("请输入商品序号(输入n or N 购物车结算,输入q or Q 退出程序):")
#     if user_input.upper() == "N":
#         while True:
#             hk = []
#             for sale in shopping_car:
#                 md = (list(sale)[0], sale[list(sale)[0]], shopping_car.count(sale))  # 获取的是目标商品,数量单价 接下来判重
#                 hk.append(md)
#             shopping_car1 = list(set(hk))  # shopping_car1 对购物车去重  输出时的购物车数据
#             shopping_money = 0  # 购物车内的物品价值总额
#             surplus_noney = int(meney) - shopping_money
#             print("商品", "单价", "数量")
#             for shopping in shopping_car1:
#                 shopping_money += int(shopping[1]) * int(shopping[2])
#                 print(shopping[0], shopping[1], shopping[2])
#             print("商品的总价格是:{}".format(shopping_money))
#             buy_input = input("请输入是否购买:是or否")
#             if buy_input == "是":
#                 if int(shopping_money) <= int(meney):  # 判断钱是否够,够了可以买钱减少,不够输入删除商品
#                     meney -= shopping_money
#                     print("恭喜购物成功,余额为{};如需退出程序请输入q 或者 Q".format(meney))
#                     import copy
#
#                     shopping_car_copy = copy.deepcopy(shopping_car1)
#                     shopping_car2.append(shopping_car_copy)
#                     shopping_car.clear()
#
#                     biaoji += 13
#                     break
#                 else:
#                     xuhao = 1  # 先输出购物车中的信息,方便让用户选择删除
#                     for dele in shopping_car:
#                         print(xuhao, dele)
#                         xuhao += 1
#                     while True:
#                         shopping_lose = input("您目前余额{}金额不足,请输入要删除的商品序号:".format(meney))
#                         if shopping_lose.isdigit():
#                             shopping_car.pop(int(shopping_lose) - 1)
#                             break
#                         else:
#                             print("输入错误，请重新输入！")
#                 biaoji += 1
#             elif buy_input == "否":
#                 print("继续添加物品请输入商品序号,退出请输入q or Q")
#                 break
#             else:
#                 print("输入有误,请重新输入!")
#         shopping_money1 += shopping_money
#     elif user_input.upper() == "Q":
#         if biaoji == 0:
#             exit(0)
#         else:
#             print("商品", "单价", "数量")
#             for shopping2 in shopping_car2:
#                 for shopping in shopping2:
#                     print(shopping[0], shopping[1], shopping[2])
#                     with open("buy_message.txt", mode="a+", encoding="utf-8") as z:
#                         z.write("{} {} {}\n".format(shopping[0], shopping[1], shopping[2]))
#             print("此次消费了{}".format(shopping_money1))
#             print("账户剩余{}".format(meney))
#             with open("buy_message.txt", mode="a+", encoding="utf-8") as z:
#                 z.write("此次消费了{}账户剩余{}\n".format(shopping_money1, meney))
#             exit(0)
#     elif user_input.isdigit():
#         if int(user_input) in range(1, len(m) + 1):
#             dic = {}
#             dic[m[int(user_input) - 1].strip().split()[0]] = m[int(user_input) - 1].strip().split()[1]
#             shopping_car.append(dic)
#             print(dic)
#         else:
#             print("输入序号不对.请重新输入!")
#     else:
#         print("输入有误,请重新输入")
#
# w = "和覅好好上课的"
# print(w.isalpha())
# max_page_name, a = divmod(689, 10)
# if a > 0:
#     max_page_name += 1
# pager = int(input("要查看第几页:"))
# if pager < 1 or pager > max_page_name:
#     print(f"页码不合法,必须是1 - {max_page_name}")
# else:

# 进制转换
# bin() # 将其他类型转换成二进制
# oct() # 将其他类型转换成八进制
# int() # 将其他类型转换成十进制
# hex() # 将其他类型转换成十八进制
# def extendList(val, list=[]):
#     # list.append(val)
#     print(locals())
#     return list

# list1 = extendList(10)
# list2 = extendList(123, [])
# list3 = extendList('a')

# print('list1=%s' % list1)  # list1 = [10,"a"]
# print('list2=%s' % list2)  # list2 = [123.]
# print('list3=%s' % list3)  # list3 = [10,"a"]
# extendList(1)
# print(globals())
# def func():
#     print(111)
#     yield 2
# func()
# def eat_baozi:
#     for i in range(1,2001):
#         yield f"{i}号包子"
# ret = eat_baozi()
# stopIterration
# yield from 节省代码提升效率 有规律
# lis = []
# for i in range(1,101):
#     lis.append(i)
# print(lis)
# print([i**2 for i in range(1,11)])
# print([f"python{i}期" for i in range(1,101)])
# print([i for i in range(2,101,2)])

# 三十以内可以被三整除的数。
# print([i for i in range(1,31) if i % 3 ==0])
# #
# # 过滤掉长度小于3的字符串列表，并将剩下的转换成大写字母
# l1 = ['太白金星', 'fdsaf', 'alex', 'sb', 'ab']
# print([i.upper() for i in l1 if len(i) > 3])
#
# # 找到嵌套列表中名字含有两个‘e’的所有名字（有难度）
# names = [['Tom', 'Billy', 'Jefferson', 'Andrew', 'Wesley', 'Steven', 'Joe'],
#          ['Alice', 'Jill', 'Ana', 'Wendy', 'Jennifer', 'Sherry', 'Eva']]
# print([n for i in names for n in i if n.count("e") == 2])
# l1 = ['小潘', '怼怼哥','西门大官人', '小泽ml亚']
# dic = {}
# for i in range(len(l1)):
#     dic[i] = l1[i]
# print(dic)
# print({i:l1[i] for i in range(len(l1))})
# t1 = "我改我寰谛凤翎过后我我都发了赶快啦"
# func1 = lambda t: (t[0],t[0])
# print(func1(t1))
# lambda
# callable() 判断对象是否可调用
# def s():
#     pass
# b = 123
# print(callable(s))
# print(1,2,end="\t")
# print(3,4)
# l1 = [('alex', 73, 170), ('武大', 35, 159), ('太白', 18, 185)]
# print(min(l1, key=lambda x: x[2]))

# dic = {'a': 3, 'b': 2, 'c': 1}
# print(min(dic, key=lambda y:dic[y]))
# print(dic[min(dic, key=lambda y:dic[y])])

# dic = {'a':['李业',67],'b':['怼哥', 95],'c':['方垚', 85]}

# 将成绩最低的从属于的那个列表返回。
# 将成绩最低的分数返回。
# def func():
#     print(111)
#     return 34
#
#
# ret = func()

#
# # # print(ret)
# # # 无论在什么时候只要函数名加括号 函数就会被执行 而 函数的返回值只有进行打印的时候才会显示出来
# def func():
#     print(111)
#     yield 123, 123
#     yield 123456
#     yield 1345313
#     yield 2222
#     yield
#
# # 生成器函数 :只要函数中出现了yieldn那么 他就不是函数,他是生成器函数
# ret = func()   #注意 如果生成器没有一个变量去承接那么每次运行next 都会从头开始
# # print(ret)    #<generator object func at 0x000001CD70330E60>
# # 打印生成器函数出现的相应的地址 而且函数名加括号函数并不能执行
# print(next(ret))
# print(next(ret))
# print(next(ret))
# print(next(ret))
# print(next(ret))
# # 连续的print(next(ret)) 并不会使func()中的print多次执行

# def func():
#     l1 = [1, 2, 3]
#     yield from l1
#
#     '''
#     yield 1
#     yield 2
#     yield 3
#     '''
# ret = func()
# print(next(ret))
# print(next(ret))
# print(next(ret))

# 列表推导式 一行代码构建一个有规律的比较复杂的列表
# l1 = [i for i in range(1,10)]
# print(l1)
# # 生成器表达式
# l1 =["hdf","好高大上放散阀"]
# # print(" ".join(l1))
# "".join()  可迭代对象中必须是字符串 可以说join 是为字符串拼接而设计
# obj = (i for i in range(1,11))
# print(next(obj))
# print(int(3.14))
# print(complex(1,2))
# print(pow(3, 3, 2))
# ret = (i for i in range(1,11) if i > 5)
# # print(next(ret))
# dic = {'c': 1, 'b': 2, 'a': 3}
# # 最小的值对应的键返回
# print(min(dic,key= lambda x:dic[x]))
# # 最小的值对应的值返回
# print(dic[min(dic,key= lambda x:dic[x])])

# list = [
#     {'name': 'alex', 'age': 73},
#     {'name': 'wusir', 'age': 35},
#     {'name': '太白', 'age': 25},
# ]
# # 将年龄最小的 字典返回。
# print(min(list,key= lambda x:x["age"]))
# # 将年龄最小的名字返回。
# print(min(list,key=lambda x:x["age"])["name"])
# # 将年龄最小的年龄返回。
# print(min(list,key= lambda x:x["age"])["age
# l1 = [('张一东', 80), ('张耳洞', 75), ('怼怼哥', 7), ('李业', 59)]
# print(sorted(l1,key=lambda x:x[1]))
# print(sorted(l1,key=lambda x:x[1],reverse=True))
#
# lst = [{'id':1,'name':'alex','age':18},
#         {'id':1,'name':'wusir','age':17},
#         {'id':1,'name':'taibai','age':35},]
# print(list(filter(lambda x: x["age"]<30,lst)))

# def make_average(price_one):  # 闭包  在全局必须有变量承接闭包中的内容
#     l1 = []
#     def average(price):
#         l1.append(price)
#         total = sum(l1)
#         return total/len(l1)
#     return average(price_one)
# print(make_average(20))
# print(make_average(40))

# def func():
#     print(111)
#     print("xiaoxin")
#     yield 123477897890
#     yield "jiangxinbixin"
# obj = func()
# print(next(obj))
# print(next(obj))

# def func():
#     for i in range(100):
#         yield f"{i}号女朋友"
# ret = func()
# for i in range(100):
#     print(next(ret))
#
# def func1():
#     l1 =[1,2,3,4,5,6,7,8,9]
#     yield from l1
# ret = func1()
# for i in range(len(list(func1()))):  # 函数没有len 方法 可以先转换成list再进行操作
#     print(next(ret))

# l1 = [56, 67, 12, 34, 78, 90,]
# print([i**2 for i in l1])
# print(map(lambda x:x**2,l1))
# print(list(map(lambda x:x**2,l1)))
# a = [i + 1 for i in l1]
# print(a)
# def fib(max):
#     n, a, b = 0, 0, 1
#     while n < max:
#         yield b
#         a, b = b, a + b
#         n += 1
#     return 'down'
#
#
# date = fib(10)
#
# print(date.__next__())
# print(date.__next__())
# print("nihao")
# print(date.__next__())
# print(date.__next__())

# def make_av():
#
#     se = []
#     # def aver(new_value):
#     #     se.append(new_value)
#     #     total = sum(se)
#     #     return total/len(se)
#     se.append('new_massage')
#
#     return se
# avg = make_av()
#
# print(avg)
#
# print(avg)
# print(avg.__code__.co_freevars)  # 打印结果是元组


# def wrapper(a,b):
#     def inner():
#         print(a)
#         print(b)
#     return inner
# a = 2
# b = 3
# ret = wrapper(b,a)
# ret()

# def make_averager():
#
#     series = []
#     def averager():
#         series.append('new_value')
#         # total = sum(series)
#         return series
#
#     return averager
# avg = make_averager()
#
#
# print(avg())  # 100000.0
# print(avg())  # 110000.0
# print(avg())  # 120000.0

# a4 = "dkfjdkfasf54"
# ret6 = a4.find("fjdk",1,6)
# print(ret6)  # 返回的找到的第一个元素的索引，如果找不到返回-1
#
# ret61 = a4.index("fjdk",4,6)
# print(ret61) # 返回的找到的元素的索引，找不到报错。

# name='alex say :i have one tesla,my name is alex'
# print(name.replace('alex','SB',0))

# l = ['太白', 'alex', 'WuSir', '女神']
# l[:] = 'abcdefg'
# print(l) # ['太白', 'a', 'b', 'c', 'd', 'e', 'f', 'g', '女神']
# tu = ('太白', [1, 2, 3, ], 'WuSir', '女神')
# print(tu.index("爱白"))
# dic = dict((('wusir',2),('alex','sb'),(1,2)))
# # print(dic)
# dic.update(alex="bigsn")
# print(dic)

# li = ['alex','银角','女神','egon','太白']
# for i in enumerate(li):
#     print(i)
# for index,name in enumerate(li,1):
#     print(index,name)
# for index, name in enumerate(li, 100):  # 起始位置默认是0，可更改
#     print(index, name)
# import time
# def test_time(x):  # x = index
#     def inner(*args,**kwargs):
#         start_time = time.time()
#         ret = x(*args,**kwargs)
#         end_time = time.time()
#         print(f'此函数的执行效率{end_time-start_time}')
#         return ret
#     return inner
#
#
# @test_time  # index = test_time(index)
# def index(n):
#     time.sleep(0.5)
#     print(f'欢迎{n}访问博客园首页')
#     return True
#
# # @test_time  # index = test_time(index)
# def func2(a,b):
#     time.sleep(0.5)
#     print(f'最终结果：{a+b}')
#     return a + b
# index("yangsne ")
# def login():
#     count = 0
#     while True:
#         if count < 3:
#             username_input = input("请输入用户名:")
#             user_password = input("请输入密码:")
#             with open("register", mode="r", encoding="utf-8") as f:
#                 line = f.readline()
#                 line1 = f.readlines()
#             l = [i.strip().split('|') for i in line1]  # [['alex', 'taibai250'], ['太白', 'alex520']]
#             for i in l:
#                 if username_input == i[0]:
#                     if user_password == i[1]:
#                         print("登陆成功")
#                         return True
#                     else:
#                         print("密码错误,请重新登陆")
#             else:
#                 print("用户名错误请重新输入:")
#         else:
#             return False
#         count += 1
# import sys
# print(sys.setrecursionlimit(10000000))
# def foo(n):
#     print(n)
#     n += 1
#     foo(n)
# foo(1)
# import copy
# a = "123456"
# b = copy.copy(a)
# print(id(a),id(b))

#
# with open("qq", mode="w", encoding="utf-8") as f:
#     f.write('''用户名|密码
# alex|taibai250
# 太白|alex520
# yangsen|140720''')
#
#
# def login(n):
#     count = 0
#     while True:
#         if count < 3:
#             username_input = input("请输入用户名:").strip()
#             user_password = input("请输入密码:").strip()
#             with open(n, mode="r", encoding="utf-8") as f:
#                 dic = {line.strip().split('|')[0]: line.strip().split('|')[1] for line in f}
#             if username_input in dic and user_password == dic[username_input]:
#                 print("登陆成功")
#                 return True
#             else:
#                 print(f"用户名或密码错误!剩余登陆次数为:{3-count}")
#         else:
#             print("超过三次退出")
#             return False
#         count += 1
#
# flag = False
# def wrapper_out(n):
#     def wrapper(f):
#         def inner(*args, **kwargs):
#             global flag
#             if flag:
#                 ret = f(*args, **kwargs)
#                 return ret
#             else:
#                 if login(n):
#                     ret = f(*args, **kwargs)
#                     flag = True
#                     return ret
#         return inner
#     return wrapper
#
# @wrapper_out("qq")
# def qq():
#     print("成功登陆qq")
# @wrapper_out("qq")
# def did():
#     print("成功登陆qq")
# @wrapper_out("qq")
# def bar():
#     print("成功登陆qq")
# @wrapper_out("qq")
# def daD():
#     print("成功登陆qq")


# def func(n):
#     print(n)
#     n+=1
#     func(n)
# func(1)

# def age(n):
#     if n == 1:
#         return 18
#     else:
#         return age(n-1)+ 2
#
# print(age(5))

# l1 = [1, 3, 5, ['太白','元宝', 34, [33, 55, [11,33]]], [77, 88],66]
# def lis(n):
#     for i in n:
#         if type(i) == list:
#             lis(i)
#         else:
#             print(i)
# lis(l1)
#
# def wrapper_out(n):
#     def wrapper(f):
#         def inner(*args,**kwargs):
#             # if n == 'qq':
#             #     username = input('请输入用户名：').strip()
#             #     password = input('请输入密码：').strip()
#             #     with open('qq',encoding='utf-8') as f1:
#             #         for line in f1:
#             #             user,pwd = line.strip().split('|')
#             #             if username == user and password == pwd:
#             #                 print('登陆成功')
#             #                 ret = f(*args,**kwargs)
#             #                 return ret
#             #         return False
#             # elif n == 'tiktok':
#             #     username = input('请输入用户名：').strip()
#             #     password = input('请输入密码：').strip()
#             #     with open('tiktok', encoding='utf-8') as f1:
#             #         for line in f1:
#             #             user, pwd = line.strip().split('|')
#             #             if username == user and password == pwd:
#             #                 print('登陆成功')
#             #                 ret = f(*args, **kwargs)
#             #                 return ret
#             #         return False
#             username = input('请输入用户名：').strip()
#             password = input('请输入密码：').strip()
#             with open(n,encoding='utf-8') as f1:
#                 for line in f1:
#                     user,pwd = line.strip().split('|')
#                     if username == user and password == pwd:
#                         print('登陆成功')
#                         ret = f(*args,**kwargs)
#                         return ret
#                 return False
#         return inner
#     return wrapper
"""
# @wrapper_out('qq')
# def qq():
#     print('成功访问qq')
# qq()
# 看到带参数的装饰器分两步执行：
'''
@wrapper_out('腾讯')
    1. 执行wrapper_out('腾讯') 这个函数，把相应的参数'腾讯' 传给 n,并且得到返回值 wrapper函数名。
    2. 将@与wrapper结合，得到我们之前熟悉的标准版的装饰器按照装饰器的执行流程执行。
'''
"""

#
# @wrapper_out('qq')
# def qq():
#     print('成功访问qq')
#
#
# @wrapper_out('tiktok')
# def tiktok():
#     print('成功访问抖音')
#
# qq()
# tiktok()

# with open("登陆",mode="w",encoding="utf-8") as ft:
#     ft.write("alex|123456")

# dic = {'username':None,'status':False}
#
# def wrapper_out(n):
#     def wrapper(f):
#         def inner(*args,**kwargs):
#             if dic['status']:
#                 ret = f(*args,**kwargs)
#                 return ret
#             else:
#                 w = 0
#                 while w < 3:
#                     username = input('请输入用户名:').strip()
#                     password = input('请输入密码:').strip()
#                     with open(n,encoding='utf-8') as f1:
#                         for i in f1:
#                             ass,pwd = i.strip().split("|")
#                             if username == ass and password == pwd:
#                                 dic['username'] = username
#                                 dic['status'] = True
#                                 print('登陆成功')
#                                 ret = f(*args,**kwargs)
#                                 return ret
#                         else:
#                             print('输入错误请重新输入')
#                             w += 1
#         return inner
#     return wrapper
#
# def user_pwd(z):
#     def wrapper1(k):
#         def inner1(*args,**kwargs):
#             with open(z, encoding='utf-8') as f2:
#                 dic = {i.strip().split("|")[0]: i.strip().split("|")[1] for i in f2}
#                 while 1:
#                     username = input('请输入您要注册的用户名：').strip()
#                     password = input('请输入注册密码密码：').strip()
#                     if password.isalnum() and 14 >= len(password) >= 6 and username not in dic:
#                             with open(z,encoding='utf-8',mode='a') as f2:
#                                 f2.write(f'\n{username}|{password}')
#                                 ret1 = k(*args,**kwargs)
#                                 return ret1
#                     elif username  in dic:
#                         print('用户名已存在')
#                     else:
#                         print('输入错误请从新输入')
#         return inner1
#     return wrapper1
#
# def  article(wzz):
#     wz_tm = input('请输入文章标题:').strip()
#     wz_nr = input('请输入文章内容:').strip()
#     with open(wzz,'a',encoding='utf-8') as f6:
#         f6.write(f'{wz_tm}\n{wz_nr}')
#
#
# def to_lead(drz):
#     with open(drz,'r',encoding='utf-8') as f7:
#         f8 = f7.read()
#         return f8
#
#
#
#
#
#
# @wrapper_out('登陆')
# def dl():
#     print('请选择其他页面')
#
# @user_pwd('登陆')
# def zc():
#     print('注册成功,返回选择页面')
# @wrapper_out('登陆')
# def wz():
#     print('欢迎进入文章界面')
#     wz_input = input('请选择1-写入文件,2导入md文件').strip()
#     wz_dic = {1:'写入文件',
#               2:'导入md文件'}
#     if wz_input.isdigit():
#         wz_input = int(wz_input)
#         if 0 < wz_input<=len(wz_dic):
#             pass
#         elif wz_input == 1:
#             article('文章')
#         elif wz_input == 2:
#             dr_input = input('请输入导入的文件名').strip()
#             print(to_lead(dr_input))
#
# @wrapper_out('登陆')
# def pl():
#     print('欢迎进入评论界面')
# @wrapper_out('登陆')
# def rj():
#     print('欢迎进入日记页面')
# @wrapper_out('登陆')
# def sc():
#     print('欢迎进入收藏界面')
# @wrapper_out('登陆')
# def zx():
#     dic['status'] = False
#     print('注销成功')
#
#
# new_dic = {1:dl,
#            2:zc,
#            3:wz,
#            4:pl,
#            5:rj,
#            6:sc,
#            7:zx,
#            8:'退出程序'}
#
# while 1:
#     print('''1.请登录
# 2.请注册
# 3.进入文章页面
# 4.进入评论页面
# 5.进入日记页面
# 6.进入收藏页面
# 7.注销账号
# 8.退出整个程序
#     ''')
#     number = input('请输入选择的序号:').strip()
#     if number.isdigit():
#         number = int(number)
#         if 0< number < len(new_dic):
#             new_dic[number]()
#         elif number == 8:
#             break
#         else:
#             print('请重新选择:')
#     else:
#         print('煞笔,认真点输入')

# 时间戳 time.tiem()
# 人类看得懂的时间 格式化时间 .
# 结构化时间
#
# import time
# # print(time.time())
# # print(time.localtime())
# timestamp = time.time()
# st = time.localtime(timestamp)
# # print(st)
# ft = time.strftime("%Y-%m-%d %H:%M:%S",st)
# print(ft)
# ST = time.strptime(ft,"%Y-%m-%d %H:%M:%S")
# print(ST)
# TIMESTAMP = time.mktime(ST)
# print(TIMESTAMP)D:\Python_study\day16自定义模块\函数的进阶.md
# import os
#
# print(os.path.isfile("D:\Python_study\经典小项目"))
# ret = os.listdir("D:\Python_study")
# print(ret)
# def func(i):
#     lis = []
#     ret = os.listdir(i)
#     print(ret)
#     # for i in ret:
#     #     print(i)
#     print(os.path.dirname())
# print(func("D:\Python_study\day18规范化开发 内置函数二"))
# ret = os.listdir(r"D:\Python_study\day18规范化开发 内置函数二\选课系统\bin\starts.py")
# print(os.path.isfile(r"D:\Python_study\day18规范化开发 内置函数二\选课系统\bin\初识函数"))
# import time
# ft = time.strftime("%Y/%m/%d")
# print(ft)
# import hashlib
#
# md5_obj = hashlib.md5("nezha".encode('utf-8'))# md5()括号中是加盐内容
# # 提高密码的安全性,也可以不加
# print(md5_obj)
# md5_obj.update('123456'.encode('utf-8'))
# print(md5_obj.hexdigest())

# import os
#
# d = os.getcwd()
# os.chdir('D:\Python_study\day08文件操作')
# print(os.getcwd())
# os.chdir('D:\Python_study')
# print(os.getcwd())
# print(complex(1,2))
# *是传递位置参数将所有的位置参数聚合成一个元组
# **是传递关键字参数,将所有关键自参数聚合成字典
# a, b, *c = [1, 2, 3, 4, 5]
# print(a, c, b)
# a, *c, b, = [1, 2, 3, 4, 5]
# print(a, c, b)
# a, *c = range(5)
# print(a, c)
# a, *c, b = (1, 2, 3, 4, 5, 6)
# print(a, c, b)
# def func():
#     lis = [1,2,3]
#     def inner():
#         print(lis)
#     return inner
# ret = func()
# print(ret())
# 闭包是存在于嵌套函数中的 内层函数对外层函数非全局变量的引用
# 称之为自由变量他不会随着函数的结束而消失,一直保存在内存,最终的目的是保证数据的安全
# func1 = lambda x, y: x + y, [i for i in range(10)]
# print(func1)
# print(type(func1))
# filter()
# print(hash('hasdjf'))
# s1 = '太白'
# # 方法一：
# print(s1.encode('utf-8'))
# # 方法二：
# print(bytes(s1,encoding='utf-8'))
#
# l1 = [1,2,3,4,5]
# tu = ('a','b','c')
# print(zip(l1,tu))
# print(list(zip(l1,tu)))
# import logging
# logging.basicConfig(
#     level=logging.DEBUG,
# )
# logging.debug('debug message')
# logging.info('info message')
# logging.warning('warning message')
# logging.error('error message')
# logging.critical('critical message')
# def func():
#     print('in func')
#     logging.debug('正常执行')
# func()

# try:
#     i = input("请输入选项")
#     int(i)
# except Exception as e:
#     logging.error(e)
# print(11)

# import logging
# logging.basicConfig(
#     level= 10 ,
#     format = '%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
#     # filename='test.log'
# )
# logging.debug("调试模式")
# logging.info('正常模式')
# logging.warning("警告信息")
# logging.error("错误信息")
# logging.critical("严重错误信息")
#
# def text 选课系统 作业设计():
#     print("lalala woshi rizhi")
#     logging.debug("正常执行")
# text 选课系统 作业设计()
#
# logger = logging.getLogger()
# fh = logging.FileHandler('标配.log',encoding='utf-8')
# sh = logging.StreamHandler()


import logging.config

# 定义三种日志输出格式 开始

# standard_format = '[%(asctime)s][%(threadName)s:%(thread)d][task_id:%(name)s][%(filename)s:%(lineno)d]' \
#                   '[%(levelname)s][%(message)s]' #其中name为getlogger指定的名字
#
# simple_format = '[%(levelname)s][%(asctime)s][%(filename)s:%(lineno)d]%(message)s'
#
# id_simple_format = '[%(levelname)s][%(asctime)s] %(message)s'
#
# # 定义日志输出格式 结束
# logfile_name = 'login.log'  # log文件名
# logfile_path_staff = r'D:\s23\day19\日志模块\旗舰版日志文件夹\staff.log'
# logfile_path_boss = r'D:\s23\day19\日志模块\旗舰版日志文件夹\boss.log'
## 标配版
# import logging
# # 创建一个logger 对象
# logger = logging.getLogger()
# # 创建一个文件对象
# fh = logging.FileHandler('标配版',encoding='utf-8')
# # 创建一个屏幕的对象
# sh = logging.StreamHandler()
# # 配置显示格式
# formatter = logging.Formatter('[%(asctime)s][%(threadName)s:%(thread)d][task_id:%(name)s]\
# [%(filename)s:%(lineno)d][%(levelname)s][%(message)s] ')
# # 将显示格式和两个对象绑定
# fh.setFormatter(formatter)
# sh.setFormatter(formatter)
# # 将两个对象和logger绑定
# logger.addHandler(fh)
# logger.addHandler(sh)
#
# logger.setLevel(10)# 总开关不设置默认30
#
# sh.setLevel(10)  # 分开关总开关不设置默认30 级别比30低按30 走
#
# fh.setLevel(40)  # 分开关比默认总开关高按照设置的40走
#
# logging.debug("debug message")
# logging.info("info message")
# logging.warning('warning message')
# logging.error('error message')
# logging.critical('critical message')

# 旗舰版
"""
logging配置
"""

# import os
# import logging.config
#
# # 定义三种日志输出格式 开始
#
# standard_format = '[%(asctime)s][%(threadName)s:%(thread)d][task_id:%(name)s][%(filename)s:%(lineno)d]' \
#                   '[%(levelname)s][%(message)s]'  # 其中name为getlogger指定的名字
#
# simple_format = '[%(levelname)s][%(asctime)s][%(filename)s:%(lineno)d]%(message)s'
#
# id_simple_format = '[%(levelname)s][%(asctime)s] %(message)s'
#
# # 定义日志输出格式 结束
#
# logfile_dir = os.path.dirname(os.path.abspath(__file__))  # log文件的目录
#
# logfile_name = 'all2.log'  # log文件名
#
# # 如果不存在定义的日志目录就创建一个
# if not os.path.isdir(logfile_dir):
#     os.mkdir(logfile_dir)
#
# # log文件的全路径
# logfile_path = os.path.join(logfile_dir, logfile_name)
#
# # log配置字典
# # 第一层所有的键不能跪改变
# LOGGING_DIC = {
#     'version': 1, # 版本起始
#     'disable_existing_loggers': False, #
#     'formatters': {  #配置的格式
#         'standard': {
#             'format': standard_format
#         },
#         'simple': {
#             'format': simple_format
#         },
#     },
#     'filters': {},# 过滤
#     'handlers': {# 配置文件句柄
#         # 打印到终端的日志
#         'console': {
#             'level': 'DEBUG',
#             'class': 'logging.StreamHandler',  # 打印到屏幕
#             'formatter': 'simple'
#         },
#         # 打印到文件的日志,收集info及以上的日志
#         'default': {
#             'level': 'DEBUG',  # 起始级别
#             'class': 'logging.handlers.RotatingFileHandler',  # 保存到文件
#             'formatter': 'standard',
#             'filename': logfile_path,  # 日志文件
#             'maxBytes': 300,#1024 * 1024 * 5,  # 日志大小 5M以字节为大小
#             'backupCount': 5,#文件的数量
#             'encoding': 'utf-8',  # 日志文件的编码，再也不用担心中文log乱码了
#         },
#     },
#     'loggers': {
#         # logging.getLogger(__name__)拿到的logger配置
#         '': {
#             'handlers': ['default', 'console'],  # 这里把上面定义的两个handler都加上，即log数据既写入文件又打印到屏幕
#             'level': 'DEBUG',
#             'propagate': True,  # 向上（更高level的logger）传递
#         },
#     },
# }
#
#
# def load_my_logging_cfg():
#     logging.config.dictConfig(LOGGING_DIC)  # 导入上面定义的logging配置
#     logger = logging.getLogger(__name__)  # 生成一个log实例
#     logger.debug('It works!')  # 记录该文件的运行状态
#
#
# if __name__ == '__main__':
#     load_my_logging_cfg()
# l1 = []
# for i in l1:
#     print(i)

# def demo():
#     for i in range(3):
#         yield i
# g = demo()
# g1 = (i for i in g)
# g2 = (i for i in g1)
# print(list(g1)) #[0, 1, 2]
# print(list(g2)) #[]
# def num():
#     return [lambda x:x*i for i in range(4)]
# print([m(2)for m in num()])
# v = (lambda: x for x in range(10))
# print(v)
# print(next(v))
# # print(next(v)())
# a = 2
# print(a.encode('unicode'))
# from functools import reduce
# print(reduce(lambda x,y: 2*x+y, [1,2,3]))
#
# class Human:
#     '''
#     此类主要是构建人类
#     '''
#     mind = '有思想'
#     dic = {}
#     l1 = []
#
#     def work(self):
#         print('人类会工作')
#     @staticmethod
#     def func():
#         pass
#
#
# # print(Human.__dict__)
# # print(Human.__dict__['mind'])
# # Human.__dict__['mind'] = "无脑" # 报错 __dict__ 只能查询 不能增删改
# # print(Human.__dict__["mind"])
# #  第二万能的点
# print(Human.mind)
# print(Human.dic)
# Human.mind = "无脑"
# print(Human.mind)
# del Human.mind  # 删除
# Human.walk  = "直立行走" # 向类中添加变量
# print(Human.__dict__["walk"])
# Human.dic['nihao'] = "大家好才是真的好,广州郝迪" # 向字典中添加xxx
# print(Human.dic)
# Human.work(1564513)
# hu = Human() # hu 就是这个实例生成的对象 即类名加()是个实例化过程 就会实例化一个具体的对象
# lie = [1,2,]
# print(sum(lie))
# class Student:
#     dialy = "学习"
#     examination = '考试'
#
#     def __init__(self, n, s, h):
#         self.name = n
#         self.sex = s
#         self.hobby = h
#
#     def work(self,c):
#         self.color = c
#         print('每天要上课')
#
#     def homework(self):
#         print('家庭作业')
#
#

# # 属性就是变量名 方法就是函数
# # obj = Student()
# # obj.age = '30'
# # del obj.n
# # obj.sex = 'nv'
# # print(obj.age)
# # print(obj.__dict__)
# liye = Student('小黑','男','洗头')
# print(liye.__dict__)
# liye.work('绿油油')
# print(liye.__dict__)
# name = "⑧"
# print(name.isdigit())
# alex = "hufidhfk5431435"
# print(alex[-7:].isdecimal())
# alex.isdecimal()
# for i in range(10, 0):
#     print(i)  # 什么都没有
# for i in range(10, 0, 1):
#     print(i)  # 什么都没有
# # 正确姿势
# for i in range(10, -1, -1):
#     print(i)  # 从10 到 0
# # list 增
# li = []
# li.insert(0, "hfsahfus")  # 两个参数 第一个是所索引值 第二个才是要添加的内容
# print(li)
# li.extend([1, 2, 3, 4, 5, 6])
# print(li)
# li.append("sauighoiadhg")  # 追加 在末尾添加
# print(li)
# # 删
# li.pop()
# print(li)
# li.pop(0)  # 索引
# print(li)
# li.remove(5)
# print(li)
# del li[2]
# print(li)
# t = (2)  # 错误元组
# print(type(t))
# tu = (2,)
# print(type(tu))
# dic = {1: "nishi", 2: "lalala", 3: "dadada"}
# print(dic.get(1, "不存在"))
# print(dic.get(5)) # None
# for i in dic.keys():
#     print(i)
# name = "ahigadih"
# print(id(name))
# a = 123
# import copy
#
# b = copy.copy(a)
# print(id(a))
# print(id(b))
# import os
#
# print(os.getcwd())
# class A:
#
#     address = '美丽富饶的沙河'
#
#     def __init__(self, name):
#         self.name = name
#
#     def func(self):
#         if self.name == 'dsb':
#             self.skins = '吉利服'
#
#     def func1(self):
#         print(self.__dict__)
#         A.aaa = '易水寒'
#
# A("dsb").func()
# print(A.__dict__)
# obj = A("dsb")
# print(obj.__dict__)
# class Elephant:
#     def __init__(self,name):
#         self.name = name
#     def open(self,ref1):
#         print(f'{self.name}大象默念三声:  芝麻开门')
#         ref1.open_door()
#     def close(self):
#         print(f'{self.name}大象默念三声:  芝麻关门')
#
#
# class Refrigerator:
#     def __init__(self,name):
#         self.name = name
#     def open_door(self):
#         print(f'{self.name}冰箱门被打开了...')
#     def close_door(self):
#         print(f"{self.name}冰箱门被关上了")
# ele = Elephant('琪琪')
# ref = Refrigerator('美菱')
# ele.open(ref)

# class Boy:
#     def __init__(self,name):
#         self.name = name
#     def meet(self,girl_friend = None):
#         self.girl_friend = girl_friend
#     def have_diner(self):
#         if self.girl_friend:
#             print(f'{self.name}请{self.girl_friend.name}一起吃六块钱的麻辣烫')
#             flower.shopping(self)
#         else:
#             print('单身狗,吃什么吃')
# class Girl:
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#     def shopping(self,shoppingman):
#         self.shoppingman = shoppingman
#         print(f'{shoppingman.name}{self.name}一起去购物!')
# wu = Boy('吴超')
# flower = Girl('如花',48)
# wu.meet(flower)
# wu.have_diner()
# import math
# print(math.pi*3)
# import numpy as np
# class Aniaml(object):
#     type_name = '动物类'
#
#     def __init__(self, name, sex, age):
#         self.name = name
#         self.age = age
#         self.sex = sex
#
#     def eat(self):
#         print('吃东西')
#
#
# class Person(Aniaml):
#     def __init__(self, name, sex, age, mind):
#         '''
#         self = p1
#         name = '春哥'
#         sex = 'laddboy'
#         age = 18
#         mind = '有思想'
#         '''
#         # super(Person,self).__init__(name,sex,age)  # 方法二
#         super().__init__(name, sex, age)  # 方法二
#         self.mind = mind
#
#     def eat(self):
#         super().eat()
#         print('%s 吃饭' % self.name)
#
#
# class Cat(Aniaml):
#     pass
#
# class Dog(Aniaml):
#     pass
#
#
# p1 = Person('春哥', 'laddboy', 18, '有思想')

# print(Aniaml.age)
# from abc import ABCMeta, abstractmethod
#
#
# class Payment(metaclass=ABCMeta):
#     @abstractmethod
#     def pay(self, money):
#         raise Exception
#
#
# class QQpay(Payment):
#     def pay(self, money):
#         print(f"利用qq支付了{money}")
#
#
# class Alipay(Payment):
#     def pay(self, money):
#         print(f"利用支付宝支付了{money}")
#
#
# class Wechatpay(Payment):
#     def fuqian(self, money):
#         print(f"利用微信支付了{money}")
#
#
# def pay(obj, money):
#     obj.pay(money)
#
#
# wei = Wechatpay()
# pay(wei, 200)
# def f(a, b, *args, **kwargs):
#     print(a, b, args, kwargs)
#
#
# # f(1, 2)
#
# f(a=1, b=2, c=3, zzz="h2")
# f(1, 2, 3, m=1, k=3, c=3)
# def f(x, l=[]):
#     for i in range(x):
#         l.append(i * i)
#     print(l)
#
#
# f(2)
# f(3, [3, 2, 1, ])
# f(3)
#
# a = "你好abc"
# a_en = a.encode(encoding="utf_8")
# print(a_en.decode(encoding="gbk"))
# def demo():
#     for i in range(3):
#         yield i
#
# g = demo()
# g1 = (i for i in g)
# print(list(g1))
#
# for i in g1:
#     print(i)
# a = "你好abc"
# a_en = a.encode(encoding="utf_8")
# print(a_en)
# print(a_en.decode(encoding="utf-8"))
# a_g = a.encode(encoding="gbk")
# print(a_g)
# print(a_g.decode(encoding="gbk"))
# class Foo:
#     __instance = None
#     def __new__(cls,*args,**kwargs):
#         if cls.__instance is None:
#             obj = object.__new__(cls)
#             cls.__instance= obj
#         return cls.__instance
# o = Foo()
# p = Foo()
# print(o)
# # print(p)
# class Foo:
#     __instance = None
#     def __init__(self):
#         pass
#
#     @classmethod  # 调用类的方法来创建一个单例
#     def get_connetion(cls):
#         if cls.__instance:
#             return cls.__instance
#         else:
#             cls.__instance = Foo()
#             return cls.__instance
#
# obj1 = Foo.get_connetion()
# print(obj1)
# obj2 = Foo.get_connetion()
# print(obj2)

# import subprocess
#
# obj = subprocess.Popen('dir',
#                        shell=True,
#                        stdout=subprocess.PIPE,
#                        stderr=subprocess.PIPE,
#                        )
#
# print(obj.stdout.read().decode('gbk'))  # 正确命令
# print('error:',obj.stderr.read().decode('gbk'))  # 错误命令

#
# dic = {1: 4, 2: 6, 3: 5}
# for k, v in dic.items():
#     print(v)
# mode = None
# dic = {'1':"正常模式", '2':'勿扰模式'}
# choice = input("请输入模式序号:\n1:正常模式\n2:勿扰模式").strip()
# if choice == "2":
#     mode = True
# print(mode)

# import socket
#
# phone = socket.socket()
# phone.connect(("127.0.0.1", 8848))
# while 1:
#     to_server = input(">>>")
#     if to_server.upper() == "Q":
#         phone.send('q'.encode('utf-8'))
#         break
#     phone.send(to_server.encode("utf-8"))
#     from_server_date = phone.recv(1024)
#     print(f"来自服务端消息:{from_server_date.decode('utf-8')}")
# phone.close()


# import hashlib
#
# import socket
#
# phone = socket.socket()
# phone.connect(('127.0.0.1', 8848))
# while 1:
#     username_input = input("请输入用户名:").strip()
#     user_password = input("请输入密码:").strip()
#     ret = hashlib.md5()
#     ret.update(user_password.encode('utf-8'))
#     if username_input.upper()== "Q":
#         phone.send('q'.encode('utf-8'))
#         break
#     info = username_input +"|" + ret.hexdigest()
#     phone.send(info.encode("utf-8"))
#     from_sever_data = phone.recv(1024*8)
#     print(from_sever_data.decode("utf-8"))
#
# phone.close()

# username_input = input("请输入用户名:").strip()
# user_password = input("请输入密码:").strip()
# ret = hashlib.md5()
# ret.update(user_password.encode('utf-8'))
#
# with open(r"D:\Python_study\day28socket 套接字\user",mode="w") as f:
#     f.write(username_input+ret.hexdigest())
# bath = r'D:\Python_study\day29黏包现象\client.data\day30笔记.md'
# with open(bath, mode='rb') as f:
#     while 1:
#         cmd = f.read(1024)
#         if cmd:
#             print(cmd)
#         else:
#             break
# data = "佛ID价格低偶家我偶觉得福利机构IER级哦即 解耦IG江湖环境"
#
# import json
#
#
# def datare(DT):
#     data = json.dumps(DT).encode('utf-8')
#     return data
#
#
# d = datare(data)
#
#
# def datase(DT):
#    data = json.loads(DT.decode('utf-8'))
#    return data
# print(datase(d))
# for i, m in enumerate([2,7,11,15]):
#     print(i,m)

# print(2.5*365)
# 可能性: 黄金最近经历了大幅度拉升 目前在1420上下浮动,
# 预计上升的空间,短时间没有重大利好消息不会有太多,回调的风险加大
# 逐渐和h4交融,慢慢走出方向
# 第一目标1500回调暂缓,做波浪
# a = "g"*20
# b = 'g'*20
# print(a is b)
# print(id(a))

# a = "ah suh fS ADF AS"
# print(a.capitalize())  # 手字母大写
# print(a.swapcase())  # 大小写翻转
#
# print(a.title())  # 首字母大写其他全部小写
#
# ret2 = a.center(22, "*")
# print(ret2)
# def add_b():
#     b = 42
#     def do_global():
#         b = 10
#         print(b)
#         def dd_nonlocal():
#             nonlocal b
#             b = b + 20
#             print(b)
#         dd_nonlocal()
#         print(b)
#     do_global()
#     print(b)
# add_b()


# def gen(name):
#     print(f'{name} ready to eat')
#     while 1:
#         food = yield
#         print(f'{name} start to eat {food}')
# dog = gen('alex')
# # next(dog)
# # 还可以给上一个yield发送值
# dog.send('骨头')
# dog.send('狗粮')
# dog.send('香肠')
# def func():
#     print(666)
# a = eval("""
# def func():
#     print(666)""")
# print(a)
# exec('func()')
#
# print(eval('2 + 2'))  # 4
# n=81
# eval("n + 4")  #
# eval()
# 数据转换执行 exec 转换成正常代码,eval只能执行现有的指令不能将字符串中的代码转换成正常的可执行的代码
# print(repr('{"name":"alex"}'))
# print('{"name":"alex"}')
# name = "taibai"
# print('我叫%s'%name)

# lst = [1, 2, 3, 4, 5]
#
#
# def func(s):
#     return s * s
#
#
# mp = map(func, lst)
#
# print(mp)
# print(next(mp))
# print(list(mp))

# from functools import reduce
#
#
# def func(x, y):
#     return x + y**2
#
#
# ret = reduce(func, [3, 4, 5, 6, 7])
# print(ret)
# import pymysql
#
# conn = pymysql.Connection(host='127.0.0.1', user='root', password='root', database='day40')
# cur = conn.cursor()
# cur.execute("""insert into book values('学python从开始到放弃','alex','人民大学出版社',50,'2018-7-1'),
# ('学mysql从开始到放弃','egon','机械工业出版社',60,'2018-6-3'),
# ('学html从开始到放弃','alex','机械工业出版社',20,'2018-4-1'),
# ('学css从开始到放弃','wusir','机械工业出版社',120,'2018-5-2'),
# ('学js从开始到放弃','wusir','机械工业出版社',100,'2018-7-30')""")
#
# conn.commit()
#
# cur.close()
# conn.close()


# print('[%-15s]' %'#')
# print('[%-15s]' %'##')
# print('[%-15s]' %'###')
# print('[%-15s]' %'####')
# print('[%%-%ds]' %50) #[%-50s]
# print(('[%%-%ds]' %50) %'#')
# print(('[%%-%ds]' %50) %'##')
# print(('[%%-%ds]' %25) %'###')


# import sys
# import time
#
#
# def progress(percent, width=50):
#     if percent >= 1:
#         percent = 1
#     show_str = ('%%-%ds' % width) % (int(width * percent) * '|')
#     print('\r%s %d%%' % (show_str, int(100 * percent)), end='')
#
#
# data_size = 1025
# recv_size = 0
# while recv_size < data_size:
#     time.sleep(0.1)  # 模拟数据的传输延迟
#     recv_size += 5  # 每次收1024
#
#     percent = recv_size / data_size  # 接收的比例
#     progress(percent, width=70)  # 进度条的宽度70


# class Human:
#     """
#     此类主要是构建人类
#     """
#     mind = '有思想'  # 第一部分：静态属性 属性 静态变量 静态字段
#     dic = {}
#     l1 = []
#
#     def work(self):  # 第二部分：方法 函数 动态属性
#         # print(self)
#         print('人类会工作')
# #
#
# print(Human.__dict__)
# print(Human.__dict__['mind'])
# Human.__dict__['mind'] = '无脑' # 报错
# # print(Human.__dict__)  # 错误
# # 通过这种方式只能查询，不能增删改.
from threading import Thread

# class Human:
#     mind = 'sixiang'
#     def __init__(self):
#         self.mind = "有思想"
#
# obj = Human()
# print(obj.__dict__)
# print(obj.mind)
# print(Human.mind)


# class Boy:
#     def __init__(self, name, girlFriend=None):
#         self.name = name
#         self.girlFriend = girlFriend
#
#     def have_a_diner(self):
#         if self.girlFriend:
#             print('%s 和 %s 一起晚饭' % (self.name, self.girlFriend.name))
#         else:
#             print('单身狗，吃什么饭')
#
#
# class Girl:
#     def __init__(self, name):
#         self.name = name


# gg = Girl('小花')
# bb = Boy('wusir', gg)
# # bb.have_a_diner()
# print(
#     bb.girlFriend
# )

# b = Boy('日天')
# b.have_a_diner() # 此时是单身狗
#
# # 突然有一天，日天牛逼了
# b.girlFriend = '如花'
# b.have_a_diner()  #共进晚餐


# class School:
#
#     def __init__(self,name,address):
#         self.name = name
#         self.address = address
#
#
# class Teacher:
#
#     def __init__(self,name,school):
#         self.name = name
#         self.school = school
#
# s1 = School('北京校区','美丽的沙河')
# s2 = School('上海校区','上海迪士尼旁边')
# s3 = School('深圳校区','南山区')
#
# t1 = Teacher('武大',s1)
# t2 = Teacher('海峰',s2)
# t3 = Teacher('日天',s3)
#
# print(t1.school.name)
# print(t2.school.name)
# print(t3.school.name)


# class Gamerole:
#     def __init__(self, name, ad, hp):
#         self.name = name
#         self.ad = ad
#         self.hp = hp
#
#     def attack(self, p1):
#         p1.hp -= self.ad
#         print('%s攻击%s,%s掉了%s血,还剩%s血' % (self.name, p1.name, p1.name, self.ad, p1.hp))
#
#
# gailun = Gamerole('盖伦', 10, 200)
# yasuo = Gamerole('亚索', 50, 80)
# # 盖伦攻击亚索
# gailun.attack(yasuo)
# # 亚索攻击盖伦
# yasuo.attack(gailun)


# class Gamerole:
#     def __init__(self, name, ad, hp):
#         self.name = name
#         self.ad = ad
#         self.hp = hp
#
#     def attack(self, p1):
#         p1.hp -= self.ad
#         print('%s攻击%s,%s掉了%s血,还剩%s血' % (self.name, p1.name, p1.name, self.ad, p1.hp))
#
#     def equip_weapon(self, wea):
#         self.wea = wea  # 组合：给一个对象封装一个属性改属性是另一个类的对象
#
#
# class Weapon:
#     def __init__(self, name, ad):
#         self.name = name
#         self.ad = ad
#
#     def weapon_attack(self, p1, p2):
#         p2.hp = p2.hp - self.ad - p1.ad
#         print('%s 利用 %s 攻击了%s,%s还剩%s血'
#               % (p1.name, self.name, p2.name, p2.name, p2.hp))
#
#
# # 实例化三个人物对象：
# barry = Gamerole('太白', 10, 200)
# panky = Gamerole('金莲', 20, 50)
# pillow = Weapon('绣花枕头', 2)
#
# # 给人物装备武器对象。
# barry.equip_weapon(pillow)
#
# # 开始攻击
# barry.wea.weapon_attack(barry, panky)


# import threading
# import time
#
#
# class MyThread(threading.Thread):
#     def __init__(self, func, args=()):
#         super(MyThread, self).__init__()
#         self.func = func
#         self.args = args
#
#     def run(self):
#         self.result = self.func(*self.args)
#
#     def get_result(self):
#         try:
#             return self.result  # 如果子线程不使用join方法，此处可能会报没有self.result的错误
#         except Exception:
#             return None
#
#
# def foo(a, b, c):
#     time.sleep(1)
#     print a * 2, b * 2, c * 2,
#     return a * 2, b * 2, c * 2
#
#
# st = time.time()
# li = []
# for i in xrange(4):
#     t = MyThread(foo, args=(i, i + 1, i + 2))
#     li.append(t)
#     t.start()
# for t in li:
#     t.join()  # 一定要join，不然主线程比子线程跑的快，会拿不到结果
#     print t.get_result()
# et = time.time()
# print et - st
#
# sql = 'insert into f"{file}" values(shuju) '
# print(sql)
# m = ""
# k = "f"
# m += ",".join(k)
# print(m)

import threading
# loop = int(1E7)
#
#
# def _add(loop: int = 1):
#     global number
#     for _ in range(loop):
#         number += 1
#
#
# def _sub(loop: int = 1):
#     global number
#     print(number)
#
#
# number = 0
# ta = threading.Thread(target=_add, args=(loop,))
# ts = threading.Thread(target=_sub, args=(loop,))
# ta.start()
# ts.start()
# ta.join()
# ts.join()
# print(number)

# import time
#
# loop = int(1E7)
#
#
# def _add(loop: int = 1):
#     global numbers
#     for _ in range(loop):
#         numbers.append(0)
#
#
# def _sub(loop: int = 1):
#     global numbers
#     for _ in range(loop):
#         while not numbers:
#             time.sleep(1E-8)
#         numbers.pop()
#
#
# numbers = [0]
# ta = threading.Thread(target=_add, args=(loop,))
# ts = threading.Thread(target=_sub, args=(loop,))
# ta.start()
# ts.start()
# ta.join()
# ts.join()
# print(numbers)

133543
133541
133542
133540
133539
129339
133528
133530
133529
131280

133509
133527
133526
133525
131223
128844
130686
130288
133524
130728

130728
131119
133515
131732
133514
133513
133512
130604
130532
133538

133510
129385
131973
133450
132867
133519
133518
130360
131630
131693

133030
131636
132443
133506
133508
129873
133505
133489
133503
132106