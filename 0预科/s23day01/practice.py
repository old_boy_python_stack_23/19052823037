"""
count = 1
while True:
    print(count,end='')
    count = count + 1
    if count >= 999:
        break
"""
"""
print('''1
2
3
4
5
''')
"""
'''
n = 1
while n <= 10:
    if n != 7:
        print(n)
    n += 1
#    if n > :
#        break
'''
'''
n = 1
while n <= 10:
    if n == 7:
        n += 1
        continue
    print(n)
    n += 1
'''
# n = 1
# prime = input('请输入一个数字')
# while n <= int(prime):


#    s = int(prime) % n
#    print(s)
#    n+=1

'''
n = input("")
if int(n) %1==0 and int(n)%int(n)==0:
    print("质数")
else:
    print("不是质数")
'''


# WeekNamePrintV1.py
# weekStr= "星期一星期二星期三星期四星期五星期六星期日"
# weekId= eval(input("请输入星期数字(1-7)："))
# pos= (weekId-1) * 3
# print(weekStr[pos: pos+3])
# -*- coding: UTF-8 -*-

# Filename : test.py
# author by : www.runoob.com

# Python 程序用于检测用户输入的数字是否为质数
# while True:
#
# # 用户输入数字
#     num = int(input("请输入一个数字: "))
#
#     # 质数大于 1
#     if num > 1:
#         # 查看因子
#         for i in range(2, num):
#             if (num % i) == 0:
#                 print(num, "不是质数")
#                 print(i, "乘于", num // i, "是", num)
#                 break
#         else:
#             print(num, "是质数")
#
#     # 如果输入的数字小于或等于 1，不是质数
#     else:
#         print(num, "不是质数")

# name = "  alex.txt.txt leNB  "
# print(name.strip())
# while True:  # 方便多次尝试
#     n = 2
#     m = int(input("请输入一个整数："))
#     if m == 2:
#         print("是质数")
#         continue
#     # elif m == 3:
#     #     print("是质数")
#     #     continue
#     elif m <= 1:
#         print("不是质数")
#     while n < m:
#         if (m % n) == 0:
#             # n += 1不用执行 此次不成立执行else，在else处 递增，如果成立直接输出结果，也不需要再递增，因为只要成立一次，
#             #这个数就不是质数
#             print("不是质数")
#             break
#         else:
#         # elif (m % n) != 0:
#
#             if n == m - 1:  # 不能停止,必须进行循环,那么改变输出的条件
#                 #但是在输入是3 的情况下出现了意外,所以3 为特例,在开头,作为特例   把n+=1 放在最下面特例就没了
#                 print("是质数")
#             n += 1
#             # break不能有,否则非质数特殊数字会变成质数
#             # 我觉得还有优化的空间
# #优化方案直接怼代码了 拼接
#     else:
#         print("是质数")

# b = "我是一个粉刷匠,粉刷本领强"
# # s = b.encode("GBK")
# # print(s)
# bs = b'\xce\xd2\xca\xc7\xd2\xbb\xb8\xf6\xb7\xdb\xcb\xa2\xbd\xb3,\xb7\xdb\xcb\xa2\xb1\xbe\xc1\xec\xc7\xbf'
# print(bs.decode("GBK"))
# m = b.encode(" utf-8")
# print(m.decode("GBK"))

#
# s1 = "jiojjJIOJFOFDJKFDGKJJLKJL343243334"
# s1.capitalize()  # 首字母大写 其他的都变为小写
# print(s1)   # 输出发现并没有任何的变化. 因为这⾥里里的字符串串本身是不不会发⽣生改变的. 需要我们 重新获取
# ret1 = s1.capitalize()
# print(ret1)


# %s ,%d, %%

# msg = '%s，学习进度5%%'
# print(msg%(input("name:")))

# %s -- 占字符串的位置
# %d -- 占整型的位置
# %% -- 转义（把占位转换成普通的%号）


# msg = '''
# ------------------- info ----------------------
# name: %s
# age:  %s
# sex : %s
# job:  %s
# hobby: %s
# ------------------- end ------------------------
# '''
#
# print(msg%('alex.txt.txt','20','nan','it','cnb'))
#
# name = input("name")
# print(f"alex.txt.txt{name},{'aaa'}")
#
# msg = f'''
# ------------------- info ----------------------
# name: {input("name:")}
# age:  {input("age:")}
# sex : {input("sex:")}
# job:  {input("job:")}
# hobby: {input("hobby:")}
# ------------------- end ------------------------
# '''
# print(msg)
#
# a = 1
# s = 0
# while a < 100:
#     if a % 2 == 1:
#         s += a
#     elif a % 2 == 0:
#         s -= a
#     a += 1
# print(s)

# num = 13456845
# print(num.bit_length())
# print(bool())


# - 移除 name 变量对应的值两边的空格,并输出处理结果
# name = " aleX leNb "
# name1 = name.strip()
# print(name1)
# # - 将 name变量对应的值中所有的空格去除掉,并输出处理结果
# print(name.replace(' ',''))
# # - 判断 name 变量是否以 "al" 开头,并输出结果（用两种方式 切片+字符串方法）
# print(name.strip().startswith('al'))
#
# print(name.strip()[0:2] == 'al')
# print(name.strip().endswith('Nb'))
# print(name.strip()[-2:] == 'Nb')
# print(name.strip().replace('l','p'))
# print(name.strip().replace('l','p',1))
# print(name.strip().split('l'))
# print(name.strip().upper())
# print(name.strip().lower())
# print(name.strip()[1])
# print(name.strip()[0:4])
# s = "123a4b5c"
# print(s[0:3])
# print(s[3:6])
# s5 = s[-1]
# print(s5)
# # s6 = s[1:6:2]
# # print(s6)
# s6 = s[:-6:-2]
# print(s6)
#
# s="你好世界"
# count = 0
# while count < len(s):
#     print(s[count])
#     count += 1

# s = "321"
# count = 0
# while count < len(s):
#     m = "倒计时%s秒"
#     print(m%(s[count]))
#     count += 1
# else:
#     print("出发")
# s = "321"
# for i in s:
#     m = "倒计时%s秒"
#     print(m%(i))
# print("出发")
# content = input("请输入内容:")
# content1 = content.replace(' ','')
# print(int(content1[0]) + int(content1[-1]))
# content = input("请输入内容：")
# n = 0
# for i in content:
#     if i == "s":
#         n += 1
# print(n)
# message = "伤情最是晚凉天，憔悴厮人不堪言。"
# countent = 0
# while countent < len(message):
#     print(message[countent])
#     countent += 1

# countent = 0
# while countent < len(message):
#     countent += 1
#     print(message[-countent])
# m = input("请输入内容")
# n = 0
# for i in m[0:4]:
#     if i == "a":
#         n += 1
# print(n)
# name = input("请输入姓名")
# site = input("请输入地点")
# hobby = input("请输入爱好")
# print("敬爱可亲的%s,最喜欢在%s地方干%s"%(name,site,hobby))
# h = input("请输入回文:")
# for i in range(len(h)):
#     pass
# print(h[i - 1] == h[-i])

# b = input("请输入一个字符串")
# for i in b:
#     print(i)
#     b1 = b.replace(i,'')
#     print(len(b)-len(b1))

# while True:
#     K = input("请输入A or B or C:")
#     if K == "A":
#         M = input("请输入公交 or 步行:")
#         if M == "公交":
#             print("十分钟到家")
#             exit(0)
#         elif M == "步行":
#             print("20分钟到家")
#             exit(0)
#     elif K == "B":
#         print("走小路回家")
#         exit(0)
#     elif K == "C":
#         print("绕道回家")
#         L = input("请输入游戏厅 or 网吧")
#         if L == "游戏厅":
#             print('一个半小时到家，爸爸在家，拿棍等你。')
#             continue
#         elif L == "网吧":
#             print('两个小时到家，妈妈已做好了战斗准备。')
#             continue
# n = 0
# m = 0
# t = 0
# d = 0
# b = input("请输入一个字符串")
# for i in b:
#     if i.isupper() == True:
#         n += 1
#     elif i.islower() == True:
#         m += 1
#     elif i.isdecimal() == True:
#         t += 1
#     else:
#         d += 1
# print("大写字母",n)
# print("小写字母",m)
# print("数字",t)
# print("其他",d)
# h = input("请输入回文:")
# if h == h[::-1]:
#     print("是回文")
# else:
#     print("不是回文")
# name = input("请输入内容:")
#
# print(name.isalnum())

# n = "你好世界"
# print(n[-1:-3:-1])
# s = "123a4b5c"
# s6 = s[-1:-6:-2]
# print(s6)
# s = "321"
# count = 0
# while count < len(s):
#     print("倒计时%s秒"%(s[count]))
#     count += 1
# else:
#     print("出发")

# n = 0
# m = 0
# t = 0
# d = 0
# b = input("请输入一个字符串")
# for i in b:
#     if i.isupper() == True:
#         n += 1
#     elif i.islower() == True:
#         m += 1
#     elif i.isdecimal() == True:
#         t += 1
#     else:
#         d += 1
# print("大写字母{},小写字母{},数字{},其他{}".format(n,m,t,d))

# while True:
#     K = input("请输入A or B or C:")
#     if K == "A":
#         M = input("请输入公交 or 步行:")
#         if M == "公交":
#             print("十分钟到家")
#             exit(0)
#         elif M == "步行":
#             print("20分钟到家")
#             exit(0)
#     elif K == "B":
#         print("走小路回家")
#         exit(0)
#     elif K == "C":
#         print("绕道回家")
#         L = input("请输入游戏厅 or 网吧")
#         if L == "游戏厅":
#             print('一个半小时到家，爸爸在家，拿棍等你。')
#             # continue
#         elif L == "网吧":
#             print('两个小时到家，妈妈已做好了战斗准备。')

# li = [1,2,3,"123",True]
#     # 0 1 2  3     4
# print(li)
#
# # 有序支持索引
# print(li[3],type(li[3]))
# print(li[-1],type(li[-1]))


# li = [1, 2, 3, 'alex.txt.txt', 3, [1, 2, 3]]
#
# li.extend([1,2,3,5,])
# print(li)
#
# dic={}
# dic["key"] = "nihao"
# print(dic)

# def yue(形参):
#     print("拿出手机")
#     print("打开陌陌")
#     print("找找心仪的姑娘")
# yue(实参)
# print("回家休息")
# yue()

# a = 10
# def yue():
#     nonlocal a # nonlocal 只能在 局部 引入离他最近的那一层变量
# global a #  global 在局部引入全局变量
# a += 30
# print(a)
# yue()
# print(a)
# for i in range

# def my_num(a, b):
#     c = a if a > b else b
#     return c
# print(my_num(1,2))
# #
# # my_num(4, 1)
# # def my_max(a, b):
# #     if a > b:
# #         return a
# #     else:
# #         return b
# # print(my_max(2,3))
#
# def num(a, b, c, *args, e = 6, **kwargs):
#     print(a, b, c, args, e, kwargs)
#
#
# # num("是","大","发","反对","范德萨","Greg","的方式",e = 123456, d= "fjhdghiioash")
# dic = {"id": 123, "name": 'sylar', "age": 18}
# print(dic.get("sylar", "⽜B"))
# print(dic.get("ok"))
lis = ['a','a','b','a','a','b','b','c','s', 'b', 'a', 'a', 'f', 'a', 's', 'b', 'f', 'a', 's', 'f', 'g', 'c']
for x in lis:
    for y in range(len(lis)-1,lis.index(x),-1):
        # print(y)
        if lis[y] == x:
            lis.pop(y)
print(lis)
