# 用户登陆,三次机会重试
n = 1  # 赋值给n进入n的循环
while n <= 3:  # 1 2 3 ,三次循环
    #    n = n + 1
    #   if n <= 3 刚开始n赋值为0  因为n <3 所以n+1可以等于3
    #    if n < 3:
    name = input('请输入用户名:')
    key_word = input('请输入密码')
    #   name_int = int(name)
    #   key_word_int = int(key_word)#刚开始设计时,下面是纯数字额
    if name == 'yangsen' and key_word == 'love':  # 统一为字符串
        print('登陆成功')
        break
    else:
        print('用户名或密码错误,请重新输入')
    print("当前登录了%s次，剩余%s次"%(n,3-n))
    n += 1  # 一次机会用尽，进入下一个机会，共三次机会
else:
    print('您的账号已被锁定')
