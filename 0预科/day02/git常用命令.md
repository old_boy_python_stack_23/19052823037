# git常用命令

1. git init   指第一次初始化一个文件夹，要确保此文件夹下生成.git隐藏文件以后都不用再初始化了
2. git add . 
   1. add 相当于一个动作，要添加动作后的名词至本地git仓库
   2. “. ”的含义是把所有此文件夹下的增删改操作都添加到本地仓库
3. git commit -m "你本次操作的记录备注信息"
   1. commit 在git语法中做提交操作，也是一个动作
   2. -m "你本次操作的记录备注信息"
4. git push -u origin master
   1. 提交本地仓库到远程仓库
   2. 切记！！！不要删除远程仓库（码云）内的任何东西
   3. 任何增删改在本地操作，操作完成之后按顺序执行：
      1. git add .                              git add .
      2. git commit -m "day02"        git commit -m "  备注 "
      3. git push -u origin master     git push -u origin master

