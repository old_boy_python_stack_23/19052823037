# coding:utf-8
# read模式  只能读
f = open("D:alex.txt",mode = "r+",encoding="GBK")
w = f.read()
m = w.replace("sb","alex")
print(w,type(w))
print(m)
f.write(m)
# for line in w:
#     print(line)
    # new_line = line.replace("sb","alex")
    # print(new_line)

#     f.write(new_line)


# import os
# # 预科/alex.txt
# with open("alex.txt",mode='r',encoding='utf-8') as f1,\
#     open("alex_副本.txt",mode='w',encoding='utf-8') as f2:
#     for line in f1:
#         new_line = line.replace('good','sb')
#         f2.write(new_line)
# os.remove("alex.txt")
# os.rename("alex_副本.txt","alex.txt")
#
# for line in f: # 内部调用的是readline()
#     # line.replace("sb","alex")
#     f.write(line.replace("sb","alex"))
# f.flush()
f.close() # 关闭

# write 模式只能写
# f = open("D:/sylar.txt",mode = "w",encoding="utf-8")
# # f.write("娃哈哈")
# f.write("周笔畅")# 写的时候先清空 再写入
# f.flush()
# f.close()

# append 模式 追加写 只能写
# f = open("D:/sylar.txt",mode = "a",encoding="utf-8")
# f.write("爽歪歪\n")
# f.write("迪士尼\n")
# f.flush()
# f.close()

# b 字节 这个时候处理的是字节
# rb
# wb
# ab
# 图片的读取和转移
# f1 = open("D:/银行卡.jpg",mode="rb")
# f2 = open("E:/银行卡.jpg",mode='wb')
# for line in f1:
#     #print(line)
    # f2.write(line)
# f2.flush()
# f2.close()

# 绝对路径 1. 从磁盘的根目录找  2. 网上的路径
# 相对路径 相对于这个程序所在的文件
# f = open("../day03/day03笔记.md",mode='r',encoding='utf-8')#"../"回到上个文件目录
# print(f.read())

# 打开网络图片
# import requests
#
# rs = requests.get("http://i0.hdslb.com/bfs/article/a8c6df0dd7455fdb133575965d0719ee17aab281.png")
# f = open("壁纸.jpg",mode='wb')
# f.write(rs.content)
# f.flush()
# f.close()

import os
# 预科/alex.txt
with open("alex.txt",mode='r',encoding='utf-8') as f1,\
    open("alex_副本.txt",mode='w',encoding='utf-8') as f2:
    for line in f1:
        new_line = line.replace('good','sb')
        f2.write(new_line)
os.remove("alex.txt")
os.rename("alex_副本.txt","alex.txt")

# lis = ['a','a','a','a','s','b','a','c','a']
# li1 = []
# for i in range(len(lis)):
#     print(i)
#     if lis[i] not in li1:
#         li1.append(lis[i])
# lis = li1
# print(lis)

#
# r = "读"
# w = "写"
# a = "追加"
# # 不可同时使用
#
#
# li = [1, 3, 4, "alex.txt.txt", [3, 7, 8, "BaoYuan"], 5, "RiTiAn"]
#
# # w = li[4]
# # for i in (li[4][::-1]):
# #     li.appand()
#
#
#
#
# num = 0
# while num < len(li):
#     if type(li[num]) == list:
#         w = li[num]
#         print(li[num][::-1])
#         # print(li[num])
#         del li[num]
#         print(li[num])
#         for i in w[::-1]:
#             li.insert(num, i)
#     num += 1
# for b in li:
#     if type(b) == str:
#         print(b.lower())
#     else:
#         print(b)
#
# lis = [[('电脑', '1999', 1), ('鼠标', '10', 1)], [('游艇', '20', 1), ('鼠标', '10', 1), ('美女', '998', 1)]]
# for i in lis:
#     for l in i:
#         print(l[0],l[1],l[2])