#
# 一、简述变量命名规范
# 1. 由字母 数字 下划线组成
# 2. 首位不能是数字，更不能是纯数字
# 3.不能太长
# 4.区分大小写
# 5.驼峰式或者下划线命名
# 6.不能是关键字
# 7.不要用中文
# 8.要有意义
#
# 二 字符串
#
# 三
# if 条件:
#     代码块
#
# if 条件:
#     代码块
# else：
#     代码块
# if 条件：
#     代码块
# elif 条件：
#     代码块
# else：
#     代码块

# 四
# print("""⽂能提笔安天下,
# 武能上马定乾坤.
# ⼼存谋略何⼈胜,
# 古今英雄唯是君.
# """)

# 五
# while True:
#     n = 66
#     m = int(input("请输入一个数字"))
#     if m > n:
#         print("大了")
#     elif m < n:
#         print("小了")
#     else:
#         print("结果正确")
#         break

# 六
# age = int(input("请输入年龄"))
# if age <10:
#     print("小屁孩")
# elif age < 20:
#     print("青春叛逆的小屁孩")
# elif age < 30:
#     print("开始定性，开始混社会的小屁孩")
# elif age < 40:
#     print("老大不小了，赶紧结婚小屁孩")
# elif age < 50:
#     print("家里有个不听话的小屁孩")
# elif age < 60:
#     print("自己马上变成不听话的老屁孩")
# elif age < 70:
#     print("活着还不错的老屁孩")
# elif age < 90:
#     print("人生就快结束了的一个老屁孩")
# else:
#     print("再见了这个世界")

# 七单行注释 多行注释
# 单行   # 内容
# 多行
# """
#
# """
# 或者
# '''
# '''

# 八
# while True:
#     name = input("请输入麻花藤")
#     if name == "麻花藤":
#         print("真聪明")
#         break
#     else:
#         print("你是傻逼吗")

# 九
# mon = int(input("请输入月份(格式:1月)"))
# if mon =="1月":
#     print("烧饼")
# elif mon == 2月:
#     print("炒饼")
# elif mon == 3月:
#     print("肉丝炒饼")
# 下面类似


# 十
# age = int(input("请输入分数"))
# if age >=90:
#     print("A")
# elif age >= 80:
#     print("B")
# elif age >= 70:
#     print("C")
# elif age >= 60:
#     print("D")
# elif age >= 50:
#     print("D")
# else:
#     print("E")


