# day01笔记

## 标题的创建

1. 可以通过鼠标点选段落，选择想要的标题
2. 也可以通过# 来实现 总共有六个标题，一次增加# 即可

## 有序列表&无序列表

有序列表

方法一： 1. ；

方法二：通过鼠标点选段落，选择有序列表；

无序列表:

可在英文半角状态下输入“-”和一个空格

`- `

- 

通过鼠标点选段落选择无序列表，实际应用根据你的需求选择有序列表或者无序列表

解除列表带来的缩进问题，使用 shift+tab

## 图片的插入

直接复制拖拽进来就可以 ctrl + v也可以

但是当你要发送的Markdown文件给别人时，要记住，你的图片在对方那里是没有的，所以要先导出到PDF再发送

## 表格的插入&使用

插入： 在段落里点选插入表格

使用： 不可合并单元格，复制表格的时候不建议使用鼠标选中直接复制，要去找到more actions里的复制表格进行复制

## 超链接的插入

鼠标点选左上方的格式，并选择超链接

在[]里写提示语，提示用户点击超链接后去哪     在()里写对应的链接地址

示例：[百度一下](www.baidu.com)

## 字体颜色变化操作

这个是在将来学完前端的时候就能实现的操作

我要给加个颜色

<font color=#0099ff size= 12>百度一下</font>

## 代码块的插入

英文状态下```加回车键

```html
<font color=#0099ff size= 12>百度一下</font>
```



~~~python
print("")
~~~

<font color=yellow size=12>yang</font>

## 引用 

> 引用 快捷键 ：">"+空格

## python安装环境配置

打开 WEB 浏览器访问 <https://www.python.org/downloads/windows/> ，一般就下载 executable installer，x86 表示是 32 位机子的，x86-64 表示 64 位机子的。 

一般安装版本选择低于最新版本的,比较稳定,现在最好安装python3.6.8

![1559127468576](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1559127468576.png)

记得勾选 **Add Python 3.6 to PATH**。

可以直接安装也可以进入customize installation 进行详细的配置

![img](https://www.runoob.com/wp-content/uploads/2018/07/20180226150011548.png)

按 **Win+R** 键，输入 cmd 调出命令提示符，输入 python:

![img](https://www.runoob.com/wp-content/uploads/2018/07/20170707155742110.png)

## python2

![1559127755606](C:\Users\17910\AppData\Roaming\Typora\typora-user-images\1559127755606.png)

## 环境变量

右键电脑 -- 属性 -- (高级系统设置)--环境变量 --  系统环境变量 -- path 添加 将python2 3 的路径地址添加进去

 ## Pycharm安装

待定,没截图

